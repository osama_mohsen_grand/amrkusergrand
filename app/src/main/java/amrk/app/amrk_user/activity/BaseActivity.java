package amrk.app.amrk_user.activity;

import android.os.Bundle;
import android.view.View;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.customViews.actionbar.BackActionBarView;
import amrk.app.amrk_user.databinding.ActivityBaseBinding;
import amrk.app.amrk_user.pages.chat.view.ChatFragment;
import amrk.app.amrk_user.pages.chatAdmin.view.ChatAdminFragment;
import amrk.app.amrk_user.pages.home.HomeMainFragment;
import amrk.app.amrk_user.pages.notifications.NotificationsFragment;
import amrk.app.amrk_user.pages.offers.OffersDetailsFragment;
import amrk.app.amrk_user.pages.splash.SplashFragment;
import amrk.app.amrk_user.uber.chat.view.ChatUberFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.LanguagesHelper;

public class BaseActivity extends ParentActivity {
    ActivityBaseBinding activityBaseBinding;
    public BackActionBarView backActionBarView;
    private final MutableLiveData<Boolean> refreshingLiveData = new MediatorLiveData<>();
    String fragmentName = null;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initializeLanguage();
        super.onCreate(savedInstanceState);
        initializeLanguage();
        IApplicationComponent component = ((MyApplication) getApplicationContext()).getApplicationComponent();
        component.inject(this);
        activityBaseBinding = DataBindingUtil.setContentView(this, R.layout.activity_base);
        backActionBarView = new BackActionBarView(this);
        enableRefresh(false);
        getNotification();
        if (!notification_checked) {
            if (getIntent().hasExtra(Constants.PAGE)) {
                fragmentName = getIntent().getStringExtra(Constants.PAGE);
                if (fragmentName != null) {
                    try {
                        if (fragmentName.equals(OffersDetailsFragment.class.getName()) || fragmentName.equals(ChatFragment.class.getName()) || fragmentName.equals(ChatAdminFragment.class.getName()))
                            backActionBarView.flag = 1;
                        Fragment fragment = (Fragment) Class.forName(fragmentName).newInstance();
                        MovementHelper.replaceFragmentTag(this, getBundle(fragment), fragmentName, "");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            } else
                MovementHelper.replaceFragment(this, new SplashFragment(), "");
        }
        activityBaseBinding.swipeContainer.setOnRefreshListener(() -> refreshingLiveData.setValue(true));
    }

    private void setTitleName(@Nullable String title, String type) {
        if (title != null) {
            backActionBarView.setTitle(title);
            if (type != null)
                backActionBarView.setBackground(type);
        } else {
            if (getIntent().hasExtra(Constants.NAME_BAR)) {
                backActionBarView.setTitle(getIntent().getStringExtra(Constants.NAME_BAR));
                if (getIntent().hasExtra(Constants.SHARE_BAR))
                    backActionBarView.showShareIcon(View.VISIBLE);
                if (getIntent().hasExtra(Constants.BACKGROUND_BAR))
                    backActionBarView.setBackground(getIntent().getStringExtra(Constants.BACKGROUND_BAR));
            }
        }
        activityBaseBinding.llBaseActionBarContainer.addView(backActionBarView);
    }

    protected void initializeLanguage() {
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
    }

    public void getNotification() {
        if (getIntent() != null && getIntent().getBooleanExtra("is_notification", false)) {
            if (getIntent().getSerializableExtra("type") != null) {
                notification_checked = true;
                String typeNotifications = getIntent().getStringExtra("type");
                String orderId = getIntent().getStringExtra("attribute_id");
                String delegateId = getIntent().getStringExtra("delegate_id");
                Bundle bundle = new Bundle();
                backActionBarView.flag = 1;
                switch (typeNotifications) {
                    case "0":  // new Offer
                        setTitleName(ResourceManager.getString(R.string.offers_title), Constants.MARKETS);
                        OffersDetailsFragment homeMainFragment = new OffersDetailsFragment();
                        bundle.putString(Constants.BUNDLE, new Gson().toJson(new PassingObject(Integer.parseInt(orderId))));
                        homeMainFragment.setArguments(bundle);
                        MovementHelper.replaceFragmentTag(this, homeMainFragment, homeMainFragment.getClass().getName(), "");
                        break;
                    case "2": { // chat
                        ChatFragment fragment = new ChatFragment();
                        bundle.putString(Constants.BUNDLE, new Gson().toJson(new PassingObject(Integer.parseInt(orderId), delegateId)));
                        fragment.setArguments(bundle);
                        MovementHelper.replaceFragmentTag(this, fragment, fragment.getClass().getName(), "");
                        break;
                    }
                    case "1": { // TRIP
                        MovementHelper.startActivityMain(this);
                        break;
                    }
                    case "5": { // chatUber
                        setTitleName(ResourceManager.getString(R.string.chat), Constants.MARKETS);
                        ChatUberFragment fragment = new ChatUberFragment();
                        bundle.putString(Constants.BUNDLE, new Gson().toJson(new PassingObject(Integer.parseInt(orderId))));
                        fragment.setArguments(bundle);
                        MovementHelper.replaceFragmentTag(this, fragment, fragment.getClass().getName(), "");
                        break;
                    }
                    default:
                        setTitleName(ResourceManager.getString(R.string.menuNotifications), Constants.MARKETS);
                        NotificationsFragment fragment = new NotificationsFragment();
                        MovementHelper.replaceFragmentTag(this, fragment, fragment.getClass().getName(), "");
                        break;
                }
            }
        }
    }

    private Fragment getBundle(Fragment fragment) {
        Bundle bundle = new Bundle();
        bundle.putString(Constants.BUNDLE, getIntent().getStringExtra(Constants.BUNDLE));
        fragment.setArguments(bundle);
        if (getIntent().hasExtra(Constants.NAME_BAR)) {
            setTitleName(null, null);
        }
        return fragment;
    }

    @Override
    public void onBackPressed() {
        try {
            if (dialogLoader != null && dialogLoader.isShowing()) {
                dialogLoader.dismiss();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        super.onBackPressed();
        if (backActionBarView.flag == 1) {
            if (isTaskRoot()) {
                // This activity is at root of task, so launch main activity
                MovementHelper.startActivityBase(this, HomeMainFragment.class.getName(), null, null);
            } else {
                // This activity isn't at root of task, so just finish()
                finish();
            }
        } else
            finish();
    }

    public void enableRefresh(boolean status) {
        activityBaseBinding.swipeContainer.setEnabled(status);
    }

    public void stopRefresh(boolean status) {
        activityBaseBinding.swipeContainer.setRefreshing(status);
    }

    public MutableLiveData<Boolean> getRefreshingLiveData() {
        return refreshingLiveData;
    }

}