package amrk.app.amrk_user.activity;

import android.os.Bundle;
import android.os.Handler;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.MutableLiveData;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.customViews.actionbar.HomeActionBarView;
import amrk.app.amrk_user.customViews.menu.NavigationDrawerView;
import amrk.app.amrk_user.databinding.ActivityMainBinding;
import amrk.app.amrk_user.uber.home.HomeUberFragment;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class MainActivity extends ParentActivity {
    public HomeActionBarView homeActionBarView = null;
    public NavigationDrawerView navigationDrawerView;
    ActivityMainBinding activityMainBinding;
    public MutableLiveData<String> liveData = new MutableLiveData<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
//        Window window = getWindow();
//        window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
//        window.setStatusBarColor(Color.WHITE);
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        IApplicationComponent component = ((MyApplication) getApplicationContext()).getApplicationComponent();
        component.inject(this);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        homeActionBarView = new HomeActionBarView(this);
        navigationDrawerView = new NavigationDrawerView(this);
        activityMainBinding.llBaseContainer.addView(navigationDrawerView);
        navigationDrawerView.layoutNavigationDrawerBinding.llBaseActionBarContainer.addView(homeActionBarView, 0);
        homeActionBarView.setNavigation(navigationDrawerView);
        homeActionBarView.connectDrawer(navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu, true);
        navigationDrawerView.setActionBar(homeActionBarView);
        homeActionBarView.setTitle(ResourceManager.getString(R.string.uber_home));
        new Handler().postDelayed(() -> {
            MovementHelper.replaceFragment(this, new HomeUberFragment(), "");
        }, 1000);
        setEvents();

    }

    private void setEvents() {
//        navigationDrawerView.liveData.observe( this, new Observer<Object>() {
//            @SuppressLint("WrongConstant")
//            public void onChanged(@Nullable Object object) {
//                liveData.setValue(new Mutable());
//                Mutable mutable = (Mutable) object;
//                if(!mutable.message.equals(Constants.LOGIN_FIRST)) {
//                    homeActionBarView.setTitle(mutable.message);
//                    navigationDrawerView.layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawer(Gravity.START);
//                }else {
//                    toastMessage(ResourceManager.getString(R.string.please_login_first), R.drawable.ic_info_white, R.color.colorPrimary);
//                }
//            }
//        });
    }

}
