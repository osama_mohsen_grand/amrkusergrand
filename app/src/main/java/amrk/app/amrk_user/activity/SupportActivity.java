package amrk.app.amrk_user.activity;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import androidx.databinding.DataBindingUtil;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.ActivitySupportBinding;
import amrk.app.amrk_user.utils.Constants;
import im.delight.android.webview.AdvancedWebView;

public class SupportActivity extends ParentActivity implements AdvancedWebView.Listener {
    ActivitySupportBinding supportBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        IApplicationComponent component = ((MyApplication) getApplicationContext()).getApplicationComponent();
        component.inject(this);
        supportBinding = DataBindingUtil.setContentView(this, R.layout.activity_support);
        String url = getIntent().getStringExtra(Constants.BUNDLE);
        String title = getIntent().getStringExtra(Constants.TERMS);
        if (title != null)
            supportBinding.tvActionBarTitle.setText(title);
        else
            supportBinding.backContainer.setVisibility(View.GONE);
        if (url != null) {
            Log.e("onCreate", "onCreate: " + url);
            supportBinding.webview.setListener(this, this);
            supportBinding.webview.setMixedContentAllowed(false);
            supportBinding.webview.setDesktopMode(true);
            supportBinding.webview.loadUrl(url);
        }
        supportBinding.imgActionBarCancel.setOnClickListener(v -> finish());
    }

    @SuppressLint("NewApi")
    @Override
    public void onResume() {
        super.onResume();
        supportBinding.webview.onResume();
        // ...
    }

    @SuppressLint("NewApi")
    @Override
    public void onPause() {
        supportBinding.webview.onPause();
        // ...
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        supportBinding.webview.onDestroy();
        // ...
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        supportBinding.webview.onActivityResult(requestCode, resultCode, intent);
    }

    @Override
    public void onBackPressed() {
        if (!supportBinding.webview.onBackPressed()) {
            return;
        }
        super.onBackPressed();
    }

    @Override
    public void onPageStarted(String url, Bitmap favicon) {
        supportBinding.webview.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPageFinished(String url) {
        supportBinding.webProgress.setVisibility(View.GONE);
    }

    @Override
    public void onPageError(int errorCode, String description, String failingUrl) {
        toastError(description);
    }

    @Override
    public void onDownloadRequested(String url, String suggestedFilename, String mimeType, long contentLength, String contentDisposition, String userAgent) {
    }

    @Override
    public void onExternalPageRequest(String url) {
    }
}