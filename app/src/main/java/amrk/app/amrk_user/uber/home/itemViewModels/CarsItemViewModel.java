package amrk.app.amrk_user.uber.home.itemViewModels;


import androidx.databinding.Bindable;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.uber.home.models.CarsData;
import amrk.app.amrk_user.utils.Constants;

public class CarsItemViewModel extends BaseViewModel {
     CarsData carsData;

    public CarsItemViewModel(CarsData carsData) {
        this.carsData = carsData;
    }

    @Bindable
    public CarsData getCarsData() {
        return carsData;
    }

    public void selectCar() {
        getLiveData().setValue(Constants.SELECT_CAR);
    }
}
