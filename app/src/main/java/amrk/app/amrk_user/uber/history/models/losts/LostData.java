package amrk.app.amrk_user.uber.history.models.losts;

import com.google.gson.annotations.SerializedName;

public class LostData {

    @SerializedName("lost")
    private String lost;

    @SerializedName("id")
    private int id;
    private boolean checked;

    public String getLost() {
        return lost;
    }

    public int getId() {
        return id;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }
}