package amrk.app.amrk_user.uber.home.itemViewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.uber.home.models.cancelTripReasons.CancelReasonsData;


public class CancelReasonsItemViewModel extends BaseViewModel {
     CancelReasonsData reasonsData;
     MutableLiveData<CancelReasonsData> itemHomeDataMutableLiveData;

    public CancelReasonsItemViewModel(CancelReasonsData reasonsData) {
        this.reasonsData = reasonsData;
        itemHomeDataMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<CancelReasonsData> getItemHomeDataMutableLiveData() {
        return itemHomeDataMutableLiveData;
    }

    @Bindable
    public CancelReasonsData getReasonsData() {
        return reasonsData;
    }


    public void itemAction() {
        itemHomeDataMutableLiveData.setValue(reasonsData);
    }
}
