package amrk.app.amrk_user.uber.favorites.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemFavoriteLocationBinding;
import amrk.app.amrk_user.pages.newOrder.models.SavedLocationsData;
import amrk.app.amrk_user.uber.favorites.viewModels.ItemFavoritesLocationsViewModel;

public class FavoritesLocationsAdapter extends RecyclerView.Adapter<FavoritesLocationsAdapter.MenuView> {
    List<SavedLocationsData> locationsList;
    private Context context;
    MutableLiveData<SavedLocationsData> savedLiveData = new MutableLiveData<>();
    MutableLiveData<Object> deleteLiveData = new MutableLiveData<>();
    public int position;

    public FavoritesLocationsAdapter() {
        this.locationsList = new ArrayList<>();
    }

    public MutableLiveData<SavedLocationsData> getSavedLiveData() {
        return savedLiveData;
    }

    public MutableLiveData<Object> getDeleteLiveData() {
        return deleteLiveData;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_favorite_location,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        SavedLocationsData menuModel = locationsList.get(position);
        ItemFavoritesLocationsViewModel itemMenuViewModel = new ItemFavoritesLocationsViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            this.position = position;
            savedLiveData.setValue(menuModel);
        });
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<SavedLocationsData> dataList) {
        this.locationsList.clear();
        locationsList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return locationsList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemFavoriteLocationBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemFavoritesLocationsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
