package amrk.app.amrk_user.uber.home.itemViewModels;



import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.uber.home.models.SavedLocations;
import amrk.app.amrk_user.utils.Constants;

public class SavedLocationItemViewModel extends BaseViewModel {
     SavedLocations savedLocations;

    public SavedLocationItemViewModel(SavedLocations savedLocations) {
        this.savedLocations = savedLocations;
    }

    @Bindable
    public SavedLocations getSavedLocations() {
        return savedLocations;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.SAVED_SELECTED);
    }
}
