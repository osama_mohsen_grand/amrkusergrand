package amrk.app.amrk_user.uber.home;


import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.animation.AnimationUtils;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.android.material.bottomsheet.BottomSheetBehavior;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;

import org.jetbrains.annotations.NotNull;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.MainActivity;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.base.maps.MapHelper;
import amrk.app.amrk_user.base.maps.models.DistanceTimeResponse;
import amrk.app.amrk_user.customViews.CustomDrawable;
import amrk.app.amrk_user.databinding.CallUserSheetBinding;
import amrk.app.amrk_user.databinding.CancelTripBottomSheetBinding;
import amrk.app.amrk_user.databinding.CancelWarningDialogBinding;
import amrk.app.amrk_user.databinding.FragmentHomeUberBinding;
import amrk.app.amrk_user.databinding.SelectCarBottomSheetBinding;
import amrk.app.amrk_user.databinding.SelectScheduleBottomSheetBinding;
import amrk.app.amrk_user.databinding.TripRateDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.uber.chat.view.ChatUberFragment;
import amrk.app.amrk_user.uber.home.models.CarsResponse;
import amrk.app.amrk_user.uber.home.models.PusherTrip;
import amrk.app.amrk_user.uber.home.models.RateRequest;
import amrk.app.amrk_user.uber.home.models.TripDataFromNotifications;
import amrk.app.amrk_user.uber.home.models.TripRequest;
import amrk.app.amrk_user.uber.home.models.TripStatusResponse;
import amrk.app.amrk_user.uber.home.models.cancelTripReasons.CancelReasonsResponse;
import amrk.app.amrk_user.uber.home.viewModels.HomeViewModels;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.services.RealTimeReceiver;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import amrk.app.amrk_user.utils.session.UserHelper;

import static android.app.Activity.RESULT_OK;


public class HomeUberFragment extends BaseFragment implements OnMapReadyCallback, RealTimeReceiver.NewTripReceiverListener, RoutingListener {
    private FragmentHomeUberBinding homeBinding;
    @Inject
    HomeViewModels homeViewModels;
    GoogleMap mMap;
    BottomSheetBehavior loadingSheet, sheetTrip;
    BottomSheetDialog carsSheet, sheetReasons, callSheet;
    double currentLat, currentLng;
    LatLng start, mid = null;
    Context context;
    int mapMoving = 0;
    MapHelper mapHelper;
    boolean fetchLocation = false;
    Dialog rateDialog, warningDialog, scheduleSheet;
    //start routing
    List<Polyline> polyline = null;
    Polyline polylineOld = null;
    SelectScheduleBottomSheetBinding binding;

    public HomeUberFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        homeBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_home_uber, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        homeBinding.setHomeViewModel(homeViewModels);
        loadingSheet = BottomSheetBehavior.from(homeBinding.loadingDriverBottomSheet.selectCard);
        loadingSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        homeBinding.loadingDriverBottomSheet.loadingText.startAnimation(AnimationUtils.loadAnimation(context, R.anim.jump_fast));
        sheetTrip = BottomSheetBehavior.from(homeBinding.driverSelectedBottomSheet.selectCard);
        sheetTrip.setState(BottomSheetBehavior.STATE_HIDDEN);
        init(savedInstanceState);
        liveDataListeners();
        connectPusher(Constants.tripEventName, Constants.USERSBeamName.concat(homeViewModels.userData.getJwt()), Constants.tripChannelName.concat(homeViewModels.userData.getJwt()));
        return homeBinding.getRoot();
    }


    private void liveDataListeners() {
        homeViewModels.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            closeKeyboard();
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            homeViewModels.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.SHOW_CARS_SHEET.equals(((Mutable) o).message)) {
                SelectCarBottomSheetBinding sortBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.select_car_bottom_sheet, null, false);
                carsSheet = new BottomSheetDialog(context);
                carsSheet.setContentView(sortBinding.getRoot());
                sortBinding.setSelectCarBottomSheetViewModels(homeViewModels);
                carsSheet.show();
            } else if (Constants.GET_CARS.equals(mutable.message)) {
                UserHelper.getInstance(context).cars(((CarsResponse) mutable.object).getData().getCarsDataList());
                UserHelper.getInstance(context).savedLocations(((CarsResponse) mutable.object).getData().getSavedLocationsList());
                UserHelper.getInstance(context).userLogin(((CarsResponse) mutable.object).getData().getUserData());
                homeViewModels.getCarsAdapter().updateData(((CarsResponse) mutable.object).getData().getCarsDataList());
                homeViewModels.notifyChange(BR.carsAdapter);
                homeViewModels.setCarsData(homeViewModels.getCarsAdapter().carsData.get(homeViewModels.getCarsAdapter().lastPosition));
            } else if (Constants.SEARCH_LOCATION.equals(((Mutable) o).message) || Constants.EDIT_LOCATION.equals(((Mutable) o).message)) {
                if (!com.google.android.libraries.places.api.Places.isInitialized()) {
                    Places.initialize(context, getString(R.string.google_map));
                }
                List<Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.NAME, Place.Field.ADDRESS,
                        com.google.android.libraries.places.api.model.Place.Field.LAT_LNG);
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(context);
                if (Constants.SEARCH_LOCATION.equals(((Mutable) o).message))
                    startActivityForResult(intent, Constants.AUTOCOMPLETE_REQUEST_CODE);
                else
                    startActivityForResult(intent, Constants.AUTOCOMPLETE_REQUEST_LOCATION_UPDATE_CODE);
            } else if (Constants.SELECT_CAR.equals(mutable.message)) {
                homeViewModels.setCarsData(homeViewModels.getCarsAdapter().carsData.get(homeViewModels.getCarsAdapter().lastPosition));
                carsSheet.dismiss();
            } else if (Constants.DROP_OFF.equals(mutable.message)) {
                MovementHelper.startActivityForResultWithBundle(context, new PassingObject(homeViewModels.getTripRequest()), null, AddDropFragment.class.getName(), null, Constants.UBER);
            } else if (Constants.GOOGLE_API.equals(mutable.message)) {
                mapHelper.getLessDriverTime(((DistanceTimeResponse) mutable.object).getRows().get(0).getElements());
            } else if (Constants.TRIP_STATUS.equals(mutable.message)) {
                homeViewModels.setTripDataFromNotifications(((TripStatusResponse) mutable.object).getDataFromNotifications());
                if (homeViewModels.getTripDataFromNotifications().getStatus() == 0) {
                    mMap.clear();
                    mapMoving = 1;
                    visibleViews(View.GONE);
                    resetAfterCancel();
                } else if (homeViewModels.getTripDataFromNotifications().getStatus() == 1 || homeViewModels.getTripDataFromNotifications().getStatus() == 2 || homeViewModels.getTripDataFromNotifications().getStatus() == 5) {
                    acceptedTrip();
                } else if (homeViewModels.getTripDataFromNotifications().getStatus() == 3) {
                    visibleViews(View.VISIBLE);
                    sheetTrip.setHideable(true);
                    sheetTrip.setState(BottomSheetBehavior.STATE_HIDDEN);
                    mapHelper.stopLocationCallBack();
                    if (homeViewModels.getTripDataFromNotifications().getPayment() == 1) {
//                        MovementHelper.startPaymentActivityForResultWithBundle(context, UserPreferenceHelper.getInstance(context).getTripId());
                    } else {
                        showRateDialog(((TripStatusResponse) mutable.object).getDataFromNotifications());
                    }
                } else if (homeViewModels.getTripDataFromNotifications().getStatus() == 4) {
                    resetAfterCancel();
                }

            } else if (Constants.CANCEL_REASONS.equals(mutable.message)) {
                if (warningDialog != null)
                    warningDialog.dismiss();
                homeViewModels.getReasonsAdapter().updateData(((CancelReasonsResponse) mutable.object).getData());
                CancelTripBottomSheetBinding sheetBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.cancel_trip_bottom_sheet, null, false);
                sheetReasons = new BottomSheetDialog(context);
                sheetReasons.setContentView(sheetBinding.getRoot());
                sheetBinding.setCancelReasonBottomSheetViewModels(homeViewModels);
                sheetReasons.show();
            } else if (Constants.CANCEL_TRIP.equals(mutable.message)) {
                cancelledByOwner();
            } else if (Constants.CALL_DRIVER.equals(mutable.message)) {
                AppHelper.openDialNumber(context, homeViewModels.getTripDataFromNotifications().getUser().getPhone());
            } else if (Constants.CALL.equals(mutable.message)) {
                showCallDialog();
            } else if (Constants.HIDE_CALL_DIALOG.equals(mutable.message)) {
                callSheet.dismiss();
                // show after hide dialog
                sheetTrip.setHideable(false);
                sheetTrip.setState(BottomSheetBehavior.STATE_EXPANDED);
            } else if (Constants.CHAT.equals(mutable.message)) {
                MovementHelper.startActivityWithBundle(context, new PassingObject(homeViewModels.getTripDataFromNotifications().getId()), getString(R.string.chat), ChatUberFragment.class.getName(), null, Constants.MARKETS);
            } else if (Constants.EMPTY_DRIVER.equals(mutable.message)) {
                ((ParentActivity) context).toastError(getString(R.string.comment_trip_rate));
            } else if (Constants.RATE_TRIP.equals(mutable.message)) {
                mMap.clear();
                mapMoving = 0;
                rateDialog.dismiss();
                homeViewModels.setRateRequest(new RateRequest());
                UserHelper.getInstance(context).addTripId(0);
            } else if (Constants.CANCEL_TRIP_WARNING_DIALOG.equals(mutable.message)) {
                showCancelWarningDialog();
            } else if (Constants.SCHEDULE.equals(mutable.message)) {
                showScheduleDialog();
            } else if (Constants.SELECT_SCHEDULE.equals(mutable.message)) {
                selectSchedule();
            } else if (Constants.DELTE_TRIP.equals(mutable.message)) {
                UserHelper.getInstance(context).addTripId(0);
                mMap.clear();
                visibleViews(View.VISIBLE);
                loadingSheet.setHideable(true);
                mapHelper.stopLocationCallBack();
                mapHelper.getAllNearestDrivers(new LatLng(currentLat, currentLng));
                loadingSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
            } else if (Constants.PUSHER_LIVE_DATA.equals(mutable.message)) {
                tripActionNavigations();
            }
        });
        ((MainActivity) context).liveData.observe((LifecycleOwner) context, mutable -> {
            homeViewModels.setLessTimeDriverVisible(View.VISIBLE);
            homeViewModels.getTripRequest().setMinTime(mutable);
            homeBinding.lessTime.setText(mutable);
            homeBinding.carTime.setText(mutable);
        });
    }

    public void connectPusher(String eventName, String beamName, String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("75b53060b1f415501d21", options);
        pusher.connect();
        Channel channel = pusher.subscribe(channelName);
        channel.bind(eventName, event -> {
            Log.e(TAG, "connectPusher: "+event );
            PusherTrip pusherTrip = new Gson().fromJson(event.getData(), PusherTrip.class);
            homeViewModels.setTripDataFromNotifications(pusherTrip.getDataFromNotifications());
            homeViewModels.liveData.postValue(new Mutable(Constants.PUSHER_LIVE_DATA));
        });
        PushNotifications.start(MyApplication.getInstance(), "4b9224fd-ac88-4e17-86bf-65c347bc0fbd");
        PushNotifications.clearAllState();
        PushNotifications.addDeviceInterest(Constants.INTEREST);
        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(Constants.PUSHER_TOKEN_URL, () -> {
            // Headers and URL query params your auth endpoint needs to
            // request a Beams Token for a given user
            HashMap<String, String> headers = new HashMap<>();
            // for example:
            headers.put("jwt", LanguagesHelper.getJwt());
            HashMap<String, String> queryParams = new HashMap<>();
            return new AuthData(
                    headers,
                    queryParams
            );
        });
        PushNotifications.setUserId(beamName, tokenProvider, new BeamsCallback<>() {
            @Override
            public void onSuccess(@NonNull Void... values) {
            }

            @Override
            public void onFailure(PusherCallbackError error) {
            }
        });

    }

    private void setChangeCameraListener() {
        mMap.setOnMyLocationChangeListener(location -> {
            if (!fetchLocation) {
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 13));
                CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(location.getLatitude(), location.getLongitude())).zoom(17).bearing(90).tilt(40).build();
                mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
                mMap.setOnMyLocationChangeListener(null);
                homeViewModels.setLessTimeDriverVisible(View.GONE);
                fetchLocation = true;
            }
        });
        mMap.setOnCameraMoveStartedListener(i -> {
        });

        mMap.setOnCameraIdleListener(() -> {
            if (mapMoving == 0) {
                mMap.clear();
                currentLat = mMap.getCameraPosition().target.latitude;
                currentLng = mMap.getCameraPosition().target.longitude;
                if (UserHelper.getInstance(context).getTripId() == 0) {
                    if (homeViewModels.getCarsAdapter().carsData.size() == 0)
                        homeViewModels.getCars();
                    homeBinding.firstMarkerContainer.setVisibility(View.VISIBLE);
                    mapHelper.getAllNearestDrivers(mMap.getCameraPosition().target);
                }
                mapHelper.getAddress(currentLat, currentLng, (address, city) -> {
                    closeKeyboard();
                    homeBinding.currentLocationText.setText(address);
                    homeViewModels.getTripRequest().setStart_address(address);
                    homeViewModels.getTripRequest().setStart_lat(currentLat);
                    homeViewModels.getTripRequest().setStart_lng(currentLng);
                });
            }

        });
        mMap.setOnMapLoadedCallback(() -> {
            if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                    != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                    Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
//            mMap.setMyLocationEnabled(false);
            if (UserHelper.getInstance(getActivity()).getTripId() != 0 && sheetTrip.getState() == BottomSheetBehavior.STATE_HIDDEN) {
                homeViewModels.getLastTrip();
            }
        });
    }

    private static final String TAG = "HomeFragment";


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        homeBinding.mapview.getMapAsync(this);
        homeBinding.mapview.onResume();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void init(Bundle savedInstanceState) {
        homeBinding.mapview.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AppCompatActivity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        } else {
            homeBinding.mapview.getMapAsync(this);
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (data != null) {
            if (requestCode == Constants.RESULT_CODE) {
                if (Objects.equals(data.getStringExtra(Constants.BUNDLE), Constants.TRIP_CREATED)) {
                    mMap.clear();
                    if (homeViewModels.getTripRequest().getType() == 1) {
                        mapMoving = 1;
                        // make home view Gone
                        visibleViews(View.GONE);
                        loadingSheet.setHideable(false);
                        loadingSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
                        mapHelper.geoQuery.removeAllListeners();
                    } else {
                        homeViewModels.setTripRequest(new TripRequest());
                        cancelledByOwner();
                    }
                }
            } else if (requestCode == Constants.AUTOCOMPLETE_REQUEST_CODE || requestCode == Constants.AUTOCOMPLETE_REQUEST_LOCATION_UPDATE_CODE) {
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    if (place.getLatLng() != null) {
                        double placeLat = place.getLatLng().latitude;
                        double placeLng = place.getLatLng().longitude;
                        homeBinding.currentLocationText.setText(place.getAddress());
                        if (requestCode == Constants.AUTOCOMPLETE_REQUEST_CODE) {
                            MovementHelper.mpaZoomCamera(place.getLatLng(), mMap);
                            homeViewModels.getTripRequest().setStart_address(place.getAddress());
                            homeViewModels.getTripRequest().setStart_lat(placeLat);
                            homeViewModels.getTripRequest().setStart_lng(placeLng);
                        } else
                            homeViewModels.updateLastLocation(place.getAddress(), placeLat, placeLng, currentLat, currentLng);
                    }
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);
                    Log.e(TAG, status.getStatusMessage());
                }
                return;
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
        closeKeyboard();
    }

    @Override
    public void onTripChanged(String dataFromNotification) {
//        homeViewModels.setTripDataFromNotifications(dataFromNotification);
        loadingSheet.setHideable(true);
        loadingSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        homeViewModels.getLastTrip();
    }

    private void tripActionNavigations() {
        loadingSheet.setHideable(true);
        loadingSheet.setState(BottomSheetBehavior.STATE_HIDDEN);
        if (homeViewModels.getTripDataFromNotifications().getStatus() == 1 || homeViewModels.getTripDataFromNotifications().getStatus() == 2) {
            UserHelper.getInstance(getActivity()).addTripId(homeViewModels.getTripDataFromNotifications().getId());
            //show driver sheet
            acceptedTrip();
        } else if (homeViewModels.getTripDataFromNotifications().getStatus() == 4) {
            resetAfterCancel();
        } else if (homeViewModels.getTripDataFromNotifications().getStatus() == 3) {
            mMap.clear();
            visibleViews(View.VISIBLE);
            sheetTrip.setHideable(true);
            mapHelper.stopLocationCallBack();
            mapHelper.getAllNearestDrivers(new LatLng(currentLat, currentLng));
            sheetTrip.setState(BottomSheetBehavior.STATE_HIDDEN);
            showRateDialog(homeViewModels.getTripDataFromNotifications());
        }
    }

    private void cancelledByOwner() {
        UserHelper.getInstance(context).addTripId(0);
        if (sheetReasons != null)
            sheetReasons.dismiss();
        visibleViews(View.VISIBLE);
        resetAfterCancel();
        mapMoving = 0;
    }

    private void acceptedTrip() {
        mapMoving = 1;
        visibleViews(View.GONE);
        int color = Color.parseColor(homeViewModels.getTripDataFromNotifications().getUser() != null && homeViewModels.getTripDataFromNotifications().getUser().getCar_color() != null
                ? homeViewModels.getTripDataFromNotifications().getUser().getCar_color() : "#F2EFF1");
        CustomDrawable customDrawable = new CustomDrawable(color, color, color, 4, getResources().getColor(R.color.colorPrimaryDark, null), 0);
        sheetTrip.setHideable(false);
        sheetTrip.setPeekHeight(200);
        sheetTrip.setState(BottomSheetBehavior.STATE_EXPANDED);
        homeBinding.driverSelectedBottomSheet.carColor.setBackground(customDrawable);
        if (homeViewModels.getTripDataFromNotifications().getStatus() == 2 && homeViewModels.getTripDataFromNotifications().getTripPaths().size() == 1) {
            toastMessage(getString(R.string.leading_captin));
            mMap.clear();
            homeBinding.driverSelectedBottomSheet.textArrivalVal.setText(getString(R.string.empty_dashs));
            homeBinding.driverSelectedBottomSheet.textDistanceVal.setText(getString(R.string.empty_dashs));
            if (mapHelper.databaseReference != null)
                mapHelper.databaseReference.removeEventListener(mapHelper.valueEventListener);
            mapHelper.requestLocationUpdates();
        } else {
            findRoutes();
        }

    }

    private void showCallDialog() {
        sheetTrip.setHideable(true);
        sheetTrip.setState(BottomSheetBehavior.STATE_HIDDEN);// hide when cancel reasons dialog opened
        CallUserSheetBinding sheetBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.call_user_sheet, null, false);
        callSheet = new BottomSheetDialog(context);
        callSheet.setContentView(sheetBinding.getRoot());
        sheetBinding.setDriverBottomSheetViewModels(homeViewModels);
        callSheet.setOnCancelListener(dialog -> {
            // show after hide dialog
            sheetTrip.setState(BottomSheetBehavior.STATE_EXPANDED);
        });
        callSheet.show();
    }

    private void showRateDialog(TripDataFromNotifications tripDataFromNotifications) {
        rateDialog = new Dialog(context, R.style.PauseDialog);
        rateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(rateDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        rateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        TripRateDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(rateDialog.getContext()), R.layout.trip_rate_dialog, null, false);
        rateDialog.setContentView(binding.getRoot());
        binding.setViewModel(homeViewModels);
        homeViewModels.setTripDataFromNotifications(tripDataFromNotifications);
        rateDialog.setOnDismissListener(dialog -> {
            homeViewModels.setTripDataFromNotifications(new TripDataFromNotifications());
            UserHelper.getInstance(context).addTripId(0);
        });
        rateDialog.show();
    }

    private void showCancelWarningDialog() {
        warningDialog = new Dialog(context, R.style.PauseDialog);
        warningDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(warningDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        warningDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CancelWarningDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(warningDialog.getContext()), R.layout.cancel_warning_dialog, null, false);
        warningDialog.setContentView(binding.getRoot());
        binding.setViewModel(homeViewModels);
        warningDialog.show();
    }

    private void showScheduleDialog() {
        binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.select_schedule_bottom_sheet, null, false);
        scheduleSheet = new BottomSheetDialog(context);
        scheduleSheet.setContentView(binding.getRoot());
        binding.setSelectScheduleBottomSheetViewModels(homeViewModels);
        scheduleSheet.show();
    }

    private void selectSchedule() {
        Date date = binding.dataTime.getDate();
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm", new Locale("en"));
        String strDate = formatter.format(date);
        String[] dates = strDate.split(" ");
        homeViewModels.getTripRequest().setType(2);
        homeViewModels.getTripRequest().setDate(dates[0]);
        homeViewModels.getTripRequest().setTime(dates[1]);
        homeBinding.selectedDate.setVisibility(View.VISIBLE);
        homeBinding.selectedDate.setText(homeViewModels.getTripRequest().getDate().concat(" , ").concat(homeViewModels.getTripRequest().getTime()));

        scheduleSheet.dismiss();
    }

    private void resetAfterCancel() {
        mMap.clear();
        //show loading sheet for new driver
        if (UserHelper.getInstance(context).getTripId() != 0) {
            loadingSheet.setHideable(false);
            loadingSheet.setState(BottomSheetBehavior.STATE_EXPANDED);
        }
        //hide driver sheet after
        sheetTrip.setHideable(true);
        sheetTrip.setPeekHeight(0);
        sheetTrip.setState(BottomSheetBehavior.STATE_HIDDEN);
    }

    private void visibleViews(int visible) {
        homeBinding.currentLocationContainer.setVisibility(visible);
        homeBinding.carDetailsContainer.setVisibility(visible);
        homeBinding.btnHomePickUp.setVisibility(visible);
        homeBinding.imageView2.setVisibility(visible);
        homeBinding.firstMarkerContainer.setVisibility(visible == 0 ? View.VISIBLE : View.GONE);
        // selected date
        homeBinding.selectedDate.setVisibility(View.GONE);
    }


    // function to find Routes.
    private void findRoutes() {
        // new Trip
        if (homeViewModels.getTripDataFromNotifications().getStatus() == 1 || homeViewModels.getTripDataFromNotifications().getStatus() == 5) {
            start = new LatLng(Double.parseDouble(homeViewModels.getTripDataFromNotifications().getUser().getLat()), Double.parseDouble(homeViewModels.getTripDataFromNotifications().getUser().getLng()));
            homeViewModels.end = new LatLng(homeViewModels.getTripDataFromNotifications().getStartLat(), homeViewModels.getTripDataFromNotifications().getStartLng());
        } else if (homeViewModels.getTripDataFromNotifications().getStatus() == 2) { //trip started
            start = new LatLng(homeViewModels.getTripDataFromNotifications().getStartLat(), homeViewModels.getTripDataFromNotifications().getStartLng());
            if (homeViewModels.getTripDataFromNotifications().getTripPaths().size() == 3) {
                homeViewModels.mid = new LatLng(Double.parseDouble(homeViewModels.getTripDataFromNotifications().getTripPaths().get(1).getLat()), Double.parseDouble(homeViewModels.getTripDataFromNotifications().getTripPaths().get(1).getLng()));
            }
            homeViewModels.end = new LatLng(Double.parseDouble(homeViewModels.getTripDataFromNotifications().getTripPaths().get(homeViewModels.getTripDataFromNotifications().getTripPaths().size() - 1).getLat()), Double.parseDouble(homeViewModels.getTripDataFromNotifications().getTripPaths().get(homeViewModels.getTripDataFromNotifications().getTripPaths().size() - 1).getLng()));
        }
        if (start == null || homeViewModels.end == null) {
            toastMessage(getString(R.string.leading_captin));
        } else {
            Routing routing;
            if (mid != null) {
                routing = new Routing.Builder()
                        .travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(this)
                        .alternativeRoutes(true)
                        .waypoints(start, homeViewModels.mid, homeViewModels.end)
                        .key(getString(R.string.google_map))  //also define your api key here.
                        .build();
            } else {
                routing = new Routing.Builder()
                        .travelMode(AbstractRouting.TravelMode.DRIVING)
                        .withListener(this)
                        .alternativeRoutes(true)
                        .waypoints(start, homeViewModels.end)
                        .key(getString(R.string.google_map))  //also define your api key here.
                        .build();
            }
            routing.execute();
        }
    }

    //Routing call back functions.
    @Override
    public void onRoutingFailure(RouteException e) {
        findRoutes();
    }

    @Override
    public void onRoutingStart() {

    }

    //If Route finding success..
    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        mMap.clear();
        mapHelper.driverMarker = null;
        mapHelper.latLngs.clear();
        PolylineOptions polyOptions = new PolylineOptions();
        polyline = new ArrayList<>();
        //add route(s) to the map using polyline
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                homeBinding.driverSelectedBottomSheet.textArrivalVal.setText(route.get(i).getDurationText());
                homeBinding.driverSelectedBottomSheet.textDistanceVal.setText(route.get(i).getDistanceText());
                polyOptions.color(getResources().getColor(R.color.black, null));
                polyOptions.width(7);
                polyOptions.addAll(route.get(i).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                if (polylineOld != null) {
                    polylineOld.remove();
                } else
                    polylineOld = polyline;
                this.polyline.add(polyline);
            }
        }
        mapHelper.addUserMarker(homeViewModels.end);
        //Add Marker on route ending position
        if (homeViewModels.getTripDataFromNotifications().getStatus() == 1 || homeViewModels.getTripDataFromNotifications().getStatus() == 5)
            mapHelper.getDriverLastLocation(); //real time locations
        else if (homeViewModels.getTripDataFromNotifications().getStatus() == 2) {
            if (mapHelper.databaseReference != null)
                mapHelper.databaseReference.removeEventListener(mapHelper.valueEventListener);
            mapHelper.requestLocationUpdates();
        }
        if (homeViewModels.getTripDataFromNotifications().getStatus() == 2 && homeViewModels.getTripDataFromNotifications().getTripPaths().size() == 3) {
            MarkerOptions midMarker = new MarkerOptions();
            midMarker.position(homeViewModels.mid);
            midMarker.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_first_location), 100, 100)));
            mMap.addMarker(midMarker);
        }
    }

    @Override
    public void onRoutingCancelled() {
        findRoutes();
    }

    @Override
    public void onMapReady(@NotNull GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.getUiSettings().setCompassEnabled(false);
        mMap.setMyLocationEnabled(true);
        mapHelper = new MapHelper(mMap, context, homeViewModels);
        setChangeCameraListener();
    }

    @Override
    public void onResume() {
        homeBinding.mapview.onResume();
        homeViewModels.getRepository().setLiveData(homeViewModels.liveData);
        MyApplication.getInstance().setTripReceiverListener(this);
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            homeViewModels.getCars();
        }
        super.onResume();
    }


    @Override
    public void onPause() {
        super.onPause();
        homeBinding.mapview.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        homeBinding.mapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        homeBinding.mapview.onLowMemory();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
