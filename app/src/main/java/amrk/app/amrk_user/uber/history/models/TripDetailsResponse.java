package amrk.app.amrk_user.uber.history.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class TripDetailsResponse extends StatusMessage {
    @SerializedName("data")
    HistoryData historyData;

    public HistoryData getHistoryData() {
        return historyData;
    }
}
