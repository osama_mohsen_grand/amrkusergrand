package amrk.app.amrk_user.uber.history.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.IssueItemBinding;
import amrk.app.amrk_user.uber.history.itemViewModels.IssueItemViewModels;
import amrk.app.amrk_user.uber.history.models.issues.IssuesData;
import amrk.app.amrk_user.utils.helper.MovementHelper;


public class IssuesAdapter extends RecyclerView.Adapter<IssuesAdapter.ViewHolder> {
    public List<IssuesData> issuesDataList;
    Context context;
    private MutableLiveData<Integer> adapterLiveData;

    public IssuesAdapter() {
        issuesDataList = new ArrayList<>();
        adapterLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Integer> getAdapterLiveData() {
        return adapterLiveData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.issue_item,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        IssuesData dataModel = issuesDataList.get(position);
        IssueItemViewModels homeItemViewModels = new IssueItemViewModels(dataModel);
        homeItemViewModels.getLiveData().observe(((LifecycleOwner) MovementHelper.unwrap(context)), new Observer<Object>() {
            @Override
            public void onChanged(Object o) {
                adapterLiveData.setValue(dataModel.getId());
            }
        });
        holder.setViewModel(homeItemViewModels);
    }

    @Override
    public int getItemCount() {
        return this.issuesDataList.size();
    }

    //
    @Override
    public void onViewAttachedToWindow(ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@Nullable List<IssuesData> data) {
        this.issuesDataList.clear();

        this.issuesDataList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        IssueItemBinding itemBinding;

        //
        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(IssueItemViewModels itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setIssueItemViewModels(itemViewModels);
            }
        }
    }
}
