package amrk.app.amrk_user.uber.history.models;

import com.google.gson.annotations.SerializedName;

public class CarInfo {
    @SerializedName("car_color")
    private String car_color;
    @SerializedName("color_name")
    private String color_name;
    @SerializedName("car_num")
    private String car_num;
    @SerializedName("car_model")
    private String car_model;
 @SerializedName("car_image")
    private String car_image;

    public String getCar_image() {
        return car_image;
    }

    public String getCar_color() {
        return car_color;
    }

    public String getColor_name() {
        return color_name;
    }

    public String getCar_num() {
        return car_num;
    }

    public String getCar_model() {
        return car_model;
    }
}
