
package amrk.app.amrk_user.uber.chat.viewmodel;

import android.text.TextUtils;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.repository.ChatRepository;
import amrk.app.amrk_user.uber.chat.adapter.ChatAdapter;
import amrk.app.amrk_user.uber.chat.model.ChatUberRequest;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class ChatUberViewModel extends BaseViewModel {
    public
    MutableLiveData<Mutable> liveData;
    @Inject
    public ChatRepository repository;
    public ChatAdapter adapter = new ChatAdapter();
    public ChatUberRequest request = new ChatUberRequest();
    public List<FileObjects> fileObjectList = new ArrayList<>();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public ChatUberViewModel(ChatRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void chat() {
        compositeDisposable.add(repository.getUberChat(getPassingObject().getId()));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public void select() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    public void sendMessage() {
        request.setTrip_id(String.valueOf(getPassingObject().getId()));
        request.setIs_captin("0");
        if (!TextUtils.isEmpty(request.getMessage()))
            repository.sendUberChat(request);
    }


}
