package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TripRequest {
    @SerializedName("type")
    private int type;
    @SerializedName("car_level_id")
    private int car_level_id;
    private int car_selected_position;
    @SerializedName("promo_id")
    private int promo_id;
    @SerializedName("payment")
    private int payment;
    @SerializedName("start_address")
    private String start_address;
    @SerializedName("start_lat")
    private double start_lat;
    @SerializedName("start_lng")
    private double start_lng;
    @SerializedName("address")
    private List<String> addressList;
    @SerializedName("lat")
    private List<Double> lat;
    @SerializedName("lng")
    private List<Double> lng;
    @SerializedName("status")
    private List<Integer> status;
    private String minTime;
    @SerializedName("date")
    private String date;
    @SerializedName("time")
    private String time;
    @SerializedName("trip_total")
    private String tripTotal;

    public String getStart_address() {
        return start_address;
    }

    public void setStart_address(String start_address) {
        this.start_address = start_address;
    }

    public double getStart_lat() {
        return start_lat;
    }

    public void setStart_lat(double start_lat) {
        this.start_lat = start_lat;
    }

    public double getStart_lng() {
        return start_lng;
    }

    public void setStart_lng(double start_lng) {
        this.start_lng = start_lng;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCar_level_id() {
        return car_level_id;
    }

    public void setCar_level_id(int car_level_id) {
        this.car_level_id = car_level_id;
    }

    public int getPromo_id() {
        return promo_id;
    }

    public void setPromo_id(int promo_id) {
        this.promo_id = promo_id;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    public List<String> getAddressList() {
        return addressList;
    }

    public void setAddressList(List<String> addressList) {
        this.addressList = addressList;
    }

    public List<Double> getLat() {
        return lat;
    }

    public void setLat(List<Double> lat) {
        this.lat = lat;
    }

    public List<Double> getLng() {
        return lng;
    }

    public void setLng(List<Double> lng) {
        this.lng = lng;
    }

    public List<Integer> getStatus() {
        return status;
    }

    public void setStatus(List<Integer> status) {
        this.status = status;
    }

    public int getCar_selected_position() {
        return car_selected_position;
    }

    public void setCar_selected_position(int car_selected_position) {
        this.car_selected_position = car_selected_position;
    }

    public String getMinTime() {
        return minTime;
    }

    public void setMinTime(String minTime) {
        this.minTime = minTime;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTripTotal() {
        return tripTotal;
    }

    public void setTripTotal(String tripTotal) {
        this.tripTotal = tripTotal;
    }

    @Override
    public String toString() {
        return "TripRequest{" +
                "type=" + type +
                ", car_level_id=" + car_level_id +
                ", car_selected_position=" + car_selected_position +
                ", promo_id=" + promo_id +
                ", payment=" + payment +
                ", start_address='" + start_address + '\'' +
                ", start_lat=" + start_lat +
                ", start_lng=" + start_lng +
                ", addressList=" + addressList +
                ", lat=" + lat +
                ", lng=" + lng +
                ", status=" + status +
                ", minTime='" + minTime + '\'' +
                '}';
    }
}
