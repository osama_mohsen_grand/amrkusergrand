package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.SerializedName;

public class StatusRequest {
    @SerializedName("trip_id")
    private int trip_id;
    @SerializedName("status")
    private int status;
    @SerializedName("cancel_id")
    private int cancel_id;

    public int getTrip_id() {
        return trip_id;
    }

    public int getCancel_id() {
        return cancel_id;
    }

    public void setCancel_id(int cancel_id) {
        this.cancel_id = cancel_id;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public void setTrip_id(int trip_id) {
        this.trip_id = trip_id;
    }
}
