package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.pages.auth.models.UserData;

public class HomeData {
    @SerializedName("cars")
    private List<CarsData> carsDataList;
    @SerializedName("savedLocations")
    private List<SavedLocations> savedLocationsList;
    @SerializedName("user")
    private UserData userData;

    public UserData getUserData() {
        return userData;
    }

    public List<CarsData> getCarsDataList() {
        return carsDataList;
    }

    public List<SavedLocations> getSavedLocationsList() {
        return savedLocationsList;
    }
}