package amrk.app.amrk_user.uber.history.models.issues;

import androidx.databinding.ObservableField;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.validation.Validate;

public class IssueLostsRequest {
    @SerializedName("description")
    private String description;
    @SerializedName("issue_id")
    private String issue_id;
    @SerializedName("lost_id")
    private String lost_id;
    @SerializedName("order_id")
    private String trip_id;
    @SerializedName("order_type")
    private int order_type;

    public transient ObservableField<String> descriptionError = new ObservableField<>();

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getIssue_id() {
        return issue_id;
    }

    public void setIssue_id(String issue_id) {
        this.issue_id = issue_id;
    }

    public String getLost_id() {
        return lost_id;
    }

    public int getOrder_type() {
        return order_type;
    }

    public void setOrder_type(int order_type) {
        this.order_type = order_type;
    }

    public void setLost_id(String lost_id) {
        this.lost_id = lost_id;
    }
    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(description, Constants.FIELD)) {
            descriptionError.set(Validate.error);
            valid = false;
        }
        return valid;
    }
}
