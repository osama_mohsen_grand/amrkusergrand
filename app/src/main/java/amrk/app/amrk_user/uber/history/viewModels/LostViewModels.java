package amrk.app.amrk_user.uber.history.viewModels;


import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.repository.HistoryRepository;
import amrk.app.amrk_user.uber.history.adapters.LostAdapter;
import amrk.app.amrk_user.uber.history.models.issues.IssueLostsRequest;
import amrk.app.amrk_user.uber.history.models.losts.LostMainData;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;


public class LostViewModels extends BaseViewModel {
    private LostAdapter lostAdapter;
    IssueLostsRequest issueLostsRequest;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    HistoryRepository repository;
    LostMainData lostMainData;

    @Inject
    public LostViewModels(HistoryRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        issueLostsRequest = new IssueLostsRequest();
    }

    public void getLost() {
        compositeDisposable.add(repository.getLost());
    }

    public void sendLost() {
        if (getLostAdapter().lastSelection != 0) {
            getIssueLostsRequest().setLost_id(String.valueOf(getLostAdapter().lastSelection));
            getIssueLostsRequest().setOrder_type(1);
            getIssueLostsRequest().setTrip_id(String.valueOf(getPassingObject().getId()));
            compositeDisposable.add(repository.sendLost(getIssueLostsRequest()));
        } else
            liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
    }

    @Bindable
    public LostMainData getLostMainData() {
        return this.lostMainData == null ? this.lostMainData = new LostMainData() : this.lostMainData;
    }

    @Bindable
    public void setLostMainData(LostMainData lostMainData) {
        getLostAdapter().updateData(lostMainData.getData());
        notifyChange(BR.lostAdapter);
        notifyChange(BR.lostMainData);
        this.lostMainData = lostMainData;
    }

    @Bindable
    public LostAdapter getLostAdapter() {
        return this.lostAdapter == null ? this.lostAdapter = new LostAdapter() : this.lostAdapter;
    }

    public IssueLostsRequest getIssueLostsRequest() {
        return issueLostsRequest;
    }

    public HistoryRepository getRepository() {
        return repository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
