package amrk.app.amrk_user.uber.home.viewModels;


import android.annotation.SuppressLint;
import android.content.Context;
import android.text.TextUtils;
import android.view.View;
import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.PromoData;
import amrk.app.amrk_user.repository.TripRepository;
import amrk.app.amrk_user.uber.home.adapters.CancelReasonsAdapter;
import amrk.app.amrk_user.uber.home.adapters.CarsAdapter;
import amrk.app.amrk_user.uber.home.adapters.SavedLocationAdapter;
import amrk.app.amrk_user.uber.home.models.CarsData;
import amrk.app.amrk_user.uber.home.models.NearestDrivers;
import amrk.app.amrk_user.uber.home.models.RateRequest;
import amrk.app.amrk_user.uber.home.models.StatusRequest;
import amrk.app.amrk_user.uber.home.models.TripDataFromNotifications;
import amrk.app.amrk_user.uber.home.models.TripRequest;
import amrk.app.amrk_user.uber.home.models.createTrip.UpdateLastLocationRequest;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;


public class HomeViewModels extends BaseViewModel {
    private CarsAdapter carsAdapter;
    private SavedLocationAdapter savedLocationAdapter;
    private CarsData carsData;
    PromoData promoData;
    private TripRequest tripRequest;
    public int dropVisibility;
    private TripDataFromNotifications tripDataFromNotifications;
    RateRequest rateRequest;
    private CancelReasonsAdapter reasonsAdapter;
    StatusRequest statusRequest;
    @Inject
    TripRepository repository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    public MutableLiveData<Mutable> liveData;
    private int paymentType = UserHelper.getInstance(MyApplication.getInstance()).getPaymentType();
    private int lessTimeDriverVisible = View.GONE;
    public LatLng end, mid;

    @Inject
    public HomeViewModels(TripRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        promoData = new PromoData();
        statusRequest = new StatusRequest();
        rateRequest = new RateRequest();
        tripDataFromNotifications = new TripDataFromNotifications();
        tripRequest = new TripRequest();
        carsData = new CarsData();
    }

    public void getCars() {
        compositeDisposable.add(repository.getCars());
    }

    public void deleteTrip() {
        setMessage(Constants.SHOW_PROGRESS);
        statusRequest.setTrip_id(UserHelper.getInstance(MyApplication.getInstance()).getTripId());
        compositeDisposable.add(repository.deleteTrip(getStatusRequest()));
        UserHelper.getInstance(MyApplication.getInstance()).addTripId(0);
    }

    public TripRepository getRepository() {
        return repository;
    }


    public RateRequest getRateRequest() {
        return rateRequest;
    }

    public void setRateRequest(RateRequest rateRequest) {
        this.rateRequest = rateRequest;
    }

    @Bindable
    public TripDataFromNotifications getTripDataFromNotifications() {
        return tripDataFromNotifications;
    }

    @Bindable
    public void setTripDataFromNotifications(TripDataFromNotifications tripDataFromNotifications) {
        notifyChange(BR.tripDataFromNotifications);
        this.tripDataFromNotifications = tripDataFromNotifications;
    }

    @Bindable
    public TripRequest getTripRequest() {
        return tripRequest;
    }

    @Bindable
    public void setTripRequest(TripRequest tripRequest) {
        getCarsAdapter().updateData(UserHelper.getInstance(MyApplication.getInstance()).getCarsData());
        getCarsAdapter().lastPosition = tripRequest.getCar_selected_position();
        getSavedLocationAdapter().updateData(UserHelper.getInstance(MyApplication.getInstance()).getSavedLocationsList());
        setCarsData(getCarsAdapter().carsData.get(tripRequest.getCar_selected_position()));
        notifyChange(BR.carsAdapter);
        notifyChange(BR.tripRequest);
        this.tripRequest = tripRequest;
    }

    public StatusRequest getStatusRequest() {
        return statusRequest;
    }

    @Bindable
    public CarsData getCarsData() {
        return carsData;
    }

    @Bindable
    public void setCarsData(CarsData carsData) {
        getTripRequest().setCar_level_id(carsData.getId());
        notifyChange(BR.carsData);
        this.carsData = carsData;
    }

    @Bindable
    public CarsAdapter getCarsAdapter() {
        return this.carsAdapter == null ? this.carsAdapter = new CarsAdapter() : this.carsAdapter;
    }

    @Bindable
    public SavedLocationAdapter getSavedLocationAdapter() {
        return this.savedLocationAdapter == null ? this.savedLocationAdapter = new SavedLocationAdapter() : this.savedLocationAdapter;
    }

    public void showCars() {
        liveData.setValue(new Mutable(Constants.SHOW_CARS_SHEET));
    }

    public void selectCar() {
        liveData.setValue(new Mutable(Constants.SELECT_CAR));
    }

    public void toPayment() {
        liveData.setValue(new Mutable(Constants.PAYMENT_METHOD));
    }

    public void scheduleSheet() {
        liveData.setValue(new Mutable(Constants.SCHEDULE));
    }

    public void selectSchedule() {
        liveData.setValue(new Mutable(Constants.SELECT_SCHEDULE));
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        getRateRequest().setRate(String.valueOf(rating));
    }

    @Bindable
    public int getDropVisibility() {
        return dropVisibility;
    }

    @Bindable
    public void setDropVisibility(int dropVisibility) {
        notifyChange(BR.dropVisibility);
        this.dropVisibility = dropVisibility;
    }

    public void toAddDropOff() {
        getTripRequest().setCar_level_id(getCarsData().getId());
        getTripRequest().setCar_selected_position(getCarsAdapter().lastPosition);
        if (getTripRequest().getDate() == null)
            getTripRequest().setType(1);
        List<String> addressList = new ArrayList<>();
        addressList.add(getTripRequest().getStart_address());
        getTripRequest().setAddressList(addressList);
        List<Double> latList = new ArrayList<>();
        latList.add(getTripRequest().getStart_lat());
        getTripRequest().setLat(latList);
        List<Double> lngList = new ArrayList<>();
        lngList.add(getTripRequest().getStart_lng());
        getTripRequest().setLng(lngList);
        if (!TextUtils.isEmpty(getTripRequest().getStart_address()))
            liveData.setValue(new Mutable(Constants.DROP_OFF));
    }

    public void toSearchPlace() {
        liveData.setValue(new Mutable(Constants.SEARCH_LOCATION));
    }

    public void toEditEndLocation() {
        liveData.setValue(new Mutable(Constants.EDIT_LOCATION));
    }

    public void firstLocationSelected() {
        if (getTripRequest().getAddressList() != null)
            setDropVisibility(1);
    }

    public void skipDestination() {
        if (getTripRequest().getAddressList() != null) {
            setDropVisibility(4);
            liveData.setValue(new Mutable(Constants.SKIP_LOCATION));
        }
    }

    public void toPromo() {
        liveData.setValue(new Mutable(Constants.PROMO_DIALOG));
    }

    public void calcTripPriceAfterPromo(int distance, int duration) {
        double distanceTripUnit = getCarsAdapter().carsData.get(getCarsAdapter().lastPosition).getDistanceTripUnit(), startTripUnit = getCarsAdapter().carsData.get(getCarsAdapter().lastPosition).getStartTripUnit(), waitingTripUnit = getCarsAdapter().carsData.get(getCarsAdapter().lastPosition).getWaiting_trip_unit();
        double tripPrice, mainTripPrice = ((distance * distanceTripUnit) + startTripUnit + (waitingTripUnit * duration));
        if (getPromoData() != null) {
            if (promoData.getType() == 0)
                tripPrice = mainTripPrice - getPromoData().getValue();
            else {
                tripPrice = mainTripPrice - ((getPromoData().getValue() / 100) * mainTripPrice);
            }
        } else
            tripPrice = mainTripPrice;
        if (tripPrice < 0)
            tripPrice = 0.0;
        getTripRequest().setTripTotal(String.valueOf(tripPrice));
        getCarsAdapter().carsData.get(getCarsAdapter().lastPosition).setShowText(String.valueOf(tripPrice).concat(getCountryCurrency()));
        setCarsData(getCarsAdapter().carsData.get(getCarsAdapter().lastPosition));
        getCarsAdapter().notifyDataSetChanged();
    }

    public void addSecondLocation() {
        setDropVisibility(2);
        liveData.setValue(new Mutable(Constants.FIRST_LOCATION_SELECTED));
    }

    public void createTrip() {
        getTripRequest().setPayment(getPaymentType());
        setDropVisibility(3);
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(repository.createTrip(getTripRequest()));
    }

    public void confirmLocationForTrip() {
        liveData.setValue(new Mutable(Constants.CONFIRM_LOCATIONS));
    }

    public void getLessDriverTime(List<NearestDrivers> nearestDriversList, String start) {
        String destination = "";
        for (NearestDrivers nearestDrivers : nearestDriversList) {
            String replacedDestination = String.valueOf(nearestDrivers.getLatLng().latitude).concat(",").concat(String.valueOf(nearestDrivers.getLatLng().longitude));
            if (TextUtils.isEmpty(destination))
                destination = replacedDestination;
            else
                destination = destination.concat("%7C").concat(replacedDestination);
        }
        compositeDisposable.add(repository.getLessTime(destination, start));
    }

    public void toBack(Context context) {
        if (getTripRequest().getAddressList() != null && getTripRequest().getAddressList().size() == 2) {
            getTripRequest().getAddressList().remove(1);
            getTripRequest().getLat().remove(1);
            getTripRequest().getLng().remove(1);
            setDropVisibility(1);
            liveData.setValue(new Mutable(Constants.SECOND_LOCATION_BACK));
        } else {
            goBack(context);
        }
    }

    public void cancelTripReasons() {
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(repository.cancelReasons());
    }

    public void cancelTrip() {
        statusRequest.setCancel_id(getReasonsAdapter().lastSelection); //canceled
        cancelTripAction();
    }

    public void checkTimeForCancel() {
        if (findDifference(getTripDataFromNotifications().getCreatedAtTime()) < 3) {
            cancelTripReasons();
        } else {
            liveData.setValue(new Mutable(Constants.CANCEL_TRIP_WARNING_DIALOG));
        }
    }

    public void cancelTripAction() {
        setMessage(Constants.SHOW_PROGRESS);
        statusRequest.setTrip_id(UserHelper.getInstance(MyApplication.getInstance()).getTripId());
        compositeDisposable.add(repository.cancelTrip(statusRequest));
    }

    @Bindable
    public CancelReasonsAdapter getReasonsAdapter() {
        return this.reasonsAdapter == null ? this.reasonsAdapter = new CancelReasonsAdapter() : this.reasonsAdapter;
    }

    public void rateTrip() {
        getRateRequest().setTrip_id(getTripDataFromNotifications().getId());
        if (getRateRequest().isRateValid()) {
            compositeDisposable.add(repository.rateTrip(getRateRequest()));
        } else {
            liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
        }
    }

    public void getLastTrip() {
        statusRequest.setTrip_id(UserHelper.getInstance(MyApplication.getInstance()).getTripId());
        compositeDisposable.add(repository.getLastTrip(statusRequest));

    }

    public void updateLastLocation(String address, double currentLat, double currentLng, double lat, double lng) {
        compositeDisposable.add(repository.updateLastLocation(new UpdateLastLocationRequest(UserHelper.getInstance(MyApplication.getInstance()).getTripId(), getTripDataFromNotifications().getTripPaths().get(getTripDataFromNotifications().getTripPaths().size() - 1).getId(), address, lat, lng, currentLat, currentLng)));
    }

    @Bindable
    public int getPaymentType() {
        return paymentType;
    }

    @Bindable
    public void setPaymentType(int paymentType) {
        notifyChange(BR.paymentType);
        this.paymentType = paymentType;
    }

    @Bindable
    public PromoData getPromoData() {
        return promoData;
    }

    @Bindable
    public void setPromoData(PromoData promoData) {
        getTripRequest().setPromo_id(promoData.getId());
        notifyChange(BR.promoData);
        this.promoData = promoData;
    }

    @Bindable
    public int getLessTimeDriverVisible() {
        return lessTimeDriverVisible;
    }

    @Bindable
    public void setLessTimeDriverVisible(int lessTimeDriverVisible) {
        notifyChange(BR.lessTimeDriverVisible);
        this.lessTimeDriverVisible = lessTimeDriverVisible;
    }

    public void toChat() {
        liveData.setValue(new Mutable(Constants.CHAT));
    }

    public void toCall() {
        liveData.setValue(new Mutable(Constants.CALL));
    }

    public void hideCallDialog() {
        liveData.setValue(new Mutable(Constants.HIDE_CALL_DIALOG));
    }

    public void callClient() {
        liveData.setValue(new Mutable(Constants.CALL_DRIVER));
    }

    @SuppressLint("SimpleDateFormat")
    public long findDifference(String end_date) {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd HH:mm");
        try {
            Date start_date = new Date();
            Date d1 = sdf.parse(sdf.format(start_date));
            Date d2 = sdf.parse(end_date);
            long difference_In_Time = Objects.requireNonNull(d1).getTime() - Objects.requireNonNull(d2).getTime();
            long difference_In_Minutes
                    = (difference_In_Time
                    / (1000 * 60))
                    % 60;
            return difference_In_Minutes;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return 0;
    }
}
