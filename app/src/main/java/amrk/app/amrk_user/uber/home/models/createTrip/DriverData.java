package amrk.app.amrk_user.uber.home.models.createTrip;

import com.google.gson.annotations.SerializedName;

public class DriverData {
    @SerializedName("image")
    private String image;
    @SerializedName("trip_status")
    private int trip_status;

    @SerializedName("car_model")
    private String carModel;
    @SerializedName("trip_msg")
    private String trip_msg;

    @SerializedName("lng")
    private String lng;

    @SerializedName("car_level")
    private int carLevel;

    @SerializedName("phone")
    private String phone;

    @SerializedName("name")
    private String name;

    @SerializedName("car_num")
    private String carNum;

    @SerializedName("car_color")
    private String carColor;

    @SerializedName("id")
    private int id;

    @SerializedName("lat")
    private String lat;
    @SerializedName("rate")
    private String rate;

    public String getTrip_msg() {
        return trip_msg;
    }

    public void setTrip_msg(String trip_msg) {
        this.trip_msg = trip_msg;
    }

    public int getTrip_status() {
        return trip_status;
    }

    public void setTrip_status(int trip_status) {
        this.trip_status = trip_status;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getImage() {
        return image;
    }

    public void setCarModel(String carModel) {
        this.carModel = carModel;
    }

    public String getCarModel() {
        return carModel;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getLng() {
        return lng;
    }

    public void setCarLevel(int carLevel) {
        this.carLevel = carLevel;
    }

    public int getCarLevel() {
        return carLevel;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPhone() {
        return phone;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setCarNum(String carNum) {
        this.carNum = carNum;
    }

    public String getCarNum() {
        return carNum;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return id;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLat() {
        return lat;
    }
}