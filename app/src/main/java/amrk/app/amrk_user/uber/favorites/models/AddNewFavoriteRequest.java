package amrk.app.amrk_user.uber.favorites.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class AddNewFavoriteRequest {
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;
    @SerializedName("address")
    private String address;
    @SerializedName("name")
    private String name;
    @SerializedName("location_id")
    private int itemId;

    public boolean isValid() {
        return (!TextUtils.isEmpty(name));
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public int getItemId() {
        return itemId;
    }

    public void setItemId(int itemId) {
        this.itemId = itemId;
    }
}
