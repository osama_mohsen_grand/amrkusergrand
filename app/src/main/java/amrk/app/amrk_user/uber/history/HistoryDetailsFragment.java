package amrk.app.amrk_user.uber.history;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.customViews.CustomDrawable;
import amrk.app.amrk_user.databinding.FragmentHistoryDetailsBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.uber.history.models.TripDetailsResponse;
import amrk.app.amrk_user.uber.history.viewModels.HistoryDetailsViewModels;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.locations.MapConfig;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class HistoryDetailsFragment extends BaseFragment implements OnMapReadyCallback, RoutingListener {
    FragmentHistoryDetailsBinding binding;
    @Inject
    HistoryDetailsViewModels historyViewModels;
    private Context context;
    GoogleMap mMap;
    List<Polyline> polyline = null;
    Polyline polylineOld = null;
    LatLng start, end = null;

    public HistoryDetailsFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history_details, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setHistoryViewModel(historyViewModels);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            historyViewModels.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
        }
        init(savedInstanceState);
        liveDataListeners();
        return binding.getRoot();
    }


    private void liveDataListeners() {
        historyViewModels.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            historyViewModels.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (mutable.message) {
                case Constants.TRIP_DETAILS:
                    historyViewModels.setHistoryData(((TripDetailsResponse) mutable.object).getHistoryData());
                    int color = Color.parseColor(historyViewModels.getHistoryData().getCarInfo() != null && historyViewModels.getHistoryData().getCarInfo().getCar_color() != null
                            ? historyViewModels.getHistoryData().getCarInfo().getCar_color() : "#F2EFF1");
                    CustomDrawable customDrawable = new CustomDrawable(color, color, color, 4, getResources().getColor(R.color.colorPrimaryDark, null), 0);
                    binding.carColor.setBackground(customDrawable);
                    findRoutes();
                    break;
                case Constants.LOST_ITEM:
                    MovementHelper.startActivityWithBundle(context, new PassingObject(historyViewModels.getHistoryData()), getString(R.string.lostText), LostItemFragment.class.getName(), null, Constants.UBER);
                    break;
                case Constants.ISSUES:
                    MovementHelper.startActivityWithBundle(context, new PassingObject(historyViewModels.getHistoryData()), getString(R.string.issueText), ReportIssueFragment.class.getName(), null, Constants.UBER);
                    break;
                case Constants.EMPTY_DRIVER:
                    ((ParentActivity) context).toastError(getString(R.string.empty_driver));
                    break;
                case Constants.DELTE_TRIP:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    MovementHelper.finishWithResult(new PassingObject(), context);
                    break;

            }
        });
    }

    // function to find Routes.
    private void findRoutes() {
        // new Trip
        start = new LatLng(Double.parseDouble(historyViewModels.getHistoryData().getTripPath().get(0).getLat()), Double.parseDouble(historyViewModels.getHistoryData().getTripPath().get(0).getLng()));
        end = new LatLng(Double.parseDouble(historyViewModels.getHistoryData().getTripPath().get(historyViewModels.getHistoryData().getTripPath().size() - 1).getLat()), Double.parseDouble(historyViewModels.getHistoryData().getTripPath().get(historyViewModels.getHistoryData().getTripPath().size() - 1).getLng()));
        if (start == null || end == null) {
            Toast.makeText(getActivity(), "Unable to get location", Toast.LENGTH_LONG).show();
        } else {
            Routing routing;
            routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(start, end)
                    .key(getString(R.string.google_map))  //also define your api key here.
                    .build();
            routing.execute();
        }
    }

    //Routing call back functions.
    @Override
    public void onRoutingFailure(RouteException e) {
//        showMessage(getResources().getString(R.string.faild_route), 1, 1);
        findRoutes();
    }

    @Override
    public void onRoutingStart() {
//        showMessage(getResources().getString(R.string.finding_route), 1, 0);

    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        CameraUpdate center = CameraUpdateFactory.newLatLng(new LatLng(Double.parseDouble(historyViewModels.getHistoryData().getTripPath().get(0).getLat()), Double.parseDouble(historyViewModels.getHistoryData().getTripPath().get(0).getLng())));
        if (polyline != null) {
            polyline.clear();
        }
        mMap.moveCamera(center);
        PolylineOptions polyOptions = new PolylineOptions();
        polyline = new ArrayList<>();
        //add route(s) to the map using polyline
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                polyOptions.color(getResources().getColor(R.color.black, null));
                polyOptions.width(7);
                polyOptions.addAll(route.get(i).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                if (polylineOld != null) {
                    polylineOld.remove();
                } else
                    polylineOld = polyline;
                int k = polyline.getPoints().size();
                this.polyline.add(polyline);
            }
        }
        addUserMarker(start);
        addUserMarker(end);
        MapConfig mapConfig = new MapConfig(context, mMap);
        ArrayList<LatLng> latLngs = new ArrayList<>();
        latLngs.add(start);
        latLngs.add(end);
        mapConfig.moveCamera(latLngs);
    }

    @Override
    public void onRoutingCancelled() {
        findRoutes();
    }

    public void addUserMarker(LatLng position) {
        MarkerOptions markerOptionsFirst = new MarkerOptions();
        markerOptionsFirst.draggable(false);
        markerOptionsFirst.position(position);
        markerOptionsFirst.anchor(0.5f, 0.5f);
        markerOptionsFirst.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_first_location), 50, 50)));
        mMap.addMarker(markerOptionsFirst);
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        historyViewModels.getTripHistory();
    }

    @Override
    public void onResume() {
        binding.tripMap.onResume();
        super.onResume();
        historyViewModels.getRepository().setLiveData(historyViewModels.liveData);
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.tripMap.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.tripMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.tripMap.onLowMemory();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        binding.tripMap.getMapAsync(this);
        binding.tripMap.onResume();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void init(Bundle savedInstanceState) {
        binding.tripMap.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AppCompatActivity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        } else {
            binding.tripMap.getMapAsync(this);
        }
    }

}
