package amrk.app.amrk_user.uber.history;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentReportIssueBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.uber.history.models.issues.IssuesResponse;
import amrk.app.amrk_user.uber.history.viewModels.IssueViewModels;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;


public class ReportIssueFragment extends BaseFragment {
    FragmentReportIssueBinding reportIssueBinding;
    @Inject
    IssueViewModels issueViewModels;
    private Context context;


    public ReportIssueFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        reportIssueBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_report_issue, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        reportIssueBinding.setIssueViewModel(issueViewModels);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            issueViewModels.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            issueViewModels.getIssues();
        }
        liveDataListeners();
        return reportIssueBinding.getRoot();
    }

    private void liveDataListeners() {
        issueViewModels.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.ISSUES.equals(mutable.message)) {
                issueViewModels.getIssuesAdapter().updateData((((IssuesResponse) mutable.object).getData()));
                issueViewModels.notifyChange(BR.issuesAdapter);
            }
        });
        issueViewModels.getIssuesAdapter().getAdapterLiveData().observe((LifecycleOwner) context, integer -> MovementHelper.startActivityWithBundle(context, new PassingObject(String.valueOf(integer), issueViewModels.getPassingObject().getObjectClass()), getString(R.string.menuHistroy), SendIssueFragment.class.getName(), null, Constants.UBER));
    }

    @Override
    public void onResume() {
        super.onResume();
        issueViewModels.getRepository().setLiveData(issueViewModels.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
