package amrk.app.amrk_user.uber.history;


import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentHistoryBinding;
import amrk.app.amrk_user.uber.history.models.HistoryResponse;
import amrk.app.amrk_user.uber.history.viewModels.HistoryViewModels;
import amrk.app.amrk_user.utils.Constants;


public class HistoryFragment extends BaseFragment {
    FragmentHistoryBinding binding;
    @Inject
    HistoryViewModels viewModel;

    public HistoryFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_history, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setHistoryViewModel(viewModel);
        viewModel.history(1, true);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), o -> {
            handleActions(o);
            if (Constants.TRIP_HISTORY.equals(o.message)) {
                viewModel.setHistoryMain(((HistoryResponse) o.object).getHistoryMain());
            } else if (Constants.TRIP_TYPE_TITLE.equals(o.message)) {
                baseActivity().backActionBarView.setTitle(viewModel.getType() == 0 ? getString(R.string.lastRides) : getString(R.string.schedule));
            }
        });
        binding.rcTrips.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (viewModel.getSearchProgressVisible() == View.GONE && !TextUtils.isEmpty(viewModel.getHistoryMain().getNextPageUrl())) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == viewModel.getHistoryAdapter().historyDataList.size() - 1) {
                        viewModel.setSearchProgressVisible(View.VISIBLE);
                        viewModel.history((viewModel.getHistoryMain().getCurrentPage() + 1), false);
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null)
            viewModel.history(1, true);
        super.onActivityResult(requestCode, resultCode, data);
    }
}
