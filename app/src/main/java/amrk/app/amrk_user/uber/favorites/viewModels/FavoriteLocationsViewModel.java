package amrk.app.amrk_user.uber.favorites.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.repository.SettingsRepository;
import amrk.app.amrk_user.uber.favorites.adapters.FavoritesLocationsAdapter;
import amrk.app.amrk_user.uber.favorites.models.AddNewFavoriteRequest;
import amrk.app.amrk_user.uber.favorites.models.DeleteLocationRequest;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class FavoriteLocationsViewModel extends BaseViewModel {
    FavoritesLocationsAdapter favoritesLocationsAdapter;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository settingsRepository;
    AddNewFavoriteRequest addNewFavoriteRequest;

    @Inject
    public FavoriteLocationsViewModel(SettingsRepository settingsRepository) {
        addNewFavoriteRequest = new AddNewFavoriteRequest();
        this.liveData = new MutableLiveData<>();
        this.settingsRepository = settingsRepository;
        this.liveData = new MutableLiveData<>();
        settingsRepository.setLiveData(liveData);
    }

    public void getSavedLocations() {
        compositeDisposable.add(settingsRepository.getFavoriteLocations());
    }

    public void sendLocation() {
        if (getAddNewFavoriteRequest().isValid())
            compositeDisposable.add(settingsRepository.sendLocation(getAddNewFavoriteRequest()));
    }

    public void deleteLocation(DeleteLocationRequest locationRequest) {
        compositeDisposable.add(settingsRepository.deleteLocation(locationRequest));
    }

    public void toSearchLocation() {
        liveData.setValue(new Mutable(Constants.FAVORITES_LOCATIONS));
    }

    public AddNewFavoriteRequest getAddNewFavoriteRequest() {
        return addNewFavoriteRequest;
    }

    public void setAddNewFavoriteRequest(AddNewFavoriteRequest addNewFavoriteRequest) {
        this.addNewFavoriteRequest = addNewFavoriteRequest;
    }

    @Bindable
    public FavoritesLocationsAdapter getFavoritesLocationsAdapter() {
        return this.favoritesLocationsAdapter == null ? this.favoritesLocationsAdapter = new FavoritesLocationsAdapter() : this.favoritesLocationsAdapter;
    }

    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }


    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
