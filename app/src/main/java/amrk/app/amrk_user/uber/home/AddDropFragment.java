package amrk.app.amrk_user.uber.home;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.tasks.Task;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.maps.MapHelper;
import amrk.app.amrk_user.databinding.FragmentAddDropBinding;
import amrk.app.amrk_user.databinding.PaymentBottomSheetBinding;
import amrk.app.amrk_user.databinding.SelectCarBottomSheetBinding;
import amrk.app.amrk_user.databinding.UberPromoDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.CheckPromoResponse;
import amrk.app.amrk_user.uber.favorites.SearchForLocationsFragment;
import amrk.app.amrk_user.uber.home.models.CarsResponse;
import amrk.app.amrk_user.uber.home.models.TripRequest;
import amrk.app.amrk_user.uber.home.models.createTrip.CreateTripResponse;
import amrk.app.amrk_user.uber.home.viewModels.HomeViewModels;
import amrk.app.amrk_user.uber.paymentMethods.PaymentMethodViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.locations.MapAddress;
import amrk.app.amrk_user.utils.locations.MapAddressInterface;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.UserHelper;

import static android.app.Activity.RESULT_OK;


public class AddDropFragment extends BaseFragment implements OnMapReadyCallback, RoutingListener {
    private FragmentAddDropBinding addDropBinding;
    @Inject
    HomeViewModels homeViewModels;
    @Inject
    PaymentMethodViewModel paymentMethodViewModel;
    private GoogleMap mMap;
    BottomSheetDialog carsSheet, paymentSheet;
    Dialog promoDialog;
    private double currentLat, currentLng;
    private int whichSelected = 0;
    private Marker secondSelectedMarker = null, thirdSelectedMarker = null, promoSelectedMarker = null;
    private int distance, duration;
    Context context;
    MapHelper mapHelper;
    String currentAddress;
    private int isFirstMapCameraMoved = 0;

    public AddDropFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        addDropBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_add_drop, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        addDropBinding.setHomeViewModel(homeViewModels);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            homeViewModels.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            homeViewModels.setTripRequest(new Gson().fromJson(String.valueOf(homeViewModels.getPassingObject().getObjectClass()), TripRequest.class));
        }
        enableLocationDialog();
        init(savedInstanceState);
        liveDataListeners();
        onBackPressed();
        return addDropBinding.getRoot();
    }

    private void onBackPressed() {
        addDropBinding.getRoot().setFocusableInTouchMode(true);
        addDropBinding.getRoot().requestFocus();
        addDropBinding.getRoot().setOnKeyListener((v, keyCode, event) -> {
            //This is the filter
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                homeViewModels.toBack(getActivity());
                return true;
            }
            return false;
        });

    }


    private void liveDataListeners() {
        homeViewModels.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            closeKeyboard();
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            homeViewModels.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (Constants.SEARCH_LOCATION.equals(((Mutable) o).message)) {
                mapHelper.searchGoogelPlaces();
            } else if (Constants.SHOW_CARS_SHEET.equals(((Mutable) o).message)) {
                SelectCarBottomSheetBinding sortBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.select_car_bottom_sheet, null, false);
                carsSheet = new BottomSheetDialog(context);
                carsSheet.setContentView(sortBinding.getRoot());
                sortBinding.setSelectCarBottomSheetViewModels(homeViewModels);
                carsSheet.show();
            } else if (Constants.PAYMENT_METHOD.equals(((Mutable) o).message)) {
                PaymentBottomSheetBinding sheetBinding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.payment_bottom_sheet, null, false);
                paymentSheet = new BottomSheetDialog(context);
                paymentSheet.setContentView(sheetBinding.getRoot());
                sheetBinding.setViewmodel(paymentMethodViewModel);
                paymentSheet.setOnCancelListener(dialog -> {
                    homeViewModels.setPaymentType(UserHelper.getInstance(context).getPaymentType());
                    paymentSheet.dismiss();
                });
                paymentSheet.show();
            } else if (Constants.SELECT_CAR.equals(mutable.message)) {
                homeViewModels.setCarsData(homeViewModels.getCarsAdapter().carsData.get(homeViewModels.getCarsAdapter().lastPosition));
                findRoutes();
                carsSheet.dismiss();
            } else if (Constants.FIRST_LOCATION_SELECTED.equals(mutable.message)) {
                homeViewModels.getTripRequest().getAddressList().add(currentAddress);
                homeViewModels.getTripRequest().getLat().add(currentLat);
                homeViewModels.getTripRequest().getLng().add(currentLng);
                whichSelected = 1;
            } else if (Constants.SKIP_LOCATION.equals(mutable.message)) {
                addDropBinding.secondSelectedLocation.setText("");
                addDropBinding.addLocation.setVisibility(View.GONE);
                secondSelectedMarker.remove();
            } else if (Constants.SECOND_LOCATION_BACK.equals(mutable.message)) {
                if (thirdSelectedMarker != null) {
                    thirdSelectedMarker.remove();
                    thirdSelectedMarker = null;
                }
                whichSelected = 0;
            } else if (Constants.PROMO_DIALOG.equals(mutable.message)) {
                showPromoDialog();
            } else if (Constants.CONFIRM_LOCATIONS.equals(mutable.message)) {
                if (homeViewModels.getDropVisibility() != 4) { // if user select destination
                    if (!TextUtils.isEmpty(addDropBinding.secondSelectedLocation.getText())) {
                        if (whichSelected == 1) {
                            homeViewModels.getTripRequest().getAddressList().add(currentAddress);
                            homeViewModels.getTripRequest().getLat().add(currentLat);
                            homeViewModels.getTripRequest().getLng().add(currentLng);
                        } else if (whichSelected == 0 && homeViewModels.getTripRequest().getLng().size() == 1) {
                            homeViewModels.getTripRequest().getAddressList().add(currentAddress);
                            homeViewModels.getTripRequest().getLat().add(currentLat);
                            homeViewModels.getTripRequest().getLng().add(currentLng);
                        }
                    }  // if user lead captain
                }
                homeViewModels.createTrip();
            } else if (Constants.CREATE_TRIP.equals(mutable.message)) {
                toastMessage(((CreateTripResponse) mutable.object).mMessage);
                Intent intent = new Intent();
                intent.putExtra(Constants.BUNDLE, Constants.TRIP_CREATED);
                ((Activity) context).setResult(RESULT_OK, intent);
                ((Activity) context).finish();
                UserHelper.getInstance(context).addTripId(((CreateTripResponse) mutable.object).getData().getId());
            } else if (mutable.message.equals(Constants.CLOSE_PAYMENT_SHEET)) {
                homeViewModels.setPaymentType(UserHelper.getInstance(context).getPaymentType());
                paymentSheet.dismiss();
            } else if (mutable.message.equals(Constants.CHECK_PROMO)) {
                toastMessage(((CheckPromoResponse) mutable.object).mMessage);
                homeViewModels.setPromoData(((CheckPromoResponse) mutable.object).getPromoData());
                homeViewModels.calcTripPriceAfterPromo(distance, duration);
                promoDialog.dismiss();
            } else if (Constants.GET_CARS.equals(mutable.message)) {
                UserHelper.getInstance(context).savedLocations(((CarsResponse) mutable.object).getData().getSavedLocationsList());
                homeViewModels.getSavedLocationAdapter().updateData(UserHelper.getInstance(context).getSavedLocationsList());
                homeViewModels.notifyChange(BR.savedLocationAdapter);
            }
        });
        //saved locations
        homeViewModels.getSavedLocationAdapter().getLocationAdapterMutableLiveData().observe((LifecycleOwner) context, savedLocations -> {
            if (!TextUtils.isEmpty(savedLocations.getStart_address())) {
                //isFirstMapCameraMoved = 1;
                addMarker(new LatLng(savedLocations.getStart_lat(), savedLocations.getStart_lng()));
                MovementHelper.mpaZoomCamera(new LatLng(savedLocations.getStart_lat(), savedLocations.getStart_lng()), mMap);
                currentAddress = savedLocations.getStart_address();
                currentLat = savedLocations.getStart_lat();
                currentLng = savedLocations.getStart_lng();
            } else {
                toastErrorMessage(getString(R.string.empty_saved_location));
                MovementHelper.startActivityForResultWithBundleRequest(context, new PassingObject(savedLocations.getId(), savedLocations.getName()), getString(R.string.edit_location).concat(" ").concat(savedLocations.getName()), SearchForLocationsFragment.class.getName(), null, null, Constants.FAVORITES_LOCATIONS_CODE);
            }
        });
    }

    private void showPromoDialog() {
        promoDialog = new Dialog(context, R.style.PauseDialog);
        promoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(promoDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        promoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        UberPromoDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(promoDialog.getContext()), R.layout.uber_promo_dialog, null, false);
        promoDialog.setContentView(binding.getRoot());
        binding.setViewModel(paymentMethodViewModel);
        promoDialog.show();
    }


    private static final String TAG = "AddDropFragment";

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (requestCode == Constants.AUTOCOMPLETE_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);
                    if (place.getLatLng() != null) {
                        addMarker(place.getLatLng());
                        MovementHelper.mpaZoomCamera(place.getLatLng(), mMap);
                        currentLat = place.getLatLng().latitude;
                        currentLng = place.getLatLng().longitude;
                        currentAddress = place.getAddress();
//                        isFirstMapCameraMoved = 1;
                    }
                } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    Status status = Autocomplete.getStatusFromIntent(data);
                    Log.e(TAG, status.getStatusMessage());
                }
                return;
            } else if (requestCode == Constants.FAVORITES_LOCATIONS_CODE) {
                homeViewModels.getCars();
            }
        }
        closeKeyboard();
        super.onActivityResult(requestCode, resultCode, data);
    }

    boolean fetchLocation = false;

    private void setChangeCameraListener() {
        mMap.setOnMyLocationChangeListener(location -> {
            if (!fetchLocation) {
                MovementHelper.mpaZoomCamera(new LatLng(location.getLatitude(), location.getLongitude()), mMap);
                mMap.setOnMyLocationChangeListener(null);
                fetchLocation = true;
            }
        });

        mMap.setOnCameraMoveStartedListener((int i) -> {
//            if (i != 3)
//                isFirstMapCameraMoved = i;
        });
        mMap.setOnCameraIdleListener(() -> {
            addMarker(mMap.getCameraPosition().target);
//            if (isFirstMapCameraMoved != 3 && isFirstMapCameraMoved != 0) {
            currentLat = mMap.getCameraPosition().target.latitude;
            currentLng = mMap.getCameraPosition().target.longitude;
            getAddress(currentLat, currentLng, (address, city) -> {
                if (whichSelected == 0) {
                    addDropBinding.secondSelectedLocation.setText(address);
                } else {
                    addDropBinding.thirdSelectedLocation.setText(address);
                }
                currentAddress = address;
                findRoutes();
            });
//            }
        });

    }

    protected void addMarker(LatLng position) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.draggable(false);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.location_search), 100, 100)));
        markerOptions.position(position);
        if (whichSelected == 0) {
            if (secondSelectedMarker != null)
                secondSelectedMarker.remove();
            secondSelectedMarker = mMap.addMarker(markerOptions);
            addDropBinding.addLocation.setVisibility(View.VISIBLE);
        } else if (whichSelected == 1) {
            if (thirdSelectedMarker != null)
                thirdSelectedMarker.remove();
            thirdSelectedMarker = mMap.addMarker(markerOptions);
            addDropBinding.addLocation.setVisibility(View.GONE);
        } else if (whichSelected == 2) {
            if (promoSelectedMarker != null)
                promoSelectedMarker.remove();
            promoSelectedMarker = mMap.addMarker(markerOptions);
        }

    }

    private void getAddress(final double lat, final double lng, MapAddressInterface mapAddressInterface) {
        MapAddress mapAddress = new MapAddress(getActivity(), lat, lng);
        mapAddress.getAddressFromUrl((address, city) -> {
            if (whichSelected == 0)
                addDropBinding.secondSelectedLocation.setText(address);
            else if (whichSelected == 1)
                addDropBinding.thirdSelectedLocation.setText(address);
            if (mapAddressInterface != null)
                mapAddressInterface.fetchFullAddress(address, city);
        });

    }

    private void init(Bundle savedInstanceState) {
        addDropBinding.mapview.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AppCompatActivity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        } else {
            addDropBinding.mapview.getMapAsync(this);
        }

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        addDropBinding.mapview.getMapAsync(this);
        addDropBinding.mapview.onResume();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onMapReady(@NotNull GoogleMap googleMap) {
        mMap = googleMap;
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mMap.setMyLocationEnabled(true);
        mMap.getUiSettings().setCompassEnabled(false);
        mapHelper = new MapHelper(mMap, context, homeViewModels);
        mapHelper.addUserMarker(new LatLng(homeViewModels.getTripRequest().getStart_lat(), homeViewModels.getTripRequest().getStart_lng()));
        setChangeCameraListener();
    }

    @Override
    public void onResume() {
        addDropBinding.mapview.onResume();
        super.onResume();
        homeViewModels.getRepository().setLiveData(homeViewModels.liveData);
        paymentMethodViewModel.getRepository().setLiveData(homeViewModels.liveData);
    }


    @Override
    public void onPause() {
        super.onPause();
        addDropBinding.mapview.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        addDropBinding.mapview.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        addDropBinding.mapview.onLowMemory();
    }


    private void enableLocationDialog() {
        LocationRequest locationRequest = getLocationRequest();
        LocationSettingsRequest settingsRequest = new LocationSettingsRequest.Builder()
                .addLocationRequest(locationRequest).build();
        SettingsClient client = LocationServices.getSettingsClient(context);
        Task<LocationSettingsResponse> task = client
                .checkLocationSettings(settingsRequest);

        task.addOnFailureListener((AppCompatActivity) context, e -> {
            int statusCode = ((ApiException) e).getStatusCode();
            if (statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                try {
                    ResolvableApiException resolvable =
                            (ResolvableApiException) e;
                    resolvable.startResolutionForResult
                            ((AppCompatActivity) context,
                                    1019);
                } catch (IntentSender.SendIntentException sendEx) {
                    sendEx.printStackTrace();
                }
            }
        });
    }

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    // function to find Routes.
    private void findRoutes() {
        LatLng end, mid = null;
        LatLng start = new LatLng(homeViewModels.getTripRequest().getStart_lat(), homeViewModels.getTripRequest().getStart_lng());
        if (whichSelected == 0)
            end = new LatLng(currentLat, currentLng);
        else {
            mid = new LatLng(homeViewModels.getTripRequest().getLat().get(1), homeViewModels.getTripRequest().getLng().get(1));
            end = new LatLng(currentLat, currentLng);
        }
        Routing routing;//also define your api key here.
        if (mid == null) {
            routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(start, end)
                    .key(getResources().getString(R.string.google_map))  //also define your api key here.
                    .build();
        } else {
            routing = new Routing.Builder()
                    .travelMode(AbstractRouting.TravelMode.DRIVING)
                    .withListener(this)
                    .alternativeRoutes(true)
                    .waypoints(start, mid, end)
                    .key(getResources().getString(R.string.google_map))  //also define your api key here.
                    .build();
        }
        routing.execute();
    }

    @Override
    public void onRoutingFailure(RouteException e) {
        findRoutes();
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                distance = route.get(i).getDistanceValue() / 1000;
                duration = route.get(i).getDurationValue() / 60;
            }
        }
        homeViewModels.calcTripPriceAfterPromo(distance, duration);
    }

    @Override
    public void onRoutingCancelled() {
        findRoutes();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


}
