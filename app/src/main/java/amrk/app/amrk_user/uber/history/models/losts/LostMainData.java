package amrk.app.amrk_user.uber.history.models.losts;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class LostMainData {
    @SerializedName("losts")
    private List<LostData> data;
    @SerializedName("lost_something_text")
    private String lostSomethingText;

    public List<LostData> getData() {
        return data;
    }

    public String getLostSomethingText() {
        return lostSomethingText;
    }
}
