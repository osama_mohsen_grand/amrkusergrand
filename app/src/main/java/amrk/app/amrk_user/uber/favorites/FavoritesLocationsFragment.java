package amrk.app.amrk_user.uber.favorites;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentFavoritesLocationsBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.newOrder.models.SavedLocationsData;
import amrk.app.amrk_user.pages.newOrder.models.SavedLocationsResponse;
import amrk.app.amrk_user.uber.favorites.models.DeleteLocationRequest;
import amrk.app.amrk_user.uber.favorites.viewModels.FavoriteLocationsViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;


public class FavoritesLocationsFragment extends BaseFragment {
    FragmentFavoritesLocationsBinding binding;
    @Inject
    FavoriteLocationsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_favorites_locations, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        viewModel.getSavedLocations();
        setEvent();
        return binding.getRoot();
    }


    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.FAVORITES_LOCATIONS.equals(((Mutable) o).message)) {
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(), getString(R.string.new_location), SearchForLocationsFragment.class.getName(), null, null);
            } else if (Constants.SAVED_LOCATIONS.equals(((Mutable) o).message)) {
                viewModel.getFavoritesLocationsAdapter().update(((SavedLocationsResponse) ((Mutable) o).object).getSavedLocationsData());
            } else if (Constants.DELETE_LOCATION.equals(((Mutable) o).message)) {
                Constants.DATA_CHANGED = true;
                viewModel.getSavedLocations();
            }
        });

        viewModel.getFavoritesLocationsAdapter().getSavedLiveData().observe(requireActivity(), savedLocationsData -> {
            if (savedLocationsData.getDeleteKey().equals(Constants.REMOVE_DIALOG_WARNING)) {
                showAlertDialogWithAutoDismiss(savedLocationsData);
            } else if (savedLocationsData.getName().equals(getString(R.string.favorite_home)) || savedLocationsData.getName().equals(getString(R.string.favorite_job))) {
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(savedLocationsData.getId(), savedLocationsData.getName()), getString(R.string.edit_location).concat(" ").concat(savedLocationsData.getName()), SearchForLocationsFragment.class.getName(), null, null);
            }
        });
    }

    public void showAlertDialogWithAutoDismiss(SavedLocationsData savedLocations) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder
                .setMessage(getString(R.string.delete_location_warning))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel())
                .setPositiveButton(getString(R.string.send), (dialog, id) -> {
                    Constants.DATA_CHANGED = true;
                    viewModel.deleteLocation(new DeleteLocationRequest(savedLocations.isEmpty(), savedLocations.getId()));
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        baseActivity().enableRefresh(false);
        viewModel.getSettingsRepository().setLiveData(viewModel.liveData);
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_CODE) {
            viewModel.getSavedLocations();
        }

    }

}
