package amrk.app.amrk_user.uber.favorites.viewModels;

import android.text.TextUtils;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.newOrder.models.SavedLocationsData;
import amrk.app.amrk_user.utils.Constants;

public class ItemFavoritesLocationsViewModel extends BaseViewModel {
    public SavedLocationsData savedLocations;

    public ItemFavoritesLocationsViewModel(SavedLocationsData savedLocations) {
        this.savedLocations = savedLocations;
    }

    @Bindable
    public SavedLocationsData getSavedLocations() {
        return savedLocations;
    }

    public void itemAction() {
        savedLocations.setDeleteKey("");
        getLiveData().setValue(Constants.SAVED_LOCATIONS);
    }

    public void removeLocation() {
        if (!TextUtils.isEmpty(savedLocations.getAddress())) {
            savedLocations.setEmpty(savedLocations.getName().equals(getString(R.string.favorite_home)) || savedLocations.getName().equals(getString(R.string.favorite_job)));
            savedLocations.setDeleteKey(Constants.REMOVE_DIALOG_WARNING);
            getLiveData().setValue(Constants.SAVED_LOCATIONS);
        }
    }

}
