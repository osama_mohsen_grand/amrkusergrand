package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.SerializedName;

public class PusherTrip {
    @SerializedName("trip")
    private TripDataFromNotifications dataFromNotifications;

    public TripDataFromNotifications getDataFromNotifications() {
        return dataFromNotifications;
    }

}
