package amrk.app.amrk_user.uber.home.models.promoRequest;

import com.google.gson.annotations.SerializedName;

public class CheckPromoRequest {
    @SerializedName("code")
    private String code;
    @SerializedName("car_level_id")
    private int car_level_id;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public int getCar_level_id() {
        return car_level_id;
    }

    public void setCar_level_id(int car_level_id) {
        this.car_level_id = car_level_id;
    }
}
