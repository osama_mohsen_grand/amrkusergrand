package amrk.app.amrk_user.uber.history.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.repository.HistoryRepository;
import amrk.app.amrk_user.uber.history.adapters.IssuesAdapter;
import io.reactivex.disposables.CompositeDisposable;

public class IssueViewModels extends BaseViewModel {
    private IssuesAdapter issuesAdapter;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    HistoryRepository repository;

    @Inject
    public IssueViewModels(HistoryRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);

    }

    public void getIssues() {
        compositeDisposable.add(repository.getIssues());
    }

    @Bindable
    public IssuesAdapter getIssuesAdapter() {
        return this.issuesAdapter == null ? this.issuesAdapter = new IssuesAdapter() : this.issuesAdapter;
    }

    public HistoryRepository getRepository() {
        return repository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
