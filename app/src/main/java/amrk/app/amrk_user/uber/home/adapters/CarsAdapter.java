package amrk.app.amrk_user.uber.home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.CarsItemBinding;
import amrk.app.amrk_user.uber.home.itemViewModels.CarsItemViewModel;
import amrk.app.amrk_user.uber.home.models.CarsData;
import amrk.app.amrk_user.utils.helper.MovementHelper;

public class CarsAdapter extends RecyclerView.Adapter<CarsAdapter.ViewHolder> {
    public List<CarsData> carsData;
    Context context;
    private int lastSelected = 1;
    public int lastPosition = 0;

    public CarsAdapter() {
        carsData = new ArrayList<>();
    }


    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cars_item,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        CarsData dataModel = carsData.get(position);
        CarsItemViewModel homeItemViewModels = new CarsItemViewModel(dataModel);
        homeItemViewModels.getLiveData().observe(((LifecycleOwner) MovementHelper.unwrap(context)), integer -> {
            lastSelected = dataModel.getId();
            lastPosition = position;
            notifyDataSetChanged();
        });
        if (lastSelected == dataModel.getId()) {
            holder.itemBinding.container.setBackgroundColor(context.getResources().getColor(R.color.selected_color, null));
        } else {
            holder.itemBinding.container.setBackgroundColor(context.getResources().getColor(R.color.white, null));
        }
        holder.setViewModel(homeItemViewModels);
    }

    @Override
    public int getItemCount() {
        return this.carsData.size();
    }

    //
    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@NotNull List<CarsData> data) {
        this.carsData.clear();

        this.carsData.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CarsItemBinding itemBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(CarsItemViewModel itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setCarsItemViewModels(itemViewModels);
            }
        }
    }
}
