package amrk.app.amrk_user.uber.home.models.promoRequest;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.ResponseHeader;


public class CheckPromoResponse extends ResponseHeader {
    @SerializedName("data")
    private CheckPromoData data;

    public CheckPromoData getData() {
        return data;
    }
}