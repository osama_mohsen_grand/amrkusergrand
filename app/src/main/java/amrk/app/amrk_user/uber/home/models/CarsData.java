package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.SerializedName;

public class CarsData {

    @SerializedName("image")
    private String image;

    @SerializedName("distance_trip_unit")
    private double distanceTripUnit;
    @SerializedName("waiting_trip_unit")
    private double waiting_trip_unit;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("id")
    private int id;

    @SerializedName("start_trip_unit")
    private double startTripUnit;
    String showText;

    public CarsData() {
    }

    public CarsData(String image, int distanceTripUnit, String name, String description, int id, double startTripUnit) {
        this.image = image;
        this.distanceTripUnit = distanceTripUnit;
        this.name = name;
        this.description = description;
        this.id = id;
        this.startTripUnit = startTripUnit;
    }

    public String getImage() {
        return image;
    }

    public String getShowText() {
        return showText;
    }

    public void setShowText(String showText) {
        this.showText = showText;
    }

    public double getDistanceTripUnit() {
        return distanceTripUnit;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public double getStartTripUnit() {
        return startTripUnit;
    }

    public double getWaiting_trip_unit() {
        return waiting_trip_unit;
    }

}