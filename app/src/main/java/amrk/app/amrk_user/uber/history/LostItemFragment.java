package amrk.app.amrk_user.uber.history;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.FragmentLostItemBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.uber.history.models.losts.LostResponse;
import amrk.app.amrk_user.uber.history.viewModels.LostViewModels;
import amrk.app.amrk_user.utils.Constants;


public class LostItemFragment extends BaseFragment {
    FragmentLostItemBinding lostItemBinding;
    @Inject
    LostViewModels lostViewModels;
    private Context context;


    public LostItemFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        lostItemBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_lost_item, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        lostItemBinding.setLostViewModel(lostViewModels);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            lostViewModels.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
        }
        lostViewModels.getLost();
        liveDataListeners();
        return lostItemBinding.getRoot();
    }


    private void liveDataListeners() {
        lostViewModels.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.LOST_ITEM.equals(mutable.message)) {
                lostViewModels.setLostMainData(((LostResponse) mutable.object).getData());
            } else if (Constants.EMPTY_DRIVER.equals(mutable.message)) {
                ((ParentActivity) context).toastError(getString(R.string.empty_lost));
            } else if (Constants.SEND_ISSUE.equals(mutable.message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                finishActivity();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        lostViewModels.getRepository().setLiveData(lostViewModels.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
