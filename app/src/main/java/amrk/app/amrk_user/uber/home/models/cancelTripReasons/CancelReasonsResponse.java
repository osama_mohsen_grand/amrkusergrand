package amrk.app.amrk_user.uber.home.models.cancelTripReasons;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.model.base.StatusMessage;


public class CancelReasonsResponse extends StatusMessage {
    @SerializedName("data")
    private List<CancelReasonsData> data;

    public List<CancelReasonsData> getData() {
        return data;
    }
}