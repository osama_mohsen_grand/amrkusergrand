package amrk.app.amrk_user.uber.paymentMethods;

import android.content.Context;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.CheckPromoRequest;
import amrk.app.amrk_user.repository.AuthRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class PaymentMethodViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private int paymentType = -1;
    @Inject
    AuthRepository repository;
    CheckPromoRequest checkPromoRequest;

    @Inject
    public PaymentMethodViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        checkPromoRequest = new CheckPromoRequest();
        setPaymentType(UserHelper.getInstance(MyApplication.getInstance()).getPaymentType());
    }

    public void checkPromo() {
        getCheckPromoRequest().setDepartment_id(1);
        if (getCheckPromoRequest().isValid())
            compositeDisposable.add(repository.updatePromoCode(checkPromoRequest));
    }

    public void payFromVisa(Context context) {
        ((ParentActivity) context).toastError(ResourceManager.getString(R.string.not_active));
//        setPaymentType(1);
    }

    public void payFromWallet(Context context) {
//        ((ParentActivity) context).toastError(ResourceManager.getString(R.string.not_active));
        setPaymentType(2);
    }

    public void payFromCash() {
        setPaymentType(0);
    }

    public void toPromoDialog() {
        liveData.setValue(new Mutable(Constants.PROMO_DIALOG));
    }

    @Bindable
    public int getPaymentType() {
        return paymentType;
    }

    @Bindable
    public void setPaymentType(int paymentType) {
        UserHelper.getInstance(MyApplication.getInstance()).addPaymentType(paymentType);
        notifyChange(BR.paymentType);
        this.paymentType = paymentType;
    }

    public CheckPromoRequest getCheckPromoRequest() {
        return checkPromoRequest;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public AuthRepository getRepository() {
        return repository;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
