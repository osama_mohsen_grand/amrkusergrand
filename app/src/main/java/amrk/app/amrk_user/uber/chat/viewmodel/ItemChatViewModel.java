package amrk.app.amrk_user.uber.chat.viewmodel;

import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.uber.chat.model.ChatData;

public class ItemChatViewModel extends BaseViewModel {
    ChatData chat;

    public ItemChatViewModel(ChatData chat) {
        this.chat = chat;
    }

    @Bindable
    public ChatData getChat() {
        return chat;
    }

    @BindingAdapter("android:layoutDirection")
    public static void chatAdminDirection(ConstraintLayout constraintLayout, int senderType) {
        if (senderType == 0) {
            constraintLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else
            constraintLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }

}
