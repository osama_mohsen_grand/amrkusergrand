package amrk.app.amrk_user.uber.home.models.promoRequest;

import com.google.gson.annotations.SerializedName;

public class CheckPromoData {

    @SerializedName("code")
    private String code;
    @SerializedName("description")
    private String description;

    @SerializedName("id")
    private int id;

    @SerializedName("type")
    private int type;

    @SerializedName("value")
    private double value;

    public String getDescription() {
        return description;
    }

    public String getCode() {
        return code;
    }

    public int getId() {
        return id;
    }

    public int getType() {
        return type;
    }

    public double getValue() {
        return value;
    }
}