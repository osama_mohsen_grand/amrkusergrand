package amrk.app.amrk_user.uber.home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.CancelReasonItemBinding;
import amrk.app.amrk_user.uber.home.itemViewModels.CancelReasonsItemViewModel;
import amrk.app.amrk_user.uber.home.models.cancelTripReasons.CancelReasonsData;
import amrk.app.amrk_user.utils.helper.MovementHelper;


public class CancelReasonsAdapter extends RecyclerView.Adapter<CancelReasonsAdapter.ViewHolder> {
    public List<CancelReasonsData> reasonsDataList;
    Context context;
    public int lastSelection;
    public int lastPosition;

    public CancelReasonsAdapter() {
        reasonsDataList = new ArrayList<>();
    }


    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.cancel_reason_item,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        CancelReasonsData dataModel = reasonsDataList.get(position);
        CancelReasonsItemViewModel homeItemViewModels = new CancelReasonsItemViewModel(dataModel);
        homeItemViewModels.getItemHomeDataMutableLiveData().observe(((LifecycleOwner) MovementHelper.unwrap(context)), reasonsData -> {
            lastSelection = reasonsData.getId();
            lastPosition = position;
            notifyDataSetChanged();
        });
        setUpCarSelection(holder, dataModel);
        holder.setViewModel(homeItemViewModels);
    }

    private void setUpCarSelection(ViewHolder holder, CancelReasonsData dataModel) {
        if (lastSelection == dataModel.getId()) {
            holder.itemBinding.radioReason.setImageResource(R.drawable.ic_check);
        } else {
            holder.itemBinding.radioReason.setImageResource(R.drawable.ic_check_white);
        }
    }


    @Override
    public int getItemCount() {
        return this.reasonsDataList.size();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@NotNull List<CancelReasonsData> data) {
        this.reasonsDataList.clear();

        this.reasonsDataList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        CancelReasonItemBinding itemBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(CancelReasonsItemViewModel itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setCancelItemViewModel(itemViewModels);
            }
        }
    }
}
