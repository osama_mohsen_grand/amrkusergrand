package amrk.app.amrk_user.uber.home.models.createTrip;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.uber.history.models.HistoryData;

public class CreateTripResponse extends StatusMessage implements Serializable {

	@SerializedName("data")
	private HistoryData data;

	public void setData(HistoryData data){
		this.data = data;
	}

	public HistoryData getData(){
		return data;
	}


}