package amrk.app.amrk_user.uber.history.itemViewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.uber.history.models.losts.LostData;
import amrk.app.amrk_user.utils.Constants;

public class LostItemViewModels extends BaseViewModel {
    private LostData lostData;

    public LostItemViewModels(LostData lostData) {
        this.lostData = lostData;
     }


    @Bindable
    public LostData getLostData() {
        return lostData;
    }


    public void itemAction() {
       getLiveData().setValue(Constants.LOST_ITEM);
    }

}
