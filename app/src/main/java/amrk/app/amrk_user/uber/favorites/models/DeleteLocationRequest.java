package amrk.app.amrk_user.uber.favorites.models;

import com.google.gson.annotations.SerializedName;

public class DeleteLocationRequest {
    @SerializedName("empty")
    private boolean empty;
    @SerializedName("location_id")
    private int location_id;

    public DeleteLocationRequest(boolean empty, int location_id) {
        this.empty = empty;
        this.location_id = location_id;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public int getLocation_id() {
        return location_id;
    }

    public void setLocation_id(int location_id) {
        this.location_id = location_id;
    }
}
