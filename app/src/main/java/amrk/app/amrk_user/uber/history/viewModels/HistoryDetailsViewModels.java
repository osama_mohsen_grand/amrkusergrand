package amrk.app.amrk_user.uber.history.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.repository.HistoryRepository;
import amrk.app.amrk_user.uber.history.models.HistoryData;
import amrk.app.amrk_user.uber.history.models.issues.IssueLostsRequest;
import amrk.app.amrk_user.uber.home.models.StatusRequest;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import io.reactivex.disposables.CompositeDisposable;


public class HistoryDetailsViewModels extends BaseViewModel {
    private HistoryData historyData;
    IssueLostsRequest issueLostsRequest;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    HistoryRepository repository;
    StatusRequest statusRequest;
    public String start, mid, end;

    @Inject
    public HistoryDetailsViewModels(HistoryRepository repository) {
        this.repository = repository;
        statusRequest = new StatusRequest();
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        issueLostsRequest = new IssueLostsRequest();

    }

    public void getTripHistory() {
        compositeDisposable.add(repository.getTripDetails(getPassingObject().getId()));
    }

    public void deleteTrip() {
        statusRequest.setTrip_id(getHistoryData().getId());
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(repository.deleteTrip(getStatusRequest()));
    }

    public void sendIssue() {
        getIssueLostsRequest().setIssue_id(getPassingObject().getObject());
        getIssueLostsRequest().setOrder_type(1);
        getIssueLostsRequest().setTrip_id(String.valueOf(getHistoryData().getId()));
        if (getIssueLostsRequest().isValid())
            compositeDisposable.add(repository.sendIssue(getIssueLostsRequest()));
        else
            liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
    }

    public IssueLostsRequest getIssueLostsRequest() {
        return issueLostsRequest;
    }

    @Bindable
    public HistoryData getHistoryData() {
        return historyData;
    }

    @Bindable
    public void setHistoryData(HistoryData historyData) {
        historyData.setStaticMap("https://maps.googleapis.com/maps/api/staticmap?center=" + historyData.getStartLat() + "," + historyData.getStartLng() +
                "&zoom=14&size=400x400&maptype=roadmap&markers=size:mid%7Ccolor:green%7C" + historyData.getStartLat() +
                "," + historyData.getStartLng() + "&markers=size:mid%7Ccolor:blackC%7C30.127913863466524,31.380474865436554&key="
                + ResourceManager.getString(R.string.google_map));
        if (historyData.getTripPath().size() > 1) {
            start = historyData.getTripPath().get(0).getAddress();
        }
        if (historyData.getTripPath().size() >= 2) {
            if (historyData.getTripPath().size() == 2)
                end = historyData.getTripPath().get(1).getAddress();
            if (historyData.getTripPath().size() == 3) {
                mid = historyData.getTripPath().get(1).getAddress();
                end = historyData.getTripPath().get(2).getAddress();
            }
        }
        notifyChange();
        this.historyData = historyData;
    }

    public void toIssues() {
        if (historyData != null && historyData.getDriverId() != 0)
            liveData.setValue(new Mutable(Constants.ISSUES));
        else
            liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
    }

    public void toLost() {
        if (historyData != null && historyData.getDriverId() != 0)
            liveData.setValue(new Mutable(Constants.LOST_ITEM));
        else
            liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
    }

    public HistoryRepository getRepository() {
        return repository;
    }

    public StatusRequest getStatusRequest() {
        return statusRequest;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
