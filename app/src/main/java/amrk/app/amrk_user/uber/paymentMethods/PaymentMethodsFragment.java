package amrk.app.amrk_user.uber.paymentMethods;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentPaymentMethodsBinding;
import amrk.app.amrk_user.databinding.UberPromoDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.models.UsersResponse;
import amrk.app.amrk_user.utils.Constants;

public class PaymentMethodsFragment extends BaseFragment {
    private Context context;
    @Inject
    PaymentMethodViewModel viewModel;
    Dialog promoDialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentPaymentMethodsBinding paymentMethodsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_payment_methods, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        paymentMethodsBinding.setViewmodel(viewModel);
        setEvent();
        return paymentMethodsBinding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (mutable.message.equals(Constants.PROMO_DIALOG)) {
                showPromoDialog();
            } else if (mutable.message.equals(Constants.UPDATE_PROFILE)) {
                toastMessage(((UsersResponse) mutable.object).mMessage);
                promoDialog.dismiss();
            }
        });
    }

    private void showPromoDialog() {
        promoDialog = new Dialog(context, R.style.PauseDialog);
        promoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(promoDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        promoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        UberPromoDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(promoDialog.getContext()), R.layout.uber_promo_dialog, null, false);
        promoDialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        promoDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }


    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
