package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.pages.auth.models.UserData;
import amrk.app.amrk_user.uber.history.models.TripPathsItem;


public class TripDataFromNotifications {

    @SerializedName("date")
    private String date;

    @SerializedName("driver_id")
    private int driverId;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("created_at_time")
    private String createdAtTime;

    @SerializedName("type")
    private String type;

    @SerializedName("trip_distance")
    private String tripDistance;

    @SerializedName("start_lng")
    private double startLng;

    @SerializedName("updated_at")
    private String updatedAt;

    @SerializedName("start_lat")
    private double startLat;

    @SerializedName("payment")
    private int payment;

    @SerializedName("id")
    private int id;

    @SerializedName("trip_paths")
    private List<TripPathsItem> tripPaths;

    @SerializedName("trip_time")
    private int tripTime;

    @SerializedName("canceled_by")
    private int canceledBy;

    @SerializedName("driver_rate")
    private String driverRate;

    @SerializedName("driver_comment")
    private String driverComment;

    @SerializedName("trip_total")
    private double tripTotal;

    @SerializedName("waiting_time")
    private int waitingTime;

    @SerializedName("car_level_id")
    private int carLevelId;

    @SerializedName("cancel_id")
    private int cancelId;

    @SerializedName("promo_id")
    private int promoId;

    @SerializedName("deleted_at")
    private String deletedAt;

    @SerializedName("user_rate")
    private String userRate;

    @SerializedName("cancel_reason")
    private String cancelReason;

    @SerializedName("user_id")
    private int userId;

    @SerializedName("start_address")
    private String startAddress;

    @SerializedName("time")
    private String time;
    @SerializedName("car_image")
    private String carImage;

    @SerializedName("driver")
    @Expose
    private UserData user;

    @SerializedName("user_comment")
    private String userComment;

    @SerializedName("status")
    private int status;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public int getDriverId() {
        return driverId;
    }

    public void setDriverId(int driverId) {
        this.driverId = driverId;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getCreatedAtTime() {
        return createdAtTime;
    }

    public void setCreatedAt(String createdAt) {
        this.createdAt = createdAt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getTripDistance() {
        return tripDistance;
    }

    public void setTripDistance(String tripDistance) {
        this.tripDistance = tripDistance;
    }

    public double getStartLng() {
        return startLng;
    }

    public void setStartLng(double startLng) {
        this.startLng = startLng;
    }

    public String getUpdatedAt() {
        return updatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        this.updatedAt = updatedAt;
    }

    public double getStartLat() {
        return startLat;
    }

    public void setStartLat(double startLat) {
        this.startLat = startLat;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public List<TripPathsItem> getTripPaths() {
        return tripPaths;
    }

    public void setTripPaths(List<TripPathsItem> tripPaths) {
        this.tripPaths = tripPaths;
    }

    public int getTripTime() {
        return tripTime;
    }

    public void setTripTime(int tripTime) {
        this.tripTime = tripTime;
    }

    public int getCanceledBy() {
        return canceledBy;
    }

    public void setCanceledBy(int canceledBy) {
        this.canceledBy = canceledBy;
    }

    public String getDriverRate() {
        return driverRate;
    }

    public void setDriverRate(String driverRate) {
        this.driverRate = driverRate;
    }

    public String getDriverComment() {
        return driverComment;
    }

    public void setDriverComment(String driverComment) {
        this.driverComment = driverComment;
    }

    public double getTripTotal() {
        return tripTotal;
    }

    public void setTripTotal(double tripTotal) {
        this.tripTotal = tripTotal;
    }

    public int getWaitingTime() {
        return waitingTime;
    }

    public void setWaitingTime(int waitingTime) {
        this.waitingTime = waitingTime;
    }

    public int getCarLevelId() {
        return carLevelId;
    }

    public void setCarLevelId(int carLevelId) {
        this.carLevelId = carLevelId;
    }

    public int getCancelId() {
        return cancelId;
    }

    public void setCancelId(int cancelId) {
        this.cancelId = cancelId;
    }

    public int getPromoId() {
        return promoId;
    }

    public void setPromoId(int promoId) {
        this.promoId = promoId;
    }

    public String getDeletedAt() {
        return deletedAt;
    }

    public void setDeletedAt(String deletedAt) {
        this.deletedAt = deletedAt;
    }

    public String getUserRate() {
        return userRate;
    }

    public void setUserRate(String userRate) {
        this.userRate = userRate;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public void setCancelReason(String cancelReason) {
        this.cancelReason = cancelReason;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public UserData getUser() {
        return user;
    }

    public void setUser(UserData user) {
        this.user = user;
    }

    public String getUserComment() {
        return userComment;
    }

    public void setUserComment(String userComment) {
        this.userComment = userComment;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getCarImage() {
        return carImage;
    }

    @Override
    public String toString() {
        return "TripDataFromNotifications{" +
                "date='" + date + '\'' +
                ", driverId=" + driverId +
                ", description='" + description + '\'' +
                ", createdAt='" + createdAt + '\'' +
                ", createdAtTime='" + createdAtTime + '\'' +
                ", type='" + type + '\'' +
                ", tripDistance='" + tripDistance + '\'' +
                ", startLng=" + startLng +
                ", updatedAt='" + updatedAt + '\'' +
                ", startLat=" + startLat +
                ", payment=" + payment +
                ", id=" + id +
                ", tripPaths=" + tripPaths +
                ", tripTime=" + tripTime +
                ", canceledBy=" + canceledBy +
                ", driverRate=" + driverRate +
                ", driverComment='" + driverComment + '\'' +
                ", tripTotal=" + tripTotal +
                ", waitingTime=" + waitingTime +
                ", carLevelId=" + carLevelId +
                ", cancelId=" + cancelId +
                ", promoId=" + promoId +
                ", deletedAt='" + deletedAt + '\'' +
                ", userRate='" + userRate + '\'' +
                ", cancelReason='" + cancelReason + '\'' +
                ", userId=" + userId +
                ", startAddress='" + startAddress + '\'' +
                ", time='" + time + '\'' +
                ", user=" + user +
                ", userComment='" + userComment + '\'' +
                ", status=" + status +
                '}';
    }
}