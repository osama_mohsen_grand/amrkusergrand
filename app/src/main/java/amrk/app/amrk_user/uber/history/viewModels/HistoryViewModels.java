package amrk.app.amrk_user.uber.history.viewModels;


import android.view.View;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.repository.HistoryRepository;
import amrk.app.amrk_user.uber.history.adapters.HistoryAdapter;
import amrk.app.amrk_user.uber.history.models.HistoryMain;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;


public class HistoryViewModels extends BaseViewModel {
    private HistoryAdapter historyAdapter;
    private int type = 0;
    @Inject
    HistoryRepository repository;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    HistoryMain historyMain;

    @Inject
    public HistoryViewModels(HistoryRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void history(int page, boolean showProgress) {
        compositeDisposable.add(repository.getHistory(type,page,showProgress));
    }


    public void changeTripType() {
        if (type == 0)
            setType(1);
        else
            setType(0);
        liveData.setValue(new Mutable(Constants.TRIP_TYPE_TITLE));
        getHistoryAdapter().historyDataList.clear();
        getHistoryAdapter().notifyDataSetChanged();
        history(1, true);
    }

    @Bindable
    public HistoryMain getHistoryMain() {
        return this.historyMain == null ? this.historyMain = new HistoryMain() : this.historyMain;
    }

    public void setHistoryMain(HistoryMain historyMain) {
        if (getHistoryAdapter().historyDataList.size() > 0) {
            getHistoryAdapter().loadMore(historyMain.getData());
        } else {
            getHistoryAdapter().updateData(historyMain.getData());
            notifyChange(BR.historyAdapter);
        }
        setSearchProgressVisible(View.GONE);
        notifyChange(BR.historyMain);
        this.historyMain = historyMain;
    }

    @Bindable
    public int getType() {
        return type;
    }

    @Bindable
    public void setType(int type) {
        notifyChange(BR.type);
        this.type = type;
    }

    @Bindable
    public HistoryAdapter getHistoryAdapter() {
        return this.historyAdapter == null ? this.historyAdapter = new HistoryAdapter() : this.historyAdapter;
    }

    public HistoryRepository getRepository() {
        return repository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
