package amrk.app.amrk_user.uber.history.itemViewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.uber.history.models.issues.IssuesData;
import amrk.app.amrk_user.utils.Constants;


public class IssueItemViewModels extends BaseViewModel {
    IssuesData issuesData;

    public IssueItemViewModels(IssuesData issuesData) {
        this.issuesData = issuesData;
    }


    @Bindable
    public IssuesData getIssuesData() {
        return issuesData;
    }


    public void itemAction() {
        getLiveData().setValue(Constants.SEND_ISSUE);
    }


}
