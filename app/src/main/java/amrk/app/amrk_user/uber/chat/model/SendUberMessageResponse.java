package amrk.app.amrk_user.uber.chat.model;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.chat.model.Chat;

public class SendUberMessageResponse extends StatusMessage {
    @SerializedName("data")
    private ChatData chatData;
    @SerializedName("message")
    private ChatData messagePusher;

    public ChatData getChatData() {
        return chatData;
    }

    public ChatData getMessagePusher() {
        return messagePusher;
    }
}
