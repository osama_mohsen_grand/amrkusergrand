package amrk.app.amrk_user.uber.home.models.createTrip;

public class TripDetailsFromMap {
    private String driverEstimateTime;
    private String startAddress;
    private String endAddress;
    private String tripEstimateTime;
    private String tripCost;
    private String tripDistance;

    public String getDriverEstimateTime() {
        return driverEstimateTime;
    }

    public void setDriverEstimateTime(String driverEstimateTime) {
        this.driverEstimateTime = driverEstimateTime;
    }

    public String getStartAddress() {
        return startAddress;
    }

    public void setStartAddress(String startAddress) {
        this.startAddress = startAddress;
    }

    public String getEndAddress() {
        return endAddress;
    }

    public void setEndAddress(String endAddress) {
        this.endAddress = endAddress;
    }

    public String getTripEstimateTime() {
        return tripEstimateTime;
    }

    public void setTripEstimateTime(String tripEstimateTime) {
        this.tripEstimateTime = tripEstimateTime;
    }

    public String getTripCost() {
        return tripCost;
    }

    public void setTripCost(String tripCost) {
        this.tripCost = tripCost;
    }

    public String getTripDistance() {
        return tripDistance;
    }

    public void setTripDistance(String tripDistance) {
        this.tripDistance = tripDistance;
    }
}
