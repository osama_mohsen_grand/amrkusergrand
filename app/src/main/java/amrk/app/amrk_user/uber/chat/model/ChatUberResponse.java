package amrk.app.amrk_user.uber.chat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.model.base.StatusMessage;

public class ChatUberResponse extends StatusMessage {

    @SerializedName("data")
    private List<ChatData> data;

    public void setData(List<ChatData> data) {
        this.data = data;
    }

    public List<ChatData> getData() {
        return data;
    }

}