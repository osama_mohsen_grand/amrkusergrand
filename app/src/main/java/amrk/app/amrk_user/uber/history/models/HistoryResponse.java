package amrk.app.amrk_user.uber.history.models;

import com.google.gson.annotations.SerializedName;


import amrk.app.amrk_user.model.base.StatusMessage;


public class HistoryResponse extends StatusMessage {

    @SerializedName("data")
    private HistoryMain historyMain;

    public HistoryMain getHistoryMain() {
        return historyMain;
    }
}