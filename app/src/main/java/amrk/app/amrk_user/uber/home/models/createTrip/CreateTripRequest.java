package amrk.app.amrk_user.uber.home.models.createTrip;

import com.google.gson.annotations.SerializedName;

public class CreateTripRequest {
    @SerializedName("type")
    private int type;
    @SerializedName("car_level_id")
    private int car_level_id;
    @SerializedName("start_address")
    private String start_address;
    @SerializedName("start_lat")
    private double start_lat;
    @SerializedName("start_lng")
    private double start_lng;
    @SerializedName("end_address")
    private String end_address;
    @SerializedName("end_lat")
    private double end_lat;
    @SerializedName("end_lng")
    private double end_lng;
    @SerializedName("promo_id")
    private int promo_id;
    @SerializedName("payment")
    private int payment;
    @SerializedName("initial_price")
    private double initial_price;

    public double getInitial_price() {
        return initial_price;
    }

    public void setInitial_price(double initial_price) {
        this.initial_price = initial_price;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getCar_level_id() {
        return car_level_id;
    }

    public void setCar_level_id(int car_level_id) {
        this.car_level_id = car_level_id;
    }

    public String getStart_address() {
        return start_address;
    }

    public void setStart_address(String start_address) {
        this.start_address = start_address;
    }

    public double getStart_lat() {
        return start_lat;
    }

    public void setStart_lat(double start_lat) {
        this.start_lat = start_lat;
    }

    public double getStart_lng() {
        return start_lng;
    }

    public void setStart_lng(double start_lng) {
        this.start_lng = start_lng;
    }

    public String getEnd_address() {
        return end_address;
    }

    public void setEnd_address(String end_address) {
        this.end_address = end_address;
    }

    public double getEnd_lat() {
        return end_lat;
    }

    public void setEnd_lat(double end_lat) {
        this.end_lat = end_lat;
    }

    public double getEnd_lng() {
        return end_lng;
    }

    public void setEnd_lng(double end_lng) {
        this.end_lng = end_lng;
    }

    public int getPromo_id() {
        return promo_id;
    }

    public void setPromo_id(int promo_id) {
        this.promo_id = promo_id;
    }

    public int getPayment() {
        return payment;
    }

    public void setPayment(int payment) {
        this.payment = payment;
    }
}
