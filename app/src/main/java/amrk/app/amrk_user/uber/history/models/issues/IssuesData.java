package amrk.app.amrk_user.uber.history.models.issues;

 import com.google.gson.annotations.SerializedName;

public class IssuesData {

	@SerializedName("issue")
	private String issue;

	@SerializedName("id")
	private int id;

	public String getIssue(){
		return issue;
	}


	public int getId(){
		return id;
	}
}