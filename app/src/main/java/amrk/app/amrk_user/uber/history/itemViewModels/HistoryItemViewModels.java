package amrk.app.amrk_user.uber.history.itemViewModels;

import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.uber.history.models.HistoryData;
import amrk.app.amrk_user.utils.Constants;


public class HistoryItemViewModels extends BaseViewModel {
    HistoryData historyData;
    public String start, mid, end;

    public HistoryItemViewModels(HistoryData historyData) {
        if (historyData.getTripPath().size() > 1) {
            start = historyData.getTripPath().get(0).getAddress();
        }
        if (historyData.getTripPath().size() >= 2) {
            if (historyData.getTripPath().size() == 2)
                end = historyData.getTripPath().get(1).getAddress();
            if (historyData.getTripPath().size() == 3) {
                mid = historyData.getTripPath().get(1).getAddress();
                end = historyData.getTripPath().get(2).getAddress();
            }
        }
        this.historyData = historyData;
    }

    @Bindable
    public HistoryData getHistoryData() {

        return historyData;
    }

    @BindingAdapter("android:rating")
    public static void setUserRating(RatingBar view, String rating) {
        if (view != null && rating != null) {
            float rate = Float.parseFloat(rating);
            view.setRating(rate);
        }
    }

    public void itemAction() {
        getLiveData().setValue(Constants.TRIP_DETAILS);
    }

}
