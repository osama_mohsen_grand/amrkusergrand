package amrk.app.amrk_user.uber.home.models;

import com.google.android.gms.maps.model.LatLng;

public class NearestDrivers {
    private String driverId;
    private LatLng latLng;

    public NearestDrivers() {
    }

    public NearestDrivers(String driverId, LatLng latLng) {
        this.driverId = driverId;
        this.latLng = latLng;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }

    public LatLng getLatLng() {
        return latLng;
    }

    public void setLatLng(LatLng latLng) {
        this.latLng = latLng;
    }
}
