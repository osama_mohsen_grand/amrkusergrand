package amrk.app.amrk_user.uber.home.models.createTrip;

import com.google.gson.annotations.SerializedName;

public class UpdateLastLocationRequest {
    @SerializedName("trip_id")
    private int tripId;
    @SerializedName("id")
    private int lastLocationId;
    @SerializedName("address")
    private String address;
    @SerializedName("lat")
    private double lat;
    @SerializedName("lng")
    private double lng;
    @SerializedName("current_lat")
    private double current_lat;
    @SerializedName("current_lng")
    private double current_lng;

    public UpdateLastLocationRequest(int tripId, int lastLocationId, String address, double lat, double lng, double current_lat, double current_lng) {
        this.tripId = tripId;
        this.lastLocationId = lastLocationId;
        this.address = address;
        this.lat = lat;
        this.lng = lng;
        this.current_lat = current_lat;
        this.current_lng = current_lng;

    }

    public int getTripId() {
        return tripId;
    }

    public void setTripId(int tripId) {
        this.tripId = tripId;
    }

    public int getLastLocationId() {
        return lastLocationId;
    }

    public void setLastLocationId(int lastLocationId) {
        this.lastLocationId = lastLocationId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public double getCurrent_lat() {
        return current_lat;
    }

    public void setCurrent_lat(double current_lat) {
        this.current_lat = current_lat;
    }

    public double getCurrent_lng() {
        return current_lng;
    }

    public void setCurrent_lng(double current_lng) {
        this.current_lng = current_lng;
    }

}
