package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;


public class CarsResponse extends StatusMessage {

    @SerializedName("data")
    private HomeData data;

    public HomeData getData() {
        return data;
    }
}