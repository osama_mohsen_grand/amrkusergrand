package amrk.app.amrk_user.uber.chat.view;

import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;
import java.util.HashMap;
import javax.inject.Inject;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentChatUberBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.uber.chat.model.ChatData;
import amrk.app.amrk_user.uber.chat.model.ChatUberResponse;
import amrk.app.amrk_user.uber.chat.model.SendUberMessageResponse;
import amrk.app.amrk_user.uber.chat.viewmodel.ChatUberViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.services.RealTimeReceiver;
import amrk.app.amrk_user.utils.session.LanguagesHelper;

public class ChatUberFragment extends BaseFragment implements RealTimeReceiver.MessageUberReceiverListener {
    private FragmentChatUberBinding binding;
    @Inject
    ChatUberViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_uber, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.chat();
        }
        setEvent();
        connectPusher(Constants.chatEventName, Constants.USERSBeamName.concat(viewModel.userData.getJwt()), Constants.chatChannelName.concat(viewModel.userData.getJwt()));

        return binding.getRoot();
    }


    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (mutable.message.equals(Constants.CHAT)) {
                viewModel.adapter.update(((ChatUberResponse) mutable.object).getData());
                if (viewModel.adapter.getChatList().size() > 0)
                    new Handler(Looper.getMainLooper()).postDelayed(() -> binding.rcChat.smoothScrollToPosition(viewModel.adapter.getChatList().size() - 1), 200);
            } else if (mutable.message.equals(Constants.IMAGE)) {
                pickImageDialogSelect();
            } else if (((Mutable) o).message.equals(Constants.SEND_MESSAGE)) {
                SendUberMessageResponse chatSendResponse = (SendUberMessageResponse) ((Mutable) o).object;
                viewModel.adapter.getChatList().add(chatSendResponse.getChatData());
                viewModel.fileObjectList.clear();
                binding.message.setText("");
                binding.message.setHint(getResources().getString(R.string.chat_hint));
                binding.rcChat.scrollToPosition(viewModel.adapter.getItemCount() - 1);
                viewModel.adapter.notifyItemChanged(viewModel.adapter.getItemCount() - 1);
            } else if (((Mutable) o).message.equals(Constants.PUSHER_LIVE_DATA)) {
                binding.rcChat.scrollToPosition(viewModel.adapter.getChatList().size() - 1);
            }
        });

    }

    public void connectPusher(String eventName, String beamName, String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("75b53060b1f415501d21", options);
        pusher.connect();
        Channel channel = pusher.subscribe(channelName);
        channel.bind(eventName, event -> {
            SendUberMessageResponse chatSendResponse = new Gson().fromJson(event.getData(), SendUberMessageResponse.class);
            viewModel.adapter.getChatList().add(chatSendResponse.getMessagePusher());
            viewModel.adapter.notifyItemInserted(viewModel.adapter.getChatList().size() - 1);
            viewModel.liveData.postValue(new Mutable(Constants.PUSHER_LIVE_DATA));
        });
        PushNotifications.start(MyApplication.getInstance(), "4b9224fd-ac88-4e17-86bf-65c347bc0fbd");
        PushNotifications.addDeviceInterest(Constants.INTEREST);
        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(Constants.PUSHER_TOKEN_URL, () -> {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("jwt", LanguagesHelper.getJwt());
            HashMap<String, String> queryParams = new HashMap<>();
            return new AuthData(
                    headers,
                    queryParams
            );
        });
        PushNotifications.setUserId(beamName, tokenProvider, new BeamsCallback<>() {
            @Override
            public void onSuccess(@NonNull Void... values) {
            }

            @Override
            public void onFailure(PusherCallbackError error) {
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
        MyApplication.getInstance().setMessageUberReceiverListener(this);
    }

    // Deleted
    @Override
    public void onMessageUberChanged(ChatData messagesItem) {
        if (messagesItem != null) {
            viewModel.adapter.getChatList().add(messagesItem);
            viewModel.adapter.notifyItemInserted(viewModel.adapter.getChatList().size() - 1);
            binding.rcChat.scrollToPosition(viewModel.adapter.getChatList().size() - 1);
        }
    }
}
