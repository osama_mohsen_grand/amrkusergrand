package amrk.app.amrk_user.uber.history.models.issues;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.model.base.StatusMessage;

public class IssuesResponse extends StatusMessage {

    @SerializedName("data")
    private List<IssuesData> data;

    public List<IssuesData> getData() {
        return data;
    }
}