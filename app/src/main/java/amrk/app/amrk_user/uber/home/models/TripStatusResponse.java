package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;


public class TripStatusResponse extends StatusMessage {
    @SerializedName("data")
    private TripDataFromNotifications dataFromNotifications;

    public TripDataFromNotifications getDataFromNotifications() {
        return dataFromNotifications;
    }
}
