package amrk.app.amrk_user.uber.history.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.LostItemBinding;
import amrk.app.amrk_user.uber.history.itemViewModels.LostItemViewModels;
import amrk.app.amrk_user.uber.history.models.losts.LostData;

public class LostAdapter extends RecyclerView.Adapter<LostAdapter.ViewHolder> {
    public List<LostData> lostDataList;
    Context context;
     MutableLiveData<Integer> adapterLiveData;
    public int lastSelection;

    public LostAdapter() {
        lostDataList = new ArrayList<>();
        adapterLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<Integer> getAdapterLiveData() {
        return adapterLiveData;
    }

    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lost_item,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NotNull ViewHolder holder, int position) {
        LostData dataModel = lostDataList.get(position);
        LostItemViewModels homeItemViewModels = new LostItemViewModels(dataModel);
        homeItemViewModels.getLiveData().observe(((LifecycleOwner) context), o -> {
            lastSelection = dataModel.getId();
            notifyDataSetChanged();
        });
        if (lastSelection == dataModel.getId()) {
            holder.itemBinding.checkWallet.setImageDrawable(context.getResources().getDrawable(R.drawable.switch_fill, null));

        } else {
            holder.itemBinding.checkWallet.setImageDrawable(context.getResources().getDrawable(R.drawable.switchempty, null));
        }
        holder.setViewModel(homeItemViewModels);
    }

    @Override
    public int getItemCount() {
        return this.lostDataList.size();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@NotNull List<LostData> data) {
        this.lostDataList.clear();

        this.lostDataList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        LostItemBinding itemBinding;

        //
        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(LostItemViewModels itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setLostItemViewModels(itemViewModels);
            }
        }
    }
}
