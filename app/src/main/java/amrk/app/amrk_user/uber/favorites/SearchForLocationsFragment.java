package amrk.app.amrk_user.uber.favorites;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentSearchLocationsBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.newOrder.models.SavedLocationsResponse;
import amrk.app.amrk_user.uber.favorites.viewModels.FavoriteLocationsViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;

import static android.app.Activity.RESULT_OK;


public class SearchForLocationsFragment extends BaseFragment {

    private Context context;
    FragmentSearchLocationsBinding binding;
    @Inject
    FavoriteLocationsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search_locations, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
        }
        setEvent();
        return binding.getRoot();
    }


    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.FAVORITES_LOCATIONS.equals(((Mutable) o).message)) {
                if (!com.google.android.libraries.places.api.Places.isInitialized()) {
                    Places.initialize(context, getString(R.string.google_map));
                }
                List<Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.NAME, Place.Field.ADDRESS,
                        com.google.android.libraries.places.api.model.Place.Field.LAT_LNG);
                Intent intent = new Autocomplete.IntentBuilder(
                        AutocompleteActivityMode.FULLSCREEN, fields)
                        .build(context);
                startActivityForResult(intent, Constants.AUTOCOMPLETE_REQUEST_CODE);
            } else if (Constants.SAVED_LOCATIONS.equals(((Mutable) o).message)) {
                viewModel.getFavoritesLocationsAdapter().update(((SavedLocationsResponse) ((Mutable) o).object).getSavedLocationsData());
            }
        });


    }


    @Override
    public void onResume() {
        super.onResume();
        baseActivity().enableRefresh(false);
        viewModel.getSettingsRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                if (place.getLatLng() != null) {
                    viewModel.getAddNewFavoriteRequest().setAddress(place.getAddress());
                    viewModel.getAddNewFavoriteRequest().setLat(place.getLatLng().latitude);
                    viewModel.getAddNewFavoriteRequest().setLng(place.getLatLng().longitude);
                    viewModel.getAddNewFavoriteRequest().setName(viewModel.getPassingObject().getObject());
                    viewModel.getAddNewFavoriteRequest().setItemId(viewModel.getPassingObject().getId());
                    if (!TextUtils.isEmpty(viewModel.getAddNewFavoriteRequest().getName()) && (viewModel.getAddNewFavoriteRequest().getName().equals(getString(R.string.favorite_home)) || viewModel.getAddNewFavoriteRequest().getName().equals(getString(R.string.favorite_job))))
                        MovementHelper.startActivityForResultWithBundle(context, new PassingObject(viewModel.getAddNewFavoriteRequest()), getString(R.string.edit_location).concat(" ").concat(viewModel.getAddNewFavoriteRequest().getName()), SendFavoriteLocationFragment.class.getName(), null, null);
                    else
                        MovementHelper.startActivityForResultWithBundle(context, new PassingObject(viewModel.getAddNewFavoriteRequest()), getString(R.string.new_location), SendFavoriteLocationFragment.class.getName(), null, null);
                }
            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                Status status = Autocomplete.getStatusFromIntent(data);
            }
        } else {
            MovementHelper.finishWithResult(new PassingObject(), context);
        }

    }
}
