package amrk.app.amrk_user.uber.history.models.losts;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class LostResponse extends StatusMessage {

    @SerializedName("data")
    private LostMainData data;

    public LostMainData getData() {
        return data;
    }
}