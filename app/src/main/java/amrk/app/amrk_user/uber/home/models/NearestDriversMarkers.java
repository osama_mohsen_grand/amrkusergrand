package amrk.app.amrk_user.uber.home.models;

import com.google.android.gms.maps.model.Marker;

public class NearestDriversMarkers {
    private String driverId;
    private Marker nearestDriverMarker;

    public NearestDriversMarkers() {
    }

    public NearestDriversMarkers(String driverId, Marker nearestDriverMarker) {
        this.driverId = driverId;
        this.nearestDriverMarker = nearestDriverMarker;
    }

    public Marker getNearestDriverMarker() {
        return nearestDriverMarker;
    }

    public void setNearestDriverMarker(Marker nearestDriverMarker) {
        this.nearestDriverMarker = nearestDriverMarker;
    }

    public String getDriverId() {
        return driverId;
    }

    public void setDriverId(String driverId) {
        this.driverId = driverId;
    }


}
