package amrk.app.amrk_user.uber.history;


import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.customViews.CustomDrawable;
import amrk.app.amrk_user.databinding.FragmentSendIssueBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.uber.history.models.HistoryData;
import amrk.app.amrk_user.uber.history.viewModels.HistoryDetailsViewModels;
import amrk.app.amrk_user.utils.Constants;


public class SendIssueFragment extends BaseFragment {
    FragmentSendIssueBinding sendIssueBinding;
    @Inject
    HistoryDetailsViewModels issueViewModels;
    private Context context;

    public SendIssueFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        sendIssueBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_send_issue, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        sendIssueBinding.setIssueViewModel(issueViewModels);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            issueViewModels.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            issueViewModels.setHistoryData(new Gson().fromJson(String.valueOf(issueViewModels.getPassingObject().getObjectClass()), HistoryData.class));
            int color = Color.parseColor(issueViewModels.getHistoryData().getCarInfo() != null && issueViewModels.getHistoryData().getCarInfo().getCar_color() != null
                    ? issueViewModels.getHistoryData().getCarInfo().getCar_color() : "#F2EFF1");
            CustomDrawable customDrawable = new CustomDrawable(color, color, color, 4, getResources().getColor(R.color.colorPrimaryDark, null), 0);
            sendIssueBinding.carColor.setBackground(customDrawable);
        }
        liveDataListeners();
        return sendIssueBinding.getRoot();
    }


    private void liveDataListeners() {
        issueViewModels.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.SEND_ISSUE.equals(mutable.message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                finishActivity();
            } else if (Constants.EMPTY_DRIVER.equals(mutable.message)) {
                ((ParentActivity) context).toastError(getString(R.string.empty_data));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        issueViewModels.getRepository().setLiveData(issueViewModels.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
