package amrk.app.amrk_user.uber.home.models;

import com.google.gson.annotations.SerializedName;

public class SavedLocations {
    @SerializedName("address")
    private String start_address;
    @SerializedName("name")
    private String name;
    @SerializedName("lat")
    private double start_lat;
    @SerializedName("lng")
    private double start_lng;
    @SerializedName("id")
    private int id;

    public int getId() {
        return id;
    }

    public String getStart_address() {
        return start_address;
    }

    public double getStart_lat() {
        return start_lat;
    }

    public double getStart_lng() {
        return start_lng;
    }

    public String getName() {
        return name;
    }
}
