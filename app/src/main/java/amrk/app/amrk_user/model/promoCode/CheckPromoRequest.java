package amrk.app.amrk_user.model.promoCode;

import androidx.databinding.ObservableField;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.validation.Validate;

public class CheckPromoRequest {
    @SerializedName("department_id")
    @Expose
    private int department_id;
    @SerializedName("code")
    @Expose
    private String code;
    public transient ObservableField<String> codeError = new ObservableField<>();

    public CheckPromoRequest() {
    }

    public CheckPromoRequest(int department_id, String code) {
        this.department_id = department_id;
        this.code = code;
    }

    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(code, Constants.FIELD)) {
            codeError.set(Validate.error);
            valid = false;
        }
        return valid;
    }

    public int getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        codeError.set(null);
        this.code = code;
    }
}
