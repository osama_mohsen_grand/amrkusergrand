package amrk.app.amrk_user.model.promoCode;

import com.google.gson.annotations.SerializedName;

public class PromoData {

	@SerializedName("code")
	private String code;

	@SerializedName("department_id")
	private int departmentId;

	@SerializedName("description")
	private Object description;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private int type;

	@SerializedName("value")
	private double value;

	public String getCode(){
		return code;
	}

	public int getDepartmentId(){
		return departmentId;
	}

	public Object getDescription(){
		return description;
	}

	public int getId(){
		return id;
	}

	public int getType(){
		return type;
	}

	public double getValue(){
		return value;
	}

	@Override
	public String toString() {
		return "PromoData{" +
				"code='" + code + '\'' +
				", departmentId=" + departmentId +
				", description=" + description +
				", id=" + id +
				", type=" + type +
				", value=" + value +
				'}';
	}
}