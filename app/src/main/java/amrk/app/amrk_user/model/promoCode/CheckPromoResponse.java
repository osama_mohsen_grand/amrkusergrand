package amrk.app.amrk_user.model.promoCode;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class CheckPromoResponse extends StatusMessage {
    @SerializedName("data")
    @Expose
    private PromoData promoData;

    public PromoData getPromoData() {
        return promoData;
    }

}