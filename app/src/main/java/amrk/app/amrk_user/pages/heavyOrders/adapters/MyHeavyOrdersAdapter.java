package amrk.app.amrk_user.pages.heavyOrders.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemHeavyOderBinding;
import amrk.app.amrk_user.pages.chat.view.ChatFragment;
import amrk.app.amrk_user.pages.myOrders.models.MyOrdersData;
import amrk.app.amrk_user.pages.myOrders.viewModels.ItemMyStoreOrdersViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;


public class MyHeavyOrdersAdapter extends RecyclerView.Adapter<MyHeavyOrdersAdapter.MenuView> {
    List<MyOrdersData> myOrdersDataList;
    private Context context;
    MutableLiveData<Object> rateMandoubLiveData = new MutableLiveData<>();
    public int position;

    public MyHeavyOrdersAdapter() {
        this.myOrdersDataList = new ArrayList<>();
    }

    public List<MyOrdersData> getMyOrdersDataList() {
        return myOrdersDataList;
    }

    public MutableLiveData<Object> getRateMandoubLiveData() {
        return rateMandoubLiveData;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_heavy_oder,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @SuppressLint("RecyclerView")
    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        MyOrdersData menuModel = myOrdersDataList.get(position);
        ItemMyStoreOrdersViewModel itemMenuViewModel = new ItemMyStoreOrdersViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            this.position = position;
            if (o.equals(Constants.CHAT)) {
                if (menuModel.getDelegate() != null)
                    MovementHelper.startActivityWithBundle(context, new PassingObject(menuModel.getId(), String.valueOf(menuModel.getDelegate().getId())), null, ChatFragment.class.getName(), null, null);
            } else if (o.equals(Constants.RATE_MANDOUB)) {
                rateMandoubLiveData.setValue(o);
            } else if (o.equals(Constants.CANCEL_ORDER)) {
                rateMandoubLiveData.setValue(o);
            } else if (o.equals(Constants.REMOVE_DIALOG_WARNING)) {
                rateMandoubLiveData.setValue(o);
            }
        });
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<MyOrdersData> dataList) {
        this.myOrdersDataList.clear();
        myOrdersDataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return myOrdersDataList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemHeavyOderBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemMyStoreOrdersViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemOrderViewModel(itemViewModels);
            }
        }
    }
}
