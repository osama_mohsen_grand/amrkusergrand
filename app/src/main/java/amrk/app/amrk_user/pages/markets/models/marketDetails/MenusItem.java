package amrk.app.amrk_user.pages.markets.models.marketDetails;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class MenusItem{

	@SerializedName("name")
	private String name;

	@SerializedName("id")
	private int id;

	@SerializedName("products")
	private List<ProductsItem> products;

	public String getName(){
		return name;
	}

	public int getId(){
		return id;
	}

	public List<ProductsItem> getProducts(){
		return products;
	}
}