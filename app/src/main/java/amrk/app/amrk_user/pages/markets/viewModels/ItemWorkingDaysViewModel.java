package amrk.app.amrk_user.pages.markets.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.markets.models.marketDetails.DaysItem;

public class ItemWorkingDaysViewModel extends BaseViewModel {
    public DaysItem daysItem;

    public ItemWorkingDaysViewModel(DaysItem daysItem) {
        this.daysItem = daysItem;
    }

    @Bindable
    public DaysItem getDaysItem() {
        return daysItem;
    }
}
