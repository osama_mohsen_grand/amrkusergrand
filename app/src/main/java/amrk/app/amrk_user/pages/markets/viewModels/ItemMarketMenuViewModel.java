package amrk.app.amrk_user.pages.markets.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.markets.adapters.MarketMenuDetailsAdapter;
import amrk.app.amrk_user.pages.markets.models.marketDetails.MenusItem;
import amrk.app.amrk_user.utils.Constants;

public class ItemMarketMenuViewModel extends BaseViewModel {
    public MenusItem menuModel;
    MarketMenuDetailsAdapter menuDetailsAdapter;

    public ItemMarketMenuViewModel(MenusItem menuModel) {
        this.menuModel = menuModel;
        menuDetailsAdapter = new MarketMenuDetailsAdapter();
        menuDetailsAdapter.update(menuModel.getProducts());
    }

    @Bindable
    public MenusItem getMenuModel() {
        return menuModel;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

    public MarketMenuDetailsAdapter getMenuDetailsAdapter() {
        return menuDetailsAdapter;
    }
}
