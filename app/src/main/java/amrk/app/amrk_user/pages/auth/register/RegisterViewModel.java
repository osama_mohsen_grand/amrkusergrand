package amrk.app.amrk_user.pages.auth.register;


import android.widget.CompoundButton;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.models.RegisterRequest;
import amrk.app.amrk_user.repository.AuthRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import amrk.app.amrk_user.utils.validation.Validate;
import io.reactivex.disposables.CompositeDisposable;

public class RegisterViewModel extends BaseViewModel {
    MutableLiveData<Mutable> liveData;
    FileObjects fileObject;
    String cpp;
    @Inject
    AuthRepository repository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    RegisterRequest request;
    boolean isTermsAccepted = false;

    @Inject
    public RegisterViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        getRequest().setGender("0");
    }

    public void register() {
        getRequest().setCountry_id(188);
        getRequest().setToken(UserHelper.getInstance(MyApplication.getInstance()).getToken());
        if (getRequest().isValid()) {
            if (fileObject != null) {
//                if (!getRequest().getPhone().contains(cpp)) {
//                    getRequest().setPhone(cpp + getRequest().getPhone());
//                }
                if (Validate.isMatchPassword(getRequest().getPassword(), getRequest().getConfirmPassword()))
                    if (isTermsAccepted) {
                        setMessage(Constants.SHOW_PROGRESS);
                        compositeDisposable.add(repository.register(getRequest(), fileObject));
                    } else
                        liveData.setValue(new Mutable(Constants.TERMS_ACCEPTED));
                else
                    liveData.setValue(new Mutable(Constants.NOT_MATCH_PASSWORD));
            } else {
                liveData.setValue(new Mutable(Constants.NEW_IMAGE));
            }
        }
    }

    public void onCheckedChange(CompoundButton button, boolean check) {
        isTermsAccepted = check;
    }

    public void imageSubmit() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    public void toTerms() {
        liveData.setValue(new Mutable(Constants.TERMS));
    }

    public void toPrivacy() {
        liveData.setValue(new Mutable(Constants.PRIVACY));
    }

    public void toSelectLocation() {
        liveData.setValue(new Mutable(Constants.PICK_UP_LOCATION));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    @Bindable
    public RegisterRequest getRequest() {
        return this.request == null ? this.request = new RegisterRequest() : this.request;
    }

    public void setImage(FileObjects fileObject) {
        this.fileObject = fileObject;
    }

}
