package amrk.app.amrk_user.pages.myOrders.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.myOrders.models.orders.OrderProductsItem;

public class ItemMyStoreOrderDetailsViewModel extends BaseViewModel {
    public OrderProductsItem orderProductsItem;

    public ItemMyStoreOrderDetailsViewModel(OrderProductsItem orderProductsItem) {
        this.orderProductsItem = orderProductsItem;
    }

    @Bindable
    public OrderProductsItem getOrderProductsItem() {
        return orderProductsItem;
    }

}
