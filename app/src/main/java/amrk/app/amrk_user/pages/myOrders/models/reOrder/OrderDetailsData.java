package amrk.app.amrk_user.pages.myOrders.models.reOrder;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.pages.auth.models.UserData;
import amrk.app.amrk_user.pages.home.model.Shop;
import amrk.app.amrk_user.pages.myOrders.models.OrderStatus;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.OrderImages;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;

public class OrderDetailsData {

    @SerializedName("notes")
    private String notes;
    @SerializedName("currency")
    private String currency;

    @SerializedName("total_cost")
    private String totalCost;

    @SerializedName("order_number")
    private String orderNumber;

    @SerializedName("delegate_comment")
    private String delegateComment;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("delivery_time")
    private String deliveryTime;

    @SerializedName("title")
    private String title;
    @SerializedName("distance")
    private String distance;

    @SerializedName("order_status")
    private OrderStatus orderStatus;

    @SerializedName("shop_rate")
    private String shopRate;

    @SerializedName("order_images")
    private List<OrderImages> orderImages;
    @SerializedName("order_products")
    private List<ProductDetails> orderProducts;
    @SerializedName("shop")
    private Shop shop;

    @SerializedName("id")
    private int id;

    @SerializedName("out_city_name")
    private String outCityName;

    @SerializedName("out_address")
    private String outAddress;

    @SerializedName("in_lat")
    private String inLat;

    @SerializedName("shop_comment")
    private String shopComment;

    @SerializedName("out_lat")
    private String outLat;

    @SerializedName("department_id")
    private int departmentId;
    @SerializedName("sub_department_id")
    private String sub_department_id;

    @SerializedName("delegate_rate")
    private String delegateRate;

    @SerializedName("delegate_id")
    private Object delegateId;

    @SerializedName("in_lng")
    private String inLng;

    @SerializedName("promo_id")
    private int promoId;

    @SerializedName("user_rate")
    private String userRate;

    @SerializedName("shop_id")
    private Object shopId;

    @SerializedName("in_address")
    private String inAddress;

    @SerializedName("in_city_name")
    private String inCityName;

    @SerializedName("confirm_code")
    private String confirmCode;

    @SerializedName("out_lng")
    private String outLng;
    @SerializedName("user")
    private UserData user;

    @SerializedName("user_comment")
    private String userComment;
    @SerializedName("last_lat")
    private double last_lat;
    @SerializedName("last_lng")
    private double last_lng;
    @SerializedName("last_address")
    private String last_address;
    @SerializedName("last_city_name")
    private String last_city_name;

    public String getNotes() {
        return notes;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getDelegateComment() {
        return delegateComment;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public String getTitle() {
        return title;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public String getShopRate() {
        return shopRate;
    }

    public List<OrderImages> getOrderImages() {
        return orderImages;
    }

    public int getId() {
        return id;
    }

    public String getOutCityName() {
        return outCityName;
    }

    public String getOutAddress() {
        return outAddress;
    }

    public String getInLat() {
        return inLat;
    }

    public String getShopComment() {
        return shopComment;
    }

    public String getOutLat() {
        return outLat;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public String getDelegateRate() {
        return delegateRate;
    }

    public Object getDelegateId() {
        return delegateId;
    }

    public String getInLng() {
        return inLng;
    }

    public int getPromoId() {
        return promoId;
    }

    public String getUserRate() {
        return userRate;
    }

    public Object getShopId() {
        return shopId;
    }

    public String getInAddress() {
        return inAddress;
    }

    public String getInCityName() {
        return inCityName;
    }

    public String getConfirmCode() {
        return confirmCode;
    }

    public String getOutLng() {
        return outLng;
    }

    public String getUserComment() {
        return userComment;
    }

    public List<ProductDetails> getOrderProducts() {
        return orderProducts;
    }

    public Shop getShop() {
        return shop;
    }

    public String getSub_department_id() {
        return sub_department_id;
    }

    public double getLast_lat() {
        return last_lat;
    }

    public double getLast_lng() {
        return last_lng;
    }

    public String getLast_address() {
        return last_address;
    }

    public String getLast_city_name() {
        return last_city_name;
    }

    public String getCurrency() {
        return currency;
    }

    public UserData getUser() {
        return user;
    }

    public String getDistance() {
        return distance;
    }

    public void setOrderStatus(OrderStatus orderStatus) {
        this.orderStatus = orderStatus;
    }


}