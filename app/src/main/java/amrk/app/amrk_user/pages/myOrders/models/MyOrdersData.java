package amrk.app.amrk_user.pages.myOrders.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.pages.auth.models.UserData;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.myOrders.models.orders.OrderProductsItem;
import amrk.app.amrk_user.pages.publicOrder.models.subDepartment.SubDepartmentData;

public class MyOrdersData {

    @SerializedName("notes")
    private String notes;

    @SerializedName("shop")
    private ShopsItem shop;

    @SerializedName("total_cost")
    private String totalCost;

    @SerializedName("distance")
    private String distance;

    @SerializedName("order_number")
    private String orderNumber;

    @SerializedName("delegate_comment")
    private String delegateComment;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("delivery_time")
    private String deliveryTime;

    @SerializedName("title")
    private String title;

    @SerializedName("delegate")
    private UserData delegate;

    @SerializedName("order_status")
    private OrderStatus orderStatus;

    @SerializedName("order_products")
    private List<OrderProductsItem> orderProducts;

    @SerializedName("shop_rate")
    private String shopRate;

    @SerializedName("order_images")
    private List<OrderImagesItem> orderImages;

    @SerializedName("id")
    private int id;

    @SerializedName("out_city_name")
    private String outCityName;

    @SerializedName("out_address")
    private String outAddress;

    @SerializedName("in_lat")
    private String inLat;

    @SerializedName("shop_comment")
    private String shopComment;

    @SerializedName("out_lat")
    private String outLat;

    @SerializedName("department_id")
    private int departmentId;

    @SerializedName("delegate_rate")
    private String delegateRate;

    @SerializedName("counter")
    private int counter;

    @SerializedName("delegate_id")
    private int delegateId;

    @SerializedName("in_lng")
    private String inLng;

    @SerializedName("promo_id")
    private int promoId;

    @SerializedName("offer_id")
    private int offerId;

    @SerializedName("user_rate")
    private String userRate;

    @SerializedName("shop_id")
    private int shopId;

    @SerializedName("in_address")
    private String inAddress;

    @SerializedName("in_city_name")
    private String inCityName;

    @SerializedName("confirm_code")
    private String confirmCode;
    @SerializedName("cancel_warning")
    private String cancel_warning;

    @SerializedName("out_lng")
    private String outLng;

    @SerializedName("user_comment")
    private String userComment;
    @SerializedName("currency")
    private String currency;
    @SerializedName("sub_department_id")
    private String sub_department_id;
    @SerializedName("sub_department")
    private SubDepartmentData sub_department;
    @SerializedName("offer")
    private OrderOffer offer;

    public String getCurrency() {
        return currency;
    }

    public String getNotes() {
        return notes;
    }

    public ShopsItem getShop() {
        return shop;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public String getDistance() {
        return distance;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public String getDelegateComment() {
        return delegateComment;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public String getTitle() {
        return title;
    }

    public UserData getDelegate() {
        return delegate;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public List<OrderProductsItem> getOrderProducts() {
        return orderProducts;
    }

    public String getShopRate() {
        return shopRate;
    }

    public List<OrderImagesItem> getOrderImages() {
        return orderImages;
    }

    public int getId() {
        return id;
    }

    public String getOutCityName() {
        return outCityName;
    }

    public String getOutAddress() {
        return outAddress;
    }

    public String getInLat() {
        return inLat;
    }

    public String getShopComment() {
        return shopComment;
    }

    public String getOutLat() {
        return outLat;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public String getDelegateRate() {
        return delegateRate;
    }

    public int getCounter() {
        return counter;
    }

    public int getDelegateId() {
        return delegateId;
    }

    public String getInLng() {
        return inLng;
    }

    public int getPromoId() {
        return promoId;
    }

    public int getOfferId() {
        return offerId;
    }

    public String getUserRate() {
        return userRate;
    }

    public int getShopId() {
        return shopId;
    }

    public String getInAddress() {
        return inAddress;
    }

    public String getInCityName() {
        return inCityName;
    }

    public String getConfirmCode() {
        return confirmCode;
    }

    public String getOutLng() {
        return outLng;
    }

    public String getUserComment() {
        return userComment;
    }

    public String getSub_department_id() {
        return sub_department_id;
    }

    public SubDepartmentData getSub_department() {
        return sub_department;
    }

    public String getCancel_warning() {
        return cancel_warning;
    }

    public OrderOffer getOffer() {
        return offer;
    }
}