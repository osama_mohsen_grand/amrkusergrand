package amrk.app.amrk_user.pages.offers;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;

import org.jetbrains.annotations.NotNull;

import java.util.HashMap;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.CancelOrderWarningDialogBinding;
import amrk.app.amrk_user.databinding.DelegateReviewsDialogBinding;
import amrk.app.amrk_user.databinding.FragmentOffersDetailsBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.chat.view.ChatFragment;
import amrk.app.amrk_user.pages.offers.models.offerDetails.OfferDetails;
import amrk.app.amrk_user.pages.offers.models.offerDetails.OfferDetailsResponse;
import amrk.app.amrk_user.pages.offers.viewModels.OffersViewModel;
import amrk.app.amrk_user.pages.reviews.ReviewsFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.services.RealTimeReceiver;
import amrk.app.amrk_user.utils.session.LanguagesHelper;

public class OffersDetailsFragment extends BaseFragment implements RealTimeReceiver.OfferDetailsReceiverListener {

    private Context context;
    FragmentOffersDetailsBinding binding;
    @Inject
    OffersViewModel viewModel;
    Dialog cancelDialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offers_details, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.oneOffer();
        }
        setEvent();
        connectPusher(Constants.NEWOFFEREventName, Constants.USERSBeamName.concat(viewModel.userData.getJwt()), Constants.NEWOFFERChannelName.concat(viewModel.userData.getJwt()));
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (mutable.message) {
                case Constants.OFFERS:
                    viewModel.getDetailsAdapter().update(((OfferDetailsResponse) mutable.object).getData());
                    viewModel.notifyChange(BR.detailsAdapter);
                    break;
                case Constants.LESS_OFFER:
                    viewModel.getDetailsAdapter().getOfferDetailsList().clear();
                    viewModel.getDetailsAdapter().resetItem(); // reset your item enable
                    viewModel.notifyChange();
                    break;
                case Constants.CANCEL_ORDER:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    Constants.DATA_CHANGED = true;
                    if (cancelDialog != null)
                        cancelDialog.dismiss();
                    finishActivity();
                    break;
                case Constants.MARKET_REVIEWS:
                    MovementHelper.startActivityWithBundle(context, new PassingObject(viewModel.getOfferDetails().getDelegate().getId(), Constants.DELEGATE_REVIEWS), getResources().getString(R.string.client_reviews), ReviewsFragment.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.ACCEPT_OFFER:
                    int id = viewModel.getDetailsAdapter().getOfferDetailsList().get(viewModel.getDetailsAdapter().lastPosition).getDelegate().getId();
                    MovementHelper.startActivityWithBundle(context, new PassingObject(viewModel.getPassingObject().getId(), String.valueOf(id)), null, ChatFragment.class.getName(), null, null);
                    Constants.DATA_CHANGED = true;
                    finishActivity();
                    break;
                case Constants.SERVER_ERROR:
                    viewModel.getDetailsAdapter().resetItem(); // reset your item enable
                    break;
            }
        });
        viewModel.getDetailsAdapter().getLiveDataAdapter().observe((LifecycleOwner) context, o -> {
            if (o.equals(Constants.LESS_OFFER)) {
                viewModel.lessOffer();
            } else if (o.equals(Constants.PROMO_DIALOG)) {
                viewModel.setOfferDetails(viewModel.getDetailsAdapter().getOfferDetailsList().get(viewModel.getDetailsAdapter().lastPosition));
                showDelegateDialog();
            } else if (o.equals(Constants.CANCEL_ORDER)) {
                if (!TextUtils.isEmpty(viewModel.getDetailsAdapter().getOfferDetailsList().get(viewModel.getDetailsAdapter().lastPosition).getCancel_warning()))
                    showCancelDialog(viewModel.getDetailsAdapter().lastPosition);
                else
                    showAlertDialogWithAutoDismiss();
            } else if (o.equals(Constants.ACCEPT_OFFER)) {
                viewModel.acceptOrder(viewModel.getDetailsAdapter().getOfferDetailsList().get(viewModel.getDetailsAdapter().lastPosition).getId());
            }
        });
    }

    public void connectPusher(String eventName, String beamName, String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("75b53060b1f415501d21", options);
        pusher.connect();
        Channel channel = pusher.subscribe(channelName);
        channel.bind(eventName, event -> {
            Log.e("connectPusher", "connectPusher: "+event.getData() );
            OfferDetailsResponse chatSendResponse = new Gson().fromJson(event.getData(), OfferDetailsResponse.class);
            if (chatSendResponse.getOfferDetailsPusher() != null && chatSendResponse.getOfferDetailsPusher().getOrderId() == viewModel.getPassingObject().getId()) {
                if (viewModel.getDetailsAdapter().getItemCount() == 0)
                    viewModel.notifyChange(BR.detailsAdapter);
               viewModel.getDetailsAdapter().updateNewOffer(chatSendResponse.getOfferDetailsPusher());
            }
        });
        PushNotifications.start(MyApplication.getInstance(), "4b9224fd-ac88-4e17-86bf-65c347bc0fbd");
        PushNotifications.clearAllState();
        PushNotifications.addDeviceInterest(Constants.INTEREST);
        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(Constants.PUSHER_TOKEN_URL, () -> {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("jwt", LanguagesHelper.getJwt());
            HashMap<String, String> queryParams = new HashMap<>();
            return new AuthData(
                    headers,
                    queryParams
            );
        });
        PushNotifications.setUserId(beamName, tokenProvider, new BeamsCallback<>() {
            @Override
            public void onSuccess(@NonNull Void... values) {
                Log.e("PusherBeams", "Successfully authenticated with Pusher Beams");
            }

            @Override
            public void onFailure(PusherCallbackError error) {

                Log.e("PusherBeams", "Pusher Beams authentication failed: " + error.getMessage());
            }
        });

    }

    private void showDelegateDialog() {
        Dialog delegateDialog = new Dialog(context, R.style.PauseDialog);
        delegateDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(delegateDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        delegateDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DelegateReviewsDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(delegateDialog.getContext()), R.layout.delegate_reviews_dialog, null, false);
        delegateDialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        delegateDialog.show();
    }

    private void showCancelDialog(int position) {
        cancelDialog = new Dialog(context, R.style.PauseDialog);
        cancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(cancelDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        cancelDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CancelOrderWarningDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(cancelDialog.getContext()), R.layout.cancel_order_warning_dialog, null, false);
        cancelDialog.setContentView(binding.getRoot());
        binding.msg.setText(viewModel.getDetailsAdapter().getOfferDetailsList().get(position).getCancel_warning());
        binding.yes.setOnClickListener(v -> viewModel.cancelOrder());
        binding.no.setOnClickListener(v -> {
            viewModel.getDetailsAdapter().resetItem(); // reset your item enable
            cancelDialog.dismiss();
        });
        cancelDialog.show();
    }

    public void showAlertDialogWithAutoDismiss() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setMessage(getString(R.string.cancel_order_warning))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> {
                    viewModel.getDetailsAdapter().resetItem(); // reset your item enable
                    dialog.cancel();
                })
                .setPositiveButton(getString(R.string.send), (dialog, id) -> {
                    viewModel.cancelOrder();
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getMarketRepository().setLiveData(viewModel.liveData);
        MyApplication.getInstance().setOfferReceiverListener(this);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onNewOrderChanged(OfferDetails offerDetails) {
        if (offerDetails != null && offerDetails.getOrderId() == viewModel.getPassingObject().getId()) {
            if (viewModel.getDetailsAdapter().getItemCount() == 0)
                viewModel.notifyChange(BR.detailsAdapter);
            viewModel.getDetailsAdapter().getOfferDetailsList().add(0, offerDetails);
            viewModel.getDetailsAdapter().notifyDataSetChanged();
        }
    }
}
