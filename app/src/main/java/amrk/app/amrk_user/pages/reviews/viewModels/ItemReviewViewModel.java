package amrk.app.amrk_user.pages.reviews.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.customViews.menu.MenuModel;
import amrk.app.amrk_user.utils.Constants;

public class ItemReviewViewModel extends BaseViewModel {
    public MenuModel menuModel;

    public ItemReviewViewModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }

    @Bindable
    public MenuModel getMenuModel() {
        return menuModel;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
