
package amrk.app.amrk_user.pages.chatAdmin.viewmodel;

import android.text.TextUtils;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.chatAdmin.adapter.ChatAdminAdapter;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdminRequest;
import amrk.app.amrk_user.repository.ChatRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class ChatAdminViewModel extends BaseViewModel {
    public
    MutableLiveData<Mutable> liveData;
    @Inject
    public ChatRepository repository;
    public ChatAdminAdapter adapter = new ChatAdminAdapter();
    public ChatAdminRequest request = new ChatAdminRequest();
    public List<FileObjects> fileObjectList = new ArrayList<>();
    CompositeDisposable compositeDisposable = new CompositeDisposable();

    @Inject
    public ChatAdminViewModel(ChatRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void chat() {
        compositeDisposable.add(repository.getAdminChat());
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public void select() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    public void sendMessage() {
        if (fileObjectList.size() > 0 || !TextUtils.isEmpty(request.getMessage())) {
            repository.sendAdminChat(request, fileObjectList);
        }
    }
}
