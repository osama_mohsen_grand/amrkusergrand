package amrk.app.amrk_user.pages.gifts;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentGiftsBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.gifts.models.ReplacePointsResponse;
import amrk.app.amrk_user.pages.gifts.viewModels.GiftViewModel;
import amrk.app.amrk_user.utils.Constants;

public class GiftsFragment extends BaseFragment {

    private Context context;
    private FragmentGiftsBinding binding;
    @Inject
    GiftViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_gifts, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.gifts();
        setEvent();

        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (mutable.message.equals(Constants.GIFTS)) {
                viewModel.setReplacedObject(((ReplacePointsResponse) mutable.object).getData());
                viewModel.notifyChange(BR.offersAdapter);
            } else if (mutable.message.equals(Constants.PICKED_SUCCESSFULLY)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                finishActivity();
            }
        });
        viewModel.getGiftsAdapter().getLiveDataAdapter().observe((LifecycleOwner) context, integer -> {
            if (Integer.parseInt(viewModel.getReplacedObject().getUser_points()) >= Integer.parseInt(viewModel.getGiftsAdapter().getReplacedPointsDataList().get(integer).getPoints())) {
                viewModel.replacePoints(viewModel.getGiftsAdapter().getReplacedPointsDataList().get(integer).getId());
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getSettingsRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
