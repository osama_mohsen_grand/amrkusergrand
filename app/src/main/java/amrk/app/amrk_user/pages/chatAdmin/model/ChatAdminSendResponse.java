package amrk.app.amrk_user.pages.chatAdmin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import amrk.app.amrk_user.model.base.StatusMessage;
public class ChatAdminSendResponse extends StatusMessage {

    @SerializedName("data")
    @Expose
    private ChatAdmin data;

    public ChatAdmin getData() {
        return data;
    }
}
