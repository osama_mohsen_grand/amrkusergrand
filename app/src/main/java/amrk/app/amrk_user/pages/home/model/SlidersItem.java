package amrk.app.amrk_user.pages.home.model;

import com.google.gson.annotations.SerializedName;

public class SlidersItem{

	@SerializedName("image")
	private String image;

	@SerializedName("shop")
	private Shop shop;

	@SerializedName("active")
	private int active;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	public String getImage(){
		return image;
	}

	public Shop getShop(){
		return shop;
	}

	public int getActive(){
		return active;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}
}