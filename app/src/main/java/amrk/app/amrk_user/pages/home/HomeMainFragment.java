package amrk.app.amrk_user.pages.home;

import android.app.Activity;
import android.content.Context;
import android.content.IntentSender;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentMainHomeBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.appWallet.AppWalletFragment;
import amrk.app.amrk_user.pages.home.viewModels.HomeViewModel;
import amrk.app.amrk_user.pages.myAccount.MyAccountFragment;
import amrk.app.amrk_user.pages.offers.OffersFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.ImmediateUpdateActivity;
import amrk.app.amrk_user.utils.helper.MovementHelper;

import static amrk.app.amrk_user.utils.ImmediateUpdateActivity.UPDATE_REQUEST_CODE;

public class HomeMainFragment extends BaseFragment {
    private Context context;
    @Inject
    HomeViewModel viewModel;
    FragmentMainHomeBinding binding;
    ImmediateUpdateActivity immediateUpdateActivity;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_main_home, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        binding.homeNavigationMenu.setItemIconTintList(null);
        MovementHelper.replaceHomeFragment(context, new HomeFragment(), "");
        immediateUpdateActivity = new ImmediateUpdateActivity((Activity) context);
        setEvent();
        onBackPressed();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.MENU_HOME:
                    MovementHelper.replaceHomeFragment(context, new HomeFragment(), "");
                    break;
                case Constants.MENU_OFFERS:
                    MovementHelper.replaceHomeFragment(context, new OffersFragment(), "");
                    break;
                case Constants.MENU_WALLET:
                    MovementHelper.replaceHomeFragment(context, new AppWalletFragment(), "");
                    break;
                case Constants.MENU_ACCOUNT:
                    MovementHelper.replaceHomeFragment(context, new MyAccountFragment(), "");
                    break;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) context).enableRefresh(false);
        updateAuto();
    }

    private void updateAuto() {
        immediateUpdateActivity.getAppUpdateManager().getAppUpdateInfo().addOnSuccessListener(it -> {
            if (it.updateAvailability() == UpdateAvailability.DEVELOPER_TRIGGERED_UPDATE_IN_PROGRESS) {
                try {
                    immediateUpdateActivity.getAppUpdateManager().startUpdateFlowForResult(it, AppUpdateType.IMMEDIATE, (Activity) context, UPDATE_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    private void onBackPressed() {
        binding.getRoot().setFocusableInTouchMode(true);
        binding.getRoot().requestFocus();
        binding.getRoot().setOnKeyListener((v, keyCode, event) -> {
            //This is the filter
            if (event.getAction() != KeyEvent.ACTION_DOWN) {
                showAlertExit();
                return true;
            }
            return false;
        });

    }

    public void showAlertExit() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setMessage(getString(R.string.exit_app))
                .setCancelable(false)
                .setNeutralButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel())
                .setPositiveButton(getString(R.string.yes), (dialog, id) -> {
                    finishActivity();
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
