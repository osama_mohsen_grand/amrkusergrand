package amrk.app.amrk_user.pages.markets;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.widget.NestedScrollView;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentMarketDetailsBinding;
import amrk.app.amrk_user.databinding.WorkingDaysDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.cart.CartFragment;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.markets.models.marketDetails.MarketDetailsResponse;
import amrk.app.amrk_user.pages.markets.models.marketDetails.MenusItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.pages.markets.viewModels.MarketDetailsViewModel;
import amrk.app.amrk_user.pages.reviews.ReviewsFragment;
import amrk.app.amrk_user.repository.CartRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.UserHelper;


public class MarketDetailsFragment extends BaseFragment {
    private FragmentMarketDetailsBinding binding;
    @Inject
    MarketDetailsViewModel viewModel;
    boolean fromScroll = false;
    int i = 0;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_market_details, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setShopsItem(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), ShopsItem.class));
            viewModel.getMarketDetails();
        }
        UserHelper.getInstance(requireActivity()).resetTemp();
        setEvent();
        return binding.getRoot();
    }


    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.GET_MARKET_DETAILS:
                    viewModel.setDetails(((MarketDetailsResponse) ((Mutable) o).object).getMarketDetails());
                    break;
                case Constants.GET_MARKET_WORKING_DAYS:
                    showTimeDialog();
                    break;
                case Constants.MARKET_REVIEWS:
                    MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getDetails().getId(), Constants.GET_MARKET_DETAILS), getResources().getString(R.string.client_reviews), ReviewsFragment.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.TO_CART:
                    MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getShopsItem()), getResources().getString(R.string.new_order_bar), CartFragment.class.getName(), null, Constants.MARKETS);
                    break;

            }
        });
        getActivityBase().connectionMutableLiveData.observe(requireActivity(), isConnected -> {
            if (isConnected)
                viewModel.getMarketDetails();
        });
        viewModel.getMenuCategoryAdapter().getLiveDataAdapter().observe( requireActivity(), integer -> {
            float y = binding.marketsDetailsMenu.getY() + binding.marketsDetailsMenu.getChildAt(integer).getY();
            binding.pageScroll.smoothScrollTo(0, (int) y);
        });
        binding.pageScroll.setOnScrollChangeListener((NestedScrollView.OnScrollChangeListener) (v, scrollX, scrollY, oldScrollX, oldScrollY) -> {

            float y = binding.marketsDetailsMenu.getY();
            if (y >= scrollY) {
                binding.marketsDetailsMenuCategories.setVisibility(View.GONE);
            } else
                binding.marketsDetailsMenuCategories.setVisibility(View.VISIBLE);


            if (fromScroll) {
                int showed = 0;
                for (i = 0; i < binding.marketsDetailsMenu.getChildCount(); i++) {
                    Rect scrollBounds = new Rect();
                    v.getHitRect(scrollBounds);
                    if (binding.marketsDetailsMenu.getChildAt(i).getLocalVisibleRect(scrollBounds)) {
                        showed = i;
                        break;
                    }
                }
                viewModel.getMenuCategoryAdapter().setLastSelect(viewModel.getMenuCategoryAdapter().getMenuModels().get(showed).getId());
                binding.marketsDetailsMenuCategories.scrollToPosition(showed);
                viewModel.getMenuCategoryAdapter().notifyDataSetChanged();
            }
            fromScroll = true;
        });
        viewModel.getMarketMenuAdapter().getLiveDataAdapter().observe( requireActivity(), newQuantity -> viewModel.setTotalQuantity(viewModel.getTotalQuantity() + newQuantity));

        viewModel.getMenuCategoryAdapter().getLiveDataAdapter().observe( requireActivity(), integer -> {
            fromScroll = false;
            viewModel.getMenuCategoryAdapter().setLastSelect(viewModel.getMenuCategoryAdapter().getMenuModels().get(integer).getId());
            float y = binding.marketsDetailsMenu.getY() + binding.marketsDetailsMenu.getChildAt(integer).getY();
            binding.pageScroll.smoothScrollTo(0, (int) y);
        });
    }


    private void showTimeDialog() {
        Dialog dialog = new Dialog( requireActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        WorkingDaysDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.working_days_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        viewModel.getDaysAdapter().update(viewModel.getDetails().getDays());
        binding.workingDaysDone.setOnClickListener(view -> dialog.dismiss());
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
        if (Constants.REVIEWS_COUNT != 0) {
            viewModel.getDetails().setRates_count(String.valueOf(Constants.REVIEWS_COUNT));
            viewModel.setDetails(viewModel.getDetails());
            Constants.REVIEWS_COUNT = 0;
        }
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
        new CartRepository(MyApplication.getInstance()).emptyCart();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new CartRepository(MyApplication.getInstance()).emptyCart();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            Bundle bundle = data.getExtras();
            if (bundle != null) {
                ArrayList<ProductDetails> myList = bundle.getParcelableArrayList(Constants.BUNDLE);
                List<MenusItem> productsItemList = viewModel.getMarketMenuAdapter().menuModels;
                if (myList.size() > 0) {
                    for (int i = 0; i < myList.size(); i++) {
                        for (int j = 0; j < productsItemList.size(); j++) {
                            for (int k = 0; k < productsItemList.get(j).getProducts().size(); k++) {
                                if (myList.get(i).getProduct_id() == productsItemList.get(j).getProducts().get(k).getId()) {
                                    viewModel.setTotalQuantity(viewModel.getTotalQuantity() - productsItemList.get(j).getProducts().get(k).getQuantity()); // remove old quantity
                                    productsItemList.get(j).getProducts().get(k).setQuantity(myList.get(i).getQuantity());
                                    viewModel.setTotalQuantity(viewModel.getTotalQuantity() + productsItemList.get(j).getProducts().get(k).getQuantity()); // add new quantity
                                    viewModel.getMarketMenuAdapter().notifyDataSetChanged();
                                }
                            }
                        }
                    }
                } else {
                    viewModel.getMarketDetails();
                    viewModel.setTotalQuantity(0);
                }
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
