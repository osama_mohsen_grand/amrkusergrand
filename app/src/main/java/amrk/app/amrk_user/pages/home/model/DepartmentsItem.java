package amrk.app.amrk_user.pages.home.model;

import com.google.gson.annotations.SerializedName;

public class DepartmentsItem{

	@SerializedName("image")
	private String image;

	@SerializedName("name")
	private String name;

	@SerializedName("active")
	private int active;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	public String getImage(){
		return image;
	}

	public String getName(){
		return name;
	}

	public int getActive(){
		return active;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}
}