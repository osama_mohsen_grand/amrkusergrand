package amrk.app.amrk_user.pages.countries.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class CountriesResponse extends StatusMessage {
    @SerializedName("data")
    private List<Countries> countriesList;

    public List<Countries> getCountriesList() {
        return countriesList;
    }
}