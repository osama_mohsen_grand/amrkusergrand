package amrk.app.amrk_user.pages.myOrders.models.followOrder;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class FollowStatusResponse extends StatusMessage {
    @SerializedName("data")
    private FollowOrderData data;

    public FollowOrderData getData() {
        return data;
    }

}