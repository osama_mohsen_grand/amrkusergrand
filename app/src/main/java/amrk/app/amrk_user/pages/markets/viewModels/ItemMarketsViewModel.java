package amrk.app.amrk_user.pages.markets.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.utils.Constants;

public class ItemMarketsViewModel extends BaseViewModel {
    public ShopsItem shopsItem;

    public ItemMarketsViewModel(ShopsItem shopsItem) {
        this.shopsItem = shopsItem;
    }

    @Bindable
    public ShopsItem getShopsItem() {
        return shopsItem;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MARKET_DETAILS);
    }

}
