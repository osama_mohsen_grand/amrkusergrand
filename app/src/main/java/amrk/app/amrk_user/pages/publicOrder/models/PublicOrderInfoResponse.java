package amrk.app.amrk_user.pages.publicOrder.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class PublicOrderInfoResponse extends StatusMessage {
    @SerializedName("data")
    private PublicInfo publicInfo;

    public PublicInfo getPublicInfo() {
        return publicInfo;
    }

}