package amrk.app.amrk_user.pages.notifications.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.pages.myOrders.models.OrderStatus;

public class NotificationsData {

    @SerializedName("created_at")
    private String created_at;

    @SerializedName("body")
    private String body;

    @SerializedName("title")
    private String title;
    @SerializedName("image")
    private String image;
    @SerializedName("id")
    private int id;
    @SerializedName("type")
    private int type;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("order_id")
    private int order_id;
    @SerializedName("department_id")
    private int department_id;
    @SerializedName("order_type")
    private int order_type;
    @SerializedName("trip_status")
    private String tripStatus;
    @SerializedName("delegate_id")
    private String delegateId;
    @SerializedName("order_status")
    private OrderStatus orderStatus;

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getType() {
        return type;
    }

    public int getUser_id() {
        return user_id;
    }

    public int getOrder_id() {
        return order_id;
    }

    public int getDepartment_id() {
        return department_id;
    }

    public String getImage() {
        return image;
    }

    public int getOrder_type() {
        return order_type;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public String getTripStatus() {
        return tripStatus;
    }

    public String getDelegateId() {
        return delegateId;
    }
}