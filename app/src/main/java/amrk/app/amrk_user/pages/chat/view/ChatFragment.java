package amrk.app.amrk_user.pages.chat.view;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.android.material.bottomsheet.BottomSheetDialog;
import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;

import java.util.HashMap;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.databinding.CancelOrderWarningDialogBinding;
import amrk.app.amrk_user.databinding.ChatOptionsBinding;
import amrk.app.amrk_user.databinding.FragmentChatBinding;
import amrk.app.amrk_user.databinding.SuccessDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.chat.model.Chat;
import amrk.app.amrk_user.pages.chat.model.ChatResponse;
import amrk.app.amrk_user.pages.chat.model.ChatSendResponse;
import amrk.app.amrk_user.pages.chat.viewmodel.ChatViewModel;
import amrk.app.amrk_user.pages.home.HomeFragment;
import amrk.app.amrk_user.pages.home.HomeMainFragment;
import amrk.app.amrk_user.pages.myFatora.MyFatooraMethodFragment;
import amrk.app.amrk_user.pages.myOrders.FollowUpOrderFragment;
import amrk.app.amrk_user.pages.myOrders.ReorderFragment;
import amrk.app.amrk_user.pages.offers.OffersDetailsFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.services.RealTimeReceiver;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import amrk.app.amrk_user.utils.session.UserHelper;
import amrk.app.amrk_user.utils.upload.FileOperations;

public class ChatFragment extends BaseFragment implements RealTimeReceiver.MessageReceiverListener {
    private FragmentChatBinding binding;
    @Inject
    ChatViewModel viewModel;
    BottomSheetDialog sheetDialog;
    Dialog cancelDialog, dialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        Constants.DATA_CHANGED = false;
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.chat();
        }
        setEvent();
        connectPusher(Constants.ORDER_CHAT_EVENT, Constants.USERSBeamName.concat(viewModel.userData.getJwt()), Constants.ORDER_CHAT_CHANNEL.concat(viewModel.userData.getJwt()));
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (mutable.message.equals(Constants.CHAT)) {
                viewModel.setChatMain(((ChatResponse) mutable.object).getChatMain());
                if (viewModel.adapter.getChatList().size() > 0)
                    new Handler(Looper.getMainLooper()).postDelayed(() -> binding.rcChat.smoothScrollToPosition(viewModel.adapter.getChatList().size() - 1), 200);
            } else if (mutable.message.equals(Constants.IMAGE)) {
                pickImageDialogSelect();
            } else if (mutable.message.equals(Constants.CALL)) {
                AppHelper.openDialNumber(requireActivity(), viewModel.getChatMain().getDelegateInfo().getPhone());
            } else if (mutable.message.equals(Constants.FOLLOW_ORDER)) {
                if (!TextUtils.isEmpty(viewModel.getChatMain().getOrder_status().getOn_first_way())) {
                    sheetDialog.dismiss();
                    MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getPassingObject().getId()), getString(R.string.follow_chat_options), FollowUpOrderFragment.class.getName(), null, Constants.MARKETS);
                } else
                    toastErrorMessage(getString(R.string.delegate_not_moving_warning));
            } else if (mutable.message.equals(Constants.CHANGE_DELEGATE) || mutable.message.equals(Constants.CANCEL_ORDER)) {
                sheetDialog.dismiss();
                Constants.DATA_CHANGED = true;
                if (cancelDialog != null)
                    cancelDialog.dismiss();
                if (mutable.message.equals(Constants.CHANGE_DELEGATE)) {
                    MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getPassingObject().getId()), ResourceManager.getString(R.string.offers_title), OffersDetailsFragment.class.getName(), null, null);
                    finishActivity();
                } else
                    MovementHelper.startActivityBase(requireActivity(), HomeMainFragment.class.getName(), null, null);
            } else if (mutable.message.equals(Constants.SEND_CURRENT_LOCATION)) {
                sheetDialog.dismiss();
                MovementHelper.startMapActivityForResultWithBundle(requireActivity(), new PassingObject(getString(R.string.send_location_chat_options)));
            } else if (((Mutable) o).message.equals(Constants.SEND_MESSAGE)) {
                ChatSendResponse chatSendResponse = (ChatSendResponse) ((Mutable) o).object;
                viewModel.adapter.getChatList().add(chatSendResponse.getData());
                viewModel.fileObjectList.clear();
                binding.message.setText("");
                binding.message.setHint(getResources().getString(R.string.chat_hint));
                binding.rcChat.scrollToPosition(viewModel.adapter.getItemCount() - 1);
                viewModel.adapter.notifyItemChanged(viewModel.adapter.getItemCount() - 1);
            } else if (mutable.message.equals(Constants.CHAT_OPTIONS)) {
                ChatOptionsBinding sortBinding = DataBindingUtil.inflate(LayoutInflater.from(requireActivity()), R.layout.chat_options, null, false);
                sheetDialog = new BottomSheetDialog(requireActivity());
                sheetDialog.setContentView(sortBinding.getRoot());
                sortBinding.setViewModel(viewModel);
                sheetDialog.show();
            } else if (mutable.message.equals(Constants.DISMISS)) {
                sheetDialog.dismiss();
            } else if (mutable.message.equals(Constants.REMOVE_DIALOG_WARNING)) {
                sheetDialog.dismiss();
                if (!TextUtils.isEmpty(viewModel.getChatMain().getCancel_warning()))
                    showCancelDialog();
                else
                    showAlertCancel();
            } else if (mutable.message.equals(Constants.REORDER)) {
                finishActivity();
                MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getPassingObject().getId()), getString(R.string.public_order_details), ReorderFragment.class.getName(), null, Constants.MARKETS);
            } else if (mutable.message.equals(Constants.CONFIRM_ORDER)) {
                ChatSendResponse chatSendResponse = (ChatSendResponse) ((Mutable) o).object;
                viewModel.adapter.notifyItemChanged(viewModel.adapter.getItemCount() - 1);
                if (chatSendResponse.getData().getConfirm() == Constants.FINISH_CHAT) { //confirm rejected
                    Constants.DATA_CHANGED = true;
                    finishActivity();
                } else
                    viewModel.chat();
            } else if (((Mutable) o).message.equals(Constants.PUSHER_LIVE_DATA)) {
                binding.rcChat.scrollToPosition(viewModel.adapter.getChatList().size() - 1);
            } else if (((Mutable) o).message.equals(Constants.HOME)) {
                finishActivity();
                MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getPassingObject().getId()), ResourceManager.getString(R.string.offers_title), OffersDetailsFragment.class.getName(), null, null);
            } else if (((Mutable) o).message.equals(Constants.SHOW_SUCCESS_DIALOG)) {
                showSuccessDialog();
            } else if (((Mutable) o).message.equals(Constants.CONFIRM_PAYMENT)) {
                viewModel.chat();
            }
        });
        viewModel.adapter.getChatObjectMutableLiveData().observe(requireActivity(), o -> {
            Log.e(TAG, "setEvent: "+UserHelper.getInstance(requireActivity()).getSupplier() );
            if (o.equals(Constants.ACCEPT_ORDER))
                viewModel.changeOrderConfirm(2);
            else if (o.equals(Constants.REJECT_ORDER))
                viewModel.changeOrderConfirm(0);
            else if (o.equals(Constants.ONLINE))
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(3, UserHelper.getInstance(requireActivity()).getTempTotal(), String.valueOf(viewModel.getPassingObject().getId())), getString(R.string.cash), MyFatooraMethodFragment.class.getName(), null, Constants.MARKETS);
            else if (o.equals(Constants.CASH))
                viewModel.confirmPayment(0);

        });
    }

    private void showCancelDialog() {
        cancelDialog = new Dialog(requireActivity(), R.style.PauseDialog);
        cancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(cancelDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        cancelDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CancelOrderWarningDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(cancelDialog.getContext()), R.layout.cancel_order_warning_dialog, null, false);
        cancelDialog.setContentView(binding.getRoot());
        binding.msg.setText(viewModel.getChatMain().getCancel_warning());
        binding.yes.setOnClickListener(v -> viewModel.cancelOrder());
        binding.no.setOnClickListener(v -> cancelDialog.dismiss());
        cancelDialog.show();
    }

    private void showSuccessDialog() {
        dialog = new Dialog(requireActivity(), R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        SuccessDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.success_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        binding.msg.setText(getString(R.string.order_delivered));
        binding.userName.setVisibility(View.GONE);
        dialog.setOnDismissListener(dialog -> {
            dialog.dismiss();
            UserHelper.getInstance(requireActivity()).addSupplier(0);
            MovementHelper.startActivityBase(requireActivity(), HomeMainFragment.class.getName(), null, null);
        });
        dialog.show();
    }

    public void showAlertCancel() {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder
                .setMessage(getString(R.string.cancel_order_warning))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel())
                .setPositiveButton(getString(R.string.send), (dialog, id) -> {
                    viewModel.cancelOrder();
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
        MyApplication.getInstance().setMessageReceiverListener(this);
        Log.e(TAG, "onResume: "+Constants.DATA_CHANGED );
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            showSuccessDialog();
        }
    }

    private static final String TAG = "CHAT_FRAGMENT";

    public void connectPusher(String eventName, String beamName, String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("75b53060b1f415501d21", options);
        pusher.connect();
        Channel channel = pusher.subscribe(channelName);
        channel.bind(eventName, event -> {
            Log.e(TAG, "connectPusher: " + event.getData());
            ChatSendResponse chatSendResponse = new Gson().fromJson(event.getData(), ChatSendResponse.class);
            viewModel.realTimePusherChat(chatSendResponse.getMessagePusher());
        });
        PushNotifications.start(MyApplication.getInstance(), "4b9224fd-ac88-4e17-86bf-65c347bc0fbd");
        PushNotifications.clearAllState();
        PushNotifications.addDeviceInterest(Constants.INTEREST);
        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(Constants.PUSHER_TOKEN_URL, () -> {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("jwt", LanguagesHelper.getJwt());
            HashMap<String, String> queryParams = new HashMap<>();
            return new AuthData(
                    headers,
                    queryParams
            );
        });
        PushNotifications.setUserId(beamName, tokenProvider, new BeamsCallback<>() {
            @Override
            public void onSuccess(@NonNull Void... values) {
                Log.e(TAG, "onSuccess: ");
            }

            @Override
            public void onFailure(PusherCallbackError error) {
            }
        });
    }

    @Override
    public void onMessageChanged(Chat messagesItem) {
//        if (messagesItem != null) {
//            if (messagesItem.getOrder_id() == viewModel.getPassingObject().getId()) {
//                if (messagesItem.getMessage() != null || messagesItem.getMessages_image() != null) {
//                    if (!TextUtils.isEmpty(messagesItem.getMessage()) || !TextUtils.isEmpty(messagesItem.getMessages_image().getImage())) {
//                        viewModel.adapter.getChatList().add(messagesItem);
//                        viewModel.adapter.notifyItemInserted(viewModel.adapter.getChatList().size() - 1);
//                        binding.rcChat.scrollToPosition(viewModel.adapter.getChatList().size() - 1);
//                    }
//                }
//                if (messagesItem.getConfirm() == Constants.ORDER_FINISHED) // order finished
//                    showSuccessDialog();
//                else if (messagesItem.getConfirm() == Constants.FINISH_CHAT) // delegate withdraw from order
//                    MovementHelper.startActivityBase(requireActivity(), HomeMainFragment.class.getName(), null, null);
//                else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_FIRST_WAY) // Update status of order and unlock track
//                    viewModel.getChatMain().getOrder_status().setOn_first_way("STARTED");
//            }
//        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            FileObjects fileObject = FileOperations.getFileObjects(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            viewModel.fileObjectList.add(fileObject);
            showAlertDialogWithAutoDismiss(getString(R.string.image_selected));
        } else if (requestCode == Constants.RESULT_CODE) {
            viewModel.request.setMessage("http://maps.google.com/?q=" + data.getDoubleExtra(Constants.LAT, 0.0) + "," + data.getDoubleExtra(Constants.LNG, 0.0));
            showAlertDialogWithAutoDismiss(getString(R.string.current_location_picked));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showAlertDialogWithAutoDismiss(String msg) {
        AlertDialog.Builder builder = new AlertDialog.Builder(requireActivity());
        builder
                .setMessage(msg)
                .setCancelable(false)
                .setNegativeButton(getString(R.string.no), (dialog, which) -> {
                    viewModel.fileObjectList.clear();
                    binding.message.setText("");
                    binding.message.setHint(getResources().getString(R.string.chat_hint));
                    dialog.cancel();
                })
                .setPositiveButton(getString(R.string.send), (dialog, id) -> {
                    viewModel.sendMessage();
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }
}
