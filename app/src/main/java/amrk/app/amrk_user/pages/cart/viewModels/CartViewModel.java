package amrk.app.amrk_user.pages.cart.viewModels;

import android.util.Log;

import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.PromoData;
import amrk.app.amrk_user.pages.cart.adapters.CartAdapter;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.NewOrderRequest;
import amrk.app.amrk_user.repository.CartRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class CartViewModel extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    CartRepository cartRepository;
    LiveData<List<ProductDetails>> cartLiveData;
    LiveData<List<VariationsItem>> variationsListLiveData;
    List<VariationsItem> variationsItemList;
    LiveData<List<OptionsItem>> optionsListLiveData;
    List<OptionsItem> optionsList;
    LiveData<String> cartTotal;
    CartAdapter cartAdapter;
    NewOrderRequest newOrderRequest;
    ShopsItem shopsItem;
    public boolean confirmBtnStatus = true;
    public double vat = Double.parseDouble(UserHelper.getInstance(MyApplication.getInstance()).getFee());
    PromoData promoData;

    @Inject
    public CartViewModel() {
        variationsItemList = new ArrayList<>();
        optionsList = new ArrayList<>();
        shopsItem = new ShopsItem();
        cartAdapter = new CartAdapter();
        this.liveData = new MutableLiveData<>();
        cartRepository = new CartRepository(MyApplication.getInstance());
        cartLiveData = cartRepository.getAllProducts();
        variationsListLiveData = cartRepository.getVariations();
        optionsListLiveData = cartRepository.getOptions();
        cartTotal = cartRepository.getCartTotal();
    }

    public CartAdapter getCartAdapter() {
        return cartAdapter;
    }

    public void toConfirmOrder() {
        confirmBtnStatus = false;
//        for (int p = 0; p < getCartAdapter().getMenuModels().size(); p++) {
//            List<VariationsItem> requestVarList = new ArrayList<>();
//            for (int v = 0; v < variationsItemList.size(); v++) {
//                if (getCartAdapter().getMenuModels().get(p).getProduct_room_id() == variationsItemList.get(v).getProductIdForeignKey()) {
//                    String variations = variationsItemList.get(v).getVariations().replace("[", "").replace("]", "").replace(" ", "");
//                    List<String> selectedVarList = Arrays.asList(variations.split(","));
//                    String lastSelected = "";
//                    for (int k = 0; k < selectedVarList.size(); k++) {
//                        if (selectedVarList.get(k).equals(lastSelected)) {
//                            continue;
//                        }
//                        lastSelected = selectedVarList.get(k);
//                        VariationsItem variationsItem = new VariationsItem();
//                        variationsItem.setVariation_id(Integer.parseInt(selectedVarList.get(k)));
//                        for (int o = 0; o < optionsList.size(); o++) {
//                            if (variationsItemList.get(v).getVariation_room_id() == optionsList.get(o).getVariationIdForeignKey()) {
//                                List<OptionsItem> requestOptList = new ArrayList<>();
//                                String options = optionsList.get(v).getOptions().replace("[", "").replace("]", "").replace(" ", "");
//                                List<String> selectedOptions = Arrays.asList(options.split(","));
//                                for (int sO = k; sO < selectedVarList.size(); sO++) {
//                                    OptionsItem optionsItem = new OptionsItem();
//                                    optionsItem.setOption_id(Integer.parseInt(selectedOptions.get(sO)));
//                                    requestOptList.add(optionsItem);
//                                    variationsItem.setOptions(requestOptList);
//                                    if (sO + 1 < selectedVarList.size() && !selectedVarList.get(sO).equals(selectedVarList.get(sO + 1))) {
//                                        break;
//                                    }
//                                }
//                                variationsItem.setOptions(requestOptList);
//                                break;
//                            }
//                        }
//                        requestVarList.add(variationsItem);
//                    }
//                    getCartAdapter().getMenuModels().get(p).setVariations(requestVarList);
//                    break;
//                }
//
//            }
//        }
        for (int cart = 0; cart < getCartAdapter().getMenuModels().size(); cart++) {
            for (int i = 0; i < variationsItemList.size(); i++) {
                getCartAdapter().getMenuModels().get(cart).setVariations(getSavedVariations(variationsItemList.get(i).getVariations()));
            }
        }
        getNewOrderRequest().setProductDetailsList(getCartAdapter().getMenuModels());
        if (getNewOrderRequest().getProductDetailsList().size() > 0) {
            getNewOrderRequest().setDepartment_id(2);
            getNewOrderRequest().setShopId(String.valueOf(getShopsItem().getId()));
            getNewOrderRequest().setShopImage(getShopsItem().getImage());
            getNewOrderRequest().setIn_addresss(getShopsItem().getAddress());
            getNewOrderRequest().setIn_lat(Double.parseDouble(getShopsItem().getLat()));
            getNewOrderRequest().setIn_lng(Double.parseDouble(getShopsItem().getLng()));
            getNewOrderRequest().setSubTotal(String.valueOf(getNewOrderRequest().tempCost));
            getNewOrderRequest().setTax(String.valueOf(vat));
            liveData.setValue(new Mutable(Constants.CONTINUE_NEW_ORDER));
        }
    }

    public List<VariationsItem> getSavedVariations(String variations) {
        Gson gson = new Gson();
        Type type = new TypeToken<List<VariationsItem>>() {
        }.getType();
        return gson.fromJson(variations, type);
    }

    @Bindable
    public NewOrderRequest getNewOrderRequest() {
        return this.newOrderRequest == null ? this.newOrderRequest = new NewOrderRequest() : this.newOrderRequest;
    }

    public CartRepository getCartRepository() {
        return cartRepository;
    }

    public LiveData<List<ProductDetails>> getCartLiveData() {
        return cartLiveData;
    }

    public void setVariationsItemList(List<VariationsItem> variationsItemList) {
        this.variationsItemList = variationsItemList;
    }

    public LiveData<List<VariationsItem>> getVariationsListLiveData() {
        return variationsListLiveData;
    }

    public LiveData<List<OptionsItem>> getOptionsListLiveData() {
        return optionsListLiveData;
    }

    public void setOptionsList(List<OptionsItem> optionsList) {
        this.optionsList = optionsList;
    }

    public LiveData<String> getCartTotal() {
        return cartTotal;
    }


    @Bindable
    public ShopsItem getShopsItem() {
        return shopsItem;
    }

    @Bindable
    public void setShopsItem(ShopsItem shopsItem) {
        notifyChange(BR.shopsItem);
        this.shopsItem = shopsItem;
    }

    @Bindable
    public PromoData getPromoData() {
        return this.promoData == null ? this.promoData = new PromoData() : this.promoData;
    }

    @Bindable
    public void setPromoData(PromoData promoData) {
        getNewOrderRequest().setCopon_id(promoData.getId());
        if (promoData.getType() == 0)
            getNewOrderRequest().discount = promoData.getValue();
        else {
            getNewOrderRequest().discount = (promoData.getValue() / 100);
        }
        calcCost();
        notifyChange(BR.promoData);
        this.promoData = promoData;
    }

    public void showPromoDialog() {
        if (getNewOrderRequest().getCopon_id() == 0)
            liveData.setValue(new Mutable(Constants.PROMO_DIALOG));
        else
            liveData.setValue(new Mutable(Constants.PROMO_EXISTS));
    }

    public void calcCost() {
        double taxValue = (vat / 100) * getNewOrderRequest().tempCost;
        double totalCost = Double.parseDouble(NumberFormat.getNumberInstance(Locale.ENGLISH).format(getNewOrderRequest().tempCost + taxValue - getNewOrderRequest().discount));
        getNewOrderRequest().setTotalCost(totalCost > 0 ? String.valueOf(totalCost) : "0.0");
        notifyChange(BR.newOrderRequest);
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
