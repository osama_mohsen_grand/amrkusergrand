package amrk.app.amrk_user.pages.reviews.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.pages.auth.models.UserData;

public class RatesItem {

    @SerializedName("rate")
    private String rate;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("comment")
    private String comment;

    @SerializedName("id")
    private int id;
    @SerializedName("count")
    private int count;

    @SerializedName("user")
    private UserData user;

    public String getRate() {
        return rate;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getComment() {
        return comment;
    }

    public int getId() {
        return id;
    }

    public UserData getUser() {
        return user;
    }

    public int getCount() {
        return count;
    }
}