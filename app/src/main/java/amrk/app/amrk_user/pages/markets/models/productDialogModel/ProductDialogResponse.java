package amrk.app.amrk_user.pages.markets.models.productDialogModel;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class ProductDialogResponse extends StatusMessage {
	@SerializedName("data")
	private ProductDetails productDetails;

	public ProductDetails getProductDetails(){
		return productDetails;
	}
}