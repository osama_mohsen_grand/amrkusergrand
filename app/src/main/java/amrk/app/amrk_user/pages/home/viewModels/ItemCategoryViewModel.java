package amrk.app.amrk_user.pages.home.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.home.model.DepartmentsItem;
import amrk.app.amrk_user.utils.Constants;

public class ItemCategoryViewModel extends BaseViewModel {
    public DepartmentsItem departmentsItem;

    public ItemCategoryViewModel(DepartmentsItem departmentsItem) {
        this.departmentsItem = departmentsItem;
    }

    @Bindable
    public DepartmentsItem getDepartmentsItem() {
        return departmentsItem;
    }

    public void itemAction() {
        if (departmentsItem.getActive() == 1)
            getLiveData().setValue(Constants.MENu);
        else {
          getLiveData().setValue(Constants.REMOVE_DIALOG_WARNING);
        }
    }

}
