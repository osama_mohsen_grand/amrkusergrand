package amrk.app.amrk_user.pages.markets.models;

import com.google.gson.annotations.SerializedName;

public class ShopsItem {

    @SerializedName("image")
    private String image;

    @SerializedName("address")
    private String address;

    @SerializedName("lng")
    private String lng;

    @SerializedName("distance")
    private String distance;

    @SerializedName("name")
    private String name;

    @SerializedName("description")
    private String description;

    @SerializedName("id")
    private int id;

    @SerializedName("cover_image")
    private String coverImage;

    @SerializedName("lat")
    private String lat;
    @SerializedName("place_id")
    private String place_id;

    public String getImage() {
        return image;
    }

    public String getAddress() {
        return address;
    }

    public String getLng() {
        return lng;
    }

    public String getDistance() {
        return distance;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public int getId() {
        return id;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public String getLat() {
        return lat;
    }

    public String getPlace_id() {
        return place_id;
    }
}