package amrk.app.amrk_user.pages.home;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.maps.model.LatLng;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentSearchBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.markets.models.SearchResponse;
import amrk.app.amrk_user.pages.markets.viewModels.MarketsViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;

public class SearchFragment extends BaseFragment {
    private Context context;
    @Inject
    MarketsViewModel viewModel;
    FragmentSearchBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_search, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (mutable.message.equals(Constants.SEARCH)) {
                viewModel.setShopsPaginate(((SearchResponse) mutable.object).getData());
            } else if (mutable.message.equals(Constants.SERVER_ERROR) || mutable.message.equals(Constants.ERROR))
                viewModel.setSearchProgressVisible(View.GONE);
        });
        binding.recMarket.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (viewModel.getSearchProgressVisible() == View.GONE && !TextUtils.isEmpty(viewModel.getShopsPaginate().getNextPageUrl())) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == viewModel.getMarketsAdapter().shopsItems.size() - 1) {
                        viewModel.setSearchProgressVisible(View.VISIBLE);
                        viewModel.searchMarkets((viewModel.getShopsPaginate().getCurrentPage() + 1));
                    }
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        initLastLocation();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void initLastLocation() {
        viewModel.location = new LatLng(Double.parseDouble(UserHelper.getInstance(context).getSaveLastKnownLocationLat()), Double.parseDouble(UserHelper.getInstance(context).getSaveLastKnownLocationLng()));
    }
}
