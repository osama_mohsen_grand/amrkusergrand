package amrk.app.amrk_user.pages.reviews.models;

import android.text.TextUtils;

import com.google.gson.annotations.SerializedName;

public class RateRequest {
    @SerializedName("shop_id")
    private int shop_id;
    @SerializedName("department_id")
    private int department_id;
    @SerializedName("rate")
    private String rate;
    @SerializedName("comment")
    private String comment;
    @SerializedName("order_id")
    private String orderId;

    public int getShop_id() {
        return shop_id;
    }

    public void setShop_id(int shop_id) {
        this.shop_id = shop_id;
    }

    public int getDepartment_id() {
        return department_id;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    public String getRate() {
        return rate;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public boolean isValid() {
        return (!TextUtils.isEmpty(comment) && !TextUtils.isEmpty(rate));
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }
}
