package amrk.app.amrk_user.pages.gifts.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemGiftBinding;
import amrk.app.amrk_user.pages.gifts.models.ReplacedPointsData;
import amrk.app.amrk_user.pages.gifts.viewModels.ItemGiftViewModel;


public class GiftsAdapter extends RecyclerView.Adapter<GiftsAdapter.MenuView> {
    List<ReplacedPointsData> replacedPointsDataList;
    MutableLiveData<Integer> liveDataAdapter = new MutableLiveData<>();
    private Context context;

    public GiftsAdapter() {
        this.replacedPointsDataList = new ArrayList<>();
    }

    public MutableLiveData<Integer> getLiveDataAdapter() {
        return liveDataAdapter;
    }

    public List<ReplacedPointsData> getReplacedPointsDataList() {
        return replacedPointsDataList;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_gift,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        ReplacedPointsData menuModel = replacedPointsDataList.get(position);
        ItemGiftViewModel itemMenuViewModel = new ItemGiftViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> liveDataAdapter.setValue(position));
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<ReplacedPointsData> dataList) {
        this.replacedPointsDataList.clear();
        replacedPointsDataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return replacedPointsDataList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemGiftBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemGiftViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
