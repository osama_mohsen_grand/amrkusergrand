package amrk.app.amrk_user.pages.myOrders.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class MyOrdersResponse extends StatusMessage {

    @SerializedName("data")
    private List<MyOrdersData> data;

    public List<MyOrdersData> getData() {
        return data;
    }
}