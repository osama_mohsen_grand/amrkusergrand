package amrk.app.amrk_user.pages.countries.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
 import amrk.app.amrk_user.pages.countries.models.Countries;
import amrk.app.amrk_user.utils.Constants;

public class ItemCountriesViewModel extends BaseViewModel {
    public Countries countries;

    public ItemCountriesViewModel(Countries countries) {
        this.countries = countries;
    }

    @Bindable
    public Countries getCountries() {
        return countries;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.SELECT_COUNTRY);
    }

}
