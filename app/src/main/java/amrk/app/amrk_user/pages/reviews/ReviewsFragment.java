package amrk.app.amrk_user.pages.reviews;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentReviewsBinding;
import amrk.app.amrk_user.databinding.ReviewDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.reviews.models.RateRequest;
import amrk.app.amrk_user.pages.reviews.models.ReviewsResponse;
import amrk.app.amrk_user.pages.reviews.models.SendReviewResponse;
import amrk.app.amrk_user.pages.reviews.viewModels.ReviewsViewModel;
import amrk.app.amrk_user.utils.Constants;

public class ReviewsFragment extends BaseFragment {
    private Context context;
    private Dialog dialog;
    @Inject
    ReviewsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentReviewsBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reviews, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.checkReviews();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.MARKET_REVIEWS:
                    viewModel.setReviewsResponse(((ReviewsResponse) mutable.object));
                    break;
                case Constants.REVIEW_NEW:
                    showRateDialog();
                    break;
                case Constants.SEND_REVIEW:
                    viewModel.getReviewsAdapter().ratesItems.add(((SendReviewResponse) ((Mutable) o).object).getRatesItem());
                    Constants.REVIEWS_COUNT = ((SendReviewResponse) ((Mutable) o).object).getRatesItem().getCount();
                    viewModel.getReviewsAdapter().notifyItemInserted(viewModel.getReviewsAdapter().getItemCount());
                    viewModel.setRateRequest(new RateRequest());
                    dialog.dismiss();
                    closeKeyboard();
                    viewModel.checkReviews();
                    break;
            }
        });
        getActivityBase().connectionMutableLiveData.observe(((LifecycleOwner) context), isConnected -> {
            if (isConnected)
                viewModel.clientReviews();
        });
        ((BaseActivity) context).getRefreshingLiveData().observe(((LifecycleOwner) context), aBoolean -> {
            baseActivity().stopRefresh(false);
            viewModel.clientReviews();
        });
    }

    private void showRateDialog() {
        dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        ReviewDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.review_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        dialog.setOnDismissListener(dialog -> {
            viewModel.setRateRequest(new RateRequest());
            viewModel.notifyChange();
        });
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getMarketRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

}
