package amrk.app.amrk_user.pages.heavyDepartment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.model.LatLng;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.databinding.FragmentHeavyFormBinding;
import amrk.app.amrk_user.databinding.SuccessDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.heavyDepartment.viewModels.HeavyFormViewModel;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.OrderImages;
import amrk.app.amrk_user.pages.publicOrder.models.subDepartment.SubDepartmentResponse;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.PopUp.PopUpMenuHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.upload.FileOperations;

public class HeavyFormFragment extends BaseFragment {
    private Context context;
    FragmentHeavyFormBinding binding;
    @Inject
    HeavyFormViewModel viewModel;
    private int imagePosition, locationFlag;
    Dialog dialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_heavy_form, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.getSubDepartment(Constants.ORDER_HEAVY_API_ID);
        viewModel.setUpOrderImages();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.CREATE_ORDER:
                    showSuccessDialog();
                    break;
                case Constants.GET_PUBLIC_SUB_DEPARTMENT:
                    viewModel.subDepartmentDataList = ((SubDepartmentResponse) mutable.object).getData();
                    break;
                case Constants.SUB_DEPARTMENT_DIALOG:
                    showSubDepartmentDialog();
                    break;
                case Constants.PICK_UP_LOCATION:
                    locationFlag = 0;
                    MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(1, getResources().getString(R.string.heavy_first_location)));
                    break;
                case Constants.PROCESSING_LOCATION:
                    locationFlag = 2;
                    MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(1, getResources().getString(R.string.order_processing_place)));
                    break;
                case Constants.DESTINATION_LOCATION:
                    locationFlag = 1;
                    MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(1, getResources().getString(R.string.heavy_delivered_location)));
                    break;
                case Constants.EMPTY_DRIVER:
                    toastErrorMessage(getString(R.string.empty_data));
                    break;

            }
        });
        viewModel.getImagesAdapter().getNewImageLiveData().observe(((LifecycleOwner) context), integer -> {
            imagePosition = integer;
            pickImageDialogSelect();
        });
    }

    private void showSuccessDialog() {
        dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        SuccessDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.success_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        dialog.setOnDismissListener(dialog -> {
            dialog.dismiss();
            finishActivity();
        });
        dialog.show();
    }

    private void showSubDepartmentDialog() {
        PopUpMenuHelper.showSubDepartmentPopUp(context, binding.serviceType, viewModel.subDepartmentDataList).setOnMenuItemClickListener(item -> {
            binding.serviceType.setText(viewModel.subDepartmentDataList.get(item.getItemId()).getName());
            viewModel.getNewOrderRequest().setSub_department_id(String.valueOf(viewModel.subDepartmentDataList.get(item.getItemId()).getId()));
            viewModel.getNewOrderRequest().setIsProcessing(viewModel.subDepartmentDataList.get(item.getItemId()).getHas_processing() == 1 ? "true" : "false");
            return false;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        baseActivity().enableRefresh(false);
        viewModel.getPublicOrderRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            FileObjects fileObject = FileOperations.getFileObjects(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            viewModel.getImagesAdapter().orderImagesList.set(imagePosition, new OrderImages(fileObject.getFilePath()));
            viewModel.getImagesAdapter().notifyDataSetChanged();
        } else if (requestCode == Constants.RESULT_CODE) {
            if (locationFlag == 0) {
                viewModel.settingOrderLocations(Constants.IN_ADDRESS, new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)), Objects.requireNonNull(data.getStringExtra(Constants.ADDRESS)), Objects.requireNonNull(data.getStringExtra(Constants.CITY)));
            } else if (locationFlag == 1) {
                viewModel.settingOrderLocations(Constants.OUT_ADDRESS, new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)), Objects.requireNonNull(data.getStringExtra(Constants.ADDRESS)), Objects.requireNonNull(data.getStringExtra(Constants.CITY)));
            } else if (locationFlag == 2) {
                viewModel.settingOrderLocations(Constants.PROCESSING_ADDRESS, new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)), Objects.requireNonNull(data.getStringExtra(Constants.ADDRESS)), Objects.requireNonNull(data.getStringExtra(Constants.CITY)));
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
