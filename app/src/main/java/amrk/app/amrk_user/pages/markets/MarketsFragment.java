package amrk.app.amrk_user.pages.markets;

import android.content.Context;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentMarketsBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.markets.models.MarketResponse;
import amrk.app.amrk_user.pages.markets.models.MarketsByCategoriesResponse;
import amrk.app.amrk_user.pages.markets.viewModels.MarketsViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;

public class MarketsFragment extends BaseFragment {
    private Context context;
    @Inject
    MarketsViewModel viewModel;
    FragmentMarketsBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_markets, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        setEvent();
        initLastLocation();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.MARKETS:
                    viewModel.setShopsPaginate(((MarketResponse) ((Mutable) o).object).getMarketData().getShopsPaginate());
                    viewModel.getCategoriesAdapter().update(((MarketResponse) ((Mutable) o).object).getMarketData().getCategories());
                    break;
                case Constants.GET_MARKETS_BY_CATEGORY:
                    viewModel.setShopsPaginate(((MarketsByCategoriesResponse) ((Mutable) o).object).getShopsPaginate());
                    break;
                case Constants.ERROR:
                case Constants.SERVER_ERROR:
                    viewModel.setSearchProgressVisible(View.GONE);
                    break;

            }
        });
        getActivityBase().connectionMutableLiveData.observe(((LifecycleOwner) context), isConnected -> {
            if (isConnected)
                initLastLocation();
        });
        viewModel.getCategoriesAdapter().getLiveDataAdapter().observe(((LifecycleOwner) context), integer -> initLastLocation());
        ((BaseActivity) context).getRefreshingLiveData().observe(((LifecycleOwner) context), aBoolean -> initLastLocation());
        binding.recMarket.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                LinearLayoutManager linearLayoutManager = (LinearLayoutManager) recyclerView.getLayoutManager();
                if (viewModel.getSearchProgressVisible() == View.GONE && !TextUtils.isEmpty(viewModel.getShopsPaginate().getNextPageUrl())) {
                    if (linearLayoutManager != null && linearLayoutManager.findLastCompletelyVisibleItemPosition() == viewModel.getMarketsAdapter().shopsItems.size() - 1) {
                        viewModel.setSearchProgressVisible(View.VISIBLE);
                        viewModel.getMarketsByCategoryId(UserHelper.getInstance(context).getSaveLastKnownLocationLat() + "&lng=" + UserHelper.getInstance(context).getSaveLastKnownLocationLng() + "&category_id=" + viewModel.getCategoriesAdapter().lastSelected + "&page=" + (viewModel.getShopsPaginate().getCurrentPage() + 1), false);
                    }
                }
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        baseActivity().enableRefresh(true);
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    public void initLastLocation() {
        baseActivity().stopRefresh(false);
        if (viewModel.getCategoriesAdapter().getItemCount() > 0) {
            viewModel.getMarketsAdapter().shopsItems.clear();
            viewModel.getMarketsByCategoryId(UserHelper.getInstance(context).getSaveLastKnownLocationLat() + "&lng=" + UserHelper.getInstance(context).getSaveLastKnownLocationLng() + "&category_id=" + viewModel.getCategoriesAdapter().lastSelected, true);
        } else
            viewModel.getMarkets(UserHelper.getInstance(context).getSaveLastKnownLocationLat() + "&lng=" + UserHelper.getInstance(context).getSaveLastKnownLocationLng(), true);
    }
}
