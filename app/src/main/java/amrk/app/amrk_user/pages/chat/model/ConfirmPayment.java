package amrk.app.amrk_user.pages.chat.model;

import com.google.gson.annotations.SerializedName;

public class ConfirmPayment {
    @SerializedName("payment_method")
    private int paymentMethod;
    @SerializedName("order_id")
    private int orderId;

    public ConfirmPayment(int paymentMethod, int orderId) {
        this.paymentMethod = paymentMethod;
        this.orderId = orderId;
    }

    public int getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(int paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public int getOrderId() {
        return orderId;
    }

    public void setOrderId(int orderId) {
        this.orderId = orderId;
    }
}
