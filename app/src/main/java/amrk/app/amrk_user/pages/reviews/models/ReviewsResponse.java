package amrk.app.amrk_user.pages.reviews.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class ReviewsResponse extends StatusMessage {
    @SerializedName("data")
    private ReviewsData reviewsData;
    public ReviewsData getReviewsData() {
        return reviewsData;
    }

}