package amrk.app.amrk_user.pages.myFatora.viewModels;

import androidx.databinding.Bindable;

import com.myfatoorah.sdk.entity.initiatepayment.PaymentMethod;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.utils.Constants;

public class ItemPaymentMethodsViewModel extends BaseViewModel {
    public PaymentMethod paymentMethod;

    public ItemPaymentMethodsViewModel(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    @Bindable
    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
