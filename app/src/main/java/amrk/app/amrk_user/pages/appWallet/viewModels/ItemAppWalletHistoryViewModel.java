package amrk.app.amrk_user.pages.appWallet.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.appWallet.models.WalletHistoryItem;

public class ItemAppWalletHistoryViewModel extends BaseViewModel {
    public WalletHistoryItem walletHistoryItem;

    public ItemAppWalletHistoryViewModel(WalletHistoryItem walletHistoryItem) {
        this.walletHistoryItem = walletHistoryItem;
    }

    @Bindable
    public WalletHistoryItem getWalletHistoryItem() {
        return walletHistoryItem;
    }
}
