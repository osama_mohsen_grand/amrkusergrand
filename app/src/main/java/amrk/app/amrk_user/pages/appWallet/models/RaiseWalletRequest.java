package amrk.app.amrk_user.pages.appWallet.models;

import com.google.gson.annotations.SerializedName;

public class RaiseWalletRequest {
    @SerializedName("amount")
    private String amount;

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
