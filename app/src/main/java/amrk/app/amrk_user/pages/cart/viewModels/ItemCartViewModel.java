package amrk.app.amrk_user.pages.cart.viewModels;


import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.repository.CartRepository;
import amrk.app.amrk_user.utils.Constants;

public class ItemCartViewModel extends BaseViewModel {
    public ProductDetails productDetails;

    public ItemCartViewModel(ProductDetails productDetails) {
        this.productDetails = productDetails;
    }

    @Bindable
    public ProductDetails getProductDetails() {
        return productDetails;
    }

    public void minusItem() {
        productDetails.setPriceAfter(String.valueOf(Double.parseDouble(productDetails.getPriceAfter()) - productDetails.getPriceItem()));
        productDetails.setQuantity(productDetails.getQuantity() - 1);
        new CartRepository(MyApplication.getInstance()).update(productDetails);
        getLiveData().setValue(Constants.MINUS);
    }

    public void plusItem() {
        productDetails.setQuantity(productDetails.getQuantity() + 1);
        productDetails.setPriceAfter(String.valueOf(Double.parseDouble(productDetails.getPriceAfter()) + productDetails.getPriceItem()));
        new CartRepository(MyApplication.getInstance()).update(productDetails);
        getLiveData().setValue(Constants.PLUS);
    }

    public void deleteItem() {
        new CartRepository(MyApplication.getInstance()).deleteItem(productDetails.getProduct_room_id());
        getLiveData().setValue(Constants.REMOVE_FROM_CART);
    }

    public void toProductDetails() {
        getLiveData().setValue(Constants.PRODUCT_DETAILS);
    }
}
