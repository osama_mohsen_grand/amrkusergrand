package amrk.app.amrk_user.pages.home.model;

import com.google.gson.annotations.SerializedName;

public class Shop {

    @SerializedName("name")
    private String name;
    @SerializedName("image")
    private String image;
    @SerializedName("lat")
    private String lat;
    @SerializedName("lng")
    private String lng;

    @SerializedName("id")
    private int id;

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getLat() {
        return lat;
    }

    public String getLng() {
        return lng;
    }

    public int getId() {
        return id;
    }
}