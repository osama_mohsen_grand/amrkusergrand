package amrk.app.amrk_user.pages.markets.models.marketDetails;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MarketDetails {

    @SerializedName("today_from")
    private String todayFrom;

    @SerializedName("rate")
    private String rate;

    @SerializedName("name")
    private String name;

    @SerializedName("today_to")
    private String todayTo;

    @SerializedName("days")
    private List<DaysItem> days;

    @SerializedName("id")
    private int id;
    @SerializedName("rates_count")
    private String rates_count;
    @SerializedName("menus")
    private List<MenusItem> menus;

    @SerializedName("cover_image")
    private String coverImage;
    @SerializedName("distance")
    private String distance;
    @SerializedName("description")
    private String description;

    private String rateInPer;

    public String getTodayFrom() {
        return todayFrom;
    }

    public String getRate() {
        return rate;
    }

    public String getName() {
        return name;
    }

    public String getTodayTo() {
        return todayTo;
    }

    public List<DaysItem> getDays() {
        return days;
    }

    public int getId() {
        return id;
    }

    public List<MenusItem> getMenus() {
        return menus;
    }

    public void setRates_count(String rates_count) {
        this.rates_count = rates_count;
    }

    public String getRates_count() {
        return rates_count;
    }

    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    public String getCoverImage() {
        return coverImage;
    }

    public String getRateInPer() {
        if (rates_count != null) {
            if (Math.abs(Integer.parseInt(rates_count) / 1000000) >= 1) {
                return rateInPer = (Integer.parseInt(rates_count) / 1000000) + "m";
            } else if (Math.abs(Integer.parseInt(rates_count) / 1000) >= 1) {
                return rateInPer = (Integer.parseInt(rates_count) / 1000) + "k";
            } else
                return rates_count;
        }
        return rateInPer;
    }

    public String getDistance() {
        return distance;
    }

    public String getDescription() {
        return description;
    }
}