package amrk.app.amrk_user.pages.chat.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.pages.auth.models.UserData;
import amrk.app.amrk_user.pages.myOrders.models.OrderStatus;

public class ChatMain {
    @SerializedName("delegate_info")
    private DelegateInfo delegateInfo;
    @SerializedName("messages")
    private List<Chat> chatList;
    @SerializedName("order_status")
    private OrderStatus order_status;
    @SerializedName("cancel_warning")
    private String cancel_warning;
    @SerializedName("department_id")
    private int departmentId;

    public int getDepartmentId() {
        return departmentId;
    }

    public String getCancel_warning() {
        return cancel_warning;
    }

    public DelegateInfo getDelegateInfo() {
        return delegateInfo;
    }

    public OrderStatus getOrder_status() {
        return order_status;
    }

    public List<Chat> getChatList() {
        return chatList;
    }
}
