package amrk.app.amrk_user.pages.chatAdmin.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.model.base.StatusMessage;


public class ChatAdminResponse extends StatusMessage {
    @SerializedName("data")
    @Expose
    private List<ChatAdmin> chats;

    public List<ChatAdmin> getChats() {
        return chats;
    }
}
