package amrk.app.amrk_user.pages.appWallet.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class HistoryWalletData {

    @SerializedName("wallet")
    private String wallet;
    @SerializedName("currency")
    private String currency;

    @SerializedName("transactions")
    private List<WalletHistoryItem> userWalletRecharges;

    @SerializedName("id")
    private int id;

    public String getWallet() {
        return wallet;
    }

    public String getCurrency() {
        return currency;
    }

    public List<WalletHistoryItem> getUserWalletRecharges() {
        return userWalletRecharges;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public int getId() {
        return id;
    }
}