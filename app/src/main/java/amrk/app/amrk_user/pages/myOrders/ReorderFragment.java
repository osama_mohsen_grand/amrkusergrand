package amrk.app.amrk_user.pages.myOrders;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentReorderBinding;
import amrk.app.amrk_user.databinding.PromoBottomSheetBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.CheckPromoResponse;
import amrk.app.amrk_user.pages.myOrders.models.reOrder.OrderDetailsResponse;
import amrk.app.amrk_user.pages.myOrders.viewModels.ReOrderViewModel;
import amrk.app.amrk_user.pages.newOrder.SavedLocationsFragment;
import amrk.app.amrk_user.pages.offers.OffersDetailsFragment;
import amrk.app.amrk_user.pages.publicOrder.FragmentPublicOrderConfirm;
import amrk.app.amrk_user.pages.publicOrder.models.CreateOrderResponse;
import amrk.app.amrk_user.repository.CartRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.PopUp.PopUp;
import amrk.app.amrk_user.utils.PopUp.PopUpMenuHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;


public class ReorderFragment extends BaseFragment {
    private Context context;
    private FragmentReorderBinding binding;
    @Inject
    ReOrderViewModel viewModel;
    Dialog promoDialog;
    private int locationFlag = 0;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reorder, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.orderDetails();
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.PICK_UP_LOCATION:
                    locationFlag = 0;
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(0), getResources().getString(R.string.new_order_bar), SavedLocationsFragment.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.DESTINATION_LOCATION:
                    locationFlag = 1;
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(1), getResources().getString(R.string.new_order_bar), SavedLocationsFragment.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.DELIVERY_TIMES:
                    showDeliveryTime();
                    break;
                case Constants.PROMO_DIALOG:
                    showPromoDialog();
                    break;
                case Constants.CONTINUE_NEW_ORDER:
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(viewModel.getNewOrderRequest()), getResources().getString(R.string.new_order_bar), FragmentPublicOrderConfirm.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.ORDER_DETAILS:
                    viewModel.setOrderDetailsData(((OrderDetailsResponse) mutable.object).getData());
                    break;
                case Constants.SERVER_ERROR:
                case Constants.ERROR:
                    viewModel.confirmBtnStatus.set(false);
                case Constants.CREATE_ORDER:
                    viewModel.confirmBtnStatus.set(false);
                    toastMessage(((CreateOrderResponse) mutable.object).mMessage);
                    MovementHelper.finishWithResult(new PassingObject(), context);
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(((CreateOrderResponse) mutable.object).getData().getId(), ((CreateOrderResponse) mutable.object).getData().getDeliveryTime()), ResourceManager.getString(R.string.offers_title), OffersDetailsFragment.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.CHECK_PROMO:
                    viewModel.getNewOrderRequest().setCopon_id(((CheckPromoResponse) ((Mutable) o).object).getPromoData().getId());
                    toastMessage(((CheckPromoResponse) ((Mutable) o).object).mMessage);
                    viewModel.getNewOrderRequest().setCopon_id(((CheckPromoResponse) mutable.object).getPromoData().getId());
                    promoDialog.dismiss();
                    break;
                case Constants.EMPTY_DRIVER:
                    toastErrorMessage(getString(R.string.empty_product));
                    finishActivity();
                    break;
            }
        });
        viewModel.getCartLiveData().observe(((LifecycleOwner) context), productDetails -> {
            viewModel.getReorderAdapter().update(productDetails);
            viewModel.notifyChange(BR.reorderAdapter);
        });
        viewModel.getVariationsListLiveData().observe((LifecycleOwner) context, variationsItems -> {
            viewModel.setVariationsItemList(variationsItems);
        });
        viewModel.getOptionsListLiveData().observe((LifecycleOwner) context, optionsItems -> viewModel.setOptionsList(optionsItems));
    }

    private void showDeliveryTime() {
        List<PopUp> popUpList = new ArrayList<>();
        popUpList.add(new PopUp(getResources().getString(R.string.oneHour), 1));
        popUpList.add(new PopUp(getResources().getString(R.string.twoHour), 2));
        popUpList.add(new PopUp(getResources().getString(R.string.threeHour), 3));
        PopUpMenuHelper.showTimesPopUp(context, binding.followWayDate, popUpList).setOnMenuItemClickListener(item -> {
            binding.followWayDate.setText(popUpList.get(item.getItemId()).getName());
            viewModel.getNewOrderRequest().setDelivery_time(popUpList.get(item.getItemId()).getName());
            return false;
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getMarketRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        new CartRepository(MyApplication.getInstance()).emptyCart();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        new CartRepository(MyApplication.getInstance()).emptyCart();
    }

    private void showPromoDialog() {
        promoDialog = new Dialog(context, R.style.PauseDialog);
        promoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(promoDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        promoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        PromoBottomSheetBinding binding = DataBindingUtil.inflate(LayoutInflater.from(promoDialog.getContext()), R.layout.promo_bottom_sheet, null, false);
        promoDialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        promoDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == Constants.RESULT_CODE) {
                if (locationFlag == 1) {
                    viewModel.getNewOrderRequest().setOut_lat(data.getDoubleExtra(Constants.LAT, 0.0));
                    viewModel.getNewOrderRequest().setOut_lng(data.getDoubleExtra(Constants.LNG, 0.0));
                    viewModel.getNewOrderRequest().setOut_addresss(Objects.requireNonNull(data.getStringExtra(Constants.ADDRESS)));
                    viewModel.getNewOrderRequest().setOut_city_name(Objects.requireNonNull(data.getStringExtra(Constants.CITY)));
                    binding.followDate.setText(viewModel.getNewOrderRequest().getOut_addresss());
                }
            }
        }
    }
}
