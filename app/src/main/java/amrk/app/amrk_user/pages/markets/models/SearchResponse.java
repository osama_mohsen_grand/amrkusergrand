package amrk.app.amrk_user.pages.markets.models;


import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class SearchResponse extends StatusMessage {

    @SerializedName("data")
    private ShopsPaginate data;

    public ShopsPaginate getData() {
        return data;
    }

}