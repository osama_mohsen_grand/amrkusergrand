package amrk.app.amrk_user.pages.chat.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class ChatSendResponse extends StatusMessage {

    @SerializedName("data")
    @Expose
    private Chat data;
    @SerializedName("message")
    private Chat messagePusher;

    public Chat getMessagePusher() {
        return messagePusher;
    }

    public Chat getData() {
        return data;
    }
}
