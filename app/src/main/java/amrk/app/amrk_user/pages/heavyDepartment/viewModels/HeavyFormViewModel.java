package amrk.app.amrk_user.pages.heavyDepartment.viewModels;

import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.publicOrder.adapters.PublicOrderImagesAdapter;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.NewOrderRequest;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.OrderImages;
import amrk.app.amrk_user.pages.publicOrder.models.subDepartment.SubDepartmentData;
import amrk.app.amrk_user.repository.PublicOrderRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class HeavyFormViewModel extends BaseViewModel {
    ArrayList<FileObjects> fileObject;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    PublicOrderRepository publicOrderRepository;
    PublicOrderImagesAdapter imagesAdapter;
    NewOrderRequest newOrderRequest;
    public List<SubDepartmentData> subDepartmentDataList;

    @Inject
    public HeavyFormViewModel(PublicOrderRepository publicOrderRepository) {
        subDepartmentDataList = new ArrayList<>();
        newOrderRequest = new NewOrderRequest();
        fileObject = new ArrayList<>();
        imagesAdapter = new PublicOrderImagesAdapter();
        this.publicOrderRepository = publicOrderRepository;
        this.liveData = new MutableLiveData<>();
        publicOrderRepository.setLiveData(liveData);
    }

    public void getSubDepartment(int department) {
        compositeDisposable.add(publicOrderRepository.getSubDepartment(department));
    }

    public PublicOrderRepository getPublicOrderRepository() {
        return publicOrderRepository;
    }

    //New order images handle
    public NewOrderRequest getNewOrderRequest() {
        return newOrderRequest;
    }

    public void setNewOrderRequest(NewOrderRequest newOrderRequest) {
        this.newOrderRequest = newOrderRequest;
    }

    public void setUpOrderImages() {
        List<OrderImages> orderImages = new ArrayList<>();
        orderImages.add(new OrderImages(""));
        orderImages.add(new OrderImages(""));
        orderImages.add(new OrderImages(""));
        orderImages.add(new OrderImages(""));
        orderImages.add(new OrderImages(""));
        getImagesAdapter().update(orderImages);
    }

    public PublicOrderImagesAdapter getImagesAdapter() {
        return imagesAdapter;
    }

    public void confirmOrder() {
        getNewOrderRequest().setOrderImages(getImagesAdapter().orderImagesList);
        getNewOrderRequest().setDepartment_id(Constants.ORDER_HEAVY_API_ID);
        int counter = 0;
        fileObject.clear();
        for (int i = 0; i < getNewOrderRequest().getOrderImages().size(); i++) {
            if (!getNewOrderRequest().getOrderImages().get(i).getUri().isEmpty()) {
                FileObjects fileObjects = new FileObjects("image[" + counter + "]", getNewOrderRequest().getOrderImages().get(i).getUri(), Constants.FILE_TYPE_IMAGE);
                fileObject.add(fileObjects);
                counter++;
            }
            if (i == getNewOrderRequest().getOrderImages().size() - 1) {
                if (getNewOrderRequest().isHeavyValid()) {
                    setMessage(Constants.SHOW_PROGRESS);
                    compositeDisposable.add(publicOrderRepository.createOrder(getNewOrderRequest(), fileObject));
                } else
                    liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
            }
        }

    }

    public void settingOrderLocations(int type, LatLng locations, String address, String city) {
        if (type == Constants.IN_ADDRESS) {
            getNewOrderRequest().setIn_lat(locations.latitude);
            getNewOrderRequest().setIn_lng(locations.longitude);
            getNewOrderRequest().setIn_addresss(address);
            getNewOrderRequest().setIn_city_name(city);
        } else if (type == Constants.OUT_ADDRESS) {
            getNewOrderRequest().setOut_lat(locations.latitude);
            getNewOrderRequest().setOut_lng(locations.longitude);
            getNewOrderRequest().setOut_addresss(address);
            getNewOrderRequest().setOut_city_name(city);
        } else {
            getNewOrderRequest().setLast_lat(locations.latitude);
            getNewOrderRequest().setLast_lng(locations.longitude);
            getNewOrderRequest().setLast_address(address);
            getNewOrderRequest().setLast_city_name(city);
        }
        notifyChange();
    }


    public void toPickLocation() {
        liveData.setValue(new Mutable(Constants.PICK_UP_LOCATION));
    }

    public void toProcessingLocation() {
        liveData.setValue(new Mutable(Constants.PROCESSING_LOCATION));
    }

    public void toDestLocation() {
        liveData.setValue(new Mutable(Constants.DESTINATION_LOCATION));
    }

    public void toShowSubDepartments() {
        liveData.setValue(new Mutable(Constants.SUB_DEPARTMENT_DIALOG));
    }


    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
