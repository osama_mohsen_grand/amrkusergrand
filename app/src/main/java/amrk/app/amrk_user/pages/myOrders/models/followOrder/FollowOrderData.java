package amrk.app.amrk_user.pages.myOrders.models.followOrder;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.pages.auth.models.UserData;
import amrk.app.amrk_user.pages.myOrders.models.OrderStatus;

public class FollowOrderData {

    @SerializedName("delegate")
    private UserData delegate;

    @SerializedName("order_status")
    private OrderStatus orderStatus;

    @SerializedName("id")
    private int id;

    @SerializedName("delegate_id")
    private int delegateId;
    @SerializedName("in_lat")
    private double in_lat;
    @SerializedName("in_lng")
    private double in_lng;
    @SerializedName("in_address")
    private String in_address;
    @SerializedName("out_lat")
    private double out_lat;
    @SerializedName("out_lng")
    private double out_lng;
    @SerializedName("out_address")
    private String out_address;
    @SerializedName("last_lat")
    private double last_lat;
    @SerializedName("last_lng")
    private double last_lng;
    @SerializedName("last_address")
    private String last_address;
    @SerializedName("sub_department_id")
    private String sub_department_id;

    public UserData getDelegate() {
        return delegate;
    }

    public OrderStatus getOrderStatus() {
        return orderStatus;
    }

    public int getId() {
        return id;
    }

    public double getIn_lat() {
        return in_lat;
    }

    public double getIn_lng() {
        return in_lng;
    }

    public String getIn_address() {
        return in_address;
    }

    public double getOut_lat() {
        return out_lat;
    }

    public double getOut_lng() {
        return out_lng;
    }

    public String getOut_address() {
        return out_address;
    }

    public int getDelegateId() {
        return delegateId;
    }

    public String getSub_department_id() {
        return sub_department_id;
    }

    public double getLast_lat() {
        return last_lat;
    }

    public double getLast_lng() {
        return last_lng;
    }

    public String getLast_address() {
        return last_address;
    }
}