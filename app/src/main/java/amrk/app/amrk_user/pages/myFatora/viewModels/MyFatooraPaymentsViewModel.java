package amrk.app.amrk_user.pages.myFatora.viewModels;

import android.widget.CompoundButton;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.myfatoorah.sdk.entity.initiatepayment.PaymentMethod;
import com.myfatoorah.sdk.entity.paymentstatus.MFGetPaymentStatusResponse;

import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.myFatora.adapters.PaymentMethodsAdapter;
import amrk.app.amrk_user.pages.myFatora.models.SendPaymentRequest;
import amrk.app.amrk_user.repository.AuthRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class MyFatooraPaymentsViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    PaymentMethodsAdapter methodsAdapter;
    @Inject
    AuthRepository repository;
    SendPaymentRequest sendPaymentRequest;
    PaymentMethod paymentMethod;

    @Inject
    public MyFatooraPaymentsViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void sendPayment() {
        if (getSendPaymentRequest().isValid()) {
            setMessage(Constants.SHOW_PROGRESS);
            liveData.setValue(new Mutable(Constants.SEND_PAYMENT));
        }
    }

    public void checkPayment(MFGetPaymentStatusResponse response) {
        getSendPaymentRequest().setAmount(Double.parseDouble(getPassingObject().getObject()));
        getSendPaymentRequest().setTransactionId(Objects.requireNonNull(response.getInvoiceId()));
        getSendPaymentRequest().setStatus(1);
        getSendPaymentRequest().setType(getPassingObject().getId());
        getSendPaymentRequest().setOrderId(getPassingObject().getObject2());
        compositeDisposable.add(repository.checkPayment(getSendPaymentRequest()));
    }

    public void checkPaymentError(String invoiceId) {
        getSendPaymentRequest().setAmount(Double.parseDouble(getPassingObject().getObject()));
        getSendPaymentRequest().setTransactionId(Integer.parseInt(invoiceId));
        getSendPaymentRequest().setStatus(0);
        getSendPaymentRequest().setType(getPassingObject().getId());
        getSendPaymentRequest().setOrderId(getPassingObject().getObject2());
        compositeDisposable.add(repository.checkPayment(getSendPaymentRequest()));
    }

    public void liveAction(String action) {
        liveData.setValue(new Mutable(action));
    }

    public void onCheckedChange(CompoundButton button, boolean check) {
        getSendPaymentRequest().setSave(check);
    }

    @Bindable
    public SendPaymentRequest getSendPaymentRequest() {
        return this.sendPaymentRequest == null ? this.sendPaymentRequest = new SendPaymentRequest() : this.sendPaymentRequest;
    }

    @Bindable
    public PaymentMethod getPaymentMethod() {
        return this.paymentMethod == null ? this.paymentMethod = new PaymentMethod() : this.paymentMethod;
    }

    @Bindable
    public void setPaymentMethod(PaymentMethod paymentMethod) {
        notifyChange(BR.paymentMethod);
        this.paymentMethod = paymentMethod;
    }

    public AuthRepository getRepository() {
        return repository;
    }


    @Bindable
    public PaymentMethodsAdapter getMethodsAdapter() {
        return this.methodsAdapter == null ? this.methodsAdapter = new PaymentMethodsAdapter() : this.methodsAdapter;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
