package amrk.app.amrk_user.pages.chatAdmin.view;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.databinding.FragmentChatAdminBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdmin;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdminResponse;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdminSendResponse;
import amrk.app.amrk_user.pages.chatAdmin.viewmodel.ChatAdminViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.services.RealTimeReceiver;
import amrk.app.amrk_user.utils.upload.FileOperations;

public class ChatAdminFragment extends BaseFragment implements RealTimeReceiver.MessageAdminReceiverListener {

    private Context context;
    private FragmentChatAdminBinding binding;
    @Inject
    ChatAdminViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_chat_admin, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.chat();
        setEvent();
        return binding.getRoot();
    }


    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (mutable.message.equals(Constants.CHAT_ADMIN)) {
                viewModel.adapter.update(((ChatAdminResponse) mutable.object).getChats());
                if (viewModel.adapter.getChatList().size() > 0)
                    new Handler().postDelayed(() -> binding.rcChat.smoothScrollToPosition(viewModel.adapter.getChatList().size() - 1), 200);
            } else if (mutable.message.equals(Constants.IMAGE)) {
                pickImageDialogSelect();
            } else if (((Mutable) o).message.equals(Constants.SEND_MESSAGE)) {
                ChatAdminSendResponse chatSendResponse = (ChatAdminSendResponse) ((Mutable) o).object;
                viewModel.adapter.getChatList().add(chatSendResponse.getData());
                viewModel.fileObjectList.clear();
                binding.message.setText("");
                binding.message.setHint(getResources().getString(R.string.chat_hint));
                binding.rcChat.scrollToPosition(viewModel.adapter.getItemCount() - 1);
                viewModel.adapter.notifyItemChanged(viewModel.adapter.getItemCount() - 1);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
        MyApplication.getInstance().setMessageAdminReceiverListener(this);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onMessageChanged(ChatAdmin messagesItem) {
        if (messagesItem != null) {
            viewModel.adapter.getChatList().add(messagesItem);
            viewModel.adapter.notifyItemInserted(viewModel.adapter.getChatList().size() - 1);
            binding.rcChat.scrollToPosition(viewModel.adapter.getChatList().size() - 1);
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            FileObjects fileObject = FileOperations.getFileObjects(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            viewModel.fileObjectList.add(fileObject);
            showAlertDialogWithAutoDismiss();
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public void showAlertDialogWithAutoDismiss() {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setMessage(getString(R.string.image_selected))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.no), (dialog, which) -> {
                    viewModel.fileObjectList.clear();
                    binding.message.setText("");
                    binding.message.setHint(getResources().getString(R.string.chat_hint));
                    dialog.cancel();
                })
                .setPositiveButton(getString(R.string.send), (dialog, id) -> {
                    viewModel.sendMessage();
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

}
