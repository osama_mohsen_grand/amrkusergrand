package amrk.app.amrk_user.pages.markets.viewModels;

import android.util.Log;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.markets.adapters.productDetails.ProductAdapter;
import amrk.app.amrk_user.pages.markets.adapters.productDetails.ProductVariationsAdapter;
import amrk.app.amrk_user.pages.markets.models.marketDetails.ProductsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.repository.CartRepository;
import amrk.app.amrk_user.repository.MarketRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class MarketMenuItemDialogViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    private final CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ProductsItem productsItem;
    @Inject
    MarketRepository repository;
    private final CartRepository cartRepository;
    ProductVariationsAdapter variationsAdapter;
    private final ProductAdapter productAdapter;
    private ProductDetails details;
    private int quantity = 1;
    private double total = 0.0;
    public List<OptionsItem> optionsItems;

    @Inject
    public MarketMenuItemDialogViewModel(MarketRepository repository) {
        productsItem = new ProductsItem();
        optionsItems = new ArrayList<>();
        details = new ProductDetails();
        productAdapter = new ProductAdapter();
        cartRepository = new CartRepository(MyApplication.getInstance());
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void productDetails() {
        compositeDisposable.add(repository.getProductDetails(getProductsItem().getId()));
    }

    protected void unSubscribeFromObservable() {
        if (!compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public MarketRepository getRepository() {
        return repository;
    }

    @Bindable
    public ProductsItem getProductsItem() {
        return productsItem;
    }

    @Bindable
    public void setProductsItem(ProductsItem productsItem) {
        notifyChange(BR.productsItem);
        this.productsItem = productsItem;
    }

    @Bindable
    public ProductVariationsAdapter getVariationsAdapter() {
        return this.variationsAdapter == null ? this.variationsAdapter = new ProductVariationsAdapter() : this.variationsAdapter;

    }

    public ProductAdapter getProductAdapter() {
        return productAdapter;
    }

    @Bindable
    public ProductDetails getDetails() {
        return details;
    }

    private static final String TAG = "Osama";

    @Bindable
    public void setDetails(ProductDetails details) {
        setQuantity(details.getQuantity() == 0 ? 1 : details.getQuantity());
        if (details.getVariations() != null && details.getVariations().size() > 0) {
            getVariationsAdapter().update(details.getVariations());
            getProductAdapter().update(details.getVariations());
        } else {
//            setTotal(Double.parseDouble(details.getPriceAfter()));
        }
        notifyChange(BR.variationsAdapter);
        notifyChange(BR.details);
        this.details = details;
        //osama
        for (VariationsItem variationsItem : details.getVariations()) {
            if (variationsItem.getRequired() == 1 && variationsItem.getOptions().size() >= 1)
                setTotal(getTotal() + Double.parseDouble(variationsItem.getOptions().get(0).getPrice()));
        }
        if (getTotal() == 0) setTotal(Double.parseDouble(details.getPriceAfter()));
        Log.d(TAG, "setDetails: " + getTotal());
    }

    @Bindable
    public int getQuantity() {
        return quantity;
    }

    @Bindable
    public void setQuantity(int quantity) {
        notifyChange(BR.quantity);
        this.quantity = quantity;
    }

    public CartRepository getCartRepository() {
        return cartRepository;
    }

    //Cart Operation

    public void addToCart() {
        if (getDetails().getPriceItem() == 0.0)
            getDetails().setPriceItem(Double.parseDouble(getDetails().getPriceAfter()));
        getDetails().setPriceAfter(String.valueOf(getTotal()));
        getDetails().setQuantity(getQuantity());
        Log.e(TAG, "addToCart: " + getDetails());
        getCartRepository().insert(getDetails(), optionsItems);
        liveData.setValue(new Mutable(Constants.ADD_TO_CART));
    }

    public void dismiss() {
        liveData.setValue(new Mutable(Constants.DISMISS));
    }

    public void plus() {
        if (getTotal() != 0.0) {
//            if (getQuantity() == 1)
//                lastTotal = getTotal();
            setQuantity(getQuantity() + 1);
            if (UserHelper.getInstance(MyApplication.getInstance()).getTempTotal().equals("0.0"))
                setTotal(Double.parseDouble(getDetails().getPriceAfter()) * getQuantity());
            else
                setTotal(Double.parseDouble(UserHelper.getInstance(MyApplication.getInstance()).getTempTotal()) * getQuantity());
//            if (getDetails().getVariations() != null && getDetails().getVariations().size() > 0)
//                setTotal(getTotal() + Double.parseDouble(optionsItems.get(isFound(lastSingleItemId)).getPrice()));
//            else
//                setTotal(getTotal() + Double.parseDouble(getDetails().getPriceAfter()));
        }
    }

    public void minus() {
        if (getQuantity() != 1) {
//            setQuantity(getQuantity() - 1);
//            if (getDetails().getVariations() != null && getDetails().getVariations().size() > 0)
//                setTotal(getTotal() - Double.parseDouble(optionsItems.get(isFound(lastSingleItemId)).getPrice()));
//            else
//                setTotal(getTotal() - Double.parseDouble(getDetails().getPriceAfter()));
//            if (getQuantity() == 1)
//                lastTotal = getTotal();
            setQuantity(getQuantity() - 1);
            if (UserHelper.getInstance(MyApplication.getInstance()).getTempTotal().equals("0.0"))
                setTotal(Double.parseDouble(getDetails().getPriceAfter()) * getQuantity());
            else
                setTotal(Double.parseDouble(UserHelper.getInstance(MyApplication.getInstance()).getTempTotal()) * getQuantity());
        }
    }

    public void checkCalc(List<VariationsItem> variationsItemList) {
        if (getVariationsAdapter().getItemCount() > 0) {
            getDetails().setVariations(variationsItemList);
            Log.e(TAG, "checkCalc: " + UserHelper.getInstance(MyApplication.getInstance()).getTempTotal());
            setTotal(quantity * Double.parseDouble(UserHelper.getInstance(MyApplication.getInstance()).getTempTotal()));
            getDetails().setPriceItem(getTotal() / quantity);
        }


//        OptionsItem optionsItem = (OptionsItem) object.getObjectClass();
//        int isFound = isFound(optionsItem.getOption_id());
//        Log.e("checkCalc", "checkCalc: " + isFound);
//        if (optionsItem.getType() == 0) {
//            if (isFound == -1) { //not exists
//                if (lastSingleItemId != 0) {
//                    setTotal(getTotal() - (Double.parseDouble(optionsItems.get(isFound(lastSingleItemId)).getPrice()) * quantity));
//                    optionsItems.remove(isFound(lastSingleItemId));
//                }
//                optionsItems.add(optionsItem);
//                lastSingleItemId = optionsItem.getOption_id();
//                getDetails().setPriceItem(Double.parseDouble(optionsItem.getPrice()));
//                setTotal(getTotal() + (Double.parseDouble(optionsItem.getPrice()) * quantity));
//                if (getQuantity() == 1)
//                    lastTotal = getTotal();
//            }
//        } else {
//            if (isFound != -1) { //exists
//                optionsItems.remove(isFound);
//                setTotal(getTotal() - Double.parseDouble(optionsItem.getPrice()));
//            } else {
//                optionsItems.add(optionsItem);
//                setTotal(getTotal() + Double.parseDouble(optionsItem.getPrice()));
//
//            }
//            if (getQuantity() == 1)
//                lastTotal = getTotal();
//        }
    }

    private int isFound(int optionId) {
        for (int i = 0; i < optionsItems.size(); i++) {
            if (optionsItems.get(i).getOption_id() == optionId) {
                return i;
            }
        }
        return -1;
    }

    @Bindable
    public double getTotal() {
        return total;
    }

    @Bindable
    public void setTotal(double total) {
        if (total == 0.0)
            this.total = total * getQuantity();
        notifyChange(BR.total);
        this.total = total;
    }
}
