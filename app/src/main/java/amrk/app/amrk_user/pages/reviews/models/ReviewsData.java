package amrk.app.amrk_user.pages.reviews.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class ReviewsData {

    @SerializedName("rates")
    private List<RatesItem> rates;

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;

    public List<RatesItem> getRates() {
        return rates;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }
}