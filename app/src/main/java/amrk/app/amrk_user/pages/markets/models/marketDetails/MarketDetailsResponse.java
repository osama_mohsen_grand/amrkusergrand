package amrk.app.amrk_user.pages.markets.models.marketDetails;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class MarketDetailsResponse extends StatusMessage {
    @SerializedName("data")
    private MarketDetails marketDetails;

    public MarketDetails getMarketDetails() {
        return marketDetails;
    }


}