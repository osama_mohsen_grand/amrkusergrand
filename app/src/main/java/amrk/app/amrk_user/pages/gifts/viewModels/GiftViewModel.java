package amrk.app.amrk_user.pages.gifts.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.gifts.adapters.GiftsAdapter;
import amrk.app.amrk_user.pages.gifts.models.ReplacedObject;
import amrk.app.amrk_user.repository.SettingsRepository;
import io.reactivex.disposables.CompositeDisposable;

public class GiftViewModel extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    private CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository settingsRepository;
    private GiftsAdapter giftsAdapter;
    private ReplacedObject replacedObject;

    @Inject
    public GiftViewModel(SettingsRepository settingsRepository) {
        replacedObject = new ReplacedObject();
        this.settingsRepository = settingsRepository;
        this.liveData = new MutableLiveData<>();
        settingsRepository.setLiveData(liveData);
    }

    public void gifts() {
        compositeDisposable.add(settingsRepository.gifts());
    }

    public void replacePoints(int offerId) {
        compositeDisposable.add(settingsRepository.replacePoints(offerId));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    @Bindable
    public GiftsAdapter getGiftsAdapter() {
        return this.giftsAdapter == null ? this.giftsAdapter = new GiftsAdapter() : this.giftsAdapter;
    }

    @Bindable
    public ReplacedObject getReplacedObject() {
        return replacedObject;
    }

    @Bindable
    public void setReplacedObject(ReplacedObject replacedObject) {
        getGiftsAdapter().update(replacedObject.getReplacedPointsData());
        notifyChange(BR.giftsAdapter);
        notifyChange(BR.replacedObject);
        this.replacedObject = replacedObject;
    }


}
