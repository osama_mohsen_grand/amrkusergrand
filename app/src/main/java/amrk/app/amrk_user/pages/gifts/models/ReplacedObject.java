package amrk.app.amrk_user.pages.gifts.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ReplacedObject {
    @SerializedName("user_points")
    private String user_points;
    @SerializedName("user_offer_points")
    private List<ReplacedPointsData> replacedPointsData;

    public String getUser_points() {
        return user_points;
    }

    public List<ReplacedPointsData> getReplacedPointsData() {
        return replacedPointsData;
    }
}
