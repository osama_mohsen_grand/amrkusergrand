package amrk.app.amrk_user.pages.heavyOrders;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;

import com.google.android.material.tabs.TabLayout;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentHeavyOrdersMainBinding;
import amrk.app.amrk_user.pages.heavyOrders.viewModels.MyHeavyOrdersViewModel;
import amrk.app.amrk_user.utils.helper.MovementHelper;

public class MyHeavyOrdersMainFragment extends BaseFragment {

    private Context context;
    FragmentHeavyOrdersMainBinding binding;
    @Inject
    MyHeavyOrdersViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_heavy_orders_main, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        MovementHelper.replaceWaitingFragment(context, new CurrentHeavyOrdersFragment());
        binding.tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (tab.getPosition() == 0) {
                    MovementHelper.replaceWaitingFragment(context, new CurrentHeavyOrdersFragment());
                } else if (tab.getPosition() == 1) {
                    MovementHelper.replaceWaitingFragment(context, new PreviousHeavyOrdersFragment());
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        return binding.getRoot();
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
