package amrk.app.amrk_user.pages.gifts.models;

import com.google.gson.annotations.SerializedName;

public class ReplacedPointsData {

    @SerializedName("money")
    private String money;

    @SerializedName("code")
    private String code;

    @SerializedName("description")
    private String description;

    @SerializedName("image")
    private String image;

    @SerializedName("id")
    private int id;

    @SerializedName("used")
    private int used;

    @SerializedName("points")
    private String points;

    public String getMoney() {
        return money;
    }

    public String getCode() {
        return code;
    }

    public String getDescription() {
        return description;
    }

    public String getImage() {
        return image;
    }

    public int getId() {
        return id;
    }

    public int getUsed() {
        return used;
    }

    public String  getPoints() {
        return points;
    }
}