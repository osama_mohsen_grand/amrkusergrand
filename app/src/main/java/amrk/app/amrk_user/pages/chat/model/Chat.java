package amrk.app.amrk_user.pages.chat.model;

import com.google.gson.annotations.SerializedName;

import org.jetbrains.annotations.NotNull;

import amrk.app.amrk_user.pages.myOrders.models.OrderImagesItem;

public class Chat {

    @SerializedName("id")
    private int id;
    @SerializedName("message")
    private String message;
    @SerializedName("order_id")
    private int order_id;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("sender_id")
    private int sender_id;
    @SerializedName("sender_type")
    private int sender_type;
    @SerializedName("receiver_id")
    private int receiver_id;
    @SerializedName("confirm_accept_order")
    private int confirm;
    @SerializedName("receiver_type")
    private int receiver_type;
    @SerializedName("messages_image")
    private OrderImagesItem messages_image;
    @SerializedName("user_image")
    private String user_image;
    @SerializedName("delegate_image")
    private String delegate_image;
    @SerializedName("invoice")
    private Invoice invoice;

    public Invoice getInvoice() {
        return invoice;
    }
    public int getId() {
        return id;
    }

    public String getMessage() {
        return message;
    }

    public int getOrder_id() {
        return order_id;
    }

    public String getCreated_at() {
        return created_at;
    }

    public int getSender_id() {
        return sender_id;
    }

    public int getSender_type() {
        return sender_type;
    }

    public int getReceiver_id() {
        return receiver_id;
    }

    public int getReceiver_type() {
        return receiver_type;
    }

    public OrderImagesItem getMessages_image() {
        return messages_image;
    }

    public String getUser_image() {
        return user_image;
    }

    public String getDelegate_image() {
        return delegate_image;
    }

    public int getConfirm() {
        return confirm;
    }

    @NotNull
    @Override
    public String toString() {
        return "Chat{" +
                "id=" + id +
                ", message='" + message + '\'' +
                ", order_id=" + order_id +
                ", created_at='" + created_at + '\'' +
                ", sender_id=" + sender_id +
                ", sender_type=" + sender_type +
                ", receiver_id=" + receiver_id +
                ", receiver_type=" + receiver_type +
                ", messages_image=" + messages_image +
                ", user_image='" + user_image + '\'' +
                ", delegate_image='" + delegate_image + '\'' +
                '}';
    }
}