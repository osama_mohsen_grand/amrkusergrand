package amrk.app.amrk_user.pages.markets.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class MarketData {

    @SerializedName("shops")
    private ShopsPaginate shopsPaginate;

    @SerializedName("categories")
    private List<CategoriesItem> categories;

    public ShopsPaginate getShopsPaginate() {
        return shopsPaginate;
    }

    public List<CategoriesItem> getCategories() {
        return categories;
    }
}