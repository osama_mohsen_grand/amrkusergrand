package amrk.app.amrk_user.pages.publicOrder;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentPublicOrderConfirmBinding;
import amrk.app.amrk_user.databinding.MarketMarkerBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.newOrder.SavedLocationsFragment;
import amrk.app.amrk_user.pages.offers.OffersDetailsFragment;
import amrk.app.amrk_user.pages.publicOrder.models.CreateOrderResponse;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.NewOrderRequest;
import amrk.app.amrk_user.pages.publicOrder.viewModels.PublicOrdersViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.PopUp.PopUp;
import amrk.app.amrk_user.utils.PopUp.PopUpMenuHelper;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.locations.MapConfig;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.UserHelper;

import static amrk.app.amrk_user.base.maps.MapHelper.createDrawableFromView;

public class FragmentPublicOrderConfirm extends BaseFragment implements OnMapReadyCallback {
    private int locationFlag = 0;
    private Context context;
    private FragmentPublicOrderConfirmBinding binding;
    @Inject
    PublicOrdersViewModel viewModel;
    private Marker pickMarker = null, deliveredMarker = null, processingMarker = null;
    private GoogleMap mMap;
    ArrayList<LatLng> latLngs = new ArrayList<>();
    MapConfig mapConfig;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_public_order_confirm, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setNewOrderRequest(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), NewOrderRequest.class));
        }
        setEvent();
        init(savedInstanceState);
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.PICK_UP_LOCATION:
                    locationFlag = 0;
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(0), getResources().getString(R.string.new_order_bar), SavedLocationsFragment.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.DESTINATION_LOCATION:
                    locationFlag = 1;
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(1), getResources().getString(R.string.new_order_bar), SavedLocationsFragment.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.PROCESSING_LOCATION:
                    locationFlag = 2;
                    viewModel.getNewOrderRequest().setIsProcessing("true");
                    MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(1, getResources().getString(R.string.order_processing_place)));
                    break;
                case Constants.DELIVERY_TIMES:
                    showDeliveryTime();
                    break;
                case Constants.CREATE_ORDER:
                    toastMessage(((CreateOrderResponse) mutable.object).mMessage);
                    MovementHelper.finishWithResult(new PassingObject(), context);
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(((CreateOrderResponse) mutable.object).getData().getId(), ((CreateOrderResponse) mutable.object).getData().getDeliveryTime()), ResourceManager.getString(R.string.offers_title), OffersDetailsFragment.class.getName(), null, Constants.MARKETS);
                    break;
            }
        });
    }

    private void showDeliveryTime() {
        List<PopUp> popUpList = new ArrayList<>();
        popUpList.add(new PopUp(getResources().getString(R.string.oneHour), 1));
        popUpList.add(new PopUp(getResources().getString(R.string.twoHour), 2));
        popUpList.add(new PopUp(getResources().getString(R.string.threeHour), 3));
        PopUpMenuHelper.showTimesPopUp(context, binding.followWayDate, popUpList).setOnMenuItemClickListener(item -> {
            binding.followWayDate.setText(popUpList.get(item.getItemId()).getName());
            viewModel.getNewOrderRequest().setDelivery_time(popUpList.get(item.getItemId()).getName());
            return false;
        });
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
        MapsInitializer.initialize(context);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (data != null) {
            if (requestCode == Constants.RESULT_CODE) {
                if (locationFlag == 0) {
                    viewModel.settingOrderLocations(Constants.IN_ADDRESS, new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)), Objects.requireNonNull(data.getStringExtra(Constants.ADDRESS)), Objects.requireNonNull(data.getStringExtra(Constants.CITY)));
                } else if (locationFlag == 1) {
                    viewModel.settingOrderLocations(Constants.OUT_ADDRESS, new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)), Objects.requireNonNull(data.getStringExtra(Constants.ADDRESS)), Objects.requireNonNull(data.getStringExtra(Constants.CITY)));
                } else if (locationFlag == 2) {
                    viewModel.settingOrderLocations(Constants.PROCESSING_ADDRESS, new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)), Objects.requireNonNull(data.getStringExtra(Constants.ADDRESS)), Objects.requireNonNull(data.getStringExtra(Constants.CITY)));
                }
                addMarker(new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)));
            }
        }
    }

    private void init(Bundle savedInstanceState) {
        binding.mapView.onCreate(savedInstanceState);
        binding.mapView.getMapAsync(this);
    }

    @Override
    public void onMapReady(@NotNull GoogleMap googleMap) {
        mMap = googleMap;
        mapConfig = new MapConfig(context, mMap);
        mMap.setOnMapLoadedCallback(this::initLastLocation);

    }

    public void initLastLocation() {
        LatLng latLng = new LatLng(Double.parseDouble(UserHelper.getInstance(context).getSaveLastKnownLocationLat()), Double.parseDouble(UserHelper.getInstance(context).getSaveLastKnownLocationLng()));
        locationFlag = 1;
        addMarker(latLng);
        MapConfig.getAddress(latLng.latitude, latLng.longitude, context, (address, city) -> {
            viewModel.settingOrderLocations(Constants.OUT_ADDRESS, latLng, address, city);
            binding.followDate.setText(address);
        });
        if (viewModel.getNewOrderRequest().getDepartment_id() == 2) {
            locationFlag = 0;
            addMarker(new LatLng(viewModel.getNewOrderRequest().getIn_lat(), viewModel.getNewOrderRequest().getIn_lng()));
        }
    }

    private void addMarker(LatLng position) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(position);
        if (locationFlag == 0) {
            if (pickMarker != null)
                pickMarker.remove();
            markerOptions.title(getResources().getString(R.string.order_pick_location));
            if (viewModel.getNewOrderRequest().getDepartment_id() == 2) {
                MarketMarkerBinding binding = DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.market_marker, null, false);
                binding.setViewModel(viewModel);
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(context, binding.getRoot())));
                mMap.addMarker(markerOptions);
            } else {
                markerOptions.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_current), 50, 50)));
                pickMarker = mMap.addMarker(markerOptions);
            }
        } else if (locationFlag == 1) {
            if (deliveredMarker != null)
                deliveredMarker.remove();
            markerOptions.title(getResources().getString(R.string.order_delivered_location));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.location_search), 100, 100)));
            deliveredMarker = mMap.addMarker(markerOptions);
        } else if (locationFlag == 2) {
            if (processingMarker != null)
                processingMarker.remove();
            markerOptions.title(getResources().getString(R.string.order_delivered_location));
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_processing_location), ResourceManager.getDrawable(R.drawable.ic_processing_location).getMinimumWidth(), ResourceManager.getDrawable(R.drawable.ic_processing_location).getMinimumHeight())));
            processingMarker = mMap.addMarker(markerOptions);
        }
        latLngs.add(position);
        mapConfig.moveCamera(latLngs);
    }

    @Override
    public void onResume() {
        binding.mapView.onResume();
        super.onResume();
        baseActivity().enableRefresh(false);
        viewModel.getPublicOrderRepository().setLiveData(viewModel.liveData);
    }


    @Override
    public void onPause() {
        super.onPause();
        binding.mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.mapView.onLowMemory();
    }
}
