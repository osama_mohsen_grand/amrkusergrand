package amrk.app.amrk_user.pages.home.viewModels;

import android.view.MenuItem;

import androidx.annotation.NonNull;
import androidx.databinding.Bindable;
import androidx.databinding.BindingMethod;
import androidx.databinding.BindingMethods;
import androidx.lifecycle.MutableLiveData;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.smarteist.autoimageslider.SliderView;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.home.adapters.CategoriesAdapter;
import amrk.app.amrk_user.pages.home.adapters.HomeSliderAdapter;
import amrk.app.amrk_user.repository.SettingsRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class HomeViewModel extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository settingsRepository;
    HomeSliderAdapter sliderAdapter;
    CategoriesAdapter categoriesAdapter;

    @Inject
    public HomeViewModel(SettingsRepository settingsRepository) {
        sliderAdapter = new HomeSliderAdapter();
        this.settingsRepository = settingsRepository;
        this.liveData = new MutableLiveData<>();
        settingsRepository.setLiveData(liveData);
    }

    public void setServices() {
        compositeDisposable.add(settingsRepository.homeSlider());
    }

    @Bindable
    public CategoriesAdapter getCategoriesAdapter() {
        return this.categoriesAdapter == null ? this.categoriesAdapter = new CategoriesAdapter() : this.categoriesAdapter;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    @BindingMethods({
            @BindingMethod(
                    type = BottomNavigationView.class,
                    attribute = "app:onNavigationItemSelected",
                    method = "setOnNavigationItemSelectedListener"
            ),
    })
    public class DataBindingAdapter {

    }

    public boolean onNavigationClick(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menuHome:
                liveData.setValue(new Mutable(Constants.MENU_HOME));
                return true;
            case R.id.menuOffers:
                liveData.setValue(new Mutable(Constants.MENU_OFFERS));
                return true;
            case R.id.menuWallet:
                liveData.setValue(new Mutable(Constants.MENU_WALLET));
                return true;
            case R.id.menuAccount:
                liveData.setValue(new Mutable(Constants.MENU_ACCOUNT));
                return true;
            default:
                return true;
        }
    }

    public HomeSliderAdapter getSliderAdapter() {
        return sliderAdapter;
    }

    public void setupSlider(SliderView sliderView) {
        sliderView.setSliderAdapter(sliderAdapter);
    }

    public void toNotifications() {
        liveData.setValue(new Mutable(Constants.NOTIFICATIONS));
    }

    public void toSearch() {
        liveData.setValue(new Mutable(Constants.SEARCH));
    }

}
