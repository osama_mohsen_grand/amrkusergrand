package amrk.app.amrk_user.pages.myOrders.models.reOrder;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class OrderDetailsResponse extends StatusMessage {

	@SerializedName("data")
	private OrderDetailsData data;

	public OrderDetailsData getData(){
		return data;
	}

}