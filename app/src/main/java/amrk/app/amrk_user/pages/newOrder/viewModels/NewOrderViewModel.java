package amrk.app.amrk_user.pages.newOrder.viewModels;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.newOrder.adapters.SavedLocationsAdapter;
import amrk.app.amrk_user.repository.SettingsRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class NewOrderViewModel extends BaseViewModel {
    SavedLocationsAdapter savedLocationsAdapter;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository settingsRepository;

    @Inject
    public NewOrderViewModel(SettingsRepository settingsRepository) {
        savedLocationsAdapter = new SavedLocationsAdapter();
        this.liveData = new MutableLiveData<>();
        this.settingsRepository = settingsRepository;
        this.liveData = new MutableLiveData<>();
        settingsRepository.setLiveData(liveData);
    }

    public void getSavedLocations() {
        compositeDisposable.add(settingsRepository.SavedLocations(getPassingObject().getId()));
    }

    public SavedLocationsAdapter getSavedLocationsAdapter() {
        return savedLocationsAdapter;
    }

    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    public void toGetLocationFromMap() {
        liveData.setValue(new Mutable(Constants.PICK_UP_LOCATION));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
