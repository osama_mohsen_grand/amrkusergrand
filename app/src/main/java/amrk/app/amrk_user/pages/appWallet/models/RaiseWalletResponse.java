package amrk.app.amrk_user.pages.appWallet.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class RaiseWalletResponse extends StatusMessage {

    @SerializedName("data")
    private WalletHistoryItem data;

    public WalletHistoryItem getData() {
        return data;
    }

}