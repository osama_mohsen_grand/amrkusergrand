
package amrk.app.amrk_user.pages.chat.viewmodel;

import android.text.TextUtils;
import android.util.Log;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.chat.adapter.ChatAdapter;
import amrk.app.amrk_user.pages.chat.model.Chat;
import amrk.app.amrk_user.pages.chat.model.ChatMain;
import amrk.app.amrk_user.pages.chat.model.ChatRequest;
import amrk.app.amrk_user.pages.chat.model.ConfirmPayment;
import amrk.app.amrk_user.repository.ChatRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class ChatViewModel extends BaseViewModel {
    public
    MutableLiveData<Mutable> liveData;
    @Inject
    public ChatRepository repository;
    public ChatAdapter adapter = new ChatAdapter();
    public ChatRequest request = new ChatRequest();
    public List<FileObjects> fileObjectList = new ArrayList<>();
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    private ChatMain chatMain;

    @Inject
    public ChatViewModel(ChatRepository repository) {
        chatMain = new ChatMain();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void chat() {
        compositeDisposable.add(repository.getChat(getPassingObject().getId(), getPassingObject().getObject()));
    }

    public void changeDelegate() {
        liveData.setValue(new Mutable(Constants.DISMISS));
        compositeDisposable.add(repository.changeDelegate(getPassingObject().getId()));
    }

    public void cancelOrder() {
        compositeDisposable.add(repository.CancelOrder(getPassingObject().getId()));
    }

    public void cancelOrderDialog() {
        liveData.setValue(new Mutable(Constants.REMOVE_DIALOG_WARNING));
    }

    public void changeOrderConfirm(int type) {
        compositeDisposable.add(repository.changeOrderConfirm(getPassingObject().getId(), type));
    }

    public void confirmPayment(int type) {
        compositeDisposable.add(repository.confirmPayment(new ConfirmPayment(type, getPassingObject().getId())));
    }

    public void realTimePusherChat(Chat messagesItem) {
        Log.e("realTimePusherChat", "realTimePusherChat: " + messagesItem.getConfirm() + " " + getPassingObject().getId());
        if (messagesItem != null) {
            if (messagesItem.getOrder_id() == getPassingObject().getId()) {
                if (messagesItem.getMessage() != null || messagesItem.getMessages_image() != null) {
//                    if (!TextUtils.isEmpty(messagesItem.getMessage()) || messagesItem.getMessages_image() != null) {
                    adapter.getChatList().add(messagesItem);
                    adapter.notifyItemInserted(adapter.getChatList().size() - 1);
                    liveData.postValue(new Mutable(Constants.PUSHER_LIVE_DATA));
//                    }
                }
                if (messagesItem.getConfirm() == Constants.ORDER_FINISHED) // order finished
                    liveData.postValue(new Mutable(Constants.SHOW_SUCCESS_DIALOG));
                else if (messagesItem.getConfirm() == Constants.FINISH_CHAT) {// delegate withdraw from order
                    liveData.postValue(new Mutable(Constants.HOME));
                } else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_FIRST_WAY) // Update status of order and unlock track
                    getChatMain().getOrder_status().setOn_first_way("STARTED");
            }
        }
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public void select() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    public void sendMessage() {
        request.setOrder_id(String.valueOf(getPassingObject().getId()));
        if (fileObjectList.size() > 0 || !TextUtils.isEmpty(request.getMessage())) {
            repository.sendChat(request, fileObjectList);
        }
    }

    @Bindable
    public ChatMain getChatMain() {
        return chatMain;
    }

    @Bindable
    public void setChatMain(ChatMain chatMain) {
        request.setReceiver_id(String.valueOf(chatMain.getDelegateInfo().getId()));
        adapter.update(chatMain.getChatList());
        notifyChange(BR.chatMain);
        this.chatMain = chatMain;
        Log.e("setChatMain", "setChatMain: "+chatMain.getDelegateInfo());
        UserHelper.getInstance(MyApplication.getInstance()).addSupplier(chatMain.getDelegateInfo().getSupplier_code());
    }

    public void callDelegate() {
        liveData.setValue(new Mutable(Constants.CALL));
    }

    public void followOrder() {
        liveData.setValue(new Mutable(Constants.FOLLOW_ORDER));
    }

    public void sendCurrentLocation() {
        liveData.setValue(new Mutable(Constants.SEND_CURRENT_LOCATION));
    }

    public void dismissDialog() {
        liveData.setValue(new Mutable(Constants.DISMISS));
    }

    public void toChatSettings() {
        liveData.setValue(new Mutable(Constants.CHAT_OPTIONS));
    }

    public void toReOrder() {
        liveData.setValue(new Mutable(Constants.REORDER));
    }

}
