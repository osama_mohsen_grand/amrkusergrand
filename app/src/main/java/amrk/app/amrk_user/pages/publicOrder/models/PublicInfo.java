package amrk.app.amrk_user.pages.publicOrder.models;

import com.google.gson.annotations.SerializedName;

public class PublicInfo {

    @SerializedName("rates_count")
    private String ratesCount;

    @SerializedName("avg_rates")
    private String avgRates;

    public String getRatesCount() {
        if (ratesCount != null) {
            if (Math.abs(Integer.parseInt(ratesCount) / 1000000) >= 1) {
                return ratesCount = (Integer.parseInt(ratesCount) / 1000000) + "m";
            } else if (Math.abs(Integer.parseInt(ratesCount) / 1000) >= 1) {
                return ratesCount = (Integer.parseInt(ratesCount) / 1000) + "k";
            }
        }
        return ratesCount;
    }

    public String getAvgRates() {
        return avgRates;
    }
}