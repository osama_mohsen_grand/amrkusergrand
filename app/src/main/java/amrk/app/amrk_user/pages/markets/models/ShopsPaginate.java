package amrk.app.amrk_user.pages.markets.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ShopsPaginate {
    @SerializedName("current_page")
    int currentPage;
    @SerializedName("data")
    List<ShopsItem> shops;
    @SerializedName("next_page_url")
    String nextPageUrl;

    public int getCurrentPage() {
        return currentPage;
    }

    public List<ShopsItem> getShops() {
        return shops;
    }

    public String getNextPageUrl() {
        return nextPageUrl;
    }
}
