package amrk.app.amrk_user.pages.publicOrder.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.myOrders.models.MyOrdersData;

public class CreateOrderResponse extends StatusMessage {
    @SerializedName("data")
    private MyOrdersData data;

    public MyOrdersData getData() {
        return data;
    }

}