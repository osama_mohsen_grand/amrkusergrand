package amrk.app.amrk_user.pages.newOrder;

import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.FragmentSavedLocationsBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.newOrder.models.SavedLocationsResponse;
import amrk.app.amrk_user.pages.newOrder.viewModels.NewOrderViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;

import static android.app.Activity.RESULT_OK;


public class SavedLocationsFragment extends BaseFragment {

    private Context context;
    FragmentSavedLocationsBinding binding;
    @Inject
    NewOrderViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_saved_locations, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
        }
        viewModel.getSavedLocations();
        setEvent();
        return binding.getRoot();
    }

    private static final String TAG = "SavedLocationsFragment";

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.PICK_UP_LOCATION.equals(((Mutable) o).message)) {
                if (viewModel.getPassingObject().getId() == 0) {
                    MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(getResources().getString(R.string.order_pick_location)));
                } else
                    MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(getResources().getString(R.string.order_delivered_location)));
            } else if (Constants.SAVED_LOCATIONS.equals(((Mutable) o).message)) {
                viewModel.getSavedLocationsAdapter().update(((SavedLocationsResponse) ((Mutable) o).object).getSavedLocationsData());
            }
        });
        viewModel.getSavedLocationsAdapter().getSavedLiveData().observe(((LifecycleOwner) context), savedLocationsData -> {
            Intent intent = new Intent();
            intent.putExtra(Constants.LAT, savedLocationsData.getLat());
            intent.putExtra(Constants.LNG, savedLocationsData.getLng());
            intent.putExtra(Constants.ADDRESS, savedLocationsData.getAddress());
            intent.putExtra(Constants.CITY, savedLocationsData.getCity_name());
            ((ParentActivity) context).setResult(RESULT_OK, intent);
            viewModel.goBack(context);
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        baseActivity().enableRefresh(false);
        viewModel.getSettingsRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_CODE) {
            Intent intent = new Intent();
            intent.putExtra(Constants.LAT, data.getDoubleExtra(Constants.LAT, 0.0));
            intent.putExtra(Constants.LNG, data.getDoubleExtra(Constants.LNG, 0.0));
            intent.putExtra(Constants.ADDRESS, data.getStringExtra(Constants.ADDRESS));
            intent.putExtra(Constants.CITY, data.getStringExtra(Constants.CITY));
            ((ParentActivity) context).setResult(RESULT_OK, intent);
            viewModel.goBack(context);
        }

    }
}
