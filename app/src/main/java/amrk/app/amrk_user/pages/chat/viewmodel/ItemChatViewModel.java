package amrk.app.amrk_user.pages.chat.viewmodel;

import android.view.View;
import android.webkit.URLUtil;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.pages.chat.model.Chat;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.session.UserHelper;

public class ItemChatViewModel extends BaseViewModel {
    Chat chat;
    public boolean isUrl = false;

    public ItemChatViewModel(Chat chat) {

        this.chat = chat;
        if (URLUtil.isValidUrl(chat.getMessage())) {
            isUrl = true;
        }
    }

    @Bindable
    public Chat getChat() {
        return chat;
    }

    @BindingAdapter("android:layoutDirection")
    public static void chatAdminDirection(ConstraintLayout constraintLayout, int senderType) {
        if (senderType == 0) {
            constraintLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else
            constraintLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }

    public void toMap() {
        if (URLUtil.isValidUrl(chat.getMessage())) {
            AppHelper.startAndroidGoogleMap(MyApplication.getInstance(), chat.getMessage());
        }
    }

    public void rejectOrder() {
        getLiveData().setValue(Constants.REJECT_ORDER);
    }

    public void acceptOrder() {
        getLiveData().setValue(Constants.ACCEPT_ORDER);
    }

    public void confirmPayment(String action) {
        if (getChat().getInvoice() != null)
            UserHelper.getInstance(MyApplication.getInstance()).addTempProductTotal(String.valueOf(getChat().getInvoice().getTotal()));
        getLiveData().setValue(action);
    }

}
