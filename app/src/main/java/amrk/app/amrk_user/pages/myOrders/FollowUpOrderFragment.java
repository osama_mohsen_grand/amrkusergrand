package amrk.app.amrk_user.pages.myOrders;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.directions.route.AbstractRouting;
import com.directions.route.Route;
import com.directions.route.RouteException;
import com.directions.route.Routing;
import com.directions.route.RoutingListener;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.PusherOptions;
import com.pusher.client.channel.Channel;
import com.pusher.pushnotifications.BeamsCallback;
import com.pusher.pushnotifications.PushNotifications;
import com.pusher.pushnotifications.PusherCallbackError;
import com.pusher.pushnotifications.auth.AuthData;
import com.pusher.pushnotifications.auth.BeamsTokenProvider;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.maps.LastLocationModel;
import amrk.app.amrk_user.databinding.FragmentFollowUpOrdersBinding;
import amrk.app.amrk_user.databinding.SuccessDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.chat.model.Chat;
import amrk.app.amrk_user.pages.chat.model.ChatSendResponse;
import amrk.app.amrk_user.pages.home.HomeMainFragment;
import amrk.app.amrk_user.pages.myOrders.models.followOrder.FollowStatusResponse;
import amrk.app.amrk_user.pages.myOrders.viewModels.FollowUpOrderViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.locations.MapConfig;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.services.RealTimeReceiver;
import amrk.app.amrk_user.utils.session.LanguagesHelper;

public class FollowUpOrderFragment extends BaseFragment implements OnMapReadyCallback, RoutingListener, RealTimeReceiver.MessageReceiverListener {

    private Context context;
    FragmentFollowUpOrdersBinding binding;
    @Inject
    FollowUpOrderViewModel viewModel;
    GoogleMap mMap;
    List<Polyline> polyline = null;
    Polyline polylineOld = null;
    LatLng start = null, end = null, mid = null, processing = null;
    public DatabaseReference databaseReference;
    public ValueEventListener valueEventListener;
    Marker markerDriver;
    Bundle bundle;
    Dialog dialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_follow_up_orders, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
        }
        init(savedInstanceState);
        setEvent();
        connectPusher(Constants.ORDER_CHAT_EVENT, Constants.USERSBeamName.concat(viewModel.userData.getJwt()), Constants.ORDER_CHAT_CHANNEL.concat(viewModel.userData.getJwt()));
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (mutable.message.equals(Constants.ORDER_STATUS)) {
                viewModel.setFollowOrderData(((FollowStatusResponse) mutable.object).getData());
                findRoutes();
            } else if (((Mutable) o).message.equals(Constants.SHOW_SUCCESS_DIALOG)) {
                showSuccessDialog();
            } else if (((Mutable) o).message.equals(Constants.HOME)) {
                MovementHelper.startActivityBase(requireActivity(), HomeMainFragment.class.getName(), null, null);
            }
        });
    }

    public void getDriverLastLocation() {
        databaseReference = FirebaseDatabase.getInstance().getReference("Mandoub").child(String.valueOf(viewModel.getFollowOrderData().getDelegateId()));
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@org.jetbrains.annotations.NotNull DataSnapshot dataSnapshot) {
                LastLocationModel lastDriverLocation = dataSnapshot.getValue(LastLocationModel.class);
                if (lastDriverLocation != null) {
                    addUserMarker(new LatLng(lastDriverLocation.getLat(), lastDriverLocation.getLng()), ResourceManager.getDrawable(R.drawable.ic_current), true);
                } else {
                    addUserMarker(new LatLng(Double.parseDouble(viewModel.getFollowOrderData().getDelegate().getLat()), Double.parseDouble(viewModel.getFollowOrderData().getDelegate().getLng())), ResourceManager.getDrawable(R.drawable.ic_current), true);
                }
            }

            @Override
            public void onCancelled(@org.jetbrains.annotations.NotNull DatabaseError databaseError) {
                Log.e("TAG", "loadPost:onCancelled", databaseError.toException());
            }
        };
        databaseReference.addValueEventListener(valueEventListener);
    }

    private static final String TAG = "FollowUpOrderFragment";

    // function to find Routes.
    private void findRoutes() {
        // new Trip
        start = new LatLng(Double.parseDouble(viewModel.getFollowOrderData().getDelegate().getLat()), Double.parseDouble(viewModel.getFollowOrderData().getDelegate().getLng()));
        mid = new LatLng(viewModel.getFollowOrderData().getOut_lat(), viewModel.getFollowOrderData().getOut_lng());
        end = new LatLng(viewModel.getFollowOrderData().getIn_lat(), viewModel.getFollowOrderData().getIn_lng());
        if (!TextUtils.isEmpty(viewModel.getFollowOrderData().getLast_address()))
            processing = new LatLng(viewModel.getFollowOrderData().getLast_lat(), viewModel.getFollowOrderData().getLast_lng());
        if (start == null || end == null || mid == null) {
            Toast.makeText(getActivity(), "Unable to get location", Toast.LENGTH_LONG).show();
        } else {
            Log.e(TAG, "findRoutes: " + mid);
            Routing routing;
            try {
                if (processing == null) {
                    routing = new Routing.Builder()
                            .travelMode(AbstractRouting.TravelMode.DRIVING)
                            .withListener(this)
                            .alternativeRoutes(true)
                            .waypoints(start, end)
                            .key(getString(R.string.google_map))  //also define your api key here.
                            .build();
                } else
                    routing = new Routing.Builder()
                            .travelMode(AbstractRouting.TravelMode.DRIVING)
                            .withListener(this)
                            .alternativeRoutes(true)
                            .waypoints(start, processing, end)
                            .key(getString(R.string.google_map))  //also define your api key here.
                            .build();
                routing.execute();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    //Routing call back functions.
    @Override
    public void onRoutingFailure(RouteException e) {
        e.printStackTrace();
        findRoutes();
    }

    @Override
    public void onRoutingStart() {
    }

    @Override
    public void onRoutingSuccess(ArrayList<Route> route, int shortestRouteIndex) {
        if (polyline != null) {
            polyline.clear();
        }
        PolylineOptions polyOptions = new PolylineOptions();
        polyline = new ArrayList<>();
        //add route(s) to the map using polyline
        for (int i = 0; i < route.size(); i++) {
            if (i == shortestRouteIndex) {
                polyOptions.color(getResources().getColor(R.color.black, null));
                polyOptions.width(7);
                polyOptions.addAll(route.get(i).getPoints());
                Polyline polyline = mMap.addPolyline(polyOptions);
                if (polylineOld != null) {
                    polylineOld.remove();
                } else
                    polylineOld = polyline;
                this.polyline.add(polyline);
            }
        }

        MapConfig mapConfig = new MapConfig(context, mMap);
        ArrayList<LatLng> latLngs = new ArrayList<>();
        latLngs.add(start);
//        latLngs.add(mid);
        latLngs.add(end);
        mapConfig.moveCamera(latLngs);
        addUserMarker(start, ResourceManager.getDrawable(R.drawable.ic_current), true);
//        addUserMarker(mid, ResourceManager.getDrawable(R.drawable.ic_first_location), false);
        addUserMarker(end, ResourceManager.getDrawable(R.drawable.ic_location_destination), false);
        if (processing != null) {
            latLngs.add(processing);
            addUserMarker(processing, ResourceManager.getDrawable(R.drawable.ic_first_location), false);
        }

        getDriverLastLocation();
    }

    @Override
    public void onRoutingCancelled() {
        findRoutes();
    }

    public void addUserMarker(LatLng position, Drawable drawable, boolean myLocation) {
        MarkerOptions markerOptionsFirst = new MarkerOptions();
        markerOptionsFirst.draggable(false);
        markerOptionsFirst.position(position);
        markerOptionsFirst.anchor(1f, 1f);
        markerOptionsFirst.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(drawable, drawable.getMinimumWidth(), drawable.getMinimumHeight())));
        if (markerDriver != null)
            markerDriver.remove();
        if (myLocation)
            markerDriver = mMap.addMarker(markerOptionsFirst);
        else
            mMap.addMarker(markerOptionsFirst);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        binding.followImgMap.getMapAsync(this);
        binding.followImgMap.onResume();
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    private void init(Bundle savedInstanceState) {
        binding.followImgMap.onCreate(savedInstanceState);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((AppCompatActivity) context, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        } else {
            binding.followImgMap.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(@NonNull GoogleMap googleMap) {
        mMap = googleMap;
        Log.e(TAG, "onMapReady: " );
        mMap.setOnMapLoadedCallback(() -> {
            if (bundle != null) {
                viewModel.orderStatus();
            }
        });

    }

    @Override
    public void onResume() {
        binding.followImgMap.onResume();
        super.onResume();
        MyApplication.getInstance().setMessageReceiverListener(this);
        viewModel.getChatRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onPause() {
        super.onPause();
        binding.followImgMap.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        binding.followImgMap.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        binding.followImgMap.onLowMemory();
    }

    public void connectPusher(String eventName, String beamName, String channelName) {
        PusherOptions options = new PusherOptions();
        options.setCluster("eu");
        Pusher pusher = new Pusher("75b53060b1f415501d21", options);
        pusher.connect();
        Channel channel = pusher.subscribe(channelName);
        channel.bind(eventName, event -> {
            ChatSendResponse chatSendResponse = new Gson().fromJson(event.getData(), ChatSendResponse.class);
            viewModel.realTimePusherChat(chatSendResponse.getMessagePusher());
        });
        PushNotifications.start(MyApplication.getInstance(), "4b9224fd-ac88-4e17-86bf-65c347bc0fbd");
        PushNotifications.addDeviceInterest(Constants.INTEREST);
        PushNotifications.clearAllState();
        BeamsTokenProvider tokenProvider = new BeamsTokenProvider(Constants.PUSHER_TOKEN_URL, () -> {
            HashMap<String, String> headers = new HashMap<>();
            headers.put("jwt", LanguagesHelper.getJwt());
            HashMap<String, String> queryParams = new HashMap<>();
            return new AuthData(
                    headers,
                    queryParams
            );
        });
        PushNotifications.setUserId(beamName, tokenProvider, new BeamsCallback<>() {
            @Override
            public void onSuccess(@NonNull Void... values) {
            }

            @Override
            public void onFailure(PusherCallbackError error) {
            }
        });

    }

    @Override
    public void onMessageChanged(Chat messagesItem) {
        if (messagesItem != null) {
            if (messagesItem.getOrder_id() == viewModel.getPassingObject().getId()) {
                if (messagesItem.getConfirm() == Constants.ORDER_FINISHED) // order finished
                    showSuccessDialog();
                else if (messagesItem.getConfirm() == Constants.FINISH_CHAT) // delegate withdraw from order
                    MovementHelper.startActivityBase(context, HomeMainFragment.class.getName(), null, null);
                else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_FIRST_WAY)
                    viewModel.getFollowOrderData().getOrderStatus().setOn_first_way("STARTED");
                else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_RECEIVED) // Update status of order and unlock track
                    viewModel.getFollowOrderData().getOrderStatus().setReceived(String.valueOf(Constants.ORDER_STATUS_RECEIVED));
                else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_WAY_PROCESSING) // Update status of order and unlock track
                    viewModel.getFollowOrderData().getOrderStatus().setOnWay(String.valueOf(Constants.ORDER_STATUS_WAY_PROCESSING));
                else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_PROCESSED) // Update status of order and unlock track
                    viewModel.getFollowOrderData().getOrderStatus().setProcessed(String.valueOf(Constants.ORDER_STATUS_PROCESSED));
                else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_LAST_WAY) // Update status of order and unlock track
                    viewModel.getFollowOrderData().getOrderStatus().setOnLastWay(String.valueOf(Constants.ORDER_STATUS_LAST_WAY));
                viewModel.notifyChange();
            }
        }
    }

    private void showSuccessDialog() {
        dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        SuccessDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.success_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        binding.msg.setText(getString(R.string.order_delivered));
        binding.userName.setVisibility(View.GONE);
        dialog.setOnDismissListener(dialog -> {
            dialog.dismiss();
            MovementHelper.startActivityBase(context, HomeMainFragment.class.getName(), null, null);
        });
        dialog.show();
    }
}
