package amrk.app.amrk_user.pages.publicOrder.models.newOrder;

import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import kotlin.jvm.Transient;

public class NewOrderRequest implements Serializable {
    @SerializedName("department_id")
    private int department_id = 3;
    @SerializedName("title")
    private String title;
    @SerializedName("total_cost")
    private String totalCost;
    @SerializedName("delivery_time")
    private String delivery_time;
    @SerializedName("notes")
    private String notes;
    @SerializedName("in_lat")
    private double in_lat;
    @SerializedName("in_lng")
    private double in_lng;
    @SerializedName("in_address")
    private String in_addresss;
    @SerializedName("in_city_name")
    private String in_city_name;
    @SerializedName("last_lat")
    private double last_lat;
    @SerializedName("last_lng")
    private double last_lng;
    @SerializedName("last_address")
    private String last_address;
    @SerializedName("last_city_name")
    private String last_city_name;
    @SerializedName("out_lat")
    private double out_lat;
    @SerializedName("out_lng")
    private double out_lng;
    @SerializedName("out_address")
    private String out_addresss;
    @SerializedName("out_city_name")
    private String out_city_name;
    @SerializedName("shop_name")
    private String shop_name_google;
    @SerializedName("shop_image")
    private String shop_image_google;
    @SerializedName("distance")
    private String shop_distance_google;
    @SerializedName("sub_department_id")
    private String sub_department_id;
    @SerializedName("promo_id")
    private int copon_id;
    private List<OrderImages> orderImages;
    @SerializedName("products")
    private List<ProductDetails> productDetailsList;
    @SerializedName("shop_id")
    private String shopId;
    @Transient
    private String shopImage;
    @SerializedName("order_id")
    private String orderId;
    @SerializedName("sub_total")
    private String subTotal;
    @SerializedName("tax")
    private String tax;

    @Transient
    private String isProcessing = "false";
    public ObservableField<Boolean> isProcessingError = new ObservableField<>();
    // temp vars
    public transient double discount;
    public transient double tempCost;

    public boolean isTitleValid() {
        return (!TextUtils.isEmpty(title) && !TextUtils.isEmpty(sub_department_id));
    }

    public boolean isValid() {
        return (
                !TextUtils.isEmpty(title) &&
                        !TextUtils.isEmpty(out_addresss) &&
                        !TextUtils.isEmpty(out_city_name) &&
                        out_lng != 0.0 &&
                        out_lat != 0.0 &&
                        !TextUtils.isEmpty(in_addresss) &&
                        !TextUtils.isEmpty(in_city_name) &&
                        in_lng != 0.0 &&
                        in_lat != 0.0 &&
                        !TextUtils.isEmpty(delivery_time) &&
                        department_id != 0.0
        );
    }

    public boolean isHeavyValid() {
        if (getIsProcessing().equals("false"))
            return (
                    !TextUtils.isEmpty(out_addresss) &&
                            !TextUtils.isEmpty(out_city_name) &&
                            out_lng != 0.0 &&
                            out_lat != 0.0 &&
                            !TextUtils.isEmpty(in_addresss) &&
                            !TextUtils.isEmpty(in_city_name) &&
                            in_lng != 0.0 &&
                            in_lat != 0.0 &&
                            !TextUtils.isEmpty(sub_department_id) &&
                            !TextUtils.isEmpty(delivery_time) &&
                            department_id != 0.0
            );
        else
            return (
                    !TextUtils.isEmpty(sub_department_id) &&
                            !TextUtils.isEmpty(out_addresss) &&
                            !TextUtils.isEmpty(out_city_name) &&
                            out_lng != 0.0 &&
                            out_lat != 0.0 &&
                            !TextUtils.isEmpty(in_addresss) &&
                            !TextUtils.isEmpty(in_city_name) &&
                            in_lng != 0.0 &&
                            in_lat != 0.0 &&
                            !TextUtils.isEmpty(last_address) &&
                            !TextUtils.isEmpty(last_city_name) &&
                            last_lat != 0.0 &&
                            last_lng != 0.0 &&
                            !TextUtils.isEmpty(delivery_time) &&
                            department_id != 0.0
            );
    }

    public boolean isStoreValid() {
        return (
                !TextUtils.isEmpty(out_addresss) &&
                        !TextUtils.isEmpty(out_city_name) &&
                        out_lng != 0.0 &&
                        out_lat != 0.0 &&
                        !TextUtils.isEmpty(delivery_time) &&
                        department_id != 0.0
        );
    }

    public List<OrderImages> getOrderImages() {
        return orderImages;
    }

    public void setOrderImages(List<OrderImages> orderImages) {
        this.orderImages = orderImages;
    }

    public int getDepartment_id() {
        return department_id;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public void setDelivery_time(String delivery_time) {
        this.delivery_time = delivery_time;
    }

    public void setDepartment_id(int department_id) {
        this.department_id = department_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getNotes() {
        return notes;
    }

    public void setNotes(String notes) {
        this.notes = notes;
    }

    public double getIn_lat() {
        return in_lat;
    }

    public void setIn_lat(double in_lat) {
        this.in_lat = in_lat;
    }

    public double getIn_lng() {
        return in_lng;
    }

    public void setIn_lng(double in_lng) {
        this.in_lng = in_lng;
    }

    public String getIn_addresss() {
        return in_addresss;
    }

    public void setIn_addresss(String in_addresss) {
        this.in_addresss = in_addresss;
    }

    public String getIn_city_name() {
        return in_city_name;
    }

    public void setIn_city_name(String in_city_name) {
        this.in_city_name = in_city_name;
    }

    public String getOut_city_name() {
        return out_city_name;
    }

    public void setOut_city_name(String out_city_name) {
        this.out_city_name = out_city_name;
    }

    public double getOut_lat() {
        return out_lat;
    }

    public void setOut_lat(double out_lat) {
        this.out_lat = out_lat;
    }

    public double getOut_lng() {
        return out_lng;
    }

    public void setOut_lng(double out_lng) {
        this.out_lng = out_lng;
    }

    public String getOut_addresss() {
        return out_addresss;
    }

    public void setOut_addresss(String out_addresss) {
        this.out_addresss = out_addresss;
    }

    public int getCopon_id() {
        return copon_id;
    }

    public void setCopon_id(int copon_id) {
        this.copon_id = copon_id;
    }

    public List<ProductDetails> getProductDetailsList() {
        return productDetailsList;
    }

    public void setProductDetailsList(List<ProductDetails> productDetailsList) {
        this.productDetailsList = productDetailsList;
    }

    public String getShopId() {
        return shopId;
    }

    public void setShopId(String shopId) {
        this.shopId = shopId;
    }

    public String getShopImage() {
        return shopImage;
    }

    public void setShopImage(String shopImage) {
        this.shopImage = shopImage;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public String getShop_name_google() {
        return shop_name_google;
    }

    public void setShop_name_google(String shop_name_google) {
        this.shop_name_google = shop_name_google;
    }

    public String getShop_image_google() {
        return shop_image_google;
    }

    public void setShop_image_google(String shop_image_google) {
        this.shop_image_google = shop_image_google;
    }

    public String getShop_distance_google() {
        return shop_distance_google;
    }

    public void setShop_distance_google(String shop_distance_google) {
        this.shop_distance_google = shop_distance_google;
    }

    public String getSub_department_id() {
        return sub_department_id;
    }

    public void setSub_department_id(String sub_department_id) {
        this.sub_department_id = sub_department_id;
    }

    public double getLast_lat() {
        return last_lat;
    }

    public void setLast_lat(double last_lat) {
        this.last_lat = last_lat;
    }

    public double getLast_lng() {
        return last_lng;
    }

    public void setLast_lng(double last_lng) {
        this.last_lng = last_lng;
    }

    public String getLast_address() {
        return last_address;
    }

    public void setLast_address(String last_address) {
        this.last_address = last_address;
    }

    public String getLast_city_name() {
        return last_city_name;
    }

    public void setLast_city_name(String last_city_name) {
        this.last_city_name = last_city_name;
    }

    public String getIsProcessing() {
        return isProcessing;
    }

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public void setIsProcessing(String isProcessing) {
        if (isProcessing.equals("false")) {
            setLast_address(null);
            setLast_city_name(null);
            setLast_lng(0.0);
            setLast_lat(0.0);
        }
        isProcessingError.set(isProcessing.equals("true"));
        this.isProcessing = isProcessing;
    }

    public String getSubTotal() {
        return subTotal;
    }

    public void setSubTotal(String subTotal) {
        this.subTotal = subTotal;
    }

    public void setTax(String tax) {
        this.tax = tax;
    }

    public String getTax() {
        return tax;
    }

    @Override
    public String toString() {
        return "NewOrderRequest{" +
                "department_id=" + department_id +
                ", title='" + title + '\'' +
                ", totalCost='" + totalCost + '\'' +
                ", delivery_time='" + delivery_time + '\'' +
                ", notes='" + notes + '\'' +
                ", in_lat=" + in_lat +
                ", in_lng=" + in_lng +
                ", in_addresss='" + in_addresss + '\'' +
                ", in_city_name='" + in_city_name + '\'' +
                ", last_lat=" + last_lat +
                ", last_lng=" + last_lng +
                ", last_address='" + last_address + '\'' +
                ", last_city_name='" + last_city_name + '\'' +
                ", out_lat=" + out_lat +
                ", out_lng=" + out_lng +
                ", out_addresss='" + out_addresss + '\'' +
                ", out_city_name='" + out_city_name + '\'' +
                ", shop_name_google='" + shop_name_google + '\'' +
                ", shop_image_google='" + shop_image_google + '\'' +
                ", shop_distance_google='" + shop_distance_google + '\'' +
                ", sub_department_id='" + sub_department_id + '\'' +
                ", copon_id=" + copon_id +
                ", orderImages=" + orderImages +
                ", productDetailsList=" + productDetailsList +
                ", shopId='" + shopId + '\'' +
                ", shopImage='" + shopImage + '\'' +
                ", isProcessing='" + isProcessing + '\'' +
                ", isProcessingError=" + isProcessingError +
                '}';
    }
}
