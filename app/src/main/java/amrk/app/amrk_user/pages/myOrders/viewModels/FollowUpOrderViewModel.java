package amrk.app.amrk_user.pages.myOrders.viewModels;


import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.chat.model.Chat;
import amrk.app.amrk_user.pages.home.HomeMainFragment;
import amrk.app.amrk_user.pages.myOrders.models.followOrder.FollowOrderData;
import amrk.app.amrk_user.repository.ChatRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class FollowUpOrderViewModel extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    ChatRepository chatRepository;
    private FollowOrderData followOrderData;

    @Inject
    public FollowUpOrderViewModel(ChatRepository chatRepository) {
        followOrderData = new FollowOrderData();
        this.chatRepository = chatRepository;
        this.liveData = new MutableLiveData<>();
        chatRepository.setLiveData(liveData);
    }

    public void orderStatus() {
        compositeDisposable.add(chatRepository.getOrderStatus(getPassingObject().getId()));
    }

    public void realTimePusherChat(Chat messagesItem) {
        if (messagesItem.getOrder_id() == getPassingObject().getId()) {
            if (messagesItem.getConfirm() == Constants.ORDER_FINISHED) // order finished
                liveData.postValue(new Mutable(Constants.SHOW_SUCCESS_DIALOG));
            else if (messagesItem.getConfirm() == Constants.FINISH_CHAT) // delegate withdraw from order
                liveData.postValue(new Mutable(Constants.HOME));
            else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_FIRST_WAY)
                getFollowOrderData().getOrderStatus().setOn_first_way("STARTED");
            else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_RECEIVED) // Update status of order and unlock track
                getFollowOrderData().getOrderStatus().setReceived(String.valueOf(Constants.ORDER_STATUS_RECEIVED));
            else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_WAY_PROCESSING) // Update status of order and unlock track
                getFollowOrderData().getOrderStatus().setOnWay(String.valueOf(Constants.ORDER_STATUS_WAY_PROCESSING));
            else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_PROCESSED) // Update status of order and unlock track
                getFollowOrderData().getOrderStatus().setProcessed(String.valueOf(Constants.ORDER_STATUS_PROCESSED));
            else if (messagesItem.getConfirm() == Constants.ORDER_STATUS_LAST_WAY) // Update status of order and unlock track
                getFollowOrderData().getOrderStatus().setOnLastWay(String.valueOf(Constants.ORDER_STATUS_LAST_WAY));
            notifyChange();
        }
    }

    public ChatRepository getChatRepository() {
        return chatRepository;
    }

    @Bindable
    public FollowOrderData getFollowOrderData() {
        return followOrderData;
    }

    public void toGoogleMap() {
        if (getFollowOrderData().getDelegate() != null)
            AppHelper.startAndroidGoogleMap(MyApplication.getInstance(), new LatLng(Double.parseDouble(UserHelper.getInstance(MyApplication.getInstance()).getSaveLastKnownLocationLat()), Double.parseDouble(UserHelper.getInstance(MyApplication.getInstance()).getSaveLastKnownLocationLng())), new LatLng(Double.parseDouble(getFollowOrderData().getDelegate().getLat()), Double.parseDouble(getFollowOrderData().getDelegate().getLng())));
    }

    @Bindable
    public void setFollowOrderData(FollowOrderData followOrderData) {
        notifyChange(BR.followOrderData);
        this.followOrderData = followOrderData;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }


}
