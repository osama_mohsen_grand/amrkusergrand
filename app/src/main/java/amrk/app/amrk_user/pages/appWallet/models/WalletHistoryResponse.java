package amrk.app.amrk_user.pages.appWallet.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class WalletHistoryResponse extends StatusMessage {

    @SerializedName("data")
    private HistoryWalletData data;

    public HistoryWalletData getData() {
        return data;
    }
}