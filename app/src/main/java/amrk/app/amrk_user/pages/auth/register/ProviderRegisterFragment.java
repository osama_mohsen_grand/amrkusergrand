package amrk.app.amrk_user.pages.auth.register;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import java.io.File;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.databinding.FragmentProviderRegisterBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.confirmCode.ConfirmCodeFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.UserHelper;
import amrk.app.amrk_user.utils.upload.FileOperations;

public class ProviderRegisterFragment extends BaseFragment {
    private Context context;
    private FragmentProviderRegisterBinding binding;
    @Inject
    RegisterViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_provider_register, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        binding.ccp.setCustomMasterCountries(UserHelper.getInstance(context).getCountryCodes());
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.cpp = binding.ccp.getDefaultCountryCode();
        binding.ccp.setOnCountryChangeListener(() -> {
            if (viewModel.getRequest().getPhone().contains(viewModel.cpp)) {
                viewModel.getRequest().setPhone(viewModel.getRequest().getPhone().replace(viewModel.cpp, ""));
            }
            viewModel.cpp = binding.ccp.getSelectedCountryCode();
        });

        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.IMAGE:
                    pickImageDialogSelect();
                    break;
                case Constants.REGISTER:
                    toastMessage(((Mutable) o).message);
                    viewModel.goBack(context);
                    MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.CHECK_CONFIRM_NAV_REGISTER, viewModel.getRequest().getPhone()), null, ConfirmCodeFragment.class.getName(), null, null);
                    break;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            FileObjects fileObject = FileOperations.getFileObjects(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            viewModel.setImage(fileObject);
            binding.imgRegister.setImageURI(Uri.fromFile(new File(fileObject.getFilePath())));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
