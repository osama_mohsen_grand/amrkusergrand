package amrk.app.amrk_user.pages.reviews.viewModels;


import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.reviews.adapters.ClientReviewsAdapter;
import amrk.app.amrk_user.pages.reviews.models.RateRequest;
import amrk.app.amrk_user.pages.reviews.models.ReviewsResponse;
import amrk.app.amrk_user.repository.MarketRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class ReviewsViewModel extends BaseViewModel {
    ClientReviewsAdapter reviewsAdapter;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    MarketRepository marketRepository;
    private ReviewsResponse reviewsResponse;
    RateRequest rateRequest;

    @Inject
    public ReviewsViewModel(MarketRepository marketRepository) {
        rateRequest = new RateRequest();
        reviewsAdapter = new ClientReviewsAdapter();
        reviewsResponse = new ReviewsResponse();
        this.marketRepository = marketRepository;
        this.liveData = new MutableLiveData<>();
        marketRepository.setLiveData(liveData);
    }

    public void clientReviews() {
        compositeDisposable.add(marketRepository.getClientReviews(getPassingObject().getId(), getPassingObject().getObject()));
    }

    public void delegateReviews() {
        compositeDisposable.add(marketRepository.getDelegateReviews(getPassingObject().getId()));
    }

    public void sendRate() {
        if (getPassingObject().getObject().equals(Constants.GET_PUBLIC_ORDER_INFO)) {
            getRateRequest().setDepartment_id(3);
        } else
            getRateRequest().setShop_id(getPassingObject().getId());
        if (getRateRequest().isValid())
            compositeDisposable.add(marketRepository.sendReview(getRateRequest()));
    }

    public void toRate() {
        liveData.setValue(new Mutable(Constants.REVIEW_NEW));
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        getRateRequest().setRate(String.valueOf(rating));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public MarketRepository getMarketRepository() {
        return marketRepository;
    }

    public ClientReviewsAdapter getReviewsAdapter() {
        return reviewsAdapter;
    }

    @Bindable
    public ReviewsResponse getReviewsResponse() {
        return reviewsResponse;
    }

    @Bindable
    public void setReviewsResponse(ReviewsResponse reviewsResponse) {
        getReviewsAdapter().update(reviewsResponse.getReviewsData().getRates());
        notifyChange(BR.reviewsResponse);
        this.reviewsResponse = reviewsResponse;
    }

    public RateRequest getRateRequest() {
        return rateRequest;
    }

    @Bindable
    public void setRateRequest(RateRequest rateRequest) {
        notifyChange(BR.rateRequest);
        this.rateRequest = rateRequest;
    }

    public void checkReviews() {
        if (getPassingObject().getObject().equals(Constants.DELEGATE_REVIEWS))
            delegateReviews();
        else
           clientReviews();
    }
}
