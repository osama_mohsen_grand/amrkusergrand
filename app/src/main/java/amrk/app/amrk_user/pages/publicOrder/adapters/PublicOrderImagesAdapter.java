package amrk.app.amrk_user.pages.publicOrder.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemOrderImageBinding;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.OrderImages;
import amrk.app.amrk_user.pages.publicOrder.viewModels.ItemPublicOrderImagesViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class PublicOrderImagesAdapter extends RecyclerView.Adapter<PublicOrderImagesAdapter.MenuView> {
    public List<OrderImages> orderImagesList;
    private Context context;
    private MutableLiveData<Integer> newImageLiveData = new MediatorLiveData<>();

    public PublicOrderImagesAdapter() {
        this.orderImagesList = new ArrayList<>();
    }

    public MutableLiveData<Integer> getNewImageLiveData() {
        return newImageLiveData;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_order_image,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        OrderImages menuModel = orderImagesList.get(position);
        ItemPublicOrderImagesViewModel itemMenuViewModel = new ItemPublicOrderImagesViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) MovementHelper.unwrap(context)), o -> {
            if (o.equals(Constants.NEW_IMAGE)) {
                newImageLiveData.setValue(position);
            } else if (o.equals(Constants.REMOVE_IMAGE)) {
                orderImagesList.get(position).setUri("");
                holder.itemMenuBinding.orderImage.setImageDrawable(ResourceManager.getDrawable(R.drawable.border_gray));
                notifyDataSetChanged();
            }
        });
        if (!menuModel.getUri().isEmpty())
            itemMenuViewModel.setSelected(true);
        else
            itemMenuViewModel.setSelected(false);
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<OrderImages> dataList) {
        this.orderImagesList.clear();
        orderImagesList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return orderImagesList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemOrderImageBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemPublicOrderImagesViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
