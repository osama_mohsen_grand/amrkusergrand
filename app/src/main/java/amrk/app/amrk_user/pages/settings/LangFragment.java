package amrk.app.amrk_user.pages.settings;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import javax.inject.Inject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentLangBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.onBoard.OnBoardFragment;
import amrk.app.amrk_user.pages.settings.viewModels.SettingsViewModel;
import amrk.app.amrk_user.pages.splash.SplashFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import amrk.app.amrk_user.utils.session.UserHelper;

public class LangFragment extends BaseFragment {
    @Inject
    SettingsViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentLangBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_lang, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.LANGUAGE)) {
                LanguagesHelper.setLanguage(viewModel.lang);
                if (UserHelper.getInstance(MyApplication.getInstance()).getIsFirst()) {
                    MovementHelper.startActivityBase(requireActivity(), OnBoardFragment.class.getName(), null, null);
                } else
                    MovementHelper.startActivityBase(requireActivity(), SplashFragment.class.getName(), null, null);

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getRepository().setLiveData(viewModel.liveData);
    }

}
