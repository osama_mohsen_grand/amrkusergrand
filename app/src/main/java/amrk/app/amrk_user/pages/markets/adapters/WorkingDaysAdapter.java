package amrk.app.amrk_user.pages.markets.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemWorkingDaysBinding;
import amrk.app.amrk_user.pages.markets.models.marketDetails.DaysItem;
import amrk.app.amrk_user.pages.markets.viewModels.ItemWorkingDaysViewModel;

public class WorkingDaysAdapter extends RecyclerView.Adapter<WorkingDaysAdapter.MenuView> {
    private List<DaysItem> menuModels;
     private Context context;

    public WorkingDaysAdapter() {
        this.menuModels = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_working_days,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        DaysItem menuModel = menuModels.get(position);
        ItemWorkingDaysViewModel itemMenuViewModel = new ItemWorkingDaysViewModel(menuModel);
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<DaysItem> dataList) {
        this.menuModels.clear();
        menuModels.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemWorkingDaysBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemWorkingDaysViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
