package amrk.app.amrk_user.pages.myOrders.adapters;


import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;

import org.jetbrains.annotations.NotNull;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.pages.myOrders.NormalOrdersFragment;
import amrk.app.amrk_user.pages.myOrders.StoreOrdersFragment;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class ViewPagerAdapter extends FragmentStatePagerAdapter {
    public ViewPagerAdapter(FragmentManager fm) {
        super(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT);
    }

    @Override
    public @NotNull Fragment getItem(int position) {
        if (position == 1) {
            return new NormalOrdersFragment();
        } else {
            return new StoreOrdersFragment();
        }
    }

    @Override
    public int getCount() {
        return 2;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        String title = null;

        if (position == 1) {
            title = ResourceManager.getString(R.string.normal_orders);
        } else if (position == 0) {
            title = ResourceManager.getString(R.string.stores_orders);
        }
        return title;
    }
}
