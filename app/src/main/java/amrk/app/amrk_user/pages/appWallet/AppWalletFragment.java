package amrk.app.amrk_user.pages.appWallet;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentAppWalletBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.appWallet.models.RaiseWalletResponse;
import amrk.app.amrk_user.pages.appWallet.models.WalletHistoryResponse;
import amrk.app.amrk_user.pages.appWallet.viewModels.AppWalletViewModel;
import amrk.app.amrk_user.pages.myFatora.MyFatooraMethodFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;

public class AppWalletFragment extends BaseFragment {
    FragmentAppWalletBinding binding;
    @Inject
    AppWalletViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_app_wallet, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.walletHistory();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.RAISE_WALLET.equals(mutable.message)) {
                toastMessage(((RaiseWalletResponse) mutable.object).mMessage);
                viewModel.getAppWalletAdapter().getWalletHistoryItemList().add(((RaiseWalletResponse) mutable.object).getData());
                viewModel.getHistoryWalletData().setWallet(String.valueOf(Double.parseDouble(viewModel.getHistoryWalletData().getWallet()) + Double.parseDouble(((RaiseWalletResponse) mutable.object).getData().getAmount())));
                viewModel.getAppWalletAdapter().notifyItemInserted(viewModel.getAppWalletAdapter().getItemCount() - 1);
                viewModel.notifyChange(BR.historyWalletData);
            } else if (Constants.WALLET_HISTORY.equals(mutable.message)) {
                viewModel.setHistoryWalletData(((WalletHistoryResponse) mutable.object).getData());
            } else if (Constants.EMPTY_DRIVER.equals(mutable.message)) {
//                toastErrorMessage(getString(R.string.not_active));
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(0, viewModel.getRaiseWalletRequest().getAmount()), getString(R.string.app_wallet), MyFatooraMethodFragment.class.getName(), null, Constants.MARKETS);
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getSettingsRepository().setLiveData(viewModel.liveData);
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            viewModel.walletHistory();
//            MovementHelper.replaceHomeFragment(requireActivity(), new AppWalletFragment(), "");
        }
    }

}
