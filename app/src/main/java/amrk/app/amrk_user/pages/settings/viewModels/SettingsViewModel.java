package amrk.app.amrk_user.pages.settings.viewModels;

import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.settings.models.AboutData;
import amrk.app.amrk_user.pages.settings.models.ContactRequest;
import amrk.app.amrk_user.repository.SettingsRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class SettingsViewModel extends BaseViewModel {
    private AboutData aboutData;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository repository;
    ContactRequest contactRequest;

    @Inject
    public SettingsViewModel(SettingsRepository repository) {
        contactRequest = new ContactRequest();
        aboutData = new AboutData();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void sendContact() {
        getContactRequest().setType(0);
        if (getContactRequest().isValid()) {
            setMessage(Constants.SHOW_PROGRESS);
            compositeDisposable.add(repository.sendContact(getContactRequest()));
        }
    }

    public void onLangChange(RadioGroup radioGroup, int id) {
        if (id == R.id.arabic) {
            lang = "ar";
        } else
            lang = "en";
    }

    public void getAbout() {
        compositeDisposable.add(repository.getAbout());
    }

    public void getTerms() {
        if (getPassingObject().getObject().equals(Constants.TERMS))
            compositeDisposable.add(repository.getTerms());
        else
            compositeDisposable.add(repository.privacyPolicy());
    }

    public void changeLang() {
        liveData.setValue(new Mutable(Constants.LANGUAGE));
        if (UserHelper.getInstance(MyApplication.getInstance()).getUserData() != null)
            compositeDisposable.add(repository.updateLang(lang));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public SettingsRepository getRepository() {
        return repository;
    }

    @Bindable
    public AboutData getAboutData() {
        return aboutData;
    }

    @Bindable
    public void setAboutData(AboutData aboutData) {
        notifyChange(BR.aboutData);
        this.aboutData = aboutData;
    }

    public ContactRequest getContactRequest() {
        return contactRequest;
    }
}
