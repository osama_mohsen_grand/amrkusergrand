package amrk.app.amrk_user.pages.markets.viewModels.products;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.markets.adapters.productDetails.ProductDetailsAdapter;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.utils.Constants;

public class ItemProductVariationsViewModel extends BaseViewModel {
    public VariationsItem variationsItem;
    ProductDetailsAdapter productDetailsAdapter;

    public ItemProductVariationsViewModel(VariationsItem variationsItem) {
        this.variationsItem = variationsItem;
        productDetailsAdapter = new ProductDetailsAdapter();
        productDetailsAdapter.update(variationsItem.getOptions(), variationsItem.getRequired());
    }

    @Bindable
    public VariationsItem getVariationsItem() {
        return variationsItem;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

    public ProductDetailsAdapter getProductDetailsAdapter() {
        return productDetailsAdapter;
    }
}
