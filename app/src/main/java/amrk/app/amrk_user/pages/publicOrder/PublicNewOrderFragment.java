package amrk.app.amrk_user.pages.publicOrder;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.databinding.FragmentPublicNewOrderBinding;
import amrk.app.amrk_user.databinding.PromoDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.CheckPromoResponse;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.OrderImages;
import amrk.app.amrk_user.pages.publicOrder.models.subDepartment.SubDepartmentResponse;
import amrk.app.amrk_user.pages.publicOrder.viewModels.PublicOrdersViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.PopUp.PopUpMenuHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.upload.FileOperations;

public class PublicNewOrderFragment extends BaseFragment {
    private Context context;
    FragmentPublicNewOrderBinding binding;
    @Inject
    PublicOrdersViewModel viewModel;
    private int imagePosition;
    Dialog dialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_public_new_order, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setShopsItem(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), ShopsItem.class));
            viewModel.getSubDepartment(Constants.ORDER_ANY_THING_API_ID);
        }
        viewModel.setUpOrderImages();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            switch (((Mutable) o).message) {
                case Constants.CONTINUE_NEW_ORDER:
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(viewModel.getNewOrderRequest()), getResources().getString(R.string.new_order_bar), FragmentPublicOrderConfirm.class.getName(), null, Constants.MARKETS);
                    break;
                case Constants.CHECK_PROMO:
                    viewModel.getNewOrderRequest().setCopon_id(((CheckPromoResponse) ((Mutable) o).object).getPromoData().getId());
                    toastMessage(((CheckPromoResponse) ((Mutable) o).object).mMessage);
                    dialog.dismiss();
                    break;
                case Constants.PROMO_DIALOG:
                    showPromoDialog();
                    break;
                case Constants.GET_PUBLIC_SUB_DEPARTMENT:
                    viewModel.subDepartmentDataList = ((SubDepartmentResponse) mutable.object).getData();
                    break;
                case Constants.SUB_DEPARTMENT_DIALOG:
                    showSubDepartmentDialog();
                    break;
                case Constants.EMPTY_DRIVER:
                    toastErrorMessage(getString(R.string.empty_data));
                    break;

            }
        });
        viewModel.getImagesAdapter().getNewImageLiveData().observe(((LifecycleOwner) context), integer -> {
            imagePosition = integer;
            pickImageDialogSelect();
        });
    }

    private void showSubDepartmentDialog() {
        PopUpMenuHelper.showSubDepartmentPopUp(context, binding.serviceType, viewModel.subDepartmentDataList).setOnMenuItemClickListener(item -> {
            binding.serviceType.setText(viewModel.subDepartmentDataList.get(item.getItemId()).getName());
            viewModel.getNewOrderRequest().setSub_department_id(String.valueOf(viewModel.subDepartmentDataList.get(item.getItemId()).getId()));
            viewModel.getNewOrderRequest().setIsProcessing(viewModel.subDepartmentDataList.get(item.getItemId()).getHas_processing() == 1 ? "true" : "false");
            return false;
        });
    }

    private void showPromoDialog() {
        dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        PromoDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.promo_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        baseActivity().enableRefresh(false);
        viewModel.getPublicOrderRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            FileObjects fileObject = FileOperations.getFileObjects(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            viewModel.getImagesAdapter().orderImagesList.set(imagePosition, new OrderImages(fileObject.getFilePath()));
            viewModel.getImagesAdapter().notifyDataSetChanged();
        } else if (requestCode == Constants.RESULT_CODE) {
            viewModel.goBack(context);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
