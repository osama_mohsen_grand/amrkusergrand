package amrk.app.amrk_user.pages.myFatora;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;
import com.myfatoorah.sdk.entity.initiatepayment.MFInitiatePaymentResponse;
import com.myfatoorah.sdk.entity.paymentstatus.MFGetPaymentStatusResponse;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentMyFatooraMethodsBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.models.UsersResponse;
import amrk.app.amrk_user.pages.myFatora.viewModels.MyFatooraPaymentsViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;

public class MyFatooraMethodFragment extends BaseFragment {
    FragmentMyFatooraMethodsBinding methodsBinding;
    @Inject
    MyFatooraPaymentsViewModel viewModel;
    MyFatoraInit myFatoraInit;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        methodsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_fatoora_methods, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        methodsBinding.setViewmodel(viewModel);
        myFatoraInit = new MyFatoraInit(requireActivity(), viewModel.liveData);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            myFatoraInit.initiatePayment(Double.parseDouble(viewModel.getPassingObject().getObject()));
        }
        setEvent();
        return methodsBinding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            Log.e("setEvent", "setEvent: " + mutable.message);
            switch (((Mutable) o).message) {
                case Constants.MY_FATOORA_METHODS:
                    MFInitiatePaymentResponse paymentResponse = ((MFInitiatePaymentResponse) mutable.object);
                    if (paymentResponse.getPaymentMethods() != null)
                        viewModel.getMethodsAdapter().update(paymentResponse.getPaymentMethods());
                    break;
                case Constants.PAYMENT_METHOD:
                    if (viewModel.getMethodsAdapter().lastSelected != -1)
//                    MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(viewModel.getMethodsAdapter().getPaymentMethodList().get(viewModel.getMethodsAdapter().lastSelected)), ResourceManager.getString(R.string.card_details), SendPaymentFragment.class.getName(), null, Constants.MARKETS);
                        myFatoraInit.sendPayment(viewModel.getMethodsAdapter().getPaymentMethodList().get(viewModel.getMethodsAdapter().lastSelected).getPaymentMethodId(), Double.parseDouble(viewModel.getPassingObject().getObject()));
                    break;
                case Constants.SEND_PAYMENT:
                    MFGetPaymentStatusResponse payment = ((MFGetPaymentStatusResponse) mutable.object);
                    viewModel.checkPayment(payment);
                    break;
                case Constants.PAYMENT_ERROR:
                    viewModel.checkPaymentError(((String) mutable.object));
                    break;
                case Constants.CHECK_PAYMENT:
                    toastMessage(((UsersResponse) mutable.object).mMessage);
                    UserHelper.getInstance(requireActivity()).userLogin(((UsersResponse) mutable.object).getData());
                    if (!TextUtils.isEmpty(viewModel.getPassingObject().getObject2())) {
                        if (viewModel.getSendPaymentRequest().getStatus() == 1)
                            Constants.DATA_CHANGED = true;
                    } else
                        Constants.DATA_CHANGED = true;
                    finishActivity();
                    break;

            }
        });
    }

}
