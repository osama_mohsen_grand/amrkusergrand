package amrk.app.amrk_user.pages.heavyOrders.viewModels;

import android.text.TextUtils;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.myOrders.adapters.MyStoreOrderDetailsAdapter;
import amrk.app.amrk_user.pages.myOrders.models.MyOrdersData;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class ItemHeavyOrdersViewModel extends BaseViewModel {
    public MyOrdersData myOrdersData;
    public String orderStatus;
    MyStoreOrderDetailsAdapter myStoreOrderDetailsAdapter;

    public ItemHeavyOrdersViewModel(MyOrdersData myOrdersData) {
        this.myOrdersData = myOrdersData;
        if (!TextUtils.isEmpty(myOrdersData.getOrderStatus().getAccept())) {
            orderStatus = ResourceManager.getString(R.string.order_accepted);
        } else
            orderStatus = ResourceManager.getString(R.string.offer_waiting_acceptance);

        if (!TextUtils.isEmpty(myOrdersData.getOrderStatus().getOnWay())) {
            orderStatus = ResourceManager.getString(R.string.order_way);
        }
        if (!TextUtils.isEmpty(myOrdersData.getOrderStatus().getFinished())) {
            orderStatus = ResourceManager.getString(R.string.order_finished);
        }
        if (!TextUtils.isEmpty(myOrdersData.getOrderStatus().getCancelled())) {
            orderStatus = ResourceManager.getString(R.string.order_cancelled);
        }
        if (myOrdersData.getOrderProducts() != null)
            getMyStoreOrderDetailsAdapter().update(myOrdersData.getOrderProducts(), myOrdersData.getCurrency());
    }

    @Bindable
    public MyOrdersData getMyOrdersData() {
        return myOrdersData;
    }

    public void toChat() {
        getLiveData().setValue(Constants.CHAT);
    }

    public void toRateMandoub() {
        if (!TextUtils.isEmpty(myOrdersData.getOrderStatus().getFinished()) && TextUtils.isEmpty(myOrdersData.getOrderStatus().getCancelled()))
            getLiveData().setValue(Constants.RATE_MANDOUB);
    }

    @Bindable
    public MyStoreOrderDetailsAdapter getMyStoreOrderDetailsAdapter() {
        return this.myStoreOrderDetailsAdapter == null ? this.myStoreOrderDetailsAdapter = new MyStoreOrderDetailsAdapter() : this.myStoreOrderDetailsAdapter;
    }
}
