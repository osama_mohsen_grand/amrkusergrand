package amrk.app.amrk_user.pages.publicOrder.models.subDepartment;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class SubDepartmentResponse extends StatusMessage {

    @SerializedName("data")
    private List<SubDepartmentData> data;

    public List<SubDepartmentData> getData() {
        return data;
    }

}