package amrk.app.amrk_user.pages.markets.models.marketDetails;

import com.google.gson.annotations.SerializedName;

public class DaysItem{

	@SerializedName("from")
	private String from;

	@SerializedName("id")
	private int id;

	@SerializedName("to")
	private String to;

	@SerializedName("day_id")
	private int dayId;

	@SerializedName("day")
	private String day;

	@SerializedName("name_en")
	private String nameEn;

	public String getFrom(){
		return from;
	}

	public int getId(){
		return id;
	}

	public String getTo(){
		return to;
	}

	public int getDayId(){
		return dayId;
	}

	public String getDay(){
		return day;
	}

	public String getNameEn(){
		return nameEn;
	}
}