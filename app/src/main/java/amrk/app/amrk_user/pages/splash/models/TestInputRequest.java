package amrk.app.amrk_user.pages.splash.models;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class TestInputRequest {

    @SerializedName("password")
    private String password;

    @SerializedName("phone")
    private String phone;

    @SerializedName("email")
    private String email;


    private int validator = 0;


    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;

    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;

    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;

    }

    public void setValidator(int validator) {
        this.validator = validator;
    }


    public int getValidator() {
         return validator;
    }


    public boolean validate() {
        Log.e("validator", " Validatorz " + validator);
        return validator == 3;
    }

}
