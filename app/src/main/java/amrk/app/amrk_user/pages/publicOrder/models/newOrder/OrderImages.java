package amrk.app.amrk_user.pages.publicOrder.models.newOrder;

import com.google.gson.annotations.SerializedName;

public class OrderImages {
    private String uri;
    private boolean selected;
    @SerializedName("image")
    private String image;

    public OrderImages() {
    }

    public String getImage() {
        return image;
    }

    public OrderImages(boolean selected) {
        this.selected = selected;
    }

    public OrderImages(String uri) {
        this.uri = uri;
    }

    public String getUri() {
        return uri;
    }

    public void setUri(String uri) {
        this.uri = uri;
    }

    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }
}
