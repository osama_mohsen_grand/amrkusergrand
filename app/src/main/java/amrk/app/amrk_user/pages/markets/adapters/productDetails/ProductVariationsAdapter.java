package amrk.app.amrk_user.pages.markets.adapters.productDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemMarketMenuItemDialogCategoryBinding;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.pages.markets.viewModels.products.ItemProductVariationsViewModel;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class ProductVariationsAdapter extends RecyclerView.Adapter<ProductVariationsAdapter.MenuView> {
    List<VariationsItem> variationsItemList;
    MutableLiveData<Integer> productPriceLiveData = new MutableLiveData<>();
    Context context;
    private int lastSelect = 0;

    public ProductVariationsAdapter() {
        this.variationsItemList = new ArrayList<>();
    }

    public MutableLiveData<Integer> getProductPriceLiveData() {
        return productPriceLiveData;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_market_menu_item_dialog_category,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        VariationsItem menuModel = variationsItemList.get(position);
        ItemProductVariationsViewModel itemMenuViewModel = new ItemProductVariationsViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) MovementHelper.unwrap(context)), o -> {
            lastSelect = menuModel.getVariation_id();
            productPriceLiveData.setValue(position);
            notifyDataSetChanged();
        });
        if (lastSelect == menuModel.getVariation_id()) {
            holder.itemMenuBinding.btnCategory.setBackgroundColor(ResourceManager.getColor(R.color.colorPrimaryDark));
        } else
            holder.itemMenuBinding.btnCategory.setBackgroundColor(ResourceManager.getColor(R.color.medium_color));

        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<VariationsItem> dataList) {
        this.variationsItemList.clear();
        variationsItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return variationsItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemMarketMenuItemDialogCategoryBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemProductVariationsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
