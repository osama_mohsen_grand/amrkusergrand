package amrk.app.amrk_user.pages.markets.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class MarketResponse extends StatusMessage {
    @SerializedName("data")
    private MarketData marketData;

    public MarketData getMarketData() {
        return marketData;
    }

}