package amrk.app.amrk_user.pages.home.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.ItemCategoryBinding;
import amrk.app.amrk_user.pages.heavyDepartment.HeavyFormFragment;
import amrk.app.amrk_user.pages.home.model.DepartmentsItem;
import amrk.app.amrk_user.pages.home.viewModels.ItemCategoryViewModel;
import amrk.app.amrk_user.pages.markets.MarketsFragment;
import amrk.app.amrk_user.pages.publicOrder.PublicOrdersFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;

public class CategoriesAdapter extends RecyclerView.Adapter<CategoriesAdapter.MenuView> {
    List<DepartmentsItem> departmentsItemList;
    Context context;

    public CategoriesAdapter() {
        this.departmentsItemList = new ArrayList<>();
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        DepartmentsItem menuModel = departmentsItemList.get(position);
        ItemCategoryViewModel itemMenuViewModel = new ItemCategoryViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe((LifecycleOwner) MovementHelper.unwrap(context), o -> {
            if (o.equals(Constants.MENu)) {
                if (menuModel.getId() == Constants.STORES_API_ID)
                    MovementHelper.startActivity(context, MarketsFragment.class.getName(), menuModel.getName(), null, Constants.MARKETS);
                else if (menuModel.getId() == Constants.UBER_HOME_API_ID)
                    MovementHelper.startActivityMain(context);
                else if (menuModel.getId() == Constants.ORDER_ANY_THING_API_ID)
                    MovementHelper.startActivity(context, PublicOrdersFragment.class.getName(), menuModel.getName(), Constants.SHARE_BAR, Constants.MARKETS);
                else if (menuModel.getId() == Constants.ORDER_HEAVY_API_ID)
                    MovementHelper.startActivity(context, HeavyFormFragment.class.getName(), menuModel.getName(), null, Constants.MARKETS);
            }else {
                ((ParentActivity)context).toastError(context.getResources().getString(R.string.service_stops));
            }
        });
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<DepartmentsItem> dataList) {
        this.departmentsItemList.clear();
        departmentsItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return departmentsItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemCategoryBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
                int screenHeight = AppHelper.getScreenHeight(itemView.getContext()) - (int) itemView.getContext().getResources().getDimension(R.dimen.dp160h);
                screenHeight -= ((int) itemView.getContext().getResources().getDimension(R.dimen.dp48h));
                screenHeight -= ((int) itemView.getContext().getResources().getDimension(R.dimen.dp80h));
                int temp = (int) ((int) ((double) screenHeight / 3) - itemView.getContext().getResources().getDimension(R.dimen.dp8h));
                itemMenuBinding.homeRestaurantImg.setLayoutParams(new ConstraintLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, temp));
                //itemMenuBinding.llCategoryMainContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,(int)((double)screenHeight/2)));
                itemMenuBinding.resturantContainer.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, (int) ((double) screenHeight / 2)));
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemCategoryViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
