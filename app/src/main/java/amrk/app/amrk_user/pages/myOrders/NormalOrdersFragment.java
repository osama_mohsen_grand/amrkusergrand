package amrk.app.amrk_user.pages.myOrders;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.squareup.picasso.Picasso;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.DelegateOrderRateDialogBinding;
import amrk.app.amrk_user.databinding.FragmentNormalOrdersBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.myOrders.models.MyOrdersResponse;
import amrk.app.amrk_user.pages.myOrders.viewModels.MyOrdersViewModel;
import amrk.app.amrk_user.pages.reviews.models.RateRequest;
import amrk.app.amrk_user.utils.Constants;

public class NormalOrdersFragment extends BaseFragment {

    private Context context;
    FragmentNormalOrdersBinding binding;
    @Inject
    MyOrdersViewModel viewModel;
    Dialog dialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_normal_orders, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        viewModel.myOrders(3, 0);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.MY_ORDERS.equals(mutable.message)) {
                viewModel.getNormalOrdersAdapter().update(((MyOrdersResponse) mutable.object).getData());
                viewModel.notifyChange(BR.normalOrdersAdapter);
            } else if (Constants.RATE_MANDOUB.equals(mutable.message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                dialog.dismiss();
                viewModel.setRateRequest(new RateRequest());
                viewModel.myOrders(3, viewModel.getType());
            }
        });
        viewModel.getNormalOrdersAdapter().getRateMandoubLiveData().observe((LifecycleOwner) context, this::showMandoupRateDialog);
    }

    private void showMandoupRateDialog(int position) {
        dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        DelegateOrderRateDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.delegate_order_rate_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        if (viewModel.getNormalOrdersAdapter().getMyOrdersDataList().get(position).getDelegate() != null)
            Picasso.get().load(viewModel.getNormalOrdersAdapter().getMyOrdersDataList().get(position).getDelegate().getImage()).placeholder(R.color.overlayBackground).into(binding.ivLogoutDialogIcon);
        binding.btnSendReview.setOnClickListener(v -> viewModel.sendRate(position, 1));
        dialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            viewModel.myOrders(3, viewModel.getType());
        }
        viewModel.getMarketRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
