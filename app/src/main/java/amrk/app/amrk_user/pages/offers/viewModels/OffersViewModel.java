package amrk.app.amrk_user.pages.offers.viewModels;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.offers.adapters.OffersAdapter;
import amrk.app.amrk_user.pages.offers.adapters.OffersDetailsAdapter;
import amrk.app.amrk_user.pages.offers.models.offerDetails.OfferDetails;
import amrk.app.amrk_user.repository.MarketRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class OffersViewModel extends BaseViewModel {

    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    MarketRepository marketRepository;
    OffersAdapter offersAdapter;
    OffersDetailsAdapter detailsAdapter;
    OfferDetails offerDetails;
    private int activeOfferButton;

    @Inject
    public OffersViewModel(MarketRepository marketRepository) {
        this.marketRepository = marketRepository;
        this.liveData = new MutableLiveData<>();
        marketRepository.setLiveData(liveData);
        offerDetails = new OfferDetails();
    }

    public void offers(int departmentId) {
        setActiveOfferButton(departmentId);
        compositeDisposable.add(marketRepository.offers(departmentId));
    }

    public void oneOffer() {
        compositeDisposable.add(marketRepository.oneOffer(getPassingObject().getId()));
    }

    public void lessOffer() {
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(marketRepository.lessOffer(getPassingObject().getId()));
    }

    public void cancelOrder() {
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(marketRepository.cancelOrder(getPassingObject().getId()));
    }

    public void acceptOrder(int offerId) {
        setMessage(Constants.SHOW_PROGRESS);
        compositeDisposable.add(marketRepository.acceptOrder(offerId));
    }

    public void delegateReviews() {
        liveData.setValue(new Mutable(Constants.MARKET_REVIEWS));
    }


    @Bindable
    public OffersAdapter getOffersAdapter() {
        return this.offersAdapter == null ? this.offersAdapter = new OffersAdapter() : this.offersAdapter;
    }

    @Bindable
    public OffersDetailsAdapter getDetailsAdapter() {
        return this.detailsAdapter == null ? this.detailsAdapter = new OffersDetailsAdapter() : this.detailsAdapter;
    }

    @Bindable
    public OfferDetails getOfferDetails() {
        return offerDetails;
    }

    @Bindable
    public void setOfferDetails(OfferDetails offerDetails) {
        notifyChange(BR.offerDetails);
        this.offerDetails = offerDetails;
    }

    @Bindable
    public int getActiveOfferButton() {
        return activeOfferButton;
    }

    @Bindable
    public void setActiveOfferButton(int activeOfferButton) {
        notifyChange(BR.activeOfferButton);
        this.activeOfferButton = activeOfferButton;
    }

    public MarketRepository getMarketRepository() {
        return marketRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
