package amrk.app.amrk_user.pages.cart;

import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.FragmentCartBinding;
import amrk.app.amrk_user.databinding.PromoDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.CheckPromoResponse;
import amrk.app.amrk_user.pages.cart.viewModels.CartViewModel;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.pages.publicOrder.FragmentPublicOrderConfirm;
import amrk.app.amrk_user.pages.publicOrder.viewModels.PublicOrdersViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;


public class CartFragment extends BaseFragment {
    FragmentCartBinding binding;
    @Inject
    CartViewModel viewModel;
    @Inject
    PublicOrdersViewModel publicOrdersViewModel;
    private Dialog promoDialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_cart, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setShopsItem(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), ShopsItem.class));
        }
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.PROMO_DIALOG.equals(((Mutable) o).message)) {
                showPromoDialog();
            } else if (Constants.CONTINUE_NEW_ORDER.equals(mutable.message)) {
                viewModel.confirmBtnStatus = true;
                MovementHelper.startActivityForResultWithBundle(requireActivity(), new PassingObject(viewModel.getNewOrderRequest()), getResources().getString(R.string.new_order_bar), FragmentPublicOrderConfirm.class.getName(), null, Constants.MARKETS);
            } else if (Constants.PROMO_EXISTS.equals(mutable.message)) {
                showError(getString(R.string.promo_exists));
            }
        });
        viewModel.getCartLiveData().observe(requireActivity(), productDetails -> {
            if (productDetails.size() > 0) {
                viewModel.getCartAdapter().update(productDetails);
            } else {
                setResult(productDetails);
                viewModel.goBack(requireActivity());
            }
        });

        viewModel.getCartTotal().observe(requireActivity(), s -> {
            if (s != null) {
                viewModel.getNewOrderRequest().tempCost = Double.parseDouble(s);
                viewModel.calcCost();
            }
        });
        viewModel.getVariationsListLiveData().observe(requireActivity(), variationsItems -> viewModel.setVariationsItemList(variationsItems));
        viewModel.getOptionsListLiveData().observe(requireActivity(), optionsItems -> viewModel.setOptionsList(optionsItems));
        viewModel.getCartAdapter().getLiveDataAdapter().observe(requireActivity(), integer -> {
            setResult(viewModel.getCartAdapter().getMenuModels());
        });
    }

    private void setResult(List<ProductDetails> productDetailsList) {
        Intent intent = new Intent();
        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList(Constants.BUNDLE, (ArrayList<ProductDetails>) productDetailsList);
        intent.putExtras(bundle);
        requireActivity().setResult(Activity.RESULT_OK, intent);
    }

    @Override
    public void onResume() {
        super.onResume();
    }


    private void showPromoDialog() {
        promoDialog = new Dialog(requireActivity(), R.style.PauseDialog);
        promoDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(promoDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        promoDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        PromoDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(promoDialog.getContext()), R.layout.promo_dialog, null, false);
        promoDialog.setContentView(binding.getRoot());
        binding.setViewModel(publicOrdersViewModel);
        publicOrdersViewModel.liveData.observe(((LifecycleOwner) MovementHelper.unwrap(promoDialog.getContext())), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.CHECK_PROMO.equals(((Mutable) o).message)) {
                toastMessage(((CheckPromoResponse) ((Mutable) o).object).mMessage);
                viewModel.setPromoData(((CheckPromoResponse) ((Mutable) o).object).getPromoData());
                promoDialog.dismiss();
            }
        });
        promoDialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data != null) {
            if (requestCode == Constants.RESULT_CODE) {
                viewModel.getCartRepository().emptyCart();
                List<ProductDetails> productDetails = new ArrayList<>();
                setResult(productDetails);
                finishActivity();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }
}
