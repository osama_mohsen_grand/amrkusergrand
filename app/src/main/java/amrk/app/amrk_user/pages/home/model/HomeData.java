package amrk.app.amrk_user.pages.home.model;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class HomeData {

    @SerializedName("departments")
    private List<DepartmentsItem> departments;
    @SerializedName("fee")
    private String fee;

    @SerializedName("sliders")
    private List<SlidersItem> sliders;

    public List<DepartmentsItem> getDepartments() {
        return departments;
    }

    public List<SlidersItem> getSliders() {
        return sliders;
    }

    public String getFee() {
        return fee;
    }
}