package amrk.app.amrk_user.pages.markets.models.marketDetails;

import com.google.gson.annotations.SerializedName;

public class ProductsItem {

    @SerializedName("price_before")
    private String priceBefore;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("rate")
    private Object rate;

    @SerializedName("price_after")
    private String priceAfter;

    @SerializedName("name")
    private String name;

    @SerializedName("has_discount")
    private String hasDiscount;

    @SerializedName("description")
    private String description;

    @SerializedName("selling")
    private String selling;

    @SerializedName("id")
    private int id;

    @SerializedName("percent")
    private Object percent;

    @SerializedName("status")
    private String status;
    @SerializedName("image")
    private String image;
    @SerializedName("product_room_id")
    private int productRoomId;

    public int getProductRoomId() {
        return productRoomId;
    }

    public void setProductRoomId(int productRoomId) {
        this.productRoomId = productRoomId;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getImage() {
        return image;
    }

    public String getPriceBefore() {
        return priceBefore;
    }

    public int getQuantity() {
        return quantity;
    }

    public Object getRate() {
        return rate;
    }

    public String getPriceAfter() {
        return priceAfter;
    }

    public String getName() {
        return name;
    }

    public String getHasDiscount() {
        return hasDiscount;
    }

    public String getDescription() {
        return description;
    }

    public String getSelling() {
        return selling;
    }

    public int getId() {
        return id;
    }

    public Object getPercent() {
        return percent;
    }

    public String getStatus() {
        return status;
    }
}