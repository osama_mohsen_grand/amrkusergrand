package amrk.app.amrk_user.pages.countries.models;

import java.util.List;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class CountriesCodesResponse extends StatusMessage {

    @SerializedName("data")
    private List<String> data;

    public List<String> getData() {
        return data;
    }

}