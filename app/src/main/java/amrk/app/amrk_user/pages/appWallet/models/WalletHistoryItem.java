package amrk.app.amrk_user.pages.appWallet.models;

import com.google.gson.annotations.SerializedName;

public class WalletHistoryItem {

	@SerializedName("amount")
	private String amount;

	@SerializedName("user_id")
	private int userId;

	@SerializedName("transaction_id")
	private String paymentId;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;
//  "id": 57,
//		  "attribute_id": 62,
//		  "attribute_type": "App\\Models\\User",
//		  "transaction_id": "1043872",
//		  "status": 1,
//		  "type": 0,
//		  "amount": "10",
//		  "created_at": "2021-10-04 08:27:31",
//		  "updated_at": "2021-10-04 08:27:31"
	public String getAmount(){
		return amount;
	}

	public int getUserId(){
		return userId;
	}

	public String getPaymentId(){
		return paymentId;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}
}