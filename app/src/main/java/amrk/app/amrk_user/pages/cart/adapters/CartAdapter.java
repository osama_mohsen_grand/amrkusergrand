package amrk.app.amrk_user.pages.cart.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemCartBinding;
import amrk.app.amrk_user.pages.cart.viewModels.ItemCartViewModel;
import amrk.app.amrk_user.pages.markets.FragmentMenuItemSheetDialog;
import amrk.app.amrk_user.pages.markets.MarketDetailsFragment;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;


public class CartAdapter extends RecyclerView.Adapter<CartAdapter.MenuView> {
    List<ProductDetails> menuModels;
    MutableLiveData<Integer> liveDataAdapter = new MutableLiveData<>();
    private Context context;

    public CartAdapter() {
        this.menuModels = new ArrayList<>();
    }

    public List<ProductDetails> getMenuModels() {
        return menuModels;
    }

    public MutableLiveData<Integer> getLiveDataAdapter() {
        return liveDataAdapter;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_cart,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        ProductDetails menuModel = menuModels.get(position);
        ItemCartViewModel itemMenuViewModel = new ItemCartViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            if (o.equals(Constants.PLUS) || o.equals(Constants.MINUS) || o.equals(Constants.REMOVE_FROM_CART)) {
                notifyItemChanged(position);
                liveDataAdapter.setValue(menuModel.getProduct_id());
            } else if (o.equals(Constants.PRODUCT_DETAILS)) {
                FragmentMenuItemSheetDialog fragmentMenuItemSheetDialog = new FragmentMenuItemSheetDialog();
                fragmentMenuItemSheetDialog.setListner((quantity, productRoomId) -> {
                    menuModel.setQuantity(menuModel.getQuantity() + quantity);
                    menuModel.setProduct_room_id(productRoomId);
                    liveDataAdapter.setValue(menuModel.getProduct_id());
                    notifyItemChanged(position);
                });
                MovementHelper.startDialogWithBundle(context, new PassingObject(menuModel), fragmentMenuItemSheetDialog, new MarketDetailsFragment());
            }
        });
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<ProductDetails> dataList) {
        this.menuModels.clear();
        menuModels.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemCartBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemCartViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
