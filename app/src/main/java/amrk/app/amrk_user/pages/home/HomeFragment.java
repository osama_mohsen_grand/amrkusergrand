package amrk.app.amrk_user.pages.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.FragmentHomeBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.home.model.HomeResponse;
import amrk.app.amrk_user.pages.home.viewModels.HomeViewModel;
import amrk.app.amrk_user.pages.markets.MarketsFragment;
import amrk.app.amrk_user.pages.notifications.NotificationsFragment;
import amrk.app.amrk_user.pages.publicOrder.PublicOrdersFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.UserHelper;


public class HomeFragment extends BaseFragment {
    private Context context;
    @Inject
    HomeViewModel viewModel;
    FragmentHomeBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        viewModel.setServices();
        binding.setViewmodel(viewModel);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.STORES.equals(((Mutable) o).message)) {
                MovementHelper.startActivity(context, MarketsFragment.class.getName(), getResources().getString(R.string.market_page), null, Constants.MARKETS);
            } else if (Constants.ORDER_ANY_THING.equals(((Mutable) o).message)) {
                MovementHelper.startActivity(context, PublicOrdersFragment.class.getName(), getResources().getString(R.string.public_order_bar_name), Constants.SHARE_BAR, Constants.MARKETS);
            } else if (Constants.NOTIFICATIONS.equals(((Mutable) o).message)) {
                UserHelper.getInstance(context).addCountNotification(0);
                binding.badge.setText(String.valueOf(UserHelper.getInstance(context).getCountNotification()));
                MovementHelper.startActivity(context, NotificationsFragment.class.getName(), getString(R.string.menuNotifications), null, Constants.MARKETS);
            } else if (Constants.SEARCH.equals(mutable.message)) {
                MovementHelper.startActivity(context, SearchFragment.class.getName(), null, null, null);
            } else if (Constants.UBER_HOME.equals(mutable.message)) {
                MovementHelper.startActivityMain(context);
            } else if (Constants.HOME.equals(mutable.message)) {
                viewModel.getSliderAdapter().updateData(((HomeResponse) mutable.object).getData().getSliders());
                viewModel.getCategoriesAdapter().update(((HomeResponse) mutable.object).getData().getDepartments());
                viewModel.setupSlider(binding.imageSlider);
                UserHelper.getInstance(context).addFee(((HomeResponse) mutable.object).getData().getFee());

            }
        });
        ((ParentActivity) context).notificationsCount.observe((LifecycleOwner) context, count -> {
            binding.badge.setVisibility(View.VISIBLE);
            binding.badge.setText(String.valueOf(count));
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        viewModel.getSettingsRepository().setLiveData(viewModel.liveData);
        if (UserHelper.getInstance(context).getCountNotification() != 0)
            binding.badge.setText(String.valueOf(UserHelper.getInstance(context).getCountNotification()));
        else
            binding.badge.setVisibility(View.GONE);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
