package amrk.app.amrk_user.pages.chat.model;

import com.google.gson.annotations.SerializedName;

public class Invoice {
    @SerializedName("id")
    private int id;
    @SerializedName("order_id")
    private int orderId;

    @SerializedName("total")
    private double total;

    @SerializedName("sub_total")
    private double subTotal;
    @SerializedName("tax")
    private double tax;
    @SerializedName("offer")
    private double offer;
    @SerializedName("tax_percentage")
    private double tax_percentage;
    @SerializedName("confirmed")
    private int confirmed;

    public int getId() {
        return id;
    }

    public int getOrderId() {
        return orderId;
    }

    public double getTotal() {
        return total;
    }

    public double getSubTotal() {
        return subTotal;
    }

    public double getTax() {
        return tax;
    }

    public double getOffer() {
        return offer;
    }

    public double getTax_percentage() {
        return tax_percentage;
    }

    public int getConfirmed() {
        return confirmed;
    }
}
