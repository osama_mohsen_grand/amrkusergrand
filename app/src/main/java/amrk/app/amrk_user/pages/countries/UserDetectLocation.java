package amrk.app.amrk_user.pages.countries;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.core.app.ActivityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.model.LatLng;

import org.jetbrains.annotations.NotNull;


import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.CheckLocationDialogBinding;
import amrk.app.amrk_user.databinding.FragmentDetectLocationBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.home.HomeMainFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.UserHelper;

public class UserDetectLocation extends BaseFragment {
    private Context context;
    @Inject
    CountriesViewModel viewModel;
    Dialog dialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentDetectLocationBinding countriesBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_detect_location, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        countriesBinding.setViewmodel(viewModel);
        setEvent();
        return countriesBinding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.HOME)) {
                initLastLocation();
                showCheckLocationDialog();
            } else if (((Mutable) o).message.equals(Constants.PICK_UP_LOCATION)) {
                MovementHelper.startMapActivityForResultWithBundle(context, new PassingObject(1, getResources().getString(R.string.choose_location)));
            }
        });
        ((ParentActivity) context).locationLiveData.observe(((LifecycleOwner) context), aBoolean -> {
            if (aBoolean) {
                ((ParentActivity) context).locationManager.removeUpdates(((ParentActivity) context).locationListenerGPS);
                MovementHelper.startActivityBase(context, HomeMainFragment.class.getName(), null, null);
            }
        });
    }

    private void showCheckLocationDialog() {
        dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CheckLocationDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.check_location_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        dialog.show();
    }

    public void initLastLocation() {
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            ((ParentActivity) context).enableLocationDialog();
            return;
        }
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener(((Activity) context), location -> new Handler(Looper.getMainLooper()).postDelayed(() -> {
                    if (location != null) {
                        UserHelper.getInstance(context).addSaveLastKnownLocation(new LatLng(location.getLatitude(), location.getLongitude()));
                        MovementHelper.startActivityBase(context, HomeMainFragment.class.getName(), null, null);
                    } else
                        ((ParentActivity) context).enableLocationDialog();
                }, 1500));

    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Constants.RESULT_CODE) {
            Intent intent = new Intent();
            intent.putExtra(Constants.LAT, data.getDoubleExtra(Constants.LAT, 0.0));
            intent.putExtra(Constants.LNG, data.getDoubleExtra(Constants.LNG, 0.0));
            UserHelper.getInstance(context).addSaveLastKnownLocation(new LatLng(data.getDoubleExtra(Constants.LAT, 0.0), data.getDoubleExtra(Constants.LNG, 0.0)));
            MovementHelper.startActivityBase(context, HomeMainFragment.class.getName(), null, null);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        if (dialog != null)
            dialog.dismiss();
    }
}
