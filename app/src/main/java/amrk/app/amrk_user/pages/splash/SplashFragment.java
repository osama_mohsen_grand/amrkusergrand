package amrk.app.amrk_user.pages.splash;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;
import javax.inject.Inject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentSplashBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.login.LoginFragment;
import amrk.app.amrk_user.pages.countries.UserDetectLocation;
import amrk.app.amrk_user.pages.onBoard.OnBoardFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.UserHelper;

public class SplashFragment extends BaseFragment {
    FragmentSplashBinding fragmentSplashBinding;
    @Inject
    SplashViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        fragmentSplashBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_splash, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        fragmentSplashBinding.setViewmodel(viewModel);
        setEvent();
        viewModel.runSplash();
        return fragmentSplashBinding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.MENU_HOME)) {
                MovementHelper.startActivityBase(requireActivity(), UserDetectLocation.class.getName(), null, null);
            } else if (((Mutable) o).message.equals(Constants.BACKGROUND_API)) {
                viewModel.getCountryCodes();
            } else if (mutable.message.equals(Constants.LOGIN)) {
                if (UserHelper.getInstance(MyApplication.getInstance()).getIsFirst()) {
//                    LanguagesHelper.setLanguage(LanguagesHelper.getCurrentLanguage().equals(Resources.getSystem().getConfiguration().locale.getLanguage()) ? Resources.getSystem().getConfiguration().locale.getLanguage() : LanguagesHelper.getCurrentLanguage());
                    MovementHelper.startActivityBase(requireActivity(), OnBoardFragment.class.getName(), null, null);
                } else {
                    MovementHelper.startActivityBase(requireActivity(), LoginFragment.class.getName(), null, null);
                }
//                UserHelper.getInstance(context).addCountryCodes(((CountriesCodesResponse) (mutable).object).getData().toString().replace("[", "").replace("]", "").replace(" ", ""));
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        ((BaseActivity) requireActivity()).enableRefresh(false);
        viewModel.repository.setLiveData(viewModel.liveData);
    }

}
