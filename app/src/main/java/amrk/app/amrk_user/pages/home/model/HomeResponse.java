package amrk.app.amrk_user.pages.home.model;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class HomeResponse extends StatusMessage {

    @SerializedName("data")
    private HomeData data;

    public HomeData getData() {
        return data;
    }
}