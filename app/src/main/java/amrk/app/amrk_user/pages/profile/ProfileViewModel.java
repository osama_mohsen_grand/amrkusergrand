package amrk.app.amrk_user.pages.profile;

import android.text.TextUtils;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.models.RegisterRequest;
import amrk.app.amrk_user.pages.auth.models.UserData;
import amrk.app.amrk_user.repository.AuthRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import amrk.app.amrk_user.utils.validation.Validate;
import io.reactivex.disposables.CompositeDisposable;

public class ProfileViewModel extends BaseViewModel {
    MutableLiveData<Mutable> liveData;
    FileObjects fileObject;
    String cpp;
    @Inject
    AuthRepository repository;
    public String text = "text";
     CompositeDisposable compositeDisposable = new CompositeDisposable();
    public RegisterRequest request;
    private static final String TAG = "RegisterViewModel";

    @Inject
    public ProfileViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        request = new RegisterRequest();
        if (UserHelper.getInstance(MyApplication.getInstance()).getUserData() != null) {
            UserData user = UserHelper.getInstance(MyApplication.getInstance()).getUserData();
            request.setName(user.getName());
            request.setPhone(user.getPhone());
            request.setEmail(user.getEmail());
            request.setUser_image(user.getImage());
            request.setCountry_id(user.getCountryId());
        }
    }

    public void submit() {
        if (request.isUpdateValid()) {
            if (!TextUtils.isEmpty(request.getPassword()) && !TextUtils.isEmpty(request.getConfirmPassword()))
                if (Validate.isMatchPassword(request.getPassword(), request.getConfirmPassword())) {
                    setMessage(Constants.SHOW_PROGRESS);
                    compositeDisposable.add(repository.updateProfile(request, fileObject));
                } else
                    liveData.setValue(new Mutable(Constants.NOT_MATCH_PASSWORD));
            else {
                setMessage(Constants.SHOW_PROGRESS);
                compositeDisposable.add(repository.updateProfile(request, fileObject));
            }
        }
    }

    public void imageSubmit() {
        liveData.setValue(new Mutable(Constants.IMAGE));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public void setImage(FileObjects fileObject) {
        this.fileObject = fileObject;
    }
}
