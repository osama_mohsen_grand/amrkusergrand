package amrk.app.amrk_user.pages.myOrders.models;

import com.google.gson.annotations.SerializedName;

public class OrderImagesItem{

	@SerializedName("image")
	private String image;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("order_id")
	private int orderId;

	public String getImage(){
		return image;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public int getOrderId(){
		return orderId;
	}
}