package amrk.app.amrk_user.pages.publicOrder.viewModels;


import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.CheckPromoRequest;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.publicOrder.adapters.PublicOrderImagesAdapter;
import amrk.app.amrk_user.pages.publicOrder.models.PublicInfo;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.NewOrderRequest;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.OrderImages;
import amrk.app.amrk_user.pages.publicOrder.models.subDepartment.SubDepartmentData;
import amrk.app.amrk_user.repository.PublicOrderRepository;

import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import io.reactivex.disposables.CompositeDisposable;

public class PublicOrdersViewModel extends BaseViewModel {
    ArrayList<FileObjects> fileObject;
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    PublicOrderRepository publicOrderRepository;
    PublicInfo publicInfo;
    PublicOrderImagesAdapter imagesAdapter;
    NewOrderRequest newOrderRequest;
    CheckPromoRequest checkPromoRequest;
    ShopsItem shopsItem;
    public List<SubDepartmentData> subDepartmentDataList;

    @Inject
    public PublicOrdersViewModel(PublicOrderRepository publicOrderRepository) {
        subDepartmentDataList = new ArrayList<>();
        checkPromoRequest = new CheckPromoRequest();
        shopsItem = new ShopsItem();
        newOrderRequest = new NewOrderRequest();
        fileObject = new ArrayList<>();
        imagesAdapter = new PublicOrderImagesAdapter();
        publicInfo = new PublicInfo();
        this.publicOrderRepository = publicOrderRepository;
        this.liveData = new MutableLiveData<>();
        publicOrderRepository.setLiveData(liveData);
    }

    public void publicOrderInfo() {
        compositeDisposable.add(publicOrderRepository.getPublicInfo());
    }

    public void getSubDepartment(int department) {
        compositeDisposable.add(publicOrderRepository.getSubDepartment(department));
    }

    public PublicOrderRepository getPublicOrderRepository() {
        return publicOrderRepository;
    }

    public void toReviews() {
        liveData.setValue(new Mutable(Constants.MARKET_REVIEWS));
    }

    public void toNewOrder() {
        liveData.setValue(new Mutable(Constants.NEW_ORDER));
    }

    public void toShowSubDepartments() {
        liveData.setValue(new Mutable(Constants.SUB_DEPARTMENT_DIALOG));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    @Bindable
    public PublicInfo getPublicInfo() {
        return publicInfo;
    }

    @Bindable
    public void setPublicInfo(PublicInfo publicInfo) {
        notifyChange(BR.publicInfo);
        this.publicInfo = publicInfo;
    }

    //New order images handle
    public NewOrderRequest getNewOrderRequest() {
        return newOrderRequest;
    }

    public void setNewOrderRequest(NewOrderRequest newOrderRequest) {
        newOrderRequest.setDelivery_time(ResourceManager.getString(R.string.oneHour));
        this.newOrderRequest = newOrderRequest;
    }

    public void setUpOrderImages() {
        List<OrderImages> orderImages = new ArrayList<>();
        orderImages.add(new OrderImages(""));
        orderImages.add(new OrderImages(""));
        orderImages.add(new OrderImages(""));
        orderImages.add(new OrderImages(""));
        getImagesAdapter().update(orderImages);
    }

    public PublicOrderImagesAdapter getImagesAdapter() {
        return imagesAdapter;
    }

    public void createOrder() {
        getNewOrderRequest().setOrderImages(getImagesAdapter().orderImagesList);
        if (getNewOrderRequest().getShopId() == null)
            getNewOrderRequest().setDepartment_id(3);
        else
            getNewOrderRequest().setDepartment_id(4);
        if (getNewOrderRequest().isTitleValid())
            liveData.setValue(new Mutable(Constants.CONTINUE_NEW_ORDER));
        else
            liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
    }

    public void confirmOrder() {
        if (getNewOrderRequest().getDepartment_id() == 3 || getNewOrderRequest().getDepartment_id() == 4) {
            int counter = 0;
            fileObject.clear();
            for (int i = 0; i < getNewOrderRequest().getOrderImages().size(); i++) {
                if (!getNewOrderRequest().getOrderImages().get(i).getUri().isEmpty()) {
                    FileObjects fileObjects = new FileObjects("image[" + counter + "]", getNewOrderRequest().getOrderImages().get(i).getUri(), Constants.FILE_TYPE_IMAGE);
                    fileObject.add(fileObjects);
                    counter++;
                }
                if (i == getNewOrderRequest().getOrderImages().size() - 1) {
                    if (getNewOrderRequest().isHeavyValid()) {
                        setMessage(Constants.SHOW_PROGRESS);
                        compositeDisposable.add(publicOrderRepository.createOrder(getNewOrderRequest(), fileObject));
                    }
                }
            }
        } else if (getNewOrderRequest().getDepartment_id() == 2) {
//            Log.e("confirmOrder", "confirmOrder: "+getNewOrderRequest() );
            if (getNewOrderRequest().isStoreValid()) {
                setMessage(Constants.SHOW_PROGRESS);
                compositeDisposable.add(publicOrderRepository.createOrder(getNewOrderRequest(), fileObject));
            }
        }
    }

    public void settingOrderLocations(int type, LatLng locations, String address, String city) {
        if (type == Constants.IN_ADDRESS) {
            getNewOrderRequest().setIn_lat(locations.latitude);
            getNewOrderRequest().setIn_lng(locations.longitude);
            getNewOrderRequest().setIn_addresss(address);
            getNewOrderRequest().setIn_city_name(city);
        } else if (type == Constants.OUT_ADDRESS) {
            getNewOrderRequest().setOut_lat(locations.latitude);
            getNewOrderRequest().setOut_lng(locations.longitude);
            getNewOrderRequest().setOut_addresss(address);
            getNewOrderRequest().setOut_city_name(city);
        } else {
            getNewOrderRequest().setLast_lat(locations.latitude);
            getNewOrderRequest().setLast_lng(locations.longitude);
            getNewOrderRequest().setLast_address(address);
            getNewOrderRequest().setLast_city_name(city);
        }
        notifyChange();
    }

    @BindingAdapter("android:layout_marginTop")
    public static void setTopMargin(View view, float bottomMargin) {
        ViewGroup.MarginLayoutParams layoutParams = (ViewGroup.MarginLayoutParams) view.getLayoutParams();
        layoutParams.setMargins(layoutParams.leftMargin, Math.round(bottomMargin),
                layoutParams.rightMargin, layoutParams.bottomMargin);
        view.setLayoutParams(layoutParams);
    }

    public void checkPromo() {
        checkPromoRequest.setDepartment_id(3);
        if (getCheckPromoRequest().isValid())
            compositeDisposable.add(publicOrderRepository.checkPromo(checkPromoRequest));
    }

    public void setShopsItem(ShopsItem shopsItem) {
        if (shopsItem != null) {
            getNewOrderRequest().setIn_lng(Double.parseDouble(shopsItem.getLat()));
            getNewOrderRequest().setIn_lat(Double.parseDouble(shopsItem.getLng()));
            getNewOrderRequest().setIn_addresss(shopsItem.getAddress());
            getNewOrderRequest().setIn_city_name(shopsItem.getAddress());
            getNewOrderRequest().setShop_image_google(shopsItem.getImage());
            getNewOrderRequest().setShop_name_google(shopsItem.getName());
            getNewOrderRequest().setShop_distance_google(shopsItem.getDistance());
            getNewOrderRequest().setShopId(shopsItem.getPlace_id());
        }
        this.shopsItem = shopsItem;
    }

    public CheckPromoRequest getCheckPromoRequest() {
        return checkPromoRequest;
    }

    public void checkPromoDialog() {
        liveData.setValue(new Mutable(Constants.PROMO_DIALOG));
    }

    public void toPickLocation() {
        liveData.setValue(new Mutable(Constants.PICK_UP_LOCATION));
    }

    public void toProcessingLocation() {
        liveData.setValue(new Mutable(Constants.PROCESSING_LOCATION));
    }

    public void toDestLocation() {
        liveData.setValue(new Mutable(Constants.DESTINATION_LOCATION));
    }

    public void toDeliveryTimes() {
        liveData.setValue(new Mutable(Constants.DELIVERY_TIMES));
    }

    public void switchSameLocation(boolean checked) {
        if (checked) {
            getNewOrderRequest().setOut_lat(getNewOrderRequest().getIn_lat());
            getNewOrderRequest().setOut_lng(getNewOrderRequest().getIn_lng());
            getNewOrderRequest().setOut_addresss(getNewOrderRequest().getIn_addresss());
            getNewOrderRequest().setOut_city_name(getNewOrderRequest().getIn_city_name());
        } else {
            getNewOrderRequest().setOut_lat(0.0);
            getNewOrderRequest().setOut_lng(0.0);
            getNewOrderRequest().setOut_addresss(null);
            getNewOrderRequest().setOut_city_name(null);
        }
        notifyChange();
    }
}
