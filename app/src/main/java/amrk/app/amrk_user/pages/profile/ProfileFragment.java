package amrk.app.amrk_user.pages.profile;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;


import org.jetbrains.annotations.NotNull;

import java.io.File;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.databinding.FragmentProfileBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.models.UsersResponse;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import amrk.app.amrk_user.utils.upload.FileOperations;

public class ProfileFragment extends BaseFragment {

    private Context context;
    private FragmentProfileBinding binding;
    private static final String TAG = "RegisterFragment";
    @Inject
    ProfileViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_profile, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
//        binding.ccp.setCustomMasterCountries(UserHelper.getInstance(context).getCountryCodes());
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
//        viewModel.cpp = "+" + binding.ccp.getDefaultCountryCode();
//        binding.ccp.setOnCountryChangeListener(() -> {
//            if (viewModel.request.getPhone().contains(viewModel.cpp)) {
//                viewModel.request.setPhone(viewModel.request.getPhone().replace(viewModel.cpp, ""));
//            }
//            viewModel.cpp = "+" + binding.ccp.getSelectedCountryCode();
//        });

        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.IMAGE:
                    pickImageDialogSelect();
                    break;
                case Constants.UPDATE_PROFILE:
                    toastMessage(((UsersResponse) mutable.object).mMessage);
                    viewModel.goBack(context);
                    UserHelper.getInstance(context).userLogin(((UsersResponse) ((Mutable) o).object).getData());
                    break;
                case Constants.NOT_MATCH_PASSWORD:
                    showError(getResources().getString(R.string.password_not_match));
                    break;

            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }


    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super method removed
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            FileObjects fileObject = FileOperations.getFileObjects(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            viewModel.setImage(fileObject);
            Log.e(TAG, "onActivityResult: " + fileObject.getFilePath());
            binding.imgRegister.setImageURI(null);
            binding.imgRegister.setImageURI(Uri.fromFile(new File(fileObject.getFilePath())));
        }
        super.onActivityResult(requestCode, resultCode, data);
    }


}
