package amrk.app.amrk_user.pages.myOrders.models.orders;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class OrderProductVariationsItem{

	@SerializedName("order_product_id")
	private int orderProductId;

	@SerializedName("variation_id")
	private int variationId;

	@SerializedName("name")
	private String name;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private Object type;

	@SerializedName("order_product_variation_options")
	private List<OrderProductVariationOptionsItem> orderProductVariationOptions;

	@SerializedName("required")
	private Object required;

	public int getOrderProductId(){
		return orderProductId;
	}

	public int getVariationId(){
		return variationId;
	}

	public String getName(){
		return name;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getId(){
		return id;
	}

	public Object getType(){
		return type;
	}

	public List<OrderProductVariationOptionsItem> getOrderProductVariationOptions(){
		return orderProductVariationOptions;
	}

	public Object getRequired(){
		return required;
	}
}