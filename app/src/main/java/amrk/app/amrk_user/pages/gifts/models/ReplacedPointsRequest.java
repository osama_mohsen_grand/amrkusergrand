package amrk.app.amrk_user.pages.gifts.models;

import com.google.gson.annotations.SerializedName;

public class ReplacedPointsRequest {
    @SerializedName("points")
    private String points;
    @SerializedName("offer_point_id")
    private int offer_point_id;

    public ReplacedPointsRequest(int offer_point_id) {
        this.offer_point_id = offer_point_id;
    }

    public ReplacedPointsRequest(String points) {
        this.points = points;
    }

    public String getPoints() {
        return points;
    }

    public void setPoints(String points) {
        this.points = points;
    }

    public int getOffer_point_id() {
        return offer_point_id;
    }

    public void setOffer_point_id(int offer_point_id) {
        this.offer_point_id = offer_point_id;
    }
}
