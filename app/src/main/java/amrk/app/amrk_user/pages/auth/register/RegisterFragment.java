package amrk.app.amrk_user.pages.auth.register;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import java.io.File;
import javax.inject.Inject;
import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.databinding.FragmentRegisterBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.confirmCode.ConfirmCodeFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import amrk.app.amrk_user.utils.upload.FileOperations;

public class RegisterFragment extends BaseFragment {
    private FragmentRegisterBinding binding;
    @Inject
    RegisterViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_register, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
//        binding.ccp.setCustomMasterCountries(UserHelper.getInstance(context).getCountryCodes());
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
//        viewModel.cpp = binding.ccp.getDefaultCountryCode();
//        binding.ccp.setOnCountryChangeListener(() -> {
//            if (viewModel.getRequest().getPhone().contains(viewModel.cpp)) {
//                viewModel.getRequest().setPhone(viewModel.getRequest().getPhone().replace(viewModel.cpp, ""));
//            }
//            viewModel.cpp = binding.ccp.getSelectedCountryCode();
//        });

        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.IMAGE:
                    pickImageDialogSelect();
                    break;
                case Constants.REGISTER:
                    closeKeyboard();
                    toastMessage(((Mutable) o).message);
                    viewModel.goBack(requireActivity());
                    MovementHelper.startActivityWithBundle(requireActivity(), new PassingObject(Constants.CHECK_CONFIRM_NAV_REGISTER, viewModel.getRequest().getPhone()), null, ConfirmCodeFragment.class.getName(), null, null);
                    break;
                case Constants.PRIVACY:
                    MovementHelper.openCustomTabs(requireActivity(), URLS.POLICY_URL + 0 + "/" + LanguagesHelper.getCurrentLanguage(), getString(R.string.privacy));
                    break;
                case Constants.TERMS:
                    MovementHelper.openCustomTabs(requireActivity(), URLS.TERMS_URL + 0 + "/" + LanguagesHelper.getCurrentLanguage(), getString(R.string.terms));
                    break;
                case Constants.NOT_MATCH_PASSWORD:
                    toastErrorMessage(getString(R.string.password_not_match));
                    break;
                case Constants.TERMS_ACCEPTED:
                    toastErrorMessage(getString(R.string.terms_accepted));
                    break;
                case Constants.NEW_IMAGE:
                    toastErrorMessage(getString(R.string.select_image_profile));
                    break;
                case Constants.PICK_UP_LOCATION:
                    MovementHelper.startMapActivityForResultWithBundle(requireActivity(), new PassingObject(1, getResources().getString(R.string.choose_location)));
                    break;
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == Constants.FILE_TYPE_IMAGE) {
            FileObjects fileObject = FileOperations.getFileObjects(getActivity(), data, Constants.IMAGE, Constants.FILE_TYPE_IMAGE);
            viewModel.setImage(fileObject);
            binding.imgRegister.setImageURI(Uri.fromFile(new File(fileObject.getFilePath())));
        } else if (requestCode == Constants.RESULT_CODE) {
            viewModel.request.setCityName(data.getStringExtra(Constants.CITY));
            viewModel.notifyChange(BR.request);
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

}
