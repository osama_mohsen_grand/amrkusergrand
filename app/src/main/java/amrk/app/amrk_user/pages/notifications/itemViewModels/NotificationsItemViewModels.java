package amrk.app.amrk_user.pages.notifications.itemViewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.notifications.models.NotificationsData;
import amrk.app.amrk_user.utils.Constants;


public class NotificationsItemViewModels extends BaseViewModel {
    NotificationsData notificationsData;

    public NotificationsItemViewModels(NotificationsData notificationsData) {
        this.notificationsData = notificationsData;
    }


    @Bindable
    public NotificationsData getNotificationsData() {
        return notificationsData;
    }

    //0=>witout, 1=>trip, 2=>shops_order, 3=>normal_order,5=>heavey
    public void itemAction() {
        if (notificationsData.getOrder_type() == Constants.NOTIFICATION_TRIP_TYPE) {
            getLiveData().setValue(Constants.TRIP_DETAILS);
        } else if (notificationsData.getOrder_type() == 2 || notificationsData.getOrder_type() == 3 || notificationsData.getOrder_type() == 5) {
            getLiveData().setValue(Constants.ORDER_DETAILS);
        }
    }

}
