package amrk.app.amrk_user.pages.markets.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemMarketMenuCategoryBinding;
import amrk.app.amrk_user.pages.markets.models.marketDetails.MenusItem;
import amrk.app.amrk_user.pages.markets.viewModels.ItemMarketMenuViewModel;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class MarketMenuCategoryAdapter extends RecyclerView.Adapter<MarketMenuCategoryAdapter.MenuView> {
     List<MenusItem> menuModels;
     MutableLiveData<Integer> liveDataAdapter = new MutableLiveData<>();
    private Context context;
    private int lastSelect = 0;

    public MarketMenuCategoryAdapter() {
        this.menuModels = new ArrayList<>();
    }

    public MutableLiveData<Integer> getLiveDataAdapter() {
        return liveDataAdapter;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_market_menu_category,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        MenusItem menuModel = menuModels.get(position);
        ItemMarketMenuViewModel itemMenuViewModel = new ItemMarketMenuViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            lastSelect = menuModel.getId();
            liveDataAdapter.setValue(position);
            notifyDataSetChanged();
        });
        if (lastSelect == menuModel.getId()) {
            holder.itemMenuBinding.btnCategory.setBackgroundColor(ResourceManager.getColor(R.color.colorPrimaryDark));
        } else
            holder.itemMenuBinding.btnCategory.setBackgroundColor(ResourceManager.getColor(R.color.medium_color));
        holder.setViewModel(itemMenuViewModel);
    }

    public void setLastSelect(int lastSelect) {
        this.lastSelect = lastSelect;
    }

    public List<MenusItem> getMenuModels() {
        return menuModels;
    }

    public void update(List<MenusItem> dataList) {
        this.menuModels.clear();
        menuModels.addAll(dataList);
        lastSelect = dataList.size() > 0 ? dataList.get(0).getId() : 0;
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemMarketMenuCategoryBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemMarketMenuViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
