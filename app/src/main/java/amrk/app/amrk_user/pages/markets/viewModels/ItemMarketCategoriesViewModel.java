package amrk.app.amrk_user.pages.markets.viewModels;


import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.markets.models.CategoriesItem;
import amrk.app.amrk_user.utils.Constants;

public class ItemMarketCategoriesViewModel extends BaseViewModel {
    public CategoriesItem categoriesItem;

    public ItemMarketCategoriesViewModel(CategoriesItem categoriesItem) {
        this.categoriesItem = categoriesItem;
    }

    @Bindable
    public CategoriesItem getCategoriesItem() {
        return categoriesItem;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.GET_MARKETS_BY_CATEGORY);
    }

}
