package amrk.app.amrk_user.pages.myAccount.viewModels;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.repository.SettingsRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class MyAccountViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository settingsRepository;

    @Inject
    public MyAccountViewModel(SettingsRepository settingsRepository) {
        this.settingsRepository = settingsRepository;
        this.liveData = new MutableLiveData<>();
        settingsRepository.setLiveData(liveData);
    }

    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public void toExit() {
        liveData.setValue(new Mutable(Constants.EXIT_DIALOG));
    }

    public void toAbout() {
        liveData.setValue(new Mutable(Constants.ABOUT));
    }

    public void toChatAdmin() {
        liveData.setValue(new Mutable(Constants.CHAT_ADMIN));
    }

    public void toMyHeavyOrders() {
        liveData.setValue(new Mutable(Constants.MY_HEAVY_ORDERS));
    }

    public void toMyOrders() {
        liveData.setValue(new Mutable(Constants.MY_ORDERS));
    }

    public void toContact() {
        liveData.setValue(new Mutable(Constants.CONTACT));
    }

    public void toTerms() {
        liveData.setValue(new Mutable(Constants.TERMS));
    }

    public void shareApp() {
        liveData.setValue(new Mutable(Constants.SHARE_BAR));
    }

    public void rateApp() {
        liveData.setValue(new Mutable(Constants.RATE_APP));
    }

    public void toSelectCountry() {
        liveData.setValue(new Mutable(Constants.SELECT_COUNTRY));
    }

    public void toProfile() {
        liveData.setValue(new Mutable(Constants.UPDATE_PROFILE));
    }

    public void registerShop() {
        liveData.setValue(new Mutable(Constants.REGISTER_SHOP));
    }

    public void dialogAction(String action) {
        liveData.setValue(new Mutable(action));
    }

}
