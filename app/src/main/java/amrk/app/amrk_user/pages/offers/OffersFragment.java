package amrk.app.amrk_user.pages.offers;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentOffersBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.offers.models.OffersResponse;
import amrk.app.amrk_user.pages.offers.viewModels.OffersViewModel;
import amrk.app.amrk_user.utils.Constants;

public class OffersFragment extends BaseFragment {

    private Context context;
    @Inject
    OffersViewModel viewModel;
    FragmentOffersBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_offers, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.offers(viewModel.getActiveOfferButton());
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (((Mutable) o).message.equals(Constants.OFFERS)) {
                viewModel.getOffersAdapter().update(((OffersResponse) mutable.object).getData());
                viewModel.notifyChange(BR.offersAdapter);
            }
        });
        ((BaseActivity) context).getRefreshingLiveData().observe(((LifecycleOwner) context), aBoolean -> {
            baseActivity().stopRefresh(false);
            viewModel.offers(viewModel.getActiveOfferButton());
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            viewModel.offers(viewModel.getActiveOfferButton());
        }
        baseActivity().enableRefresh(true);
        viewModel.getMarketRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
