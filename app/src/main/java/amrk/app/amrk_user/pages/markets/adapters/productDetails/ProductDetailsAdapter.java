package amrk.app.amrk_user.pages.markets.adapters.productDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemMenuDialogItemDetailsBinding;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.pages.markets.viewModels.products.ItemProductDetailsViewModel;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.UserHelper;

public class ProductDetailsAdapter extends RecyclerView.Adapter<ProductDetailsAdapter.MenuView> {
    List<OptionsItem> optionsItemList;
    MutableLiveData<VariationsItem> optionsLiveData = new MutableLiveData<>();
    Context context;
    public int lastSelection = 0;
    int lastSingleItemId = 0;
    double total = 0.0, lastPrice;
    List<OptionsItem> selectedOptionList = new ArrayList<>();
    List<VariationsItem> selectedVariationsList = new ArrayList<>();

    public ProductDetailsAdapter() {
        this.optionsItemList = new ArrayList<>();
    }

    public MutableLiveData<VariationsItem> getProductPriceLiveData() {
        return optionsLiveData;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu__dialog_item_details,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        OptionsItem menuModel = optionsItemList.get(position);
//        menuModel.setChecked(optionsItemList.size() == 1 && menuModel.getType() == 0);
        ItemProductDetailsViewModel itemMenuViewModel = new ItemProductDetailsViewModel(menuModel);
        itemMenuViewModel.getOptionLiveData().observe((LifecycleOwner) MovementHelper.unwrap(context), o -> {
//            if (o.getObject().equals(Constants.PRODUCT_SINGLE_SELECTION)) {
            lastSelection = menuModel.getOption_id();
////                isFound(menuModel);
//                Log.e("onBindViewHolder", "onBindViewHolder: " + menuModel.isChecked());
//            } else {
//                if (menuModel.isChecked() && optionsItemList.size() > 1) {
//            optionsLiveData.setValue(selectedOptionList);
            settingOptions(menuModel);
//                }
//            }
            notifyDataSetChanged();
        });
        if (lastSelection == menuModel.getOption_id()) {
            holder.itemMenuBinding.itemRadio.setImageResource(R.drawable.ic_filled_radio);
        } else {
            holder.itemMenuBinding.itemRadio.setImageResource(R.drawable.ic_empty_radio);
        }
        holder.setViewModel(itemMenuViewModel);

    }


    private static final String TAG = "ADAPTER";

    private void settingOptions(OptionsItem optionsItem) {
        VariationsItem variationsItem = new VariationsItem();
        //getting new data shared
        total = Double.parseDouble(UserHelper.getInstance(context).getTempTotal());
        if (optionsItem.getType() == 0) {
            if (lastSingleItemId != 0) {
                total = total - lastPrice;
                selectedOptionList.remove(isFound(lastSingleItemId));
            }
            selectedOptionList.add(optionsItem);
            total = total + Double.parseDouble(optionsItem.getPrice());
            lastSingleItemId = optionsItem.getOption_id();
            lastPrice = Double.parseDouble(optionsItem.getPrice());
            variationsItem.setRequired(1);
            UserHelper.getInstance(context).addTempProductPriceItem(optionsItem.getPrice());
        } else {
            if (selectedOptionList.contains(optionsItem)) { //exists
                selectedOptionList.remove(optionsItem);
                total = total - Double.parseDouble(optionsItem.getPrice());
            } else {
                selectedOptionList.add(optionsItem);
                total = total + Double.parseDouble(optionsItem.getPrice());
            }
            variationsItem.setRequired(0);
        }
        variationsItem.setVariation_id(optionsItem.getVariationId());
        variationsItem.setOptions(selectedOptionList);
        //setting new data to shared
        UserHelper.getInstance(context).addTempProductTotal(String.valueOf(total));
        optionsLiveData.setValue(variationsItem);
    }

    private int isFound(int optionId) {
        for (int i = 0; i < selectedOptionList.size(); i++) {
            if (selectedOptionList.get(i).getOption_id() == optionId) {
                return i;
            }
        }
        return -1;
    }

    public void update(List<OptionsItem> dataList, int required) {
        this.optionsItemList.clear();
        optionsItemList.addAll(dataList);
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return optionsItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemMenuDialogItemDetailsBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemProductDetailsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
