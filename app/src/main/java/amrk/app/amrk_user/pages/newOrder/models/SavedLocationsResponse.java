package amrk.app.amrk_user.pages.newOrder.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;

public class SavedLocationsResponse extends StatusMessage {

	@SerializedName("data")
	private List<SavedLocationsData> savedLocationsData;

	public List<SavedLocationsData> getSavedLocationsData(){
		return savedLocationsData;
	}

}