package amrk.app.amrk_user.pages.myOrders.models;

import com.google.gson.annotations.SerializedName;

public class OrderStatus {
    @SerializedName("on_last_way")
    private String onLastWay;
    @SerializedName("cancel_reason")
    private String cancelReason;

    @SerializedName("cancelled")
    private String cancelled;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("on_way")
    private String onWay;

    @SerializedName("finished")
    private String finished;

    @SerializedName("id")
    private int id;

    @SerializedName("cancel_by")
    private int cancelBy;

    @SerializedName("order_id")
    private int orderId;

    @SerializedName("accept")
    private String accept;
    @SerializedName("received")
    private String received;
    @SerializedName("processed")
    private String processed;
    @SerializedName("on_first_way")
    private String on_first_way;

    public void setOn_first_way(String on_first_way) {
        this.on_first_way = on_first_way;
    }

    public String getCancelReason() {
        return cancelReason;
    }

    public String getCancelled() {
        return cancelled;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public String getOnWay() {
        return onWay;
    }

    public String getFinished() {
        return finished;
    }

    public int getId() {
        return id;
    }

    public int getCancelBy() {
        return cancelBy;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getAccept() {
        return accept;
    }

    public String getReceived() {
        return received;
    }

    public String getProcessed() {
        return processed;
    }

    public String getOn_first_way() {
        return on_first_way;
    }

    public String getOnLastWay() {
        return onLastWay;
    }

    public void setOnLastWay(String onLastWay) {
        this.onLastWay = onLastWay;
    }

    public void setOnWay(String onWay) {
        this.onWay = onWay;
    }

    public void setFinished(String finished) {
        this.finished = finished;
    }

    public void setReceived(String received) {
        this.received = received;
    }

    public void setProcessed(String processed) {
        this.processed = processed;
    }
}