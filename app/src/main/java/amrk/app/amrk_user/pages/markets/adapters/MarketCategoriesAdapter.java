
package amrk.app.amrk_user.pages.markets.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemMarketCategoryBinding;
import amrk.app.amrk_user.pages.markets.models.CategoriesItem;
import amrk.app.amrk_user.pages.markets.viewModels.ItemMarketCategoriesViewModel;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class MarketCategoriesAdapter extends RecyclerView.Adapter<MarketCategoriesAdapter.ViewHolder> {
    List<CategoriesItem> categoriesItemList;
    MutableLiveData<Integer> liveDataAdapter = new MutableLiveData<>();
    private Context context;
    public int lastSelected = 0;

    public MarketCategoriesAdapter() {
        this.categoriesItemList = new ArrayList<>();
    }

    public MutableLiveData<Integer> getLiveDataAdapter() {
        return liveDataAdapter;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_market_category,
                parent, false);
        this.context = parent.getContext();
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        CategoriesItem categoriesItem = categoriesItemList.get(position);
        ItemMarketCategoriesViewModel itemMenuViewModel = new ItemMarketCategoriesViewModel(categoriesItem);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            lastSelected = categoriesItem.getId();
            notifyDataSetChanged();
            liveDataAdapter.setValue(lastSelected);
        });
        if (lastSelected == categoriesItem.getId()) {
            holder.binding.marketCategoryCard.setBorderColor(ResourceManager.getColor(R.color.colorPrimary));
            holder.binding.marketCategoryCard.setBorderWidth(ResourceManager.getDimens(R.dimen.dp2w));
        } else {
            holder.binding.marketCategoryCard.setBorderColor(ResourceManager.getColor(R.color.white));
            holder.binding.marketCategoryCard.setBorderWidth(ResourceManager.getDimens(R.dimen.dp1w));
        }
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<CategoriesItem> dataList) {
        this.categoriesItemList.clear();
        categoriesItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MarketCategoriesAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MarketCategoriesAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return categoriesItemList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemMarketCategoryBinding binding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel(ItemMarketCategoriesViewModel itemViewModels) {
            if (binding != null) {
                binding.setItemViewModel(itemViewModels);
            }
        }
    }
}
