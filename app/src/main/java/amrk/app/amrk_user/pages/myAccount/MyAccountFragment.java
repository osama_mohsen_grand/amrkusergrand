package amrk.app.amrk_user.pages.myAccount;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;
import org.jetbrains.annotations.NotNull;
import java.util.Objects;
import javax.inject.Inject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.FragmentMyAccountBinding;
import amrk.app.amrk_user.databinding.RegisterInAmrkDialogBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.heavyOrders.MyHeavyOrdersMainFragment;
import amrk.app.amrk_user.pages.myAccount.viewModels.MyAccountViewModel;
import amrk.app.amrk_user.pages.myOrders.MyOrdersMainFragment;
import amrk.app.amrk_user.pages.profile.ProfileFragment;
import amrk.app.amrk_user.pages.settings.AboutAppFragment;
import amrk.app.amrk_user.pages.settings.ContactUsFragment;
import amrk.app.amrk_user.pages.settings.LangFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.LanguagesHelper;

public class MyAccountFragment extends BaseFragment {

    private Context context;
    FragmentMyAccountBinding binding;
    @Inject
    MyAccountViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_my_account, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.UPDATE_PROFILE.equals(((Mutable) o).message)) {
                MovementHelper.startActivity(context, ProfileFragment.class.getName(), getResources().getString(R.string.profile_title), null, Constants.MARKETS);
            } else if (Constants.CONTACT.equals(((Mutable) o).message)) {
                MovementHelper.startActivity(context, ContactUsFragment.class.getName(), getResources().getString(R.string.contact), null, Constants.MARKETS);
            } else if (Constants.ABOUT.equals(((Mutable) o).message)) {
                MovementHelper.startActivity(context, AboutAppFragment.class.getName(), getResources().getString(R.string.about_app), null, Constants.MARKETS);
            } else if (Constants.TERMS.equals(((Mutable) o).message)) {
                MovementHelper.openCustomTabs(context, URLS.TERMS_URL + 0 + "/" + LanguagesHelper.getCurrentLanguage(), getString(R.string.terms));
            } else if (Constants.MY_HEAVY_ORDERS.equals(mutable.message)) {
                MovementHelper.startActivity(context, MyHeavyOrdersMainFragment.class.getName(), getResources().getString(R.string.heavy_orders_main_title), null, Constants.MARKETS);
            } else if (Constants.MY_ORDERS.equals(mutable.message)) {
                MovementHelper.startActivity(context, MyOrdersMainFragment.class.getName(), getResources().getString(R.string.last_orders), null, Constants.MARKETS);
            } else if (Constants.CHAT_ADMIN.equals(mutable.message)) {
                MovementHelper.startPaymentActivityForResultWithBundle(context, viewModel.lang.equals(Constants.ENGLISH_CHAT_BOT) ? Constants.ENGLISH_CHAT_BOT : Constants.ARABIC_CHAT_BOT, getString(R.string.support));
            } else if (Constants.SHARE_BAR.equals(mutable.message)) {
                AppHelper.shareApp(((ParentActivity) context));
            } else if (Constants.RATE_APP.equals(mutable.message)) {
                AppHelper.rateApp(context, context.getPackageName());
            } else if (Constants.SELECT_COUNTRY.equals(mutable.message)) {
                MovementHelper.startActivity(context, LangFragment.class.getName(), getString(R.string.lang), null, Constants.MARKETS);
            } else if (Constants.EXIT_DIALOG.equals(mutable.message)) {
                exitDialog(getString(R.string.log_out_text));
            } else if (Constants.REGISTER_SHOP.equals(mutable.message)) {
                showAmrkDialog();
            } else if (Constants.AMRK_DRIVER.equals(mutable.message)) {
                AppHelper.rateApp(context, "amrk.app.amrk_driver");
            } else if (Constants.AMRK_DELEGATE.equals(mutable.message)) {
                AppHelper.rateApp(context, "amrk.app.amrk_delegate");
            } else if (Constants.AMRK_DELEGATE_HEAVY.equals(mutable.message)) {
                AppHelper.rateApp(context, "amrk.app.amrk_delegate_heavy");
            } else if (Constants.AMRK_SHOP.equals(mutable.message)) {
                AppHelper.openBrowser(context, "https://amrrek.net/register-shop");
            }
        });
    }

    private void showAmrkDialog() {
        Dialog amrkDialog = new Dialog(context, R.style.PauseDialog);
        amrkDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(amrkDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        amrkDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RegisterInAmrkDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(amrkDialog.getContext()), R.layout.register_in_amrk_dialog, null, false);
        amrkDialog.setContentView(binding.getRoot());
        binding.setViewModel(viewModel);
        amrkDialog.show();
    }

    @Override
    public void onResume() {
        super.onResume();
        baseActivity().enableRefresh(false);
        viewModel.getSettingsRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
