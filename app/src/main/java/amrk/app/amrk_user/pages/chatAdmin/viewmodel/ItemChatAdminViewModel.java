package amrk.app.amrk_user.pages.chatAdmin.viewmodel;

import android.view.View;

import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.databinding.Bindable;
import androidx.databinding.BindingAdapter;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdmin;

public class ItemChatAdminViewModel extends BaseViewModel {
    ChatAdmin chat;

    public ItemChatAdminViewModel(ChatAdmin chat) {
        this.chat = chat;
    }

    @Bindable
    public ChatAdmin getChat() {
        return chat;
    }

    @BindingAdapter("android:layoutDirection")
    public static void chatAdminDirection(ConstraintLayout constraintLayout, int senderType) {
        if (senderType == 0) {
            constraintLayout.setLayoutDirection(View.LAYOUT_DIRECTION_RTL);
        } else
            constraintLayout.setLayoutDirection(View.LAYOUT_DIRECTION_LTR);
    }
}
