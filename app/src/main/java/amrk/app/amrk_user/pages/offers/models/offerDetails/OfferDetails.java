package amrk.app.amrk_user.pages.offers.models.offerDetails;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.pages.auth.models.UserData;

public class OfferDetails {

    @SerializedName("offer")
    private String offer;

    @SerializedName("delegate")
    private UserData delegate;

    @SerializedName("distance")
    private String distance;

    @SerializedName("created_at")
    private String createdAt;

    @SerializedName("delivery_time")
    private String delivery_time;
    @SerializedName("cancel_warning")
    private String cancel_warning;

    @SerializedName("id")
    private int id;

    @SerializedName("delegate_id")
    private int delegateId;

    @SerializedName("order_id")
    private int orderId;

    @SerializedName("status")
    private int status;
    @SerializedName("lower_offers_counter")
    private int lower_offers_counter;
    @SerializedName("department_id")
    private int departmentId;

    private boolean isReset = true;

    public boolean isReset() {
        return isReset;
    }

    public void setReset(boolean reset) {
        isReset = reset;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public String getOffer() {
        return offer;
    }

    public UserData getDelegate() {
        return delegate;
    }

    public String getDistance() {
        return distance;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getId() {
        return id;
    }

    public int getDelegateId() {
        return delegateId;
    }

    public int getOrderId() {
        return orderId;
    }

    public int getStatus() {
        return status;
    }

    public int getLower_offers_counter() {
        return lower_offers_counter;
    }

    public String getDelivery_time() {
        return delivery_time;
    }

    public String getCancel_warning() {
        return cancel_warning;
    }
}