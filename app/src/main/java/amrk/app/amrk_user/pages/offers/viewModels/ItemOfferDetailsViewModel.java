package amrk.app.amrk_user.pages.offers.viewModels;


import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.offers.models.offerDetails.OfferDetails;
import amrk.app.amrk_user.utils.Constants;

public class ItemOfferDetailsViewModel extends BaseViewModel {
    public OfferDetails offerDetails;


    public ItemOfferDetailsViewModel(OfferDetails offerDetails) {
        this.offerDetails = offerDetails;
        if (offerDetails.isReset())
            setMessage(Constants.HIDE_PROGRESS);
        else
            setMessage(Constants.SHOW_PROGRESS);
    }

    @Bindable
    public OfferDetails getOfferDetails() {
        return offerDetails;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

    public void showDialogDelegateInfo() {
        getLiveData().setValue(Constants.PROMO_DIALOG);
    }

    public void lessOffer() {
            if (getOfferDetails().getLower_offers_counter() != 3)
                getLiveData().setValue(Constants.LESS_OFFER);
            else
                getLiveData().setValue(Constants.CANCEL_ORDER);
    }

    public void acceptOffer() {
        getLiveData().setValue(Constants.ACCEPT_OFFER);
    }
}
