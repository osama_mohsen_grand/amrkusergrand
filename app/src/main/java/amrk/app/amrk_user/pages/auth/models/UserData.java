package amrk.app.amrk_user.pages.auth.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserData {

    @SerializedName("suspend")
    private int suspend;

    @SerializedName("wallet_flag")
    private int walletFlag;

    @SerializedName("image")
    private String image;

    @SerializedName("wallet")
    private String wallet;

    @SerializedName("lng")
    private String lng;

    @SerializedName("jwt")
    private String jwt;

    @SerializedName("verified")
    private String verified;

    @SerializedName("active")
    private int active;

    @SerializedName("promo_code")
    private String promoCode;

    @SerializedName("token")
    private String token;

    @SerializedName("points")
    private int points;

    @SerializedName("user_code")
    private String userCode;

    @SerializedName("phone")
    private String phone;

    @SerializedName("name")
    private String name;

    @SerializedName("f_name")
    @Expose
    private String f_name;

    @SerializedName("l_name")
    @Expose
    private String l_name;

    @SerializedName("id")
    private int id;

    @SerializedName("email")
    private String email;

    @SerializedName("country_id")
    private int countryId;

    @SerializedName("lat")
    private String lat;
    @SerializedName("orders_count")
    private String orders_count;

    @SerializedName("comments_count")
    private String comments_count;

    @SerializedName("rate")
    private String rate;
    @SerializedName("car_model")
    @Expose
    private String car_model;
    @SerializedName("car_image")
    @Expose
    private String car_image;
    @SerializedName("car_level")
    @Expose
    private String car_level;
    @SerializedName("car_num")
    @Expose
    private String car_num;
    @SerializedName("car_color")
    @Expose
    private String car_color;
    @SerializedName("color_name")
    @Expose
    private String color_name;
    @SerializedName("no_of_trips")
    @Expose
    private String no_of_trips;
    @SerializedName("supplier_code ")
    private int supplier_code;

    public int getSupplier_code() {
        return supplier_code;
    }

    public String getNo_of_trips() {
        return no_of_trips;
    }

    public int getSuspend() {
        return suspend;
    }

    public int getWalletFlag() {
        return walletFlag;
    }

    public String getImage() {
        return image;
    }

    public String getWallet() {
        return wallet;
    }

    public String getLng() {
        return lng;
    }

    public String getJwt() {
        return jwt;
    }

    public String getVerified() {
        return verified;
    }

    public int getActive() {
        return active;
    }

    public String getPromoCode() {
        return promoCode;
    }

    public String getToken() {
        return token;
    }

    public int getPoints() {
        return points;
    }

    public String getUserCode() {
        return userCode;
    }

    public String getPhone() {
        return phone;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public int getCountryId() {
        return countryId;
    }

    public String getLat() {
        return lat;
    }

    public String getOrders_count() {
        return orders_count;
    }

    public String getComments_count() {
        return comments_count;
    }

    public String getRate() {
        return rate;
    }

    public String getF_name() {
        return f_name;
    }

    public String getCar_model() {
        return car_model;
    }

    public String getCar_image() {
        return car_image;
    }

    public String getCar_level() {
        return car_level;
    }

    public String getCar_num() {
        return car_num;
    }

    public String getCar_color() {
        return car_color;
    }

    public void setF_name(String f_name) {
        this.f_name = f_name;
    }

    public void setWallet(String wallet) {
        this.wallet = wallet;
    }

    public String getColor_name() {
        return color_name;
    }

    @Override
    public String toString() {
        return "UserData{" +
                "suspend=" + suspend +
                ", walletFlag=" + walletFlag +
                ", image='" + image + '\'' +
                ", wallet='" + wallet + '\'' +
                ", lng='" + lng + '\'' +
                ", jwt='" + jwt + '\'' +
                ", verified='" + verified + '\'' +
                ", active=" + active +
                ", promoCode='" + promoCode + '\'' +
                ", token='" + token + '\'' +
                ", points=" + points +
                ", userCode='" + userCode + '\'' +
                ", phone='" + phone + '\'' +
                ", name='" + name + '\'' +
                ", f_name='" + f_name + '\'' +
                ", l_name='" + l_name + '\'' +
                ", id=" + id +
                ", email='" + email + '\'' +
                ", countryId=" + countryId +
                ", lat='" + lat + '\'' +
                ", orders_count='" + orders_count + '\'' +
                ", comments_count='" + comments_count + '\'' +
                ", rate='" + rate + '\'' +
                ", car_model='" + car_model + '\'' +
                ", car_image='" + car_image + '\'' +
                ", car_level='" + car_level + '\'' +
                ", car_num='" + car_num + '\'' +
                ", car_color='" + car_color + '\'' +
                ", color_name='" + color_name + '\'' +
                ", no_of_trips='" + no_of_trips + '\'' +
                ", supplier_code=" + supplier_code +
                '}';
    }
}