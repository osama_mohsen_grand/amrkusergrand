package amrk.app.amrk_user.pages.gifts.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.gifts.models.ReplacedPointsData;
import amrk.app.amrk_user.utils.Constants;

public class ItemGiftViewModel extends BaseViewModel {
    public ReplacedPointsData replacedPointsData;

    public ItemGiftViewModel(ReplacedPointsData replacedPointsData) {
        this.replacedPointsData = replacedPointsData;
    }

    @Bindable
    public ReplacedPointsData getReplacedPointsData() {
        return replacedPointsData;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
