package amrk.app.amrk_user.pages.publicOrder;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentPublicOrderBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.publicOrder.models.PublicOrderInfoResponse;
import amrk.app.amrk_user.pages.publicOrder.viewModels.PublicOrdersViewModel;
import amrk.app.amrk_user.pages.reviews.ReviewsFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;

public class PublicOrdersFragment extends BaseFragment {

    private Context context;
    @Inject
    PublicOrdersViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        FragmentPublicOrderBinding binding = DataBindingUtil.inflate(inflater, R.layout.fragment_public_order, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.publicOrderInfo();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.GET_PUBLIC_ORDER_INFO.equals(((Mutable) o).message)) {
                viewModel.setPublicInfo(((PublicOrderInfoResponse) ((Mutable) o).object).getPublicInfo());
            } else if (Constants.MARKET_REVIEWS.equals(((Mutable) o).message)) {
                MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.DEPARTMENT_3, Constants.GET_PUBLIC_ORDER_INFO), getResources().getString(R.string.client_reviews), ReviewsFragment.class.getName(), null,Constants.MARKETS);
            } else if (Constants.NEW_ORDER.equals(((Mutable) o).message)) {
                MovementHelper.startActivityWithBundle(context, new PassingObject(), getResources().getString(R.string.new_order_bar), PublicNewOrderFragment.class.getName(), null,Constants.MARKETS);
            }
        });
        ((BaseActivity) context).getRefreshingLiveData().observe(((LifecycleOwner) context), aBoolean -> {
            baseActivity().stopRefresh(false);
            viewModel.publicOrderInfo();
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        viewModel.getPublicOrderRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
