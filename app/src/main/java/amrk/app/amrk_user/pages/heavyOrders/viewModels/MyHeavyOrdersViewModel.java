package amrk.app.amrk_user.pages.heavyOrders.viewModels;

import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.heavyOrders.adapters.MyHeavyOrdersAdapter;
import amrk.app.amrk_user.pages.myOrders.viewModels.MyOrdersViewModel;
import amrk.app.amrk_user.pages.reviews.models.RateRequest;
import amrk.app.amrk_user.repository.MarketRepository;
import io.reactivex.disposables.CompositeDisposable;

public class MyHeavyOrdersViewModel extends MyOrdersViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    MarketRepository marketRepository;
    MyHeavyOrdersAdapter heavyOrdersAdapter;
    RateRequest rateRequest;

    @Inject
    public MyHeavyOrdersViewModel(MarketRepository marketRepository) {
        this.liveData = new MutableLiveData<>();
        this.marketRepository = marketRepository;
        marketRepository.setLiveData(liveData);
        rateRequest = new RateRequest();
    }

    public void myOrders(int departmentId, int type) {
        compositeDisposable.add(marketRepository.myOrders(departmentId, type));
    }

    public void cancelOrder(int orderId) {
        compositeDisposable.add(marketRepository.CancelOrder(orderId));
    }

    public void sendRate(int position, int pageType) {
        getRateRequest().setOrderId(String.valueOf(getHeavyOrdersAdapter().getMyOrdersDataList().get(position).getId()));
        if (getRateRequest().isValid())
            compositeDisposable.add(marketRepository.sendOrderReview(getRateRequest()));
    }

    public MarketRepository getMarketRepository() {
        return marketRepository;
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        getRateRequest().setRate(String.valueOf(rating));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }


    public RateRequest getRateRequest() {
        return rateRequest;
    }

    public void setRateRequest(RateRequest rateRequest) {
        this.rateRequest = rateRequest;
    }

    @Bindable
    public MyHeavyOrdersAdapter getHeavyOrdersAdapter() {
        return this.heavyOrdersAdapter == null ? this.heavyOrdersAdapter = new MyHeavyOrdersAdapter() : this.heavyOrdersAdapter;
    }

}
