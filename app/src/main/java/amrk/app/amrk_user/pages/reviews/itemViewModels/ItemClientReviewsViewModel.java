package amrk.app.amrk_user.pages.reviews.itemViewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.reviews.models.RatesItem;

public class ItemClientReviewsViewModel extends BaseViewModel {
    public RatesItem ratesItem;

    public ItemClientReviewsViewModel(RatesItem ratesItem) {
        this.ratesItem = ratesItem;
    }

    @Bindable
    public RatesItem getRatesItem() {
        return ratesItem;
    }


}
