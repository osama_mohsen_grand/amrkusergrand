package amrk.app.amrk_user.pages.myOrders.viewModels;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableField;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.promoCode.CheckPromoRequest;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.pages.myOrders.adapters.ReorderAdapter;
import amrk.app.amrk_user.pages.myOrders.models.reOrder.OrderDetailsData;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.NewOrderRequest;
import amrk.app.amrk_user.repository.CartRepository;
import amrk.app.amrk_user.repository.MarketRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import io.reactivex.disposables.CompositeDisposable;

public class ReOrderViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    MarketRepository marketRepository;
    CheckPromoRequest checkPromoRequest;
    NewOrderRequest newOrderRequest;
    ReorderAdapter reorderAdapter;
    OrderDetailsData orderDetailsData;
    CartRepository cartRepository;
    LiveData<List<ProductDetails>> cartLiveData;
    LiveData<List<VariationsItem>> variationsListLiveData;
    List<VariationsItem> variationsItemList;
    LiveData<List<OptionsItem>> optionsListLiveData;
    List<OptionsItem> optionsList;
    public ObservableField<Boolean> confirmBtnStatus = new ObservableField<>();

    @Inject
    public ReOrderViewModel(MarketRepository marketRepository) {
        orderDetailsData = new OrderDetailsData();
        checkPromoRequest = new CheckPromoRequest();
        newOrderRequest = new NewOrderRequest();
        variationsItemList = new ArrayList<>();
        optionsList = new ArrayList<>();
        cartRepository = new CartRepository(MyApplication.getInstance());
        cartLiveData = cartRepository.getAllProducts();
        variationsListLiveData = cartRepository.getVariations();
        optionsListLiveData = cartRepository.getOptions();
        this.liveData = new MutableLiveData<>();
        this.marketRepository = marketRepository;
        marketRepository.setLiveData(liveData);
    }

    public void orderDetails() {
        compositeDisposable.add(marketRepository.getOrderDetails(getPassingObject().getId()));
    }

    public void checkPromo() {
        checkPromoRequest.setDepartment_id(getNewOrderRequest().getDepartment_id());
        if (getCheckPromoRequest().isValid())
            compositeDisposable.add(getMarketRepository().checkPromo(checkPromoRequest));
    }

    public void createOrder() {
        confirmBtnStatus.set(true);
        if (getNewOrderRequest().getDepartment_id() == Constants.STORES_API_ID) {
            for (int p = 0; p < getReorderAdapter().getMenuModels().size(); p++) {
                List<VariationsItem> requestVarList = new ArrayList<>();
                for (int v = 0; v < getVariationsItemList().size(); v++) {
                    if (getReorderAdapter().getMenuModels().get(p).getProduct_room_id() == getVariationsItemList().get(v).getProductIdForeignKey()) {
                        String variations = getVariationsItemList().get(v).getVariations().replace("[", "").replace("]", "").replace(" ", "");
                        List<String> selectedVarList = Arrays.asList(variations.split(","));
                        String lastSelected = "";
                        for (int k = 0; k < selectedVarList.size(); k++) {
                            if (selectedVarList.get(k).equals(lastSelected)) {
                                continue;
                            }
                            lastSelected = selectedVarList.get(k);
                            VariationsItem variationsItem = new VariationsItem();
                            variationsItem.setVariation_id(Integer.parseInt(selectedVarList.get(k)));
                            for (int o = 0; o < getOptionsList().size(); o++) {
                                if (getVariationsItemList().get(v).getVariation_room_id() == getOptionsList().get(o).getVariationIdForeignKey()) {
                                    List<OptionsItem> requestOptList = new ArrayList<>();
                                    String options = getOptionsList().get(v).getOptions().replace("[", "").replace("]", "").replace(" ", "");
                                    List<String> selectedOptions = Arrays.asList(options.split(","));
                                    for (int sO = k; sO < selectedVarList.size(); sO++) {
                                        OptionsItem optionsItem = new OptionsItem();
                                        optionsItem.setOption_id(Integer.parseInt(selectedOptions.get(sO)));
                                        requestOptList.add(optionsItem);
                                        variationsItem.setOptions(requestOptList);
                                        if (sO + 1 < selectedVarList.size() && !selectedVarList.get(sO).equals(selectedVarList.get(sO + 1))) {
                                            break;
                                        }
                                    }
                                    variationsItem.setOptions(requestOptList);
                                    break;
                                }
                            }
                            requestVarList.add(variationsItem);
                        }
                        getReorderAdapter().getMenuModels().get(p).setVariations(requestVarList);
                        break;
                    }

                }
            }
            getNewOrderRequest().setProductDetailsList(getReorderAdapter().getMenuModels());
            if (getReorderAdapter().getItemCount() > 0)
                compositeDisposable.add(marketRepository.reOrder(getNewOrderRequest()));
            else
                liveData.setValue(new Mutable(Constants.EMPTY_DRIVER));
        } else
            compositeDisposable.add(marketRepository.reOrder(getNewOrderRequest()));

    }

    public MarketRepository getMarketRepository() {
        return marketRepository;
    }

    public void toDestLocation() {
        liveData.setValue(new Mutable(Constants.DESTINATION_LOCATION));
    }

    public void toDeliveryTimes() {
        liveData.setValue(new Mutable(Constants.DELIVERY_TIMES));
    }

    public void showPromoDialog() {
        liveData.setValue(new Mutable(Constants.PROMO_DIALOG));
    }

    public CartRepository getCartRepository() {
        return cartRepository;
    }

    public LiveData<List<ProductDetails>> getCartLiveData() {
        return cartLiveData;
    }

    public void setVariationsItemList(List<VariationsItem> variationsItemList) {
        this.variationsItemList = variationsItemList;
    }

    public List<VariationsItem> getVariationsItemList() {
        return variationsItemList;
    }

    public LiveData<List<VariationsItem>> getVariationsListLiveData() {
        return variationsListLiveData;
    }

    public LiveData<List<OptionsItem>> getOptionsListLiveData() {
        return optionsListLiveData;
    }

    public void setOptionsList(List<OptionsItem> optionsList) {
        this.optionsList = optionsList;
    }

    public List<OptionsItem> getOptionsList() {
        return optionsList;
    }

    public CheckPromoRequest getCheckPromoRequest() {
        return checkPromoRequest;
    }

    public void checkPromoDialog() {
        liveData.setValue(new Mutable(Constants.PROMO_DIALOG));
    }

    @Bindable
    public ReorderAdapter getReorderAdapter() {
        return this.reorderAdapter == null ? this.reorderAdapter = new ReorderAdapter() : this.reorderAdapter;
    }

    @Bindable
    public void setOrderDetailsData(OrderDetailsData orderDetailsData) {
        // setting data to create request
        if (orderDetailsData.getDepartmentId() == Constants.STORES_API_ID) {
            getNewOrderRequest().setShopId(String.valueOf(orderDetailsData.getShop().getId()));
            getNewOrderRequest().setShopImage(orderDetailsData.getShop().getImage());
        }
        getNewOrderRequest().setOrderId(String.valueOf(orderDetailsData.getId()));
        getNewOrderRequest().setIn_addresss(orderDetailsData.getInAddress());
        getNewOrderRequest().setIn_lat(Double.parseDouble(orderDetailsData.getInLat()));
        getNewOrderRequest().setIn_lng(Double.parseDouble(orderDetailsData.getInLng()));
        getNewOrderRequest().setIn_city_name(orderDetailsData.getInCityName());

        getNewOrderRequest().setOut_addresss(orderDetailsData.getOutAddress());
        getNewOrderRequest().setOut_lat(Double.parseDouble(orderDetailsData.getOutLat()));
        getNewOrderRequest().setOut_lng(Double.parseDouble(orderDetailsData.getOutLng()));
        getNewOrderRequest().setOut_city_name(orderDetailsData.getOutCityName());

        getNewOrderRequest().setLast_address(orderDetailsData.getLast_address());
        getNewOrderRequest().setLast_lat(orderDetailsData.getLast_lat());
        getNewOrderRequest().setLast_lng(orderDetailsData.getLast_lng());
        getNewOrderRequest().setLast_city_name(orderDetailsData.getLast_city_name());

        getNewOrderRequest().setTitle(orderDetailsData.getTitle());
        getNewOrderRequest().setSub_department_id(orderDetailsData.getSub_department_id());
        getNewOrderRequest().setDepartment_id(orderDetailsData.getDepartmentId());
        getNewOrderRequest().setOut_lng(Double.parseDouble(orderDetailsData.getOutLng()));
        getNewOrderRequest().setOut_city_name(orderDetailsData.getOutCityName());
        if (orderDetailsData.getDeliveryTime().equals(ResourceManager.getString(R.string.oneHour))) {
            getNewOrderRequest().setDelivery_time(ResourceManager.getString(R.string.oneHour));
        } else if (orderDetailsData.getDeliveryTime().equals(ResourceManager.getString(R.string.twoHour))) {
            getNewOrderRequest().setDelivery_time(ResourceManager.getString(R.string.twoHour));
        } else
            getNewOrderRequest().setDelivery_time(ResourceManager.getString(R.string.threeHour));
        if (orderDetailsData.getOrderProducts() != null)
            getCartRepository().insertReOrder(orderDetailsData.getOrderProducts());
        notifyChange(BR.orderDetailsData);
        notifyChange(BR.newOrderRequest);
        this.orderDetailsData = orderDetailsData;
    }

    @Bindable
    public void setNewOrderRequest(NewOrderRequest newOrderRequest) {
        this.newOrderRequest = newOrderRequest;
    }

    @Bindable
    public NewOrderRequest getNewOrderRequest() {
        return newOrderRequest;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

}
