package amrk.app.amrk_user.pages.auth.login;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentLoginBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.auth.confirmCode.ConfirmCodeFragment;
import amrk.app.amrk_user.pages.auth.forgetPassword.ForgetPasswordFragment;
import amrk.app.amrk_user.pages.auth.models.UsersResponse;
import amrk.app.amrk_user.pages.auth.register.RegisterFragment;
import amrk.app.amrk_user.pages.countries.UserDetectLocation;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import amrk.app.amrk_user.utils.session.UserHelper;

public class LoginFragment extends BaseFragment {
    private Context context;
    @Inject
    LoginViewModel viewModel;
    FragmentLoginBinding binding;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
//        binding.ccp.setCustomMasterCountries(UserHelper.getInstance(context).getCountryCodes());
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
//        viewModel.cpp = binding.ccp.getDefaultCountryCode();
//        binding.ccp.setOnCountryChangeListener(() -> {
//            if (viewModel.getLoginRequest().getPhone().contains(viewModel.cpp)) {
//                viewModel.getLoginRequest().setPhone(viewModel.getLoginRequest().getPhone().replace(viewModel.cpp, ""));
//            }
//            viewModel.cpp = binding.ccp.getSelectedCountryCode();
//        });

        viewModel.liveData.observe((LifecycleOwner) context, (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            switch (((Mutable) o).message) {
                case Constants.PHONE_VERIFIED:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    viewModel.setLoginStatus(View.GONE);
                    break;
                case Constants.LOGIN:
                    toastMessage(((StatusMessage) mutable.object).mMessage);
                    UserHelper.getInstance(context).userLogin(((UsersResponse) ((Mutable) o).object).getData());
                    MovementHelper.startActivityBase(context, UserDetectLocation.class.getName(), null, null);
                    break;
                case Constants.FORGET_PASSWORD:
                    MovementHelper.startActivity(context, ForgetPasswordFragment.class.getName(), null, null, null);
                    break;
                case Constants.REGISTER:
                    MovementHelper.startActivity(context, RegisterFragment.class.getName(), null, null, null);
                    break;
                case Constants.PRIVACY:
                    MovementHelper.openCustomTabs(context, URLS.POLICY_URL + 0 + "/" + LanguagesHelper.getCurrentLanguage(),getString(R.string.privacy));
                    break;
                case Constants.TERMS:
                    MovementHelper.openCustomTabs(context, URLS.TERMS_URL + 0 + "/" + LanguagesHelper.getCurrentLanguage(), getString(R.string.terms));
                    break;
                case Constants.NOT_VERIFIED:
                    MovementHelper.startActivity(context, ConfirmCodeFragment.class.getName(), null, null, null);
                    MovementHelper.startActivityWithBundle(context, new PassingObject(viewModel.getLoginRequest().getPhone()), null, ConfirmCodeFragment.class.getName(), null, null);
                    break;
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        viewModel.repository.setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
