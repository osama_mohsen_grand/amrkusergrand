package amrk.app.amrk_user.pages.publicOrder.models.subDepartment;

import com.google.gson.annotations.SerializedName;

public class SubDepartmentData {

    @SerializedName("name")
    private String name;

    @SerializedName("id")
    private int id;
    @SerializedName("has_processing")
    private int has_processing;

    public String getName() {
        return name;
    }

    public int getHas_processing() {
        return has_processing;
    }

    public int getId() {
        return id;
    }
}