package amrk.app.amrk_user.pages.offers.models;

import com.google.gson.annotations.SerializedName;

public class OrderOfferRequest {
    @SerializedName("order_id")
    private String orderId;

    @SerializedName("order_offer_id")
    private String order_offer_id;
    @SerializedName("type")
    private String type;

    public String getOrderId() {
        return orderId;
    }

    public void setOrderId(String orderId) {
        this.orderId = orderId;
    }

    public String getOrder_offer_id() {
        return order_offer_id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setOrder_offer_id(String order_offer_id) {
        this.order_offer_id = order_offer_id;
    }
}
