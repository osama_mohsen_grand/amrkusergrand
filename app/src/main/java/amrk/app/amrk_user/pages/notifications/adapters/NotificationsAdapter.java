package amrk.app.amrk_user.pages.notifications.adapters;

import android.content.Context;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.NotifyItemBinding;
import amrk.app.amrk_user.pages.chat.view.ChatFragment;
import amrk.app.amrk_user.pages.notifications.itemViewModels.NotificationsItemViewModels;
import amrk.app.amrk_user.pages.notifications.models.NotificationsData;
import amrk.app.amrk_user.pages.offers.OffersDetailsFragment;
import amrk.app.amrk_user.uber.history.HistoryDetailsFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;


public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {
    public List<NotificationsData> notificationsDataList;
    Context context;

    public NotificationsAdapter() {
        notificationsDataList = new ArrayList<>();
    }


    @NotNull
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.notify_item,
                parent, false);
        context = parent.getContext();
        return new ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NotificationsData dataModel = notificationsDataList.get(position);
        NotificationsItemViewModels homeItemViewModels = new NotificationsItemViewModels(dataModel);
        homeItemViewModels.getLiveData().observe((LifecycleOwner) MovementHelper.unwrap(context), o -> {
            if (o.equals(Constants.TRIP_DETAILS))
                MovementHelper.startActivityForResultWithBundle(context, new PassingObject(dataModel.getOrder_id()), ResourceManager.getString(R.string.menuHistroy), HistoryDetailsFragment.class.getName(), null, null);
            else if (o.equals(Constants.ORDER_DETAILS)) {
                if (TextUtils.isEmpty(dataModel.getOrderStatus().getAccept()))
                    MovementHelper.startActivityForResultWithBundle(context, new PassingObject(dataModel.getOrder_id()), ResourceManager.getString(R.string.offers_title), OffersDetailsFragment.class.getName(), null, Constants.MARKETS);
                else if (TextUtils.isEmpty(dataModel.getOrderStatus().getFinished())||TextUtils.isEmpty(dataModel.getOrderStatus().getCancelled()))
                    MovementHelper.startActivityWithBundle(context, new PassingObject(dataModel.getOrder_id(), dataModel.getDelegateId()), null, ChatFragment.class.getName(), null, null);

            }
        });
        holder.setViewModel(homeItemViewModels);
    }

    @Override
    public int getItemCount() {
        return this.notificationsDataList.size();
    }

    //
    @Override
    public void onViewAttachedToWindow(@NotNull ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    public void updateData(@NotNull List<NotificationsData> data) {
        this.notificationsDataList.clear();
        this.notificationsDataList.addAll(data);
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        NotifyItemBinding itemBinding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemBinding == null) {
                itemBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemBinding != null) {
                itemBinding.unbind();
            }
        }

        void setViewModel(NotificationsItemViewModels itemViewModels) {
            if (itemBinding != null) {
                itemBinding.setNotifyItemViewModels(itemViewModels);
            }
        }
    }
}
