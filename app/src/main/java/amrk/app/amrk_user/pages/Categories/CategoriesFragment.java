package amrk.app.amrk_user.pages.Categories;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentCategoriesBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.home.SearchFragment;
import amrk.app.amrk_user.pages.home.model.HomeResponse;
import amrk.app.amrk_user.pages.home.viewModels.HomeViewModel;
import amrk.app.amrk_user.pages.markets.MarketsFragment;
import amrk.app.amrk_user.pages.notifications.NotificationsFragment;
import amrk.app.amrk_user.pages.publicOrder.PublicOrdersFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;

public class CategoriesFragment extends BaseFragment {

    private Context context;
    FragmentCategoriesBinding binding;
    @Inject
    HomeViewModel viewModel;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_categories, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewmodel(viewModel);
        viewModel.setServices();
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.STORES.equals(((Mutable) o).message)) {
                MovementHelper.startActivity(context, MarketsFragment.class.getName(), getResources().getString(R.string.market_page), null, Constants.MARKETS);
            } else if (Constants.ORDER_ANY_THING.equals(((Mutable) o).message)) {
                MovementHelper.startActivity(context, PublicOrdersFragment.class.getName(), getResources().getString(R.string.public_order_bar_name), Constants.SHARE_BAR, Constants.MARKETS);
            } else if (Constants.NOTIFICATIONS.equals(((Mutable) o).message)) {
                MovementHelper.startActivity(context, NotificationsFragment.class.getName(), getString(R.string.menuNotifications), null, Constants.MARKETS);
            } else if (Constants.SEARCH.equals(mutable.message)) {
                MovementHelper.startActivity(context, SearchFragment.class.getName(), null, null, null);
            } else if (Constants.HOME.equals(mutable.message)) {
                viewModel.getCategoriesAdapter().update(((HomeResponse) mutable.object).getData().getDepartments());
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        viewModel.getSettingsRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
