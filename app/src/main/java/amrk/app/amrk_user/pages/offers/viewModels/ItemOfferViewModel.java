package amrk.app.amrk_user.pages.offers.viewModels;


import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.offers.models.OffersData;
import amrk.app.amrk_user.utils.Constants;

public class ItemOfferViewModel extends BaseViewModel {
    public OffersData offersData;

    public ItemOfferViewModel(OffersData offersData) {
        this.offersData = offersData;
    }

    @Bindable
    public OffersData getOffersData() {
        return offersData;
    }

    public void itemAction() {
//        if (offersData.getHasOffers() == 1)
            getLiveData().setValue(Constants.MENu);
    }

}
