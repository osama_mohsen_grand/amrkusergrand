package amrk.app.amrk_user.pages.markets.viewModels;


import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.markets.adapters.MarketMenuAdapter;
import amrk.app.amrk_user.pages.markets.adapters.MarketMenuCategoryAdapter;
import amrk.app.amrk_user.pages.markets.adapters.WorkingDaysAdapter;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.markets.models.marketDetails.MarketDetails;
import amrk.app.amrk_user.repository.MarketRepository;
import amrk.app.amrk_user.utils.Constants;
import io.reactivex.disposables.CompositeDisposable;

public class MarketDetailsViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    MarketRepository repository;
    private ShopsItem shopsItem;
    private MarketDetails details;
    WorkingDaysAdapter daysAdapter;
    MarketMenuCategoryAdapter menuCategoryAdapter;
    MarketMenuAdapter marketMenuAdapter;
    private int totalQuantity;

    @Inject
    public MarketDetailsViewModel(MarketRepository repository) {
        marketMenuAdapter = new MarketMenuAdapter();
        menuCategoryAdapter = new MarketMenuCategoryAdapter();
        daysAdapter = new WorkingDaysAdapter();
        shopsItem = new ShopsItem();
        details = new MarketDetails();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public void getMarketDetails() {
        compositeDisposable.add(repository.getMarketDetails(getShopsItem().getId()));
    }

    public void toWorkingDays() {
        liveData.setValue(new Mutable(Constants.GET_MARKET_WORKING_DAYS));
    }

    public void toReviews() {
        liveData.setValue(new Mutable(Constants.MARKET_REVIEWS));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public MarketRepository getRepository() {
        return repository;
    }

    @Bindable
    public ShopsItem getShopsItem() {
        return shopsItem;
    }

    @Bindable
    public void setShopsItem(ShopsItem shopsItem) {
        notifyChange(BR.shopsItem);
        this.shopsItem = shopsItem;
    }

    @Bindable
    public MarketDetails getDetails() {
        return details;
    }

    @Bindable
    public void setDetails(MarketDetails details) {
        getMenuCategoryAdapter().update(details.getMenus());
        getMarketMenuAdapter().update(details.getMenus());
        notifyChange(BR.details);
        this.details = details;
    }

    @Bindable
    public int getTotalQuantity() {
        return totalQuantity;
    }

    @Bindable
    public void setTotalQuantity(int totalQuantity) {
        notifyChange(BR.totalQuantity);
        this.totalQuantity = totalQuantity;
    }

    public WorkingDaysAdapter getDaysAdapter() {
        return daysAdapter;
    }

    public MarketMenuCategoryAdapter getMenuCategoryAdapter() {
        return menuCategoryAdapter;
    }

    public MarketMenuAdapter getMarketMenuAdapter() {
        return marketMenuAdapter;
    }

    public void toCart() {
        if (getTotalQuantity() > 0)
            liveData.setValue(new Mutable(Constants.TO_CART));
    }
}
