package amrk.app.amrk_user.pages.chat.model;

import com.google.gson.annotations.SerializedName;

public class DelegateInfo {
    @SerializedName("id")
    private int id;
    @SerializedName("f_name")
    private String f_name;
    @SerializedName("l_name")
    private String l_name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("supplier_code")
    private int supplier_code;
    @SerializedName("currency")
    private String currency;

    public int getId() {
        return id;
    }

    public String getF_name() {
        return f_name;
    }

    public String getL_name() {
        return l_name;
    }

    public String getPhone() {
        return phone;
    }

    public int getSupplier_code() {
        return supplier_code;
    }

    public String getCurrency() {
        return currency;
    }
}
