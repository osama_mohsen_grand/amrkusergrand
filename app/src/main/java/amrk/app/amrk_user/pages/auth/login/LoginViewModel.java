package amrk.app.amrk_user.pages.auth.login;

import android.view.View;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.models.LoginRequest;

import amrk.app.amrk_user.repository.AuthRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class LoginViewModel extends BaseViewModel {
    MutableLiveData<Mutable> liveData;
    @Inject
    AuthRepository repository;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    LoginRequest loginRequest;
    private int loginStatus = View.VISIBLE;
    String cpp;

    @Inject
    public LoginViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
        loginRequest = new LoginRequest();
    }

    public void register() {
        liveData.setValue(new Mutable(Constants.REGISTER));
    }

    public void forgetPassword() {
        liveData.setValue(new Mutable(Constants.FORGET_PASSWORD));
    }

    public void loginPhone() {
        if (getLoginRequest().isValid()) {
//            if (!getLoginRequest().getPhone().contains(cpp)) {
//                getLoginRequest().setPhone(cpp + getLoginRequest().getPhone());
//            }
            repository.loginPhone(loginRequest);
        }
    }

    public void loginPassword() {
        getLoginRequest().setToken(UserHelper.getInstance(MyApplication.getInstance()).getToken());
        if (getLoginRequest().isPasswordValid()) {
            setMessage(Constants.SHOW_PROGRESS);
            repository.loginPassword(loginRequest);
        }
    }

    public void toRegister() {
        liveData.setValue(new Mutable(Constants.REGISTER));
    }

    public void toTerms() {
        liveData.setValue(new Mutable(Constants.TERMS));
    }

    public void toPrivacy() {
        liveData.setValue(new Mutable(Constants.PRIVACY));
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public LoginRequest getLoginRequest() {
        return loginRequest;
    }

    @Bindable
    public int getLoginStatus() {
        return loginStatus;
    }

    @Bindable
    public void setLoginStatus(int loginStatus) {
        notifyChange(BR.loginStatus);
        this.loginStatus = loginStatus;
    }
}
