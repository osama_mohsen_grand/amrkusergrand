package amrk.app.amrk_user.pages.offers.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemOffersDetailsBinding;
import amrk.app.amrk_user.pages.offers.models.offerDetails.OfferDetails;
import amrk.app.amrk_user.pages.offers.viewModels.ItemOfferDetailsViewModel;
import amrk.app.amrk_user.utils.Constants;


public class OffersDetailsAdapter extends RecyclerView.Adapter<OffersDetailsAdapter.MenuView> {
    List<OfferDetails> offerDetailsList;
    MutableLiveData<Object> liveDataAdapter = new MutableLiveData<>();
    private Context context;
    public int lastPosition = -1;
    boolean isClickable = true; // form disabled or enabled single click on item

    public OffersDetailsAdapter() {
        this.offerDetailsList = new ArrayList<>();
    }

    public List<OfferDetails> getOfferDetailsList() {
        return offerDetailsList;
    }

    public MutableLiveData<Object> getLiveDataAdapter() {
        return liveDataAdapter;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offers_details,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, @SuppressLint("RecyclerView") final int position) {
        OfferDetails menuModel = offerDetailsList.get(position);
        ItemOfferDetailsViewModel itemMenuViewModel = new ItemOfferDetailsViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            if (o.equals(Constants.PROMO_DIALOG)) {
                lastPosition = position;
                liveDataAdapter.setValue(o);
            } else {
                if (isClickable) {
                    isClickable = false;
                    itemMenuViewModel.setMessage(Constants.SHOW_PROGRESS);
                    lastPosition = position;
                    liveDataAdapter.setValue(o);
                }
            }
        });
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(List<OfferDetails> dataList) {
        this.offerDetailsList.clear();
        offerDetailsList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return offerDetailsList.size();
    }

    public void resetItem() {
        if (lastPosition != -1 && lastPosition < offerDetailsList.size()) {
            offerDetailsList.get(lastPosition).setReset(true);
            notifyItemChanged(lastPosition);
        }
        isClickable = true;
    }

    public void updateNewOffer(OfferDetails offerDetailsPusher) {
        getOfferDetailsList().add(offerDetailsPusher);
        notifyItemInserted(getOfferDetailsList().size() - 1);
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemOffersDetailsBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemOfferDetailsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemOfferViewModel(itemViewModels);
            }
        }
    }
}
