package amrk.app.amrk_user.pages.offers.models.offerDetails;

import com.google.gson.annotations.SerializedName;

public class Delegate{

	@SerializedName("image")
	private String image;

	@SerializedName("orders_count")
	private int ordersCount;

	@SerializedName("rate")
	private int rate;

	@SerializedName("comments_count")
	private int commentsCount;

	@SerializedName("l_name")
	private String lName;

	@SerializedName("f_name")
	private String fName;

	@SerializedName("id")
	private int id;

	public String getImage(){
		return image;
	}

	public int getOrdersCount(){
		return ordersCount;
	}

	public int getRate(){
		return rate;
	}

	public int getCommentsCount(){
		return commentsCount;
	}

	public String getLName(){
		return lName;
	}

	public String getFName(){
		return fName;
	}

	public int getId(){
		return id;
	}
}