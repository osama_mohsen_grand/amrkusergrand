package amrk.app.amrk_user.pages.myFatora;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.Observer;

import com.google.gson.Gson;
import com.myfatoorah.sdk.entity.initiatepayment.PaymentMethod;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentSendPaymentBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.myFatora.viewModels.MyFatooraPaymentsViewModel;
import amrk.app.amrk_user.utils.Constants;

public class SendPaymentFragment extends BaseFragment {
    FragmentSendPaymentBinding methodsBinding;
    @Inject
    MyFatooraPaymentsViewModel viewModel;
    MyFatoraInit myFatoraInit;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        methodsBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_send_payment, container, false);
        IApplicationComponent component = ((MyApplication) requireActivity().getApplicationContext()).getApplicationComponent();
        component.inject(this);
        methodsBinding.setViewmodel(viewModel);
        myFatoraInit = new MyFatoraInit(requireActivity(), viewModel.liveData);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            viewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            viewModel.setPaymentMethod(new Gson().fromJson(String.valueOf(viewModel.getPassingObject().getObjectClass()), PaymentMethod.class));
        }
        setEvent();
        return methodsBinding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(requireActivity(), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            viewModel.setMessage(mutable.message.equals(Constants.HIDE_PROGRESS) ? mutable.message : "");
            if (((Mutable) o).message.equals(Constants.SEND_PAYMENT)) {
                myFatoraInit.customPaymentUI(viewModel.getPaymentMethod().getPaymentMethodId(), viewModel.getPaymentMethod().getTotalAmount(), viewModel.getSendPaymentRequest());
            }
        });
    }

}
