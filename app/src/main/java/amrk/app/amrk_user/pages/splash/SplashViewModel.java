package amrk.app.amrk_user.pages.splash;

import android.os.Handler;
import android.widget.RadioGroup;

import androidx.databinding.Bindable;
import androidx.databinding.ObservableBoolean;
import androidx.lifecycle.MutableLiveData;

import com.smarteist.autoimageslider.SliderView;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.onBoard.OnBoardAdapter;
import amrk.app.amrk_user.repository.AuthRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.CompositeDisposable;

public class SplashViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    OnBoardAdapter onBoardAdapter = new OnBoardAdapter();
    @Inject
    AuthRepository repository;
    public ObservableBoolean isLangShow = new ObservableBoolean();

    @Inject
    public SplashViewModel(AuthRepository repository) {
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);
    }

    public AuthRepository getRepository() {
        return repository;
    }

    public void runSplash() {
        new Handler().postDelayed(() -> {
            if (UserHelper.getInstance(MyApplication.getInstance()).getUserData() != null) {
                liveData.setValue(new Mutable(Constants.MENU_HOME));
            } else if (!UserHelper.getInstance(MyApplication.getInstance()).getIsFirst()) {
                liveData.setValue(new Mutable(Constants.LOGIN));
            } else {
                isLangShow.set(true);
            }
        }, 3000);
    }

    public void onLangChange(RadioGroup radioGroup, int id) {
        if (id == R.id.arabic) {
            lang = "ar";
        } else
            lang = "en";

    }

    @Bindable
    public OnBoardAdapter getOnBoardAdapter() {
        notifyChange(BR.onBoardAdapter);
        return onBoardAdapter;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public void getSlider() {
        compositeDisposable.add(repository.getBoard());
    }

    public void setupSlider(SliderView sliderView) {
        sliderView.setSliderAdapter(onBoardAdapter);
    }

    public void toNext() {
        liveData.setValue(new Mutable(Constants.NEXT));
    }

    public void toLogin() {
        liveData.setValue(new Mutable(Constants.LOGIN));
    }

    public void changeLang() {
        LanguagesHelper.setLanguage(lang);
        liveData.setValue(new Mutable(Constants.LOGIN));
    }

    public void getCountryCodes() {
        compositeDisposable.add(repository.getCountriesCodes());
    }
}
