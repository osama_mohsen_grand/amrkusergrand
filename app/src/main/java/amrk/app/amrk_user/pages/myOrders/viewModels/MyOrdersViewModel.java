package amrk.app.amrk_user.pages.myOrders.viewModels;

import android.widget.RatingBar;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.myOrders.adapters.MyStoreOrdersAdapter;
import amrk.app.amrk_user.pages.reviews.models.RateRequest;
import amrk.app.amrk_user.repository.MarketRepository;
import io.reactivex.disposables.CompositeDisposable;

public class MyOrdersViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    MarketRepository marketRepository;
    int type;
    MyStoreOrdersAdapter storeOrdersAdapter, normalOrdersAdapter;
    RateRequest rateRequest;

    @Inject
    public MyOrdersViewModel(MarketRepository marketRepository) {
        this.liveData = new MutableLiveData<>();
        this.marketRepository = marketRepository;
        marketRepository.setLiveData(liveData);
        rateRequest = new RateRequest();
    }

    public MyOrdersViewModel() {
    }

    public void myOrders(int departmentId, int type) {
        compositeDisposable.add(marketRepository.myOrders(departmentId, type));
    }

    public void sendRate(int position, int pageType) {
        if (pageType == 0)
            getRateRequest().setOrderId(String.valueOf(getStoreOrdersAdapter().getMyOrdersDataList().get(position).getId()));
        else
            getRateRequest().setOrderId(String.valueOf(getNormalOrdersAdapter().getMyOrdersDataList().get(position).getId()));
        if (getRateRequest().isValid())
            compositeDisposable.add(marketRepository.sendOrderReview(getRateRequest()));
    }

    public void changeOrderType(int departmentId) {
        if (getType() == 0)
            setType(1);
        else
            setType(0);
        myOrders(departmentId, getType());
    }

    public MarketRepository getMarketRepository() {
        return marketRepository;
    }

    public void onRateChange(RatingBar ratingBar, float rating, boolean fromUser) {
        getRateRequest().setRate(String.valueOf(rating));
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    @Bindable
    public int getType() {
        return type;
    }

    public void setType(int type) {
        notifyChange(BR.type);
        this.type = type;
    }

    public RateRequest getRateRequest() {
        return rateRequest;
    }

    public void setRateRequest(RateRequest rateRequest) {
        this.rateRequest = rateRequest;
    }

    @Bindable
    public MyStoreOrdersAdapter getStoreOrdersAdapter() {
        return this.storeOrdersAdapter == null ? this.storeOrdersAdapter = new MyStoreOrdersAdapter() : this.storeOrdersAdapter;
    }

    @Bindable
    public MyStoreOrdersAdapter getNormalOrdersAdapter() {
        return this.normalOrdersAdapter == null ? this.normalOrdersAdapter = new MyStoreOrdersAdapter() : this.normalOrdersAdapter;
    }

}
