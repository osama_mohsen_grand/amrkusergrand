package amrk.app.amrk_user.pages.myOrders.models.orders;

import java.util.List;

import com.google.gson.annotations.SerializedName;

public class OrderProductsItem {

    @SerializedName("quantity")
    private String quantity;

    @SerializedName("product_id")
    private int productId;

    @SerializedName("description")
    private String description;

    @SerializedName("created_at")
    private String createdAt;
    @SerializedName("product_name")
    private String product_name;
    @SerializedName("product_image")
    private String product_image;
    @SerializedName("price_after")
    private String price_after;
    private String currency;

    @SerializedName("id")
    private int id;

    @SerializedName("order_product_variations")
    private List<OrderProductVariationsItem> orderProductVariations;

    @SerializedName("order_id")
    private int orderId;

    public String getQuantity() {
        return quantity;
    }

    public int getProductId() {
        return productId;
    }

    public String getDescription() {
        return description;
    }

    public String getCreatedAt() {
        return createdAt;
    }

    public int getId() {
        return id;
    }

    public List<OrderProductVariationsItem> getOrderProductVariations() {
        return orderProductVariations;
    }

    public int getOrderId() {
        return orderId;
    }

    public String getProduct_name() {
        return product_name;
    }

    public String getProduct_image() {
        return product_image;
    }

    public String getPrice_after() {
        return price_after;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public void setPrice_after(String price_after) {
        this.price_after = price_after;
    }
}