package amrk.app.amrk_user.pages.markets.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemMarketBinding;
import amrk.app.amrk_user.pages.markets.MarketDetailsFragment;
import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.markets.viewModels.ItemMarketsViewModel;
import amrk.app.amrk_user.pages.publicOrder.PublicNewOrderFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class MarketsAdapter extends RecyclerView.Adapter<MarketsAdapter.ViewHolder> {
    public List<ShopsItem> shopsItems;
    private Context context;

    public MarketsAdapter() {
        this.shopsItems = new ArrayList<>();
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_market,
                parent, false);
        this.context = parent.getContext();
        return new ViewHolder(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final ViewHolder holder, final int position) {
        ShopsItem shopsItem = shopsItems.get(position);
        ItemMarketsViewModel itemMenuViewModel = new ItemMarketsViewModel(shopsItem);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            if (shopsItem.getPlace_id() == null)
                MovementHelper.startActivityWithBundle(context, new PassingObject(shopsItem), shopsItem.getName(), MarketDetailsFragment.class.getName(), Constants.SHARE_BAR, Constants.MARKETS);
            else
                MovementHelper.startActivityWithBundle(context, new PassingObject(shopsItem), ResourceManager.getString(R.string.new_order_bar), PublicNewOrderFragment.class.getName(), null, Constants.MARKETS);
        });

        holder.binding.offerItemDesc.setSelected(true);
        holder.setViewModel(itemMenuViewModel);
    }


    public void update(@NotNull List<ShopsItem> dataList) {
        this.shopsItems.clear();
        shopsItems.addAll(dataList);
        notifyDataSetChanged();
    }

    public void loadMore(@NotNull List<ShopsItem> dataList) {
        int start = shopsItems.size();
        Log.e("loadMore", "loadMore: "+start );
        shopsItems.addAll(dataList);
        notifyItemRangeInserted(start, dataList.size());
        Log.e("loadMore", "loadMore: "+shopsItems.size() );
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MarketsAdapter.ViewHolder holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MarketsAdapter.ViewHolder holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return shopsItems.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ItemMarketBinding binding;

        ViewHolder(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (binding == null) {
                binding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (binding != null) {
                binding.unbind();
            }
        }

        void setViewModel(ItemMarketsViewModel itemViewModels) {
            if (binding != null) {
                binding.setItemViewModel(itemViewModels);
            }
        }
    }
}
