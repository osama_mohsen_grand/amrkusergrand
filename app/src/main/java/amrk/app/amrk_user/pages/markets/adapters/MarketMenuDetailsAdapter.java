package amrk.app.amrk_user.pages.markets.adapters;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.ItemMenuDetailsBinding;
import amrk.app.amrk_user.databinding.RemoveWarningDialogBinding;
import amrk.app.amrk_user.pages.markets.FragmentMenuItemSheetDialog;
import amrk.app.amrk_user.pages.markets.MarketDetailsFragment;
import amrk.app.amrk_user.pages.markets.models.marketDetails.ProductsItem;
import amrk.app.amrk_user.pages.markets.viewModels.ItemMarketMenuDetailsViewModel;
import amrk.app.amrk_user.repository.CartRepository;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;

public class MarketMenuDetailsAdapter extends RecyclerView.Adapter<MarketMenuDetailsAdapter.MenuView> {
    List<ProductsItem> menuModels;
    private Context context;
    MutableLiveData<Integer> detailLiveData = new MediatorLiveData<>();

    public MarketMenuDetailsAdapter() {
        this.menuModels = new ArrayList<>();
    }

    public MutableLiveData<Integer> getDetailLiveData() {
        return detailLiveData;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_details,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        ProductsItem menuModel = menuModels.get(position);
        ItemMarketMenuDetailsViewModel itemMenuViewModel = new ItemMarketMenuDetailsViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> {
            if (o.equals(Constants.PRODUCT_DETAILS)) {
                FragmentMenuItemSheetDialog fragmentMenuItemSheetDialog = new FragmentMenuItemSheetDialog();
                fragmentMenuItemSheetDialog.setListner((quantity, productRoomId) -> {
                    menuModel.setQuantity(menuModel.getQuantity() + quantity);
                    menuModel.setProductRoomId(productRoomId);
                    detailLiveData.setValue(quantity);
                    MarketMenuDetailsAdapter.this.notifyItemChanged(position);
                });
                MovementHelper.startDialogWithBundle(context, new PassingObject(menuModel), fragmentMenuItemSheetDialog, new MarketDetailsFragment());
            } else if (o.equals(Constants.REMOVE_FROM_CART)) {
                deleteWarning(menuModel, position);
            }
        });

        holder.setViewModel(itemMenuViewModel);
    }

    private void deleteWarning(ProductsItem productsItem, int position) {
        Dialog dialog = new Dialog(context, R.style.PauseDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(dialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        RemoveWarningDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(dialog.getContext()), R.layout.remove_warning_dialog, null, false);
        dialog.setContentView(binding.getRoot());
        binding.yes.setOnClickListener(v -> {
            new CartRepository(MyApplication.getInstance()).deleteOneProductFromCart(productsItem.getId());
            detailLiveData.setValue(-productsItem.getQuantity());
            productsItem.setQuantity(0);
            notifyItemChanged(position);
            dialog.dismiss();
        });
        binding.no.setOnClickListener(v -> dialog.dismiss());
        dialog.show();
    }

    public void update(List<ProductsItem> dataList) {
        this.menuModels.clear();
        menuModels.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return menuModels.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemMenuDetailsBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemMarketMenuDetailsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
