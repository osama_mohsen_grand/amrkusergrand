package amrk.app.amrk_user.pages.chatAdmin.model;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.pages.myOrders.models.OrderImagesItem;


public class ChatAdmin {
    @SerializedName("message")
    private String message;
    @SerializedName("id")
    private int id;
    @SerializedName("user_id")
    private int user_id;
    @SerializedName("sender_type")
    private int sender_type;
    @SerializedName("created_at")
    private String created_at;
    @SerializedName("user_admin_messages_image")
    private OrderImagesItem user_admin_messages_image;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    public int getSender_type() {
        return sender_type;
    }

    public void setSender_type(int sender_type) {
        this.sender_type = sender_type;
    }

    public String getCreated_at() {
        return created_at;
    }

    public void setCreated_at(String created_at) {
        this.created_at = created_at;
    }

    public OrderImagesItem getUser_admin_messages_image() {
        return user_admin_messages_image;
    }

    public void setUser_admin_messages_image(OrderImagesItem user_admin_messages_image) {
        this.user_admin_messages_image = user_admin_messages_image;
    }
}