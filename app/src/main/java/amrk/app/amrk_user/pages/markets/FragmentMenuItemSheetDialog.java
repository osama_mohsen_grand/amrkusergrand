package amrk.app.amrk_user.pages.markets;


import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.google.gson.Gson;

import org.jetbrains.annotations.NotNull;

import javax.inject.Inject;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.FragmentMenuDialogItemBinding;
import amrk.app.amrk_user.pages.markets.models.marketDetails.ProductsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDialogResponse;
import amrk.app.amrk_user.pages.markets.viewModels.MarketMenuItemDialogViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.session.UserHelper;

public class FragmentMenuItemSheetDialog extends BottomSheetDialogFragment {
    FragmentMenuDialogItemBinding menuDialogItemBinding;
    @Inject
    MarketMenuItemDialogViewModel menuItemDialogViewModel;
    private Context context;
    private FragmentMenuItemSheetDialog.ItemClickListener mListener;

    public FragmentMenuItemSheetDialog() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(@NotNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        menuDialogItemBinding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu_dialog_item, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        menuDialogItemBinding.setViewmodel(menuItemDialogViewModel);
        Bundle bundle = this.getArguments();
        if (bundle != null) {
            String passingObject = bundle.getString(Constants.BUNDLE);
            menuItemDialogViewModel.setPassingObject(new Gson().fromJson(passingObject, PassingObject.class));
            menuItemDialogViewModel.setProductsItem(new Gson().fromJson(String.valueOf(menuItemDialogViewModel.getPassingObject().getObjectClass()), ProductsItem.class));
            menuItemDialogViewModel.productDetails();
        }
        setEvent();
        return menuDialogItemBinding.getRoot();

    }

    private void setEvent() {
        menuItemDialogViewModel.liveData.observe(((LifecycleOwner) context), o -> {
            switch (o.message) {
                case Constants.PRODUCT_DETAILS:
                    menuItemDialogViewModel.setDetails(((ProductDialogResponse) o.object).getProductDetails());
                    break;
                case Constants.HIDE_PROGRESS:
                    menuDialogItemBinding.varProgress.setVisibility(View.GONE);
                    break;
                case Constants.DISMISS:
                    dismiss();
                    break;
                case Constants.ADD_TO_CART:
                    mListener.onItemClick(menuItemDialogViewModel.getDetails().getQuantity(), menuItemDialogViewModel.getDetails().getProduct_room_id());
                    dismiss();
                    break;
            }
        });
        menuItemDialogViewModel.getProductAdapter().getSelectedVariations().observe(((LifecycleOwner) context), object -> {
//            Log.e("setEvent", "setEvent: OUT" + object.getObject());
//            if (object.getObject().equals(Constants.PRODUCT_NO_SELECTION)) {
//                Log.e("setEvent", "setEvent: " + object);
//                menuItemDialogViewModel.setTotal(Double.parseDouble(menuItemDialogViewModel.getDetails().getPriceAfter()));
//            } else
                menuItemDialogViewModel.checkCalc(object);
        });
        menuItemDialogViewModel.getVariationsAdapter().getProductPriceLiveData().observe(((LifecycleOwner) context), integer -> {
            float y = menuDialogItemBinding.marketsDetailsMenu.getY() + menuDialogItemBinding.marketsDetailsMenu.getChildAt(integer).getY();
            menuDialogItemBinding.pageScroll.smoothScrollTo(0, (int) y);
        });
    }

    @Override
    public void onResume() {
        super.onResume();
        menuItemDialogViewModel.getRepository().setLiveData(menuItemDialogViewModel.liveData);
    }

    public void setListner(ItemClickListener mListener) {
        this.mListener = mListener;
    }

    public interface ItemClickListener {
        void onItemClick(int quantity, int productRoomId);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;

    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        UserHelper.getInstance(context).resetTemp();
    }
}
