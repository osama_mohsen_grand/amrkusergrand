package amrk.app.amrk_user.pages.offers.models.offerDetails;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.model.base.StatusMessage;

public class OfferDetailsResponse extends StatusMessage {
    @SerializedName("data")
    private List<OfferDetails> data;

    @SerializedName("offer")
    OfferDetails offerDetailsPusher;

    public OfferDetails getOfferDetailsPusher() {
        return offerDetailsPusher;
    }

    public List<OfferDetails> getData() {
        return data;
    }

}