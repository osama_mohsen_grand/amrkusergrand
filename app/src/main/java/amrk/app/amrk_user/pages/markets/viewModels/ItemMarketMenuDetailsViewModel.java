package amrk.app.amrk_user.pages.markets.viewModels;
import androidx.databinding.Bindable;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.markets.models.marketDetails.ProductsItem;
import amrk.app.amrk_user.utils.Constants;

public class ItemMarketMenuDetailsViewModel extends BaseViewModel {
    public ProductsItem productsItem;

    public ItemMarketMenuDetailsViewModel(ProductsItem productsItem) {
        this.productsItem = productsItem;
    }

    @Bindable
    public ProductsItem getProductsItem() {
        return productsItem;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.PRODUCT_DETAILS);
    }

    public void deleteProductItem() {
        getLiveData().setValue(Constants.REMOVE_FROM_CART);
    }

}

