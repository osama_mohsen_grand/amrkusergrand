package amrk.app.amrk_user.pages.markets.adapters.productDetails;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemMenuDialogItemsBinding;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.pages.markets.viewModels.products.ItemProductVariationsViewModel;
import amrk.app.amrk_user.utils.helper.MovementHelper;
public class ProductAdapter extends RecyclerView.Adapter<ProductAdapter.MenuView> {
    List<VariationsItem> variationsItemList;
    MutableLiveData<List<VariationsItem>> selectedVariations = new MutableLiveData<>();
    Context context;
    List<VariationsItem> varList = new ArrayList<>();

    public ProductAdapter() {
        this.variationsItemList = new ArrayList<>();
    }

    public MutableLiveData<List<VariationsItem>> getSelectedVariations() {
        return selectedVariations;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_menu_dialog_items,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        VariationsItem menuModel = variationsItemList.get(position);
        ItemProductVariationsViewModel itemMenuViewModel = new ItemProductVariationsViewModel(menuModel);
        itemMenuViewModel.getProductDetailsAdapter().getProductPriceLiveData().observe(((LifecycleOwner) MovementHelper.unwrap(context)), object -> {
//            if (menuModel.getRequired() == 1 && menuModel.getOptions().size() == 1)

//            VariationsItem variationsItem = new VariationsItem();
//            variationsItem.setVariation_id(menuModel.getVariation_id());
//            variationsItem.setOptions(object);
            int index = isFound(object.getVariation_id());
            if (index != -1) {
                if (object.getRequired() == 1) {
                    varList.remove(index);
                    varList.add(object);
                } else {
                    if (object.getOptions().size() != 0) {
                        varList.remove(index);
                        varList.add(object);
                    } else {
                        varList.remove(index);
                    }
                }

            } else {
                varList.add(object);
            }
            selectedVariations.setValue(varList);
        });
        holder.setViewModel(itemMenuViewModel);
    }

    private int isFound(int varId) {
        for (int i = 0; i < varList.size(); i++) {
            if (varList.get(i).getVariation_id() == varId)
                return i;
        }
        return -1;
    }

    public void update(List<VariationsItem> dataList) {
        this.variationsItemList.clear();
        variationsItemList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return variationsItemList.size();
    }

    public class MenuView extends RecyclerView.ViewHolder {
        public ItemMenuDialogItemsBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemProductVariationsViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemViewModel(itemViewModels);
            }
        }
    }
}
