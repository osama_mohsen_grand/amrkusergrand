package amrk.app.amrk_user.pages.markets.models.productDialogModel;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "options", foreignKeys = @ForeignKey(entity = VariationsItem.class,
        parentColumns = "variation_room_id",
        childColumns = "variationIdForeignKey",
        onDelete = ForeignKey.CASCADE))
public class OptionsItem {
    @SerializedName("variation_id")
    @Ignore
    private int variationId;
    private int variationIdForeignKey;
    @SerializedName("price")
    @Ignore
    private String price;

    @SerializedName("name")
    @Ignore
    private String name;

    @SerializedName("id")
    @Ignore
    private int option_id;
    private String options;
    @PrimaryKey(autoGenerate = true)
    private int option_room_id;

    @SerializedName("type")
    @Ignore
    private int type;

    @SerializedName("status")
    @Ignore
    private Object status;
    @Ignore
    private boolean checked;
    @Ignore
    private int productId;

    public OptionsItem(int variationIdForeignKey, String options) {
        this.variationIdForeignKey = variationIdForeignKey;
        this.options = options;
    }

    public OptionsItem() {
    }

    public String getOptions() {
        return options;
    }

    public void setOptions(String options) {
        this.options = options;
    }

    public int getVariationId() {
        return variationId;
    }

    public String getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public int getOption_id() {
        return option_id;
    }

    public int getOption_room_id() {
        return option_room_id;
    }

    public void setOption_room_id(int option_room_id) {
        this.option_room_id = option_room_id;
    }

    public Object getStatus() {
        return status;
    }

    public int getType() {
        return type;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void setVariationId(int variationId) {
        this.variationId = variationId;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOption_id(int option_id) {
        this.option_id = option_id;
    }

    public void setType(int type) {
        this.type = type;
    }

    public void setStatus(Object status) {
        this.status = status;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getVariationIdForeignKey() {
        return variationIdForeignKey;
    }

    public void setVariationIdForeignKey(int variationIdForeignKey) {
        this.variationIdForeignKey = variationIdForeignKey;
    }

    @Override
    public String toString() {
        return "OptionsItem{" +
                "variationId=" + variationId +
                ", variationIdForeignKey=" + variationIdForeignKey +
                ", price='" + price + '\'' +
                ", name='" + name + '\'' +
                ", option_id=" + option_id +
                ", options='" + options + '\'' +
                ", option_room_id=" + option_room_id +
                ", type=" + type +
                ", status=" + status +
                ", checked=" + checked +
                ", productId=" + productId +
                '}';
    }
}