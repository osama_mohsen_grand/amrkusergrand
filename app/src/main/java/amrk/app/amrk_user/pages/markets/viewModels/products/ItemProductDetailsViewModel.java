package amrk.app.amrk_user.pages.markets.viewModels.products;


import android.util.Log;

import androidx.databinding.Bindable;
import androidx.lifecycle.MediatorLiveData;
import androidx.lifecycle.MutableLiveData;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.utils.Constants;

public class ItemProductDetailsViewModel extends BaseViewModel {
    public OptionsItem optionsItem;
    MutableLiveData<PassingObject> optionLiveData = new MediatorLiveData<>();

    public ItemProductDetailsViewModel(OptionsItem optionsItem) {
        this.optionsItem = optionsItem;
    }

    public MutableLiveData<PassingObject> getOptionLiveData() {
        return optionLiveData;
    }

    @Bindable
    public OptionsItem getOptionsItem() {
        return optionsItem;
    }


    public void itemAction() {
        if (getOptionsItem().getType() == 0) {
            Log.e("itemAction", "itemAction: " + getOptionsItem().isChecked());
            optionLiveData.setValue(new PassingObject(Constants.PRODUCT_SINGLE_SELECTION, getOptionsItem()));
        } else {
            if (getOptionsItem().isChecked()) {
                getOptionsItem().setChecked(false);
                optionLiveData.setValue(new PassingObject(Constants.REMOVE_FROM_CART, getOptionsItem()));
            } else {
                getOptionsItem().setChecked(true);
                optionLiveData.setValue(new PassingObject(Constants.ADD_TO_CART, getOptionsItem()));
            }
        }
    }

}
