package amrk.app.amrk_user.pages.markets.models.productDialogModel;

import android.os.Parcel;
import android.os.Parcelable;
import android.text.TextUtils;

import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.util.List;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "product")
public class ProductDetails implements Parcelable {

    @SerializedName("image")
    private String image;
    @SerializedName("product_image")
    @Ignore
    private String productImage;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("price_after")
    private String priceAfter;

    @SerializedName("description")
    private String description;

    @SerializedName("percent")
    @Ignore
    private String percent;

    @SerializedName("price_before")
    @Ignore
    private String priceBefore;

    @SerializedName("rate")
    @Ignore
    private String rate;

    @SerializedName("variations")
    @Ignore
    private List<VariationsItem> variations;

    @SerializedName("name")
    private String name;
    @SerializedName("product_name")
    @Ignore
    private String productName;

    @SerializedName("has_discount")
    @Ignore
    private String hasDiscount;

    @SerializedName("selling")
    @Ignore
    private String selling;

    @PrimaryKey(autoGenerate = true)
    private int product_room_id;
    @SerializedName("id")
    private int product_id;
    @SerializedName("product_id")
    @Ignore
    private String order_product_id;

    @SerializedName("status")
    @Ignore
    private String status;
    private double priceItem;

    public ProductDetails() {
    }

    protected ProductDetails(Parcel in) {
        image = in.readString();
        quantity = in.readInt();
        priceAfter = in.readString();
        description = in.readString();
        percent = in.readString();
        priceBefore = in.readString();
        rate = in.readString();
        name = in.readString();
        hasDiscount = in.readString();
        selling = in.readString();
        product_room_id = in.readInt();
        product_id = in.readInt();
        status = in.readString();
        priceItem = in.readDouble();
    }

    public static final Creator<ProductDetails> CREATOR = new Creator<ProductDetails>() {
        @Override
        public ProductDetails createFromParcel(Parcel in) {
            return new ProductDetails(in);
        }

        @Override
        public ProductDetails[] newArray(int size) {
            return new ProductDetails[size];
        }
    };

    public double getPriceItem() {
        return priceItem;
    }

    public void setPriceItem(double priceItem) {
        this.priceItem = priceItem;
    }

    public String getImage() {
        if (!TextUtils.isEmpty(productImage))
            return productImage;
        else
            return image;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getPriceAfter() {
        return priceAfter;
    }

    public String getDescription() {
        return description;
    }

    public String getPercent() {
        return percent;
    }

    public String getPriceBefore() {
        return priceBefore;
    }

    public String getRate() {
        return rate;
    }

    public List<VariationsItem> getVariations() {
        return variations;
    }

    public String getName() {
        if (!TextUtils.isEmpty(productName))
            return productName;
        else
            return name;
    }

    public String getHasDiscount() {
        return hasDiscount;
    }

    public String getSelling() {
        return selling;
    }

    public int getProduct_room_id() {
        return product_room_id;
    }

    public void setProduct_room_id(int product_room_id) {
        this.product_room_id = product_room_id;
    }

    public int getProduct_id() {
        if (!TextUtils.isEmpty(getOrder_product_id()))
            return Integer.parseInt(getOrder_product_id());
        else
            return product_id;
    }

    public String getStatus() {
        return status;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getOrder_product_id() {
        return order_product_id;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPriceAfter(String priceAfter) {
        this.priceAfter = priceAfter;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setPercent(String percent) {
        this.percent = percent;
    }

    public void setPriceBefore(String priceBefore) {
        this.priceBefore = priceBefore;
    }

    public void setRate(String rate) {
        this.rate = rate;
    }

    public void setVariations(List<VariationsItem> variations) {
        this.variations = variations;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setHasDiscount(String hasDiscount) {
        this.hasDiscount = hasDiscount;
    }

    public void setSelling(String selling) {
        this.selling = selling;
    }

    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String getProductImage() {
        return productImage;
    }

    public void setProductImage(String productImage) {
        this.productImage = productImage;
    }

    public String getProductName() {
        if (!TextUtils.isEmpty(productName))
            setImage(productName);
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeInt(quantity);
        dest.writeString(priceAfter);
        dest.writeString(description);
        dest.writeString(percent);
        dest.writeString(priceBefore);
        dest.writeString(rate);
        dest.writeString(name);
        dest.writeString(hasDiscount);
        dest.writeString(selling);
        dest.writeInt(product_room_id);
        dest.writeInt(product_id);
        dest.writeString(status);
        dest.writeDouble(priceItem);
        dest.writeString(productImage);
        dest.writeString(productName);
    }

    @Override
    public String toString() {
        return "ProductDetails{" +
                "image='" + image + '\'' +
                ", productImage='" + productImage + '\'' +
                ", quantity=" + quantity +
                ", priceAfter='" + priceAfter + '\'' +
                ", description='" + description + '\'' +
                ", percent='" + percent + '\'' +
                ", priceBefore='" + priceBefore + '\'' +
                ", rate='" + rate + '\'' +
                ", variations=" + variations +
                ", name='" + name + '\'' +
                ", productName='" + productName + '\'' +
                ", hasDiscount='" + hasDiscount + '\'' +
                ", selling='" + selling + '\'' +
                ", product_room_id=" + product_room_id +
                ", product_id=" + product_id +
                ", order_product_id='" + order_product_id + '\'' +
                ", status='" + status + '\'' +
                ", priceItem=" + priceItem +
                '}';
    }
}