package amrk.app.amrk_user.pages.publicOrder.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.OrderImages;
import amrk.app.amrk_user.utils.Constants;

public class ItemPublicOrderImagesViewModel extends BaseViewModel {
    public OrderImages orderImages;
    boolean selected = false;

    public ItemPublicOrderImagesViewModel(OrderImages orderImages) {
        this.orderImages = orderImages;
    }

    @Bindable
    public OrderImages getOrderImages() {
        return orderImages;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.NEW_IMAGE);
    }

    public void removeImage() {
        getLiveData().setValue(Constants.REMOVE_IMAGE);
    }

    @Bindable
    public boolean isSelected() {
        return selected;
    }

    @Bindable
    public void setSelected(boolean selected) {
        notifyChange(BR.selected);
        this.selected = selected;
    }
}
