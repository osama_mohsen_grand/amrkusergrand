package amrk.app.amrk_user.pages.auth.models;

import android.text.TextUtils;

import androidx.databinding.ObservableField;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.validation.Validate;

public class RegisterRequest {
    @SerializedName("name")
    private String name;
    @SerializedName("phone")
    private String phone;
    @SerializedName("password")
    private String password;
    private String confirmPassword;
    @SerializedName("country_id")
    private int country_id;
    @SerializedName("token")
    private String token;
    @SerializedName("email")
    private String email;
    private String user_image;
    @SerializedName("gender")
    private String gender;
    @SerializedName("lang")
    private String lang;

    @Expose
    @SerializedName("city")
    private String cityName;
    public transient ObservableField<String> cityError = new ObservableField<>();
    public transient ObservableField<String> nameError = new ObservableField<>();
    public transient ObservableField<String> phoneError = new ObservableField<>();
    public transient ObservableField<String> passwordError = new ObservableField<>();
    public transient ObservableField<String> confirmError = new ObservableField<>();
    public transient ObservableField<String> emailError = new ObservableField<>();


    public boolean isValid() {
        boolean valid = true;
        if (!Validate.isValid(name, Constants.FIELD)) {
            nameError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(phone, Constants.FIELD)) {
            phoneError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
        }else if (!Validate.isValid(cityName, Constants.FIELD)) {
            cityError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(password, Constants.FIELD)) {
            passwordError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(confirmPassword, Constants.FIELD)) {
            confirmError.set(Validate.error);
            valid = false;
        }
        return valid;
    }

    public boolean isUpdateValid() {
        boolean valid = true;
        if (!Validate.isValid(name, Constants.FIELD)) {
            nameError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(phone, Constants.FIELD)) {
            phoneError.set(Validate.error);
            valid = false;
        } else if (!Validate.isValid(email, Constants.EMAIL)) {
            emailError.set(Validate.error);
            valid = false;
        }
        return valid;
    }

    public boolean isPasswordsValid() {
        return (!TextUtils.isEmpty(password) && !TextUtils.isEmpty(confirmPassword));
    }

    public String getConfirmPassword() {
        return confirmPassword;
    }

    public void setConfirmPassword(String confirmPassword) {
        confirmError.set(null);
        this.confirmPassword = confirmPassword;
    }

    public String getUser_image() {
        return user_image;
    }

    public void setUser_image(String user_image) {
        this.user_image = user_image;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        nameError.set(null);
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        phoneError.set(null);
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        passwordError.set(null);
        this.password = password;
    }

    public int getCountry_id() {
        return country_id;
    }

    public void setCountry_id(int country_id) {
        this.country_id = country_id;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        emailError.set(null);
        this.email = email;
    }

    public String getLang() {
        return lang;
    }

    public void setLang(String lang) {
        this.lang = lang;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }


    public void setCityError(ObservableField<String> cityError) {
        this.cityError = cityError;
    }

    public String getCityName() {
        return cityName;
    }

    public void setCityName(String cityName) {
        cityError.set(null);
        this.cityName = cityName;
    }

    public ObservableField<String> getCityError() {
        return cityError;
    }

    @Override
    public String toString() {
        return "RegisterRequest{" +
                "name='" + name + '\'' +
                ", phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", confirmPassword='" + confirmPassword + '\'' +
                ", country_id=" + country_id +
                ", token='" + token + '\'' +
                ", email='" + email + '\'' +
                ", user_image='" + user_image + '\'' +
                ", gender='" + gender + '\'' +
                '}';
    }
}
