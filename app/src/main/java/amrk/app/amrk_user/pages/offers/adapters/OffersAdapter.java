package amrk.app.amrk_user.pages.offers.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.databinding.ItemOffersBinding;
import amrk.app.amrk_user.pages.offers.OffersDetailsFragment;
import amrk.app.amrk_user.pages.offers.models.OffersData;
import amrk.app.amrk_user.pages.offers.viewModels.ItemOfferViewModel;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;


public class OffersAdapter extends RecyclerView.Adapter<OffersAdapter.MenuView> {
     List<OffersData> offersDataList;
     MutableLiveData<Integer> liveDataAdapter = new MutableLiveData<>();
     Context context;

    public OffersAdapter() {
        this.offersDataList = new ArrayList<>();
    }

    public MutableLiveData<Integer> getLiveDataAdapter() {
        return liveDataAdapter;
    }

    public List<OffersData> getOffersDataList() {
        return offersDataList;
    }

    @NonNull
    @Override
    public MenuView onCreateViewHolder(@NonNull ViewGroup parent, int i) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_offers,
                parent, false);
        this.context = parent.getContext();
        return new MenuView(itemView);
    }


    @Override
    public void onBindViewHolder(@NonNull final MenuView holder, final int position) {
        OffersData menuModel = offersDataList.get(position);
        ItemOfferViewModel itemMenuViewModel = new ItemOfferViewModel(menuModel);
        itemMenuViewModel.getLiveData().observe(((LifecycleOwner) context), o -> MovementHelper.startActivityForResultWithBundle(context, new PassingObject(menuModel.getId()), ResourceManager.getString(R.string.offers_title), OffersDetailsFragment.class.getName(), null, Constants.MARKETS));
        holder.setViewModel(itemMenuViewModel);
    }

    public void update(List<OffersData> dataList) {
        this.offersDataList.clear();
        offersDataList.addAll(dataList);
        notifyDataSetChanged();
    }

    @Override
    public void onViewAttachedToWindow(@NotNull MenuView holder) {
        super.onViewAttachedToWindow(holder);
        holder.bind();
    }

    @Override
    public void onViewDetachedFromWindow(@NotNull MenuView holder) {
        super.onViewDetachedFromWindow(holder);
        holder.unbind();
    }

    @Override
    public int getItemCount() {
        return offersDataList.size();
    }



    public class MenuView extends RecyclerView.ViewHolder {
        public ItemOffersBinding itemMenuBinding;

        MenuView(View itemView) {
            super(itemView);
            bind();
        }


        void bind() {
            if (itemMenuBinding == null) {
                itemMenuBinding = DataBindingUtil.bind(itemView);
            }
        }

        void unbind() {
            if (itemMenuBinding != null) {
                itemMenuBinding.unbind();
            }
        }

        void setViewModel(ItemOfferViewModel itemViewModels) {
            if (itemMenuBinding != null) {
                itemMenuBinding.setItemOfferViewModel(itemViewModels);
            }
        }
    }
}
