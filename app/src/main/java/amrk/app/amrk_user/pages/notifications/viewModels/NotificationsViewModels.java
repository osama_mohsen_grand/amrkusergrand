package amrk.app.amrk_user.pages.notifications.viewModels;


import android.content.Context;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.notifications.adapters.NotificationsAdapter;
import amrk.app.amrk_user.repository.SettingsRepository;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import io.reactivex.disposables.CompositeDisposable;

public class NotificationsViewModels extends BaseViewModel {
    private NotificationsAdapter notificationsAdapter;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    SettingsRepository settingsRepository;
    public MutableLiveData<Mutable> liveData;

    @Inject
    public NotificationsViewModels(SettingsRepository settingsRepository) {
        this.settingsRepository = settingsRepository;
        this.liveData = new MutableLiveData<>();
        settingsRepository.setLiveData(liveData);
    }

    public void notifications() {
//        if (getPassingObject() != null && getPassingObject().getId() != 0)
//            compositeDisposable.add(settingsRepository.getNotifications(1));
//        else
            compositeDisposable.add(settingsRepository.getNotifications(0));
    }

    public void notificationSettings(Context context) {
        MovementHelper.startNotificationSettings(context);
    }

    private void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }

    public SettingsRepository getSettingsRepository() {
        return settingsRepository;
    }

    @Bindable
    public NotificationsAdapter getNotificationsAdapter() {
        return this.notificationsAdapter == null ? this.notificationsAdapter = new NotificationsAdapter() : this.notificationsAdapter;
    }
}
