package amrk.app.amrk_user.pages.offers.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.pages.markets.models.ShopsItem;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.OrderImages;
import amrk.app.amrk_user.pages.publicOrder.models.subDepartment.SubDepartmentData;

public class OffersData {

    @SerializedName("shop_id")
    private int shopId;
    @SerializedName("department_id")
    private int departmentId;

    @SerializedName("notes")
    private String notes;

    @SerializedName("shop")
    private ShopsItem shop;

    @SerializedName("order_number")
    private String orderNumber;

    @SerializedName("id")
    private int id;

    @SerializedName("delivery_time")
    private String deliveryTime;

    @SerializedName("title")
    private String title;
    @SerializedName("created_at")
    private String created_at;

    @SerializedName("has_offers")
    private int hasOffers;
    @SerializedName("order_image")
    private OrderImages orderImages;

    @SerializedName("sub_department")
    private SubDepartmentData subDepartmentData;

    public int getShopId() {
        return shopId;
    }

    public String getNotes() {
        return notes;
    }

    public ShopsItem getShop() {
        return shop;
    }

    public String getOrderNumber() {
        return orderNumber;
    }

    public int getId() {
        return id;
    }

    public String getDeliveryTime() {
        return deliveryTime;
    }

    public String getTitle() {
        return title;
    }

    public int getHasOffers() {
        return hasOffers;
    }

    public String getCreated_at() {
        return created_at;
    }

    public OrderImages getOrderImages() {
        return orderImages;
    }

    public int getDepartmentId() {
        return departmentId;
    }

    public SubDepartmentData getSubDepartmentData() {
        return subDepartmentData;
    }
}