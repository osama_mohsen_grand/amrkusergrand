package amrk.app.amrk_user.pages.myOrders.models.orders;

import com.google.gson.annotations.SerializedName;

public class OrderProductVariationOptionsItem{

	@SerializedName("price")
	private String price;

	@SerializedName("name")
	private String name;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("option_id")
	private int optionId;

	@SerializedName("id")
	private int id;

	@SerializedName("type")
	private int type;

	public String getPrice(){
		return price;
	}

	public String getName(){
		return name;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public int getOptionId(){
		return optionId;
	}

	public int getId(){
		return id;
	}

	public int getType(){
		return type;
	}
}