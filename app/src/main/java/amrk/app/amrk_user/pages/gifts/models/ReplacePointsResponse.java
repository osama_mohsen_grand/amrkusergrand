package amrk.app.amrk_user.pages.gifts.models;

import com.google.gson.annotations.SerializedName;

import amrk.app.amrk_user.model.base.StatusMessage;


public class ReplacePointsResponse extends StatusMessage {

    @SerializedName("data")
    private ReplacedObject data;

    public ReplacedObject getData() {
        return data;
    }
}