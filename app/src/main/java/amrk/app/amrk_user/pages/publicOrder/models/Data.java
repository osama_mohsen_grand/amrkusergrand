package amrk.app.amrk_user.pages.publicOrder.models;

import com.google.gson.annotations.SerializedName;

public class Data{

	@SerializedName("out_address")
	private String outAddress;

	@SerializedName("in_lat")
	private double inLat;

	@SerializedName("notes")
	private String notes;

	@SerializedName("out_lat")
	private double outLat;

	@SerializedName("total_cost")
	private String totalCost;

	@SerializedName("department_id")
	private int departmentId;

	@SerializedName("order_number")
	private String orderNumber;

	@SerializedName("created_at")
	private String createdAt;

	@SerializedName("delivery_time")
	private String deliveryTime;

	@SerializedName("title")
	private String title;

	@SerializedName("in_lng")
	private double inLng;

	@SerializedName("promo_id")
	private int promoId;

	@SerializedName("shop_id")
	private int shopId;

	@SerializedName("in_address")
	private String inAddress;

	@SerializedName("in_city_name")
	private String inCityName;

	@SerializedName("confirm_code")
	private String confirmCode;

	@SerializedName("currency")
	private String currency;

	@SerializedName("id")
	private int id;

	@SerializedName("country_id")
	private int countryId;

	@SerializedName("out_lng")
	private double outLng;

	@SerializedName("out_city_name")
	private String outCityName;

	public String getOutAddress(){
		return outAddress;
	}

	public double getInLat(){
		return inLat;
	}

	public String getNotes(){
		return notes;
	}

	public double getOutLat(){
		return outLat;
	}

	public String getTotalCost(){
		return totalCost;
	}

	public int getDepartmentId(){
		return departmentId;
	}

	public String getOrderNumber(){
		return orderNumber;
	}

	public String getCreatedAt(){
		return createdAt;
	}

	public String getDeliveryTime(){
		return deliveryTime;
	}

	public String getTitle(){
		return title;
	}

	public double getInLng(){
		return inLng;
	}

	public int getPromoId(){
		return promoId;
	}

	public int getShopId(){
		return shopId;
	}

	public String getInAddress(){
		return inAddress;
	}

	public String getInCityName(){
		return inCityName;
	}

	public String getConfirmCode(){
		return confirmCode;
	}

	public String getCurrency(){
		return currency;
	}

	public int getId(){
		return id;
	}

	public int getCountryId(){
		return countryId;
	}

	public double getOutLng(){
		return outLng;
	}

	public String getOutCityName(){
		return outCityName;
	}
}