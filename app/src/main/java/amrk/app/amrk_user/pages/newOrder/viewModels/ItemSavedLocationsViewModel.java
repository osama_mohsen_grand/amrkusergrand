package amrk.app.amrk_user.pages.newOrder.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.pages.newOrder.models.SavedLocationsData;
import amrk.app.amrk_user.utils.Constants;

public class ItemSavedLocationsViewModel extends BaseViewModel {
    public SavedLocationsData savedLocations;

    public ItemSavedLocationsViewModel(SavedLocationsData savedLocations) {
        this.savedLocations = savedLocations;
    }

    @Bindable
    public SavedLocationsData getSavedLocations() {
        return savedLocations;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.SAVED_LOCATIONS);
    }

}
