package amrk.app.amrk_user.pages.home.viewModels;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.customViews.menu.MenuModel;
import amrk.app.amrk_user.utils.Constants;

public class ItemHomeSliderViewModel extends BaseViewModel {
    public MenuModel menuModel;

    public ItemHomeSliderViewModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }

    @Bindable
    public MenuModel getMenuModel() {
        return menuModel;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
