package amrk.app.amrk_user.pages.heavyOrders;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.databinding.DataBindingUtil;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import org.jetbrains.annotations.NotNull;

import java.util.Objects;

import javax.inject.Inject;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseFragment;
import amrk.app.amrk_user.base.IApplicationComponent;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.databinding.CancelOrderWarningDialogBinding;
import amrk.app.amrk_user.databinding.FragmentCurrentHeavyOrdersBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.heavyOrders.viewModels.MyHeavyOrdersViewModel;
import amrk.app.amrk_user.pages.myOrders.models.MyOrdersResponse;
import amrk.app.amrk_user.pages.reviews.models.RateRequest;
import amrk.app.amrk_user.utils.Constants;

public class CurrentHeavyOrdersFragment extends BaseFragment {

    private Context context;
    FragmentCurrentHeavyOrdersBinding binding;
    @Inject
    MyHeavyOrdersViewModel viewModel;
    Dialog dialog, cancelDialog;

    @Nullable
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_current_heavy_orders, container, false);
        IApplicationComponent component = ((MyApplication) context.getApplicationContext()).getApplicationComponent();
        component.inject(this);
        binding.setViewModel(viewModel);
        viewModel.myOrders(5, 0);
        setEvent();
        return binding.getRoot();
    }

    private void setEvent() {
        viewModel.liveData.observe(((LifecycleOwner) context), (Observer<Object>) o -> {
            Mutable mutable = (Mutable) o;
            handleActions(mutable);
            if (Constants.MY_ORDERS.equals(mutable.message)) {
                viewModel.getHeavyOrdersAdapter().update(((MyOrdersResponse) mutable.object).getData());
                viewModel.notifyChange(BR.heavyOrdersAdapter);
            } else if (Constants.RATE_MANDOUB.equals(mutable.message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                dialog.dismiss();
                viewModel.setRateRequest(new RateRequest());
            } else if (Constants.CHANGE_DELEGATE.equals(mutable.message)) {
                toastMessage(((StatusMessage) mutable.object).mMessage);
                if (cancelDialog != null)
                    cancelDialog.dismiss();
                viewModel.getHeavyOrdersAdapter().getMyOrdersDataList().remove(viewModel.getHeavyOrdersAdapter().position);
                viewModel.getHeavyOrdersAdapter().notifyItemRemoved(viewModel.getHeavyOrdersAdapter().position);
                viewModel.notifyChange(BR.heavyOrdersAdapter);
            }
        });
        viewModel.getHeavyOrdersAdapter().getRateMandoubLiveData().observe((LifecycleOwner) context, o -> {
            if (o.equals(Constants.CANCEL_ORDER)) {
                showCancelDialog(viewModel.getHeavyOrdersAdapter().position);
            } else if (o.equals(Constants.REMOVE_DIALOG_WARNING)) {
                showAlertDialogWithAutoDismiss(viewModel.getHeavyOrdersAdapter().position);
            }
        });
    }

    private void showCancelDialog(int position) {
        cancelDialog = new Dialog(context, R.style.PauseDialog);
        cancelDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        Objects.requireNonNull(cancelDialog.getWindow()).getAttributes().windowAnimations = R.style.PauseDialogAnimation;
        cancelDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        CancelOrderWarningDialogBinding binding = DataBindingUtil.inflate(LayoutInflater.from(cancelDialog.getContext()), R.layout.cancel_order_warning_dialog, null, false);
        cancelDialog.setContentView(binding.getRoot());
        binding.msg.setText(viewModel.getHeavyOrdersAdapter().getMyOrdersDataList().get(position).getCancel_warning());
        binding.yes.setOnClickListener(v -> viewModel.cancelOrder(viewModel.getHeavyOrdersAdapter().getMyOrdersDataList().get(position).getId()));
        binding.no.setOnClickListener(v -> cancelDialog.dismiss());
        cancelDialog.show();
    }

    public void showAlertDialogWithAutoDismiss(int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder
                .setMessage(getString(R.string.cancel_order_warning))
                .setCancelable(false)
                .setNegativeButton(getString(R.string.cancel), (dialog, which) -> dialog.cancel())
                .setPositiveButton(getString(R.string.send), (dialog, id) -> {
                    viewModel.cancelOrder(viewModel.getHeavyOrdersAdapter().getMyOrdersDataList().get(position).getId());
                    dialog.cancel();
                });
        final AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }


    @Override
    public void onResume() {
        super.onResume();
        if (Constants.DATA_CHANGED) {
            Constants.DATA_CHANGED = false;
            viewModel.myOrders(5, 0);
        }
        viewModel.getMarketRepository().setLiveData(viewModel.liveData);
    }

    @Override
    public void onAttach(@NotNull Context context) {
        super.onAttach(context);
        this.context = context;
    }
}
