package amrk.app.amrk_user.pages.myOrders.models;

import com.google.gson.annotations.SerializedName;

public class OrderOffer {
    @SerializedName("id")
    private int id;
    @SerializedName("offer")
    private String offer;

    public int getId() {
        return id;
    }

    public String getOffer() {
        return offer;
    }
}
