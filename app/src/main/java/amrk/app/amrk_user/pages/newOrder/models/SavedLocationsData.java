package amrk.app.amrk_user.pages.newOrder.models;

import com.google.gson.annotations.SerializedName;

public class SavedLocationsData {

    @SerializedName("address")
    private String address;

    @SerializedName("lng")
    private double lng;

    @SerializedName("lat")
    private double lat;
    @SerializedName("city_name")
    private String city_name;
    @SerializedName("id")
    private int id;
    @SerializedName("name")
    private String name;
    private boolean empty;
    private String deleteKey;

    public String getCity_name() {
        return city_name;
    }

    public String getAddress() {
        return address;
    }

    public double getLng() {
        return lng;
    }

    public double getLat() {
        return lat;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public boolean isEmpty() {
        return empty;
    }

    public void setEmpty(boolean empty) {
        this.empty = empty;
    }

    public String getDeleteKey() {
        return deleteKey;
    }

    public void setDeleteKey(String deleteKey) {
        this.deleteKey = deleteKey;
    }
}