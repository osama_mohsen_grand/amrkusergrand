package amrk.app.amrk_user.pages.markets.viewModels;

import android.text.TextUtils;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import androidx.databinding.library.baseAdapters.BR;

import com.google.android.gms.maps.model.LatLng;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.markets.adapters.MarketCategoriesAdapter;
import amrk.app.amrk_user.pages.markets.adapters.MarketsAdapter;
import amrk.app.amrk_user.pages.markets.models.ShopsPaginate;
import amrk.app.amrk_user.repository.MarketRepository;
import io.reactivex.disposables.CompositeDisposable;

public class MarketsViewModel extends BaseViewModel {
    public MutableLiveData<Mutable> liveData;
    CompositeDisposable compositeDisposable = new CompositeDisposable();
    @Inject
    MarketRepository repository;
    MarketsAdapter marketsAdapter;
    MarketCategoriesAdapter categoriesAdapter;
    ShopsPaginate shopsPaginate;
    public String search;
    public LatLng location;

    @Inject
    public MarketsViewModel(MarketRepository repository) {
        shopsPaginate = new ShopsPaginate();
        categoriesAdapter = new MarketCategoriesAdapter();
        this.repository = repository;
        this.liveData = new MutableLiveData<>();
        repository.setLiveData(liveData);

    }

    public void getMarkets(String url, boolean showProgress) {
        compositeDisposable.add(repository.getMarkets(url, showProgress));
    }

    public void getMarketsByCategoryId(String url, boolean showProgress) {
        compositeDisposable.add(repository.getMarketsByCategoryId(url, showProgress));
    }

    public void searchMarkets(int page) {
        compositeDisposable.add(repository.searchMarkets(search, location, page));
    }


    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (!TextUtils.isEmpty(s)) {
            setSearchProgressVisible(View.VISIBLE);
            search = s.toString();
            getMarketsAdapter().shopsItems.clear();
            searchMarkets(1);
        } else
            setSearchProgressVisible(View.GONE);
    }


    public MarketRepository getRepository() {
        return repository;
    }

    @Bindable
    public MarketsAdapter getMarketsAdapter() {
        return this.marketsAdapter == null ? this.marketsAdapter = new MarketsAdapter() : this.marketsAdapter;
    }


    @Bindable
    public ShopsPaginate getShopsPaginate() {
        return shopsPaginate;
    }

    @Bindable
    public void setShopsPaginate(ShopsPaginate shopsPaginate) {
        if (getMarketsAdapter().shopsItems.size() > 0) {
            getMarketsAdapter().loadMore(shopsPaginate.getShops());
        } else {
            getMarketsAdapter().update(shopsPaginate.getShops());
            notifyChange(BR.marketsAdapter);
        }
        setSearchProgressVisible(View.GONE);
        notifyChange(BR.shopsPaginate);
        this.shopsPaginate = shopsPaginate;
    }

    public void clear() {
        search = null;
        setSearchProgressVisible(View.GONE);
        notifyChange();
        getMarketsAdapter().shopsItems.clear();
        getMarketsAdapter().notifyDataSetChanged();
    }



    public MarketCategoriesAdapter getCategoriesAdapter() {
        return categoriesAdapter;
    }

    protected void unSubscribeFromObservable() {
        if (compositeDisposable != null && !compositeDisposable.isDisposed()) {
            compositeDisposable.dispose();
        }
    }

    @Override
    protected void onCleared() {
        super.onCleared();
        unSubscribeFromObservable();
    }
}
