package amrk.app.amrk_user.customViews.menu;

public class MenuModel {
    public int id;
    public String title;
    public int icon;

    public MenuModel(int id, String title, int icon) {
        this.id = id;
        this.title = title;
        this.icon = icon;
    }

    public MenuModel() {
    }
}
