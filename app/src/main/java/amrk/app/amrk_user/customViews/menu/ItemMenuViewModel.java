package amrk.app.amrk_user.customViews.menu;

import androidx.databinding.Bindable;

import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.utils.Constants;

public class ItemMenuViewModel extends BaseViewModel {
    public MenuModel menuModel;

    public ItemMenuViewModel(MenuModel menuModel) {
        this.menuModel = menuModel;
    }

    @Bindable
    public MenuModel getMenuModel() {
        return menuModel;
    }

    public void itemAction() {
        getLiveData().setValue(Constants.MENu);
    }

}
