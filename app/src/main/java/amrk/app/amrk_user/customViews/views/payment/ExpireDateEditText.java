package amrk.app.amrk_user.customViews.views.payment;

import android.content.Context;
import android.util.AttributeSet;

public class ExpireDateEditText extends androidx.appcompat.widget.AppCompatEditText {
    Context context;

    public ExpireDateEditText(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public ExpireDateEditText(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();
    }

    public ExpireDateEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.context = context;
        init();
    }

    private void init() {
        addTextChangedListener(DateMask.insert("##/##", this));

    }

}