package amrk.app.amrk_user.customViews.menu;


import android.annotation.SuppressLint;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.view.GravityCompat;
import androidx.databinding.DataBindingUtil;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.lifecycle.MutableLiveData;

import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.customViews.actionbar.HomeActionBarView;
import amrk.app.amrk_user.databinding.LayoutNavigationDrawerBinding;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.notifications.NotificationsFragment;
import amrk.app.amrk_user.pages.profile.ProfileFragment;
import amrk.app.amrk_user.pages.settings.TermsFragment;
import amrk.app.amrk_user.uber.favorites.FavoritesLocationsFragment;
import amrk.app.amrk_user.uber.history.HistoryFragment;
import amrk.app.amrk_user.uber.paymentMethods.PaymentMethodsFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.UserHelper;


@SuppressLint("ViewConstructor")
public class NavigationDrawerView extends RelativeLayout {
    public MutableLiveData<Mutable> liveData;
    public LayoutNavigationDrawerBinding layoutNavigationDrawerBinding;
    AppCompatActivity context;
    HomeActionBarView homeActionBarView;
    private MenuViewModel menuViewModel;

    public NavigationDrawerView(AppCompatActivity context) {
        super(context);
        this.context = context;
        liveData = new MutableLiveData<>();
        init();
    }

    public NavigationDrawerView(AppCompatActivity context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();

    }

    public NavigationDrawerView(AppCompatActivity context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutNavigationDrawerBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_navigation_drawer, this, true);
        menuViewModel = new MenuViewModel();
        layoutNavigationDrawerBinding.setMenuViewModel(menuViewModel);
        setEvents();
    }


    public void setActionBar(HomeActionBarView homeActionBarView) {
        this.homeActionBarView = homeActionBarView;
    }

    private void setEvents() {
        menuViewModel.getMenuAdapter().getLiveDataAdapter().observeForever(position -> {
            layoutNavigationDrawerBinding.dlMainNavigationMenu.closeDrawer(GravityCompat.START);
            if (position == 2) {
                MovementHelper.startActivity(context, ProfileFragment.class.getName(), ResourceManager.getString(R.string.profile_title), null, Constants.MARKETS);
            } else if (position == 3) {
                MovementHelper.startActivity(context, HistoryFragment.class.getName(), ResourceManager.getString(R.string.lastRides), null, Constants.MARKETS);
            } else if (position == 4) {
                MovementHelper.startActivityWithBundle(context, new PassingObject(1), ResourceManager.getString(R.string.menuNotifications), NotificationsFragment.class.getName(), null, Constants.MARKETS);
            } else if (position == 5) {
                MovementHelper.startActivity(context, PaymentMethodsFragment.class.getName(), ResourceManager.getString(R.string.menuPayment), null, Constants.MARKETS);
            } else if (position == 6) {
                MovementHelper.startActivity(context, FavoritesLocationsFragment.class.getName(), ResourceManager.getString(R.string.fav_locations), null, Constants.MARKETS);
            } else if (position == 7) {
                MovementHelper.startActivityWithBundle(context, new PassingObject(Constants.TERMS), ResourceManager.getString(R.string.privacyUber), TermsFragment.class.getName(), null, null);
            }
        });
        layoutNavigationDrawerBinding.dlMainNavigationMenu.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(@NonNull View drawerView, float slideOffset) {

            }

            @Override
            public void onDrawerOpened(@NonNull View drawerView) {
                menuViewModel.userData = UserHelper.getInstance(context).getUserData();
                menuViewModel.notifyChange();
            }

            @Override
            public void onDrawerClosed(@NonNull View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });
    }

}
