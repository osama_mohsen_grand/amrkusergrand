package amrk.app.amrk_user.customViews.menu;


import java.util.ArrayList;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.BaseViewModel;
import amrk.app.amrk_user.utils.resources.ResourceManager;

public class MenuViewModel extends BaseViewModel {
    MenuAdapter menuAdapter = new MenuAdapter();

    public MenuViewModel() {
        ArrayList<MenuModel> list = new ArrayList<>();
        list.add(new MenuModel(1, ResourceManager.getString(R.string.menuHome), R.drawable.ic_uber_home));
        list.add(new MenuModel(2, ResourceManager.getString(R.string.uberAccount), R.drawable.uber_user));
        list.add(new MenuModel(3, ResourceManager.getString(R.string.lastRides), R.drawable.uber_history));
        list.add(new MenuModel(4, ResourceManager.getString(R.string.menuNotifications), R.drawable.uber_notifications));
        list.add(new MenuModel(5, ResourceManager.getString(R.string.menuPayment), R.drawable.ic_uber_wallet));
        list.add(new MenuModel(6, ResourceManager.getString(R.string.fav_locations), R.drawable.ic_fav_place));
        list.add(new MenuModel(7, ResourceManager.getString(R.string.privacyUber), R.drawable.ic_uber_terms));
        menuAdapter.update(list);
    }

    public MenuAdapter getMenuAdapter() {
        return menuAdapter;
    }
}
