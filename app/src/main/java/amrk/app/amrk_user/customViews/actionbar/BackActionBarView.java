package amrk.app.amrk_user.customViews.actionbar;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.widget.RelativeLayout;

import androidx.databinding.DataBindingUtil;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.databinding.LayoutActionBarBackBinding;
import amrk.app.amrk_user.pages.home.HomeMainFragment;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;


public class BackActionBarView extends RelativeLayout {
    public LayoutActionBarBackBinding layoutActionBarBackBinding;
    public int type = 0;
    public int flag = 0;
    Context context;

    public BackActionBarView(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public BackActionBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.context = context;
        init();

    }

    public BackActionBarView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs);
        this.context = context;
        init();
    }

    private void init() {
        LayoutInflater layoutInflater = LayoutInflater.from(getContext());
        layoutActionBarBackBinding = DataBindingUtil.inflate(layoutInflater, R.layout.layout_action_bar_back, this, true);
        setEvents();
    }

    private void setEvents() {
        layoutActionBarBackBinding.imgActionBarCancel.setOnClickListener(view -> {
            if (flag == 1) {
                if (((ParentActivity) context).isTaskRoot()) {
                    // This activity is at root of task, so launch main activity
                    MovementHelper.startActivityBase(context, HomeMainFragment.class.getName(), null, null);
                } else {
                    // This activity isn't at root of task, so just finish()
                    ((Activity) getContext()).finish();
                }
            } else
                ((Activity) getContext()).finish();
        });
        layoutActionBarBackBinding.imgActionBarShare.setOnClickListener(v -> AppHelper.shareCustom(((Activity) context), ResourceManager.getString(R.string.app_name), ResourceManager.getString(R.string.app_desc)));
    }

    public void setTitle(String title) {
        layoutActionBarBackBinding.tvActionBarTitle.setText(title);
    }

    public void setBackground(String color) {
        if (color != null && color.equals(Constants.UBER)) {
            layoutActionBarBackBinding.tvActionBarTitle.setTextColor(ResourceManager.getColor(R.color.colordark));
            layoutActionBarBackBinding.backContainer.setBackgroundResource(R.drawable.toolbar_dropshadow_uber);
        } else
            layoutActionBarBackBinding.backContainer.setBackgroundResource(R.drawable.toolbar_dropshadow);
    }

    public void showShareIcon(int visibility) {
        layoutActionBarBackBinding.imgActionBarShare.setVisibility(visibility);
    }
}
