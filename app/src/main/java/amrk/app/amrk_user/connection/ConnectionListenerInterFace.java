package amrk.app.amrk_user.connection;

public interface ConnectionListenerInterFace {
    void onRequestResponse(Object response);

}
