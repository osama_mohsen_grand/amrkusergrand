package amrk.app.amrk_user.repository;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import com.google.gson.Gson;

import java.util.ArrayList;
import java.util.List;

import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.utils.cart.CartDao;
import amrk.app.amrk_user.utils.cart.CartDataBase;

public class CartRepository {
    CartDao cartDao;
    LiveData<List<ProductDetails>> allProducts;
    LiveData<String> cartTotal;

    public CartRepository(Application application) {
        CartDataBase cartDataBase = CartDataBase.getInstance(application);
        cartDao = cartDataBase.cartDao();
        allProducts = cartDao.getProducts();
        cartTotal = cartDao.getCartTotal();
    }

    public void insert(ProductDetails product, List<OptionsItem> optionsItem) {
        new InsertProductAsyncTask(cartDao, optionsItem).execute(product);
    }

    public void insertReOrder(List<ProductDetails> product) {
        new InsertProductReOrderAsyncTask(cartDao).execute(product);
    }

    public void update(ProductDetails product) {
        new UpdateProductAsyncTask(cartDao).execute(product);

    }

    public void deleteItem(int productId) {
        new DeleteProductAsyncTask(cartDao).execute(productId);

    }

    public void deleteOneProductFromCart(int productId) {
        new DeleteOneProductAsyncTask(cartDao).execute(productId);

    }

    public void emptyCart() {
        new EmptyCartAsyncTask(cartDao).execute();
    }

    public LiveData<List<OptionsItem>> getOptions() {
        return cartDao.getOptions();
    }

    public LiveData<List<VariationsItem>> getVariations() {
        return cartDao.getVariations();
    }

    public LiveData<List<ProductDetails>> getAllProducts() {
        return allProducts;
    }

    public LiveData<String> getCartTotal() {
        return cartTotal;
    }

    private static final String TAG = "CartRepository";

    private static class InsertProductAsyncTask extends AsyncTask<ProductDetails, Void, Void> {
        CartDao cartDao;
        List<OptionsItem> optionsItemList;
        List<Integer> variationsItems;
        List<Integer> optionsItems;
        long var_room_id;

        public InsertProductAsyncTask(CartDao cartDao, List<OptionsItem> optionsItemList) {
            this.cartDao = cartDao;
            this.optionsItemList = optionsItemList;
        }

        @Override
        protected Void doInBackground(ProductDetails... products) {
            variationsItems = new ArrayList<>();
            optionsItems = new ArrayList<>();
            long result = cartDao.addProduct(products[0]);
            if (products[0].getVariations().size() > 0)
                cartDao.addVariations(new VariationsItem(products[0].getProduct_id(), (int) result, new Gson().toJson(products[0].getVariations())));
//            if (optionsItemList.size() > 0) {
//                for (int i = 0; i < optionsItemList.size(); i++) {
//                    variationsItems.add(optionsItemList.get(i).getVariationId());
//                    optionsItems.add(optionsItemList.get(i).getOption_id());
//                    if (i == optionsItemList.size() - 1) {
//                        int optionExists = cartDao.getIfOptionExists(optionsItems.toString(), variationsItems.toString());
//                        if (optionExists == 0) {
//                            long result = cartDao.addProduct(products[0]);
//                            try {
//                                var_room_id = cartDao.addVariations(new VariationsItem(products[0].getProduct_id(), (int) result, variationsItems.toString()));
//                                cartDao.addOption(new OptionsItem((int) var_room_id, optionsItems.toString()));
//                            } catch (Exception e) {
//                                e.printStackTrace();
//                            }
//                        } else {
//                            cartDao.updateProductQuantity(products[0].getQuantity(), products[0].getProduct_id());
//                        }
//                    }
//                }
//            } else {
//                if (cartDao.getIfProductExist(products[0].getProduct_id()) == 0)
//                    cartDao.addProduct(products[0]);
//                else
//                    cartDao.updateProductQuantity(products[0].getQuantity(), products[0].getProduct_id());
//            }
            return null;
        }
    }

    private static class InsertProductReOrderAsyncTask extends AsyncTask<List<ProductDetails>, Void, Void> {
        CartDao cartDao;
        List<Integer> variationsItems;
        List<Integer> optionsItems;
        long var_room_id;

        public InsertProductReOrderAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @SafeVarargs
        @Override
        protected final Void doInBackground(List<ProductDetails>... products) {
            variationsItems = new ArrayList<>();
            optionsItems = new ArrayList<>();
            for (int product = 0; product < products[0].size(); product++) {
                if (products[0].get(product).getVariations().size() > 0) {
                    for (int var = 0; var < products[0].get(product).getVariations().size(); var++) {
                        variationsItems.add(products[0].get(product).getVariations().get(var).getVariation_id());
                        for (int option = 0; option < products[0].get(product).getVariations().get(var).getOptions().size(); option++) {
                            optionsItems.add(products[0].get(product).getVariations().get(var).getOptions().get(option).getOption_id());
                        }
                    }
                    products[0].get(product).setPriceItem(Double.parseDouble(products[0].get(product).getVariations().get(0).getOptions().get(0).getPrice()));
                    long result = cartDao.addProduct(products[0].get(product));
                    var_room_id = cartDao.addVariations(new VariationsItem(products[0].get(product).getProduct_id(), (int) result, variationsItems.toString()));
                    cartDao.addOption(new OptionsItem((int) var_room_id, optionsItems.toString()));
                } else {
                    products[0].get(product).setPriceItem(Double.parseDouble(products[0].get(product).getPriceAfter()));
                    cartDao.addProduct(products[0].get(product));
                }
            }
            return null;
        }
    }

    private static class UpdateProductAsyncTask extends AsyncTask<ProductDetails, Void, Void> {
        CartDao cartDao;

        public UpdateProductAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @Override
        protected Void doInBackground(ProductDetails... products) {
            cartDao.updateProduct(products[0]);
            return null;
        }
    }

    private static class DeleteProductAsyncTask extends AsyncTask<Integer, Void, Void> {
        CartDao cartDao;

        public DeleteProductAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            cartDao.deleteItem(integers[0]);
            return null;
        }
    }

    private static class DeleteOneProductAsyncTask extends AsyncTask<Integer, Void, Void> {
        CartDao cartDao;

        public DeleteOneProductAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @Override
        protected Void doInBackground(Integer... integers) {
            cartDao.deleteOneProductFromCart(integers[0]);
            return null;
        }
    }

    private static class EmptyCartAsyncTask extends AsyncTask<Void, Void, Void> {
        CartDao cartDao;

        public EmptyCartAsyncTask(CartDao cartDao) {
            this.cartDao = cartDao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            cartDao.emptyProductCart();
            return null;
        }
    }

}
