package amrk.app.amrk_user.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;

import amrk.app.amrk_user.connection.ConnectionHelper;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.model.promoCode.CheckPromoRequest;
import amrk.app.amrk_user.model.promoCode.CheckPromoResponse;
import amrk.app.amrk_user.pages.offers.models.OrderOfferRequest;
import amrk.app.amrk_user.pages.publicOrder.models.subDepartment.SubDepartmentResponse;
import amrk.app.amrk_user.uber.home.models.StatusRequest;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import io.reactivex.disposables.Disposable;


public class BaseRepository {
    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getSubDepartment(int department) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_PUBLIC_SUB_DEPARTMENT + department, new Object(), SubDepartmentResponse.class,
                Constants.GET_PUBLIC_SUB_DEPARTMENT, true);
    }

    public Disposable CancelOrder(int orderId) {
        OrderOfferRequest orderOfferRequest = new OrderOfferRequest();
        orderOfferRequest.setOrderId(String.valueOf(orderId));
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CANCEL_ORDER, orderOfferRequest, StatusMessage.class,
                Constants.CHANGE_DELEGATE, true);
    }

    public Disposable deleteTrip(StatusRequest statusRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.DELETE_TRIP, statusRequest, StatusMessage.class,
                Constants.DELTE_TRIP, false);
    }
    public Disposable checkPromo(CheckPromoRequest promoRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHECK_PROMO, promoRequest, CheckPromoResponse.class,
                Constants.CHECK_PROMO, true);
    }

}
