package amrk.app.amrk_user.repository;

import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.maps.model.LatLng;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.ConnectionHelper;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.markets.models.MarketResponse;
import amrk.app.amrk_user.pages.markets.models.MarketsByCategoriesResponse;
import amrk.app.amrk_user.pages.markets.models.SearchResponse;
import amrk.app.amrk_user.pages.markets.models.marketDetails.MarketDetailsResponse;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDialogResponse;
import amrk.app.amrk_user.pages.myOrders.models.MyOrdersResponse;
import amrk.app.amrk_user.pages.myOrders.models.reOrder.OrderDetailsResponse;
import amrk.app.amrk_user.pages.offers.models.OffersResponse;
import amrk.app.amrk_user.pages.offers.models.OrderOfferRequest;
import amrk.app.amrk_user.pages.offers.models.offerDetails.OfferDetailsResponse;
import amrk.app.amrk_user.pages.publicOrder.models.CreateOrderResponse;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.NewOrderRequest;
import amrk.app.amrk_user.pages.reviews.models.RateRequest;
import amrk.app.amrk_user.pages.reviews.models.ReviewsResponse;
import amrk.app.amrk_user.pages.reviews.models.SendReviewResponse;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.Disposable;

@Singleton
public class MarketRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public MarketRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getMarkets(String url, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_MARKETS + url, new Object(), MarketResponse.class,
                Constants.MARKETS, showProgress);
    }

    public Disposable searchMarkets(String search, LatLng location, int page) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.SEARCH + search + "&lat=" + location.latitude + "&lng=" + location.longitude + "&page=" + page, new Object(), SearchResponse.class,
                Constants.SEARCH, false);
    }

    public Disposable getMarketsByCategoryId(String url, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_MARKETS_BY_CATEGORY + url, new Object(), MarketsByCategoriesResponse.class,
                Constants.GET_MARKETS_BY_CATEGORY, showProgress);
    }

    public Disposable getMarketDetails(int marketId) {
        String location = UserHelper.getInstance(MyApplication.getInstance()).getSaveLastKnownLocationLat() + "&lng=" + UserHelper.getInstance(MyApplication.getInstance()).getSaveLastKnownLocationLng();
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_MARKET_DETAILS+location.concat("&shop_id=") + marketId, new Object(), MarketDetailsResponse.class,
                Constants.GET_MARKET_DETAILS, true);
    }

    public Disposable getClientReviews(int marketId, String type) {
        String url;
        if (type.equals(Constants.GET_PUBLIC_ORDER_INFO)) {
            url = "&department_id=" + marketId;
        } else
            url = "=" + marketId;
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_CLIENT_REVIEWS + url, new Object(), ReviewsResponse.class,
                Constants.MARKET_REVIEWS, true);
    }

    public Disposable getDelegateReviews(int delegateId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_DELEGATE_REVIEWS + delegateId, new Object(), ReviewsResponse.class,
                Constants.MARKET_REVIEWS, true);
    }

    public Disposable sendReview(RateRequest rateRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SEND_REVIEW, rateRequest, SendReviewResponse.class,
                Constants.SEND_REVIEW, true);
    }

    public Disposable getProductDetails(int productId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_PRODUCT_DETAILS + productId, new Object(), ProductDialogResponse.class,
                Constants.PRODUCT_DETAILS, true);
    }

    public Disposable offers(int departmentId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.OFFERS + departmentId, new Object(), OffersResponse.class,
                Constants.OFFERS, true);
    }

    public Disposable oneOffer(int offerId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_ONE_OFFER + offerId, new Object(), OfferDetailsResponse.class,
                Constants.OFFERS, false);
    }

    public Disposable lessOffer(int offerId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.LESS_OFFER + offerId, new Object(), StatusMessage.class,
                Constants.LESS_OFFER, false);
    }

    public Disposable cancelOrder(int orderId) {
        OrderOfferRequest orderOfferRequest = new OrderOfferRequest();
        orderOfferRequest.setOrderId(String.valueOf(orderId));
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CANCEL_ORDER, orderOfferRequest, StatusMessage.class,
                Constants.CANCEL_ORDER, false);
    }

    public Disposable acceptOrder(int orderId) {
        OrderOfferRequest orderOfferRequest = new OrderOfferRequest();
        orderOfferRequest.setOrder_offer_id(String.valueOf(orderId));
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.ACCEPT_OFFER, orderOfferRequest, StatusMessage.class,
                Constants.ACCEPT_OFFER, false);
    }

    public Disposable myOrders(int departmentId, int type) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.MY_ORDERS + departmentId + "&type=" + type, new Object(), MyOrdersResponse.class,
                Constants.MY_ORDERS, true);
    }

    public Disposable sendOrderReview(RateRequest rateRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.RATE_MANDOUB, rateRequest, StatusMessage.class,
                Constants.RATE_MANDOUB, true);
    }


    public Disposable getOrderDetails(int orderId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.ORDER_DETAILS + orderId, new Object(), OrderDetailsResponse.class,
                Constants.ORDER_DETAILS, true);
    }

    public Disposable reOrder(NewOrderRequest newOrderRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.RE_ORDER, newOrderRequest, CreateOrderResponse.class,
                Constants.CREATE_ORDER, false);
    }

}