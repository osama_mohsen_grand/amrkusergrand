package amrk.app.amrk_user.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.base.maps.models.DistanceTimeResponse;
import amrk.app.amrk_user.connection.ConnectionHelper;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.uber.home.models.CarsResponse;
import amrk.app.amrk_user.uber.home.models.RateRequest;
import amrk.app.amrk_user.uber.home.models.StatusRequest;
import amrk.app.amrk_user.uber.home.models.TripRequest;
import amrk.app.amrk_user.uber.home.models.TripStatusResponse;
import amrk.app.amrk_user.uber.home.models.cancelTripReasons.CancelReasonsResponse;
import amrk.app.amrk_user.uber.home.models.createTrip.CreateTripResponse;
import amrk.app.amrk_user.uber.home.models.createTrip.UpdateLastLocationRequest;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import io.reactivex.disposables.Disposable;

@Singleton
public class TripRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public TripRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getCars() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_CARS, new Object(), CarsResponse.class,
                Constants.GET_CARS, false);
    }

    public Disposable cancelReasons() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_CANCEL_REASONS, new Object(), CancelReasonsResponse.class,
                Constants.CANCEL_REASONS, false);
    }

    public Disposable createTrip(TripRequest tripRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CREATE_TRIP, tripRequest, CreateTripResponse.class,
                Constants.CREATE_TRIP, false);
    }

    public Disposable getLastTrip(StatusRequest statusRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHANGE_STATUS, statusRequest, TripStatusResponse.class,
                Constants.TRIP_STATUS, true);
    }

    public Disposable updateLastLocation(UpdateLastLocationRequest lastLocationRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_LAST_LOCATION, lastLocationRequest, TripStatusResponse.class,
                Constants.TRIP_STATUS, true);
    }

    public Disposable cancelTrip(StatusRequest statusRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CANCEL_TRIP, statusRequest, StatusMessage.class,
                Constants.CANCEL_TRIP, false);
    }

    public Disposable getLessTime(String destination, String start) {
        String requestUrl = "https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=" + start + "&destinations=" + destination + "&language=" + LanguagesHelper.getCurrentLanguage() + "&key=" + ResourceManager.getString(R.string.google_map);
        return connectionHelper.requestGoogleApi(Constants.GET_REQUEST, requestUrl, new Object(), DistanceTimeResponse.class,
                Constants.GOOGLE_API, false);
    }

    public Disposable rateTrip(RateRequest rateRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.RATE_TRIP, rateRequest, StatusMessage.class,
                Constants.RATE_TRIP, true);
    }


}