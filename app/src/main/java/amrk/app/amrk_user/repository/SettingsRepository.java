package amrk.app.amrk_user.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.connection.ConnectionHelper;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.appWallet.models.RaiseWalletRequest;
import amrk.app.amrk_user.pages.appWallet.models.RaiseWalletResponse;
import amrk.app.amrk_user.pages.appWallet.models.WalletHistoryResponse;
import amrk.app.amrk_user.pages.auth.models.RegisterRequest;
import amrk.app.amrk_user.pages.gifts.models.ReplacePointsResponse;
import amrk.app.amrk_user.pages.gifts.models.ReplacedPointsRequest;
import amrk.app.amrk_user.pages.home.model.HomeResponse;
import amrk.app.amrk_user.pages.newOrder.models.SavedLocationsResponse;
import amrk.app.amrk_user.pages.notifications.models.NotificationsResponse;
import amrk.app.amrk_user.pages.settings.models.AboutResponse;
import amrk.app.amrk_user.pages.settings.models.ContactRequest;
import amrk.app.amrk_user.uber.favorites.models.AddNewFavoriteRequest;
import amrk.app.amrk_user.uber.favorites.models.DeleteLocationRequest;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import amrk.app.amrk_user.utils.session.UserHelper;
import io.reactivex.disposables.Disposable;

@Singleton
public class SettingsRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public SettingsRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getAbout() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.ABOUT, new Object(), AboutResponse.class,
                Constants.ABOUT, true);
    }

    public Disposable sendContact(ContactRequest contactRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CONTACT, contactRequest, StatusMessage.class,
                Constants.CONTACT, false);
    }

    public Disposable getTerms() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.TERMS, new Object(), AboutResponse.class,
                Constants.TERMS, true);
    }

    public Disposable privacyPolicy() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.PRIVACY, new Object(), AboutResponse.class,
                Constants.TERMS, true);
    }

    public Disposable getNotifications(int orderType) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.NOTIFICATIONS, new Object(), NotificationsResponse.class,
                Constants.NOTIFICATIONS, true);
    }

    public Disposable SavedLocations(int type) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.SAVED_LOCATIONS + type, new Object(), SavedLocationsResponse.class,
                Constants.SAVED_LOCATIONS, true);
    }

    public Disposable getFavoriteLocations() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.FAVORITES_LOCATIONS, new Object(), SavedLocationsResponse.class,
                Constants.SAVED_LOCATIONS, true);
    }

    public Disposable sendLocation(AddNewFavoriteRequest favoriteRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SEND_LOCATION, favoriteRequest, StatusMessage.class,
                Constants.SAVED_LOCATIONS, true);
    }

    public Disposable deleteLocation(DeleteLocationRequest deleteLocationRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.DELETE_LOCATION, deleteLocationRequest, StatusMessage.class,
                Constants.DELETE_LOCATION, true);
    }

    public Disposable gifts() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GIFTS, new Object(), ReplacePointsResponse.class,
                Constants.GIFTS, true);
    }

    public Disposable homeSlider() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.HOME, new Object(), HomeResponse.class,
                Constants.HOME, true);
    }

    public Disposable raiseWallet(RaiseWalletRequest raiseWalletRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.RAISE_WALLET, raiseWalletRequest, RaiseWalletResponse.class,
                Constants.RAISE_WALLET, true);
    }

    public Disposable walletHistory() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.WALLET_HISTORY, new Object(), WalletHistoryResponse.class,
                Constants.WALLET_HISTORY, true);
    }

    public Disposable replacePoints(int offerId) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.REPLACE_POINTS, new ReplacedPointsRequest(offerId), StatusMessage.class,
                Constants.PICKED_SUCCESSFULLY, true);
    }

    public Disposable updateLang(String lang) {
        RegisterRequest registerRequest = new RegisterRequest();
        registerRequest.setLang(lang);
        registerRequest.setCountry_id(UserHelper.getInstance(MyApplication.getInstance()).getUserData().getCountryId());
        return connectionHelper.requestApiBackground(Constants.POST_REQUEST, URLS.UPDATE_PROFILE, registerRequest);
    }
}