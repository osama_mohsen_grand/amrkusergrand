package amrk.app.amrk_user.repository;

import androidx.lifecycle.MutableLiveData;

import java.util.List;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_user.connection.ConnectionHelper;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.pages.chat.model.ChatRequest;
import amrk.app.amrk_user.pages.chat.model.ChatResponse;
import amrk.app.amrk_user.pages.chat.model.ChatSendResponse;
import amrk.app.amrk_user.pages.chat.model.ConfirmPayment;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdminRequest;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdminResponse;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdminSendResponse;
import amrk.app.amrk_user.pages.myOrders.models.followOrder.FollowStatusResponse;
import amrk.app.amrk_user.pages.offers.models.OrderOfferRequest;
import amrk.app.amrk_user.uber.chat.model.ChatUberRequest;
import amrk.app.amrk_user.uber.chat.model.ChatUberResponse;
import amrk.app.amrk_user.uber.chat.model.SendUberMessageResponse;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import io.reactivex.disposables.Disposable;

@Singleton
public class ChatRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public ChatRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getChat(int orderId, String delegateId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CHAT + orderId + "&delegate_id=" + delegateId, new Object(), ChatResponse.class,
                Constants.CHAT, true);
    }

    public Disposable getAdminChat() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CHAT_ADMIN, new Object(), ChatAdminResponse.class,
                Constants.CHAT_ADMIN, true);
    }

    public Disposable sendAdminChat(ChatAdminRequest request, List<FileObjects> fileObjectList) {
        if (fileObjectList != null && fileObjectList.size() > 0) {
            request.setMessage(null);
            return connectionHelper.requestApi(URLS.SEND_MESSAGE, request, fileObjectList, ChatAdminSendResponse.class,
                    Constants.SEND_MESSAGE, true);
        } else
            return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SEND_MESSAGE, request, ChatAdminSendResponse.class,
                    Constants.SEND_MESSAGE, true);
    }

    public Disposable sendChat(ChatRequest request, List<FileObjects> fileObjectList) {
        if (fileObjectList != null && fileObjectList.size() > 0) {
            request.setMessage(null);
            return connectionHelper.requestApi(URLS.SEND_ORDER_MESSAGE, request, fileObjectList, ChatSendResponse.class,
                    Constants.SEND_MESSAGE, true);
        } else
            return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SEND_ORDER_MESSAGE, request, ChatSendResponse.class,
                    Constants.SEND_MESSAGE, true);
    }

    public Disposable getOrderStatus(int orderId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.ORDER_STATUS + orderId, new Object(), FollowStatusResponse.class,
                Constants.ORDER_STATUS, true);
    }

    public Disposable changeDelegate(int orderId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CHANGE_DELEGATE + orderId, new Object(), StatusMessage.class,
                Constants.CHANGE_DELEGATE, true);
    }

    public Disposable CancelOrder(int orderId) {
        OrderOfferRequest orderOfferRequest = new OrderOfferRequest();
        orderOfferRequest.setOrderId(String.valueOf(orderId));
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CANCEL_ORDER, orderOfferRequest, StatusMessage.class,
                Constants.CANCEL_ORDER, true);
    }

    public Disposable changeOrderConfirm(int orderId, int type) {
        OrderOfferRequest orderOfferRequest = new OrderOfferRequest();
        orderOfferRequest.setOrderId(String.valueOf(orderId));
        orderOfferRequest.setType(String.valueOf(type));
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CONFIRM_ORDER, orderOfferRequest, ChatSendResponse.class,
                Constants.CONFIRM_ORDER, true);
    }

    //Uber
    public Disposable getUberChat(int orderId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.CHAT_UBER + orderId, new Object(), ChatUberResponse.class,
                Constants.CHAT, true);
    }

    public Disposable sendUberChat(ChatUberRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SEND_UBER_MESSAGE, request, SendUberMessageResponse.class,
                Constants.SEND_MESSAGE, true);
    }

    public Disposable confirmPayment(ConfirmPayment confirmPayment) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CONFIRM_INVOICE, confirmPayment, StatusMessage.class,
                Constants.CONFIRM_PAYMENT, true);
    }
}