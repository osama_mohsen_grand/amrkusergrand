package amrk.app.amrk_user.repository;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_user.connection.ConnectionHelper;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.publicOrder.models.CreateOrderResponse;
import amrk.app.amrk_user.pages.publicOrder.models.PublicOrderInfoResponse;
import amrk.app.amrk_user.pages.publicOrder.models.newOrder.NewOrderRequest;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import io.reactivex.disposables.Disposable;

@Singleton
public class PublicOrderRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public PublicOrderRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getPublicInfo() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_PUBLIC_ORDER_INFO + Constants.DEPARTMENT_3, new Object(), PublicOrderInfoResponse.class,
                Constants.GET_PUBLIC_ORDER_INFO, true);
    }


    public Disposable createOrder(NewOrderRequest newOrderRequest, ArrayList<FileObjects> fileObjectList) {
        if (fileObjectList != null && fileObjectList.size() > 0) {
            return connectionHelper.requestApi(URLS.CREATE_ORDER, newOrderRequest, fileObjectList, CreateOrderResponse.class,
                    Constants.CREATE_ORDER, false);
        } else
            return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CREATE_ORDER, newOrderRequest, CreateOrderResponse.class,
                    Constants.CREATE_ORDER, false);
    }
}