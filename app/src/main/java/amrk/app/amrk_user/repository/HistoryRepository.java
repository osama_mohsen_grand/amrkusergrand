package amrk.app.amrk_user.repository;

import androidx.lifecycle.MutableLiveData;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_user.connection.ConnectionHelper;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.uber.history.models.HistoryResponse;
import amrk.app.amrk_user.uber.history.models.TripDetailsResponse;
import amrk.app.amrk_user.uber.history.models.issues.IssueLostsRequest;
import amrk.app.amrk_user.uber.history.models.issues.IssuesResponse;
import amrk.app.amrk_user.uber.history.models.losts.LostResponse;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import io.reactivex.disposables.Disposable;

@Singleton
public class HistoryRepository extends BaseRepository {

    @Inject
    ConnectionHelper connectionHelper;

    MutableLiveData<Mutable> liveData;

    @Inject
    public HistoryRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getHistory(int type, int page, boolean showProgress) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.TRIP_HISTORY + type + "&page=" + page, new Object(), HistoryResponse.class,
                Constants.TRIP_HISTORY, showProgress);
    }

    public Disposable getTripDetails(int tripId) {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.TRIP_DETAILS + tripId, new Object(), TripDetailsResponse.class,
                Constants.TRIP_DETAILS, true);
    }

    public Disposable getIssues() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_ISSUES, new Object(), IssuesResponse.class,
                Constants.ISSUES, true);
    }

    public Disposable sendIssue(IssueLostsRequest issueLostsRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SEND_ISSUES, issueLostsRequest, StatusMessage.class,
                Constants.SEND_ISSUE, true);
    }

    public Disposable getLost() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_LOST, new Object(), LostResponse.class,
                Constants.LOST_ITEM, true);
    }

    public Disposable sendLost(IssueLostsRequest issueLostsRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.SEND_ISSUES, issueLostsRequest, StatusMessage.class,
                Constants.SEND_ISSUE, true);
    }
}