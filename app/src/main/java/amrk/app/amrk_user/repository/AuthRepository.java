package amrk.app.amrk_user.repository;

import androidx.lifecycle.MutableLiveData;

import java.util.ArrayList;

import javax.inject.Inject;
import javax.inject.Singleton;

import amrk.app.amrk_user.connection.ConnectionHelper;
import amrk.app.amrk_user.connection.FileObjects;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.model.base.StatusMessage;
import amrk.app.amrk_user.model.promoCode.CheckPromoRequest;
import amrk.app.amrk_user.model.promoCode.CheckPromoResponse;
import amrk.app.amrk_user.pages.auth.models.ChangePasswordRequest;
import amrk.app.amrk_user.pages.auth.models.ConfirmCodeRequest;
import amrk.app.amrk_user.pages.auth.models.ForgetPasswordRequest;
import amrk.app.amrk_user.pages.auth.models.LoginRequest;
import amrk.app.amrk_user.pages.auth.models.RegisterRequest;
import amrk.app.amrk_user.pages.auth.models.UsersResponse;
import amrk.app.amrk_user.pages.countries.models.CountriesCodesResponse;
import amrk.app.amrk_user.pages.countries.models.CountriesResponse;
import amrk.app.amrk_user.pages.countries.models.UpdateCountryRequest;
import amrk.app.amrk_user.pages.myFatora.models.SendPaymentRequest;
import amrk.app.amrk_user.pages.onBoard.models.BoardResponse;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.URLS;
import io.reactivex.disposables.Disposable;

@Singleton
public class AuthRepository extends BaseRepository {

    @Inject
    public ConnectionHelper connectionHelper;

    protected MutableLiveData<Mutable> liveData;

    @Inject
    public AuthRepository(ConnectionHelper connectionHelper) {
        this.connectionHelper = connectionHelper;
    }

    public void setLiveData(MutableLiveData<Mutable> liveData) {
        this.liveData = liveData;
        connectionHelper.liveData = liveData;
    }

    public Disposable getCountries() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.COUNTRIES, new Object(), CountriesResponse.class,
                Constants.COUNTRIES, true);
    }

    public Disposable getBoard() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.BOARD, new Object(), BoardResponse.class,
                Constants.BOARD, true);
    }

    public Disposable loginPhone(LoginRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.LOGIN_PHONE, request, StatusMessage.class,
                Constants.PHONE_VERIFIED, true);
    }

    public Disposable loginPassword(LoginRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.LOGIN_PASSWORD, request, UsersResponse.class,
                Constants.LOGIN, false);
    }

    public Disposable register(RegisterRequest request, FileObjects fileObject) {
        ArrayList<FileObjects> fileObjects = new ArrayList<>();
        fileObjects.add(fileObject);
        return connectionHelper.requestApi(URLS.REGISTER, request, fileObjects, StatusMessage.class,
                Constants.REGISTER, false);
    }

    public Disposable confirmCode(ConfirmCodeRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CONFIRM_CODE, request, UsersResponse.class,
                Constants.CONFIRM_CODE, false);
    }


    public Disposable updateProfile(RegisterRequest request, FileObjects fileObject) {
        if (fileObject == null) {
            return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_PROFILE, request, UsersResponse.class,
                    Constants.UPDATE_PROFILE, false);
        } else {
            ArrayList<FileObjects> fileObjects = new ArrayList<>();
            fileObjects.add(fileObject);
            return connectionHelper.requestApi(URLS.UPDATE_PROFILE, request, fileObjects, UsersResponse.class,
                    Constants.UPDATE_PROFILE, false);
        }

    }

    //    public Disposable updateToken(String token) {
//        return connectionHelper.requestApiBackground(Constants.POST_REQUEST, URLS.UPDATE_TOKEN, new TokenRequest(token));
//    }
//
    public Disposable forgetPassword(ForgetPasswordRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.FORGET_PASSWORD, request, StatusMessage.class,
                Constants.FORGET_PASSWORD, false);
    }

    public Disposable changePassword(ChangePasswordRequest request) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHANGE_PASSWORD, request, StatusMessage.class,
                Constants.CHANGE_PASSWORD, false);
    }


    public Disposable getCountriesCodes() {
        return connectionHelper.requestApi(Constants.GET_REQUEST, URLS.GET_COUNTRIES_CODE, new Object(), CountriesCodesResponse.class,
                Constants.GET_COUNTRIES_CODE, false);
    }

    public Disposable updateCountry(int countryId) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.UPDATE_PROFILE, new UpdateCountryRequest(countryId), UsersResponse.class,
                Constants.UPDATE_PROFILE, true);
    }

    public Disposable updatePromoCode(CheckPromoRequest promoRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHECK_PROMO, promoRequest, CheckPromoResponse.class,
                Constants.CHECK_PROMO, true);
    }

    public Disposable checkPayment(SendPaymentRequest sendPaymentRequest) {
        return connectionHelper.requestApi(Constants.POST_REQUEST, URLS.CHECK_PAYMENT, sendPaymentRequest, UsersResponse.class,
                Constants.CHECK_PAYMENT, true);
    }
}