package amrk.app.amrk_user.utils;

public class URLS {
        public final static String BASE_URL = "https://amrrek.net/api/";
//    public final static String BASE_URL = "https://amrk1.my-staff.net/api/";
    public static final String TERMS_URL = "https://amrrek.net/terms/";
    public static final String POLICY_URL = "https://amrrek.net/privacy/";
    public final static String COUNTRIES = "app/countries";
    public final static String HOME = "user/shop-sliders";
    public final static String LOGIN_PHONE = "user/phone-check";
    public final static String LOGIN_PASSWORD = "user/login";
    public final static String BOARD = "app/app-explanation/0/0";
    public final static String PRIVACY = "app/app-explanation/0/1";
    public final static String ABOUT = "app/about-us?type=0";
    public final static String CONTACT = "app/contact-us";
    public final static String TERMS = "app/terms?type=0";
    public final static String REGISTER = "user/register";
    public final static String CONFIRM_CODE = "user/code-check";
    public final static String UPDATE_PROFILE = "user/update-profile";
    public final static String GET_MARKETS = "user/categories-shops?lat=";
    public final static String SEARCH = "user/search-shops-google-maps?search_key=";
    public final static String GET_MARKETS_BY_CATEGORY = "user/shops-by-category?lat=";
    public final static String GET_MARKET_DETAILS = "user/shop-details?lat=";
    public final static String GET_CLIENT_REVIEWS = "user/shop-rates?shop_id";
    public final static String GET_DELEGATE_REVIEWS = "user/get-delegate-orders-rates?delegate_id=";
    public final static String SEND_REVIEW = "user/rate-shop";
    public final static String GET_PRODUCT_DETAILS = "user/product-details?product_id=";
    public final static String GET_PUBLIC_SUB_DEPARTMENT = "user/get-sub-departments?department_id=";
    public static final String GET_PUBLIC_ORDER_INFO = "user/home?department_id=";
    public static final String CHECK_PROMO = "user/check-promo";
    public static final String CREATE_ORDER = "user/make-order";
    public static final String RE_ORDER = "user/re-order";
    public static final String CHANGE_PASSWORD = "forget-password";
    public final static String SAVED_LOCATIONS = "user/saved-locations?type=";
    public final static String FAVORITES_LOCATIONS = "user/get-user-locations";
    public final static String SEND_LOCATION = "user/add-user-location";
    public final static String DELETE_LOCATION = "user/delete-user-location";
    public final static String NOTIFICATIONS = "app/get-notifications?type=0";
    public final static String GET_COUNTRIES_CODE = "app/countries-codes";
    public final static String OFFERS = "user/get-orders-offers?department_id=";
    public static final String FORGET_PASSWORD = "user/code-send";
    public static final String GIFTS = "user/get-offer-points";
    public static final String GET_ONE_OFFER = "user/get-offers?order_id=";
    public static final String LESS_OFFER = "user/get-lower-offer?order_id=";
    public static final String CANCEL_ORDER = "user/cancel-order";
    public static final String CONFIRM_ORDER = "user/accept-confirm-request";
    public static final String ACCEPT_OFFER = "user/accept-order";
    public static final String RAISE_WALLET = "user/raise-user-wallet";
    public static final String WALLET_HISTORY = "user/get-user-wallet-recharges";
    public static final String MY_ORDERS = "user/get-history-orders?department_id=";
    public final static String ORDER_DETAILS = "delegate/order-details?order_id=";
    public static final String RATE_MANDOUB = "user/rate-order";
    public static final String REPLACE_POINTS = "user/replace-points";
    public static final String CHAT_ADMIN = "user/get-user-admin-messages";
    public static final String SEND_MESSAGE = "user/chat-with-admin";
    public static final String SEND_ORDER_MESSAGE = "user/send-message";
    public static final String CHAT = "user/get-user-messages?order_id=";
    public static final String ORDER_STATUS = "user/get-order-status?order_id=";
    public static final String CHANGE_DELEGATE = "user/change-my-delegate?order_id=";
    //UBER
    public static final String TRIP_HISTORY = "user-uber/trip_history?type=";
    public static final String TRIP_DETAILS = "user-uber/trip_details?trip_id=";
    public static final String GET_ISSUES = "app-uber/issues?is_captin=0";
    public static final String SEND_ISSUES = "app-uber/complains_suggestions";
    public static final String GET_LOST = "app-uber/losts";
    public static final String GET_CARS = "user-uber/home";
    public static final String CREATE_TRIP = "user-uber/create_trip";
    public static final String CHANGE_STATUS = "captin/change_status";
    public static final String UPDATE_LAST_LOCATION = "user-uber/edit-destination";
    public static final String GET_CANCEL_REASONS = "user-uber/cancelling_reasons?is_captin=0";
    public static final String CANCEL_TRIP = "user-uber/cancel_trip";
    public static final String RATE_TRIP = "captin/rate_trip";
    public static final String CHAT_UBER = "user-uber/chat_history?is_captin=0&trip_id=";
    public static final String SEND_UBER_MESSAGE = "user-uber/add_message";
    public static final String CONFIRM_INVOICE = "user/confirm-invoice";
    public static final String DELETE_TRIP = "user-uber/delete_trip";
    public static final String CHECK_PAYMENT = "app/payment";

}
