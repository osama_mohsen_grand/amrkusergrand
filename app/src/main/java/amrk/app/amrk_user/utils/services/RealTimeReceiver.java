package amrk.app.amrk_user.utils.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.gson.Gson;

import amrk.app.amrk_user.pages.chat.model.Chat;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdmin;
import amrk.app.amrk_user.pages.offers.models.offerDetails.OfferDetails;
import amrk.app.amrk_user.uber.chat.model.ChatData;

public class RealTimeReceiver extends BroadcastReceiver {
    public static MessageReceiverListener messageReceiverListene;
    public static MessageAdminReceiverListener messageAdminReceiverListener;
    public static OfferDetailsReceiverListener offerDetailsReceiverListener;
    public static NewTripReceiverListener tripReceiverListener;
    public static MessageUberReceiverListener messageUberReceiverListener;
    public static NotificationCounterListener notificationCounterListener;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            if (intent.getAction().equals("app.te.receiver")) {
                if (intent.getSerializableExtra("chat") != null) {
                    String details = String.valueOf(intent.getSerializableExtra("chat"));
                    Chat messagesItem = new Gson().fromJson(details, Chat.class);
                    messageReceiverListene.onMessageChanged(messagesItem);
                }
                if (intent.getSerializableExtra("new_offer") != null) {
                    String details = String.valueOf(intent.getSerializableExtra("new_offer"));
                    OfferDetails offerDetails = new Gson().fromJson(details, OfferDetails.class);
                    offerDetailsReceiverListener.onNewOrderChanged(offerDetails);
                }
                if (intent.getSerializableExtra("trip_details") != null) {
                    String details = String.valueOf(intent.getSerializableExtra("trip_details"));
                    Log.e("onReceive", "onReceive:Trip " + details);
//                    TripDataFromNotifications messagesItem = new Gson().fromJson(details, TripDataFromNotifications.class);
                    tripReceiverListener.onTripChanged(details);
                }
                if (intent.getSerializableExtra("uber_chat") != null) {
                    String details = String.valueOf(intent.getSerializableExtra("uber_chat"));
                    Log.e("onReceive", "onReceive:Trip " + details);
                    ChatData messagesItem = new Gson().fromJson(details, ChatData.class);
                    messageUberReceiverListener.onMessageUberChanged(messagesItem);
                }
                if (intent.getSerializableExtra("counter") != null) {
                    int counter = (int) intent.getSerializableExtra("counter");
                    Log.e("onReceive", "onReceive: " + counter);
                    notificationCounterListener.onNotificationsCounter(counter);
                }
            }
        } else
            Log.i("onReceive", "onReceive: null");
    }

    public interface MessageReceiverListener {
        void onMessageChanged(Chat messagesItem);
    }

    public interface MessageUberReceiverListener {
        void onMessageUberChanged(ChatData messagesItem);
    }

    public interface MessageAdminReceiverListener {
        void onMessageChanged(ChatAdmin messagesItem);
    }

    public interface OfferDetailsReceiverListener {
        void onNewOrderChanged(OfferDetails offerDetails);
    }

    public interface NewTripReceiverListener {
        void onTripChanged(String dataFromNotification);
    }
    public interface NotificationCounterListener {
        void onNotificationsCounter(int count);
    }
}
