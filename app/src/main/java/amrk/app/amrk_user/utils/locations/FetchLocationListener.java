package amrk.app.amrk_user.utils.locations;

import android.location.Location;

public interface FetchLocationListener {
    public void location(Location location);
}
