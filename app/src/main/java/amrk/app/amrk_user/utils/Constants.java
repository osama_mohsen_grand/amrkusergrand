package amrk.app.amrk_user.utils;

public class Constants {

    public static final String SEND_CURRENT_LOCATION = "SEND_CURRENT_LOCATION";
    public static final String FOLLOW_ORDER = "FOLLOW_ORDER";
    public static final String CALL = "CALL";

    public final static String COUNTRIES = "COUNTRIES";
    public final static String SELECT_COUNTRY = "SELECT_COUNTRY";
    public final static String CONFIRM_CODE = "CONFIRM_CODE";
    public final static String NOT_VERIFIED = "NOT_VERIFIED";
    public final static String BACKGROUND_API = "BACKGROUND_API";

    public final static String PHONE_VERIFIED = "PHONE_VERIFIED";
    public final static String EMAIL = "email";
    public final static String RATE_MANDOUB = "RATE_MANDOUB";
    public final static String NOT_MATCH_PASSWORD = "NOT_MATCH_PASSWORD";

    public final static String UPDATE_PROFILE = "update_profile";
    public final static String REGISTER_SHOP = "REGISTER_SHOP";


    public final static String IMAGE = "image";
    public final static String MENU_OFFERS = "MENU_OFFERS";
    public final static String MENU_WALLET = "MENU_WALLET";
    public final static String MENU_ACCOUNT = "complain";

    public final static String LOGOUT = "logout";
    public final static String STORES = "STORES";

    public final static String MARKETS = "MARKETS";
    public final static String GET_MARKETS_BY_CATEGORY = "GET_MARKETS_BY_CATEGORY";
    public final static String MARKET_DETAILS = "MARKET_DETAILS";
    public final static String ERROR_TOAST = "error_toast";


    public final static String ERROR = "error";
    public final static String SHOW_PROGRESS = "showProgress";
    public final static String HIDE_PROGRESS = "hideProgress";
    public final static String SHOW_PROGRESS_FORCE = "showSelfProgress";
    public final static String HIDE_PROGRESS_FORCE = "hideSelfProgress";
    public final static String SERVER_ERROR = "serverError";
    public final static String UBER_HOME = "UBER_HOME";
    public final static String UBER = "UBER";
    public final static String FAILURE_CONNECTION = "failure_connection";
    public final static String FIELD = "FIELD";
    public final static String CARD_NUMBER = "CARD_NUMBER";
    public final static String CARD_EXPIRE = "CARD_EXPIRE";
    public final static String CARD_CVV = "CARD_CVV";

    public final static String LANGUAGE = "language";
    public static final String DEFAULT_LANGUAGE = "ar";
    public static final String SEND_MESSAGE = "SEND_MESSAGE";
    public static final String PAGE = "page";
    public final static String NAME_BAR = "name_bar";
    public static final String BUNDLE = "bundle";
    public static final String GET_MARKET_DETAILS = "GET_MARKET_DETAILS";
    public static final String GET_MARKET_WORKING_DAYS = "GET_MARKET_WORKING_DAYS";
    public static final String MARKET_REVIEWS = "MARKET_REVIEWS";
    public static final String REVIEW_NEW = "REVIEW_NEW";
    public static final String SEND_REVIEW = "SEND_REVIEW";
    public static final String PRODUCT_DETAILS = "PRODUCT_DETAILS";
    public static final String PRODUCT_SINGLE_SELECTION = "PRODUct_SINGLE_SELECTION";
    public static final String SHOW_SUCCESS_DIALOG = "SHOW_SUCCESS_DIALOG";
    public static final String OFFERS = "OFFERS";
    public static final String SHARE_BAR = "SHARE_BAR";
    public static final String BACKGROUND_BAR = "BACKGROUND_BAR";
    public static final String ORDER_ANY_THING = "ORDER_ANY_THING";
    public final static int UBER_HOME_API_ID = 1;
    public final static int STORES_API_ID = 2;
    public static final int ORDER_ANY_THING_API_ID = 3;
    public static final int ORDER_HEAVY_API_ID = 5;

    //REQUESTS
    public final static int POST_REQUEST = 200;
    public final static int GET_REQUEST = 201;
    public final static int DELETE_REQUEST = 202;
    // notification order type;
    public final static int NOTIFICATION_TRIP_TYPE = 1;
    //RESPONSES
    public static final int RESPONSE_SUCCESS = 200;
    public final static int AUTOCOMPLETE_REQUEST_CODE = 203;
    public final static int AUTOCOMPLETE_REQUEST_LOCATION_UPDATE_CODE = 204;
    public static final String TRIP_TYPE_TITLE = "TRIP_TYPE_TITLE";
    public static final int IN_ADDRESS = 1000;
    public static final int OUT_ADDRESS = 1001;
    public static final int PROCESSING_ADDRESS = 1002;
    public static final int RESPONSE_JWT_EXPIRE = 403;
    public static final int RESPONSE_405 = 405;
    public static final String SCHEDULE = "SCHEDULE";
    public static final String SELECT_SCHEDULE = "SELECT_SCHEDULE";
    public static final String CONFIRM_ORDER = "CONFIRM_ORDER";
    public static final String CHANGE_DELEGATE = "CHANGE_DELEGATE";
    public static final int RESULT_CODE_PAYMENT = 777;
    public static final int RESULT_CODE = 6000;
    public final static int DEPARTMENT_3 = 3;
    public final static int FILE_TYPE_IMAGE = 378;
    public final static int FILE_TYPE_DOCUMENT = 379;
    public final static int CHECK_CONFIRM_NAV_REGISTER = 0;
    public final static int CHECK_CONFIRM_NAV_FORGET = 1;

    public static final String NEW_ORDER = "NEW_ORDER";
    public static final String SUB_DEPARTMENT_DIALOG = "SUB_DEPARTMENT_DIALOG";
    public static final String ORDER_STATUS = "ORDER_STATUS";
    public static final String ABOUT = "about";
    public static final String TERMS = "terms";
    public static final String PRIVACY = "PRIVACY";
    public static final String REMOVE_IMAGE = "REMOVE_IMAGE";
    public static final String LOGIN = "login";
    public static final String GET_PUBLIC_ORDER_INFO = "GET_PUBLIC_ORDER_INFO";
    public static final String FORGET_PASSWORD = "forget_password";
    public static final String NEW_IMAGE = "NEW_IMAGE";
    public static final String CONTINUE_NEW_ORDER = "CONTINUE_NEW_ORDER";
    public static final String PICK_UP_LOCATION = "PICK_UP_LOCATION";
    public static final String DESTINATION_LOCATION = "DESTINATION_LOCATION";
    public static final String PROCESSING_LOCATION = "PROCESSING_LOCATION";


    public static final String REGISTER = "register";
    public static final String DELIVERY_TIMES = "DELIVERY_TIMES";
    public static final String AMRK_DRIVER = "AMRK_DRIVER";
    public static final String AMRK_DELEGATE = "AMRK_DELEGATE";
    public static final String AMRK_DELEGATE_HEAVY = "AMRK_DELEGATE_HEAVY";
    public static final String AMRK_SHOP = "AMRK_SHOP";
    public static final String ENGLISH_CHAT_BOT = "https://tawk.to/chat/60a389ab185beb22b30e4987/1fg1omp4s";
    public static final String ARABIC_CHAT_BOT = "https://tawk.to/chat/60a389ab185beb22b30e4987/1f5vd3i2l";
    public static boolean DATA_CHANGED = false;
    public static int REVIEWS_COUNT = 0;
    public static final String LAT = "LAT";
    public static final String LNG = "LNG";
    public static final String HIDE_CALL_DIALOG = "HIDE_CALL_DIALOG";
    public static final String CALL_DRIVER = "CALL_DRIVER";
    public static final String PICKED_SUCCESSFULLY = "PICKED_SUCCESSFULLY";
    public static final String ADDRESS = "ADDRESS";
    public static final String CHECK_PROMO = "CHECK_PROMO";
    public final static String PROMO_DIALOG = "PROMO_DIALOG";
    public final static String PROMO_EXISTS = "PROMO_EXISTS";
    public static final String CREATE_ORDER = "CREATE_ORDER";
    public static final String SAVED_LOCATIONS = "SAVED_LOCATIONS";
    public static final String DELETE_LOCATION = "DELETE_LOCATION";
    public static final String CITY = "CITY";
    public static final String CONTACT = "CONTACT";
    public static final String REMOVE_FROM_CART = "REMOVE_FROM_CART";

    public static final String ADD_TO_CART = "ADD_TO_CART";
    public static final String TO_CART = "TO_CART";
    public static final String GIFTS = "GIFTS";
    public static final String NOTIFICATIONS = "notifications";
    public static final String LESS_OFFER = "LESS_OFFER";
    public static final String CANCEL_ORDER = "CANCEL_ORDER";
    public static final String REMOVE_DIALOG_WARNING = "REMOVE_DIALOG_WARNING";
    public static final String ACCEPT_OFFER = "ACCEPT_OFFER";
    public static final String DELEGATE_REVIEWS = "DELEGATE_REVIEWS";

    public static final String CHANGE_PASSWORD = "forget-password";
    public static final String GET_COUNTRIES_CODE = "GET_COUNTRIES_CODE";
    public static final String SERVICES = "services";
    public static final String HOME = "HOME";
    public static final String RAISE_WALLET = "RAISE_WALLET";
    public static final String DISMISS = "dismiss";

    public static final String WALLET_HISTORY = "WALLET_HISTORY";
    public static final String MY_ORDERS = "MY_ORDERS";
    public static final String ORDER_DETAILS = "ORDER_DETAILS";
    public static final String MY_HEAVY_ORDERS = "MY_HEAVY_ORDERS";
    public static final String MINUS = "minus";
    public static final String PLUS = "plus";
    public static final String SEARCH = "SEARCH";
    public static final String CHAT_ADMIN = "CHAT_ADMIN";
    public static final String CHAT = "CHAT";
    public static final String MENU_HOME = "menu_home";
    public static final String GET_PUBLIC_SUB_DEPARTMENT = "GET_PUBLIC_SUB_DEPARTMENT";
    public static final String NEXT = "next";
    public static final String BOARD = "board";
    public static final String MENu = "menu";
    public static final String CHAT_OPTIONS = "CHAT_OPTIONS";
    public static final String RATE_APP = "RATE_APP";
    public static final String REJECT_ORDER = "REJECT_ORDER";
    public static final String ACCEPT_ORDER = "ACCEPT_ORDER";
    public static final String REORDER = "REORDER";
    //UBEr
    public static final String TRIP_HISTORY = "TRIP_HISTORY";
    public static final String CLOSE_PAYMENT_SHEET = "CLOSE_PAYMENT_SHEET";
    public static final String TRIP_DETAILS = "TRIP_DETAILS";
    public static final String ISSUES = "ISSUES";
    public static final String LOST_ITEM = "LOST_ITEM";
    public static final String EMPTY_DRIVER = "EMPTY_DRIVER";
    public static final String SEND_ISSUE = "SEND_ISSUE";
    public static final String SHOW_CARS_SHEET = "SHOW_CARS_SHEET";
    public static final String GET_CARS = "GET_CARS";
    public static final String SELECT_CAR = "SELECT_CAR";
    public static final String DROP_OFF = "DROP_OFF";
    public static final String TRIP_CREATED = "TRIP_CREATED";
    public static final String SAVED_SELECTED = "SAVED_SELECTED";
    public static final String SEARCH_LOCATION = "SEARCH_LOCATION";
    public static final String EDIT_LOCATION = "EDIT_LOCATION";
    public static final String FIRST_LOCATION_SELECTED = "FIRST_LOCATION_SELECTED";
    public static final String SECOND_LOCATION_BACK = "SECOND_LOCATION_BACK";
    public static final String PAYMENT_METHOD = "PAYMENT_METHOD";
    public static final String CREATE_TRIP = "CREATE_TRIP";
    public static final String TRIP_STATUS = "TRIP_STATUS";
    public static final String CANCEL_REASONS = "CANCEL_REASONS";
    public static final String CANCEL_TRIP = "CANCEL_TRIP";
    public static final String GOOGLE_API = "GOOGLE_API";
    public static final String RATE_TRIP = "RATE_TRIP";
    public static final String CANCEL_TRIP_WARNING_DIALOG = "CANCEL_TRIP_WARNING_DIALOG";
    public static final String CONFIRM_LOCATIONS = "CONFIRM_LOCATIONS";
    public static final String DELTE_TRIP = "DELTE_TRIP";
    public static final String TERMS_ACCEPTED = "TERMS_ACCEPTED";
    public static final String FAVORITES_LOCATIONS = "FAVORITES_LOCATIONS";
    public static final String SKIP_LOCATION = "SKIP_LOCATION";
    public static final int FAVORITES_LOCATIONS_CODE = 60003;
    public static final String EXIT_DIALOG = "EXIT_DIALOG";
    public static final String CONFIRM_PAYMENT = "CONFIRM_PAYMENT";
    public static final int REJECT_CONFIRM = 0;
    public static final int ORDER_FINISHED = 3;
    public static final int SEND_CONFIRM = 1;
    public static final int FINISH_CHAT = -1;
    //order status notifications
    public static final int ORDER_STATUS_FIRST_WAY = 4;
    public static final int ORDER_STATUS_RECEIVED = 5;
    public static final int ORDER_STATUS_WAY_PROCESSING = 6;
    public static final int ORDER_STATUS_PROCESSED = 7;
    public static final int ORDER_STATUS_LAST_WAY = 8;

    public static final String MY_FATOORA_METHODS = "MY_FATOORA_METHODS";
    public static final String SEND_PAYMENT = "SEND_PAYMENT";
    public static final String PAYMENT_ERROR = "PAYMENT_ERROR";
    public static final String CHECK_PAYMENT = "CHECK_PAYMENT";
    public static final String ONLINE = "ONLINE";
    public static final String CASH = "CASH";

    //PUSHER
//    public static final String PUSHER_TOKEN_URL = "https://amrk1.my-staff.net/api/user/generate-beams-token";
    public static final String PUSHER_TOKEN_URL = URLS.BASE_URL.concat("user/generate-beams-token");
    public static final String INTEREST = "users";
    public static final String PUSHER_LIVE_DATA = "PUSHER_CHAT";

    public static final String tripEventName = "App\\Events\\SendTripEvent";
    public static final String USERSBeamName = "users-";
    public static final String tripChannelName = "trip-";

    //Chat
    public static final String chatEventName = "App\\Events\\SendMessageEvent";
    public static final String chatChannelName = "message-";

    //NEW_OFFER
    public static final String NEWOFFEREventName = "App\\Events\\OffersEvent";
    public static final String NEWOFFERChannelName = "offers-";

    //ORDER_CHAT
    public static final String ORDER_CHAT_EVENT = "App\\Events\\OrdersChatEvent";
    public static final String ORDER_CHAT_CHANNEL = "order-chat-";


}

