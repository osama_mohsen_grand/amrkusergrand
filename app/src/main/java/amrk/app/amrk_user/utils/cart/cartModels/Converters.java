package amrk.app.amrk_user.utils.cart.cartModels;

import androidx.room.TypeConverter;

import com.google.gson.Gson;

import java.util.List;

import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;

public class Converters {
    @TypeConverter
    public static String convertVariations(List<VariationsItem> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

    @TypeConverter
    public static String convertOptions(List<OptionsItem> list) {
        Gson gson = new Gson();
        String json = gson.toJson(list);
        return json;
    }

}
