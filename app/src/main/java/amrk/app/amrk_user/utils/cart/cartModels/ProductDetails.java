package amrk.app.amrk_user.utils.cart.cartModels;

import android.os.Parcel;
import android.os.Parcelable;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

@Entity(tableName = "product")
public class ProductDetails implements Parcelable {

    @SerializedName("image")
    private String image;

    @SerializedName("quantity")
    private int quantity;

    @SerializedName("price_after")
    private String priceAfter;

    @SerializedName("description")
    private String description;

    @SerializedName("name")
    private String name;

    @PrimaryKey(autoGenerate = true)
    private int product_room_id;
    @SerializedName("id")
    private int product_id;
    private double priceItem;

    public ProductDetails() {
    }

    protected ProductDetails(Parcel in) {
        image = in.readString();
        quantity = in.readInt();
        priceAfter = in.readString();
        description = in.readString();
        name = in.readString();
        product_room_id = in.readInt();
        product_id = in.readInt();
        priceItem = in.readDouble();
    }

    public static final Creator<ProductDetails> CREATOR = new Creator<ProductDetails>() {
        @Override
        public ProductDetails createFromParcel(Parcel in) {
            return new ProductDetails(in);
        }

        @Override
        public ProductDetails[] newArray(int size) {
            return new ProductDetails[size];
        }
    };

    public double getPriceItem() {
        return priceItem;
    }

    public void setPriceItem(double priceItem) {
        this.priceItem = priceItem;
    }

    public String getImage() {
        return image;
    }

    public int getQuantity() {
        return quantity;
    }

    public String getPriceAfter() {
        return priceAfter;
    }

    public String getDescription() {
        return description;
    }


    public String getName() {
        return name;
    }


    public int getProduct_room_id() {
        return product_room_id;
    }

    public void setProduct_room_id(int product_room_id) {
        this.product_room_id = product_room_id;
    }

    public int getProduct_id() {
        return product_id;
    }


    public void setImage(String image) {
        this.image = image;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public void setPriceAfter(String priceAfter) {
        this.priceAfter = priceAfter;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public void setName(String name) {
        this.name = name;
    }


    public void setProduct_id(int product_id) {
        this.product_id = product_id;
    }


    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(image);
        dest.writeInt(quantity);
        dest.writeString(priceAfter);
        dest.writeString(description);
        dest.writeString(name);
        dest.writeInt(product_room_id);
        dest.writeInt(product_id);
        dest.writeDouble(priceItem);
    }
}