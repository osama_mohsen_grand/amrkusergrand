package amrk.app.amrk_user.utils.cart.cartModels;

import androidx.room.Entity;
import androidx.room.ForeignKey;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.util.List;

import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;

@Entity(tableName = "variations", foreignKeys = @ForeignKey(entity = ProductDetails.class,
        parentColumns = "product_room_id",
        childColumns = "productIdForeignKey",
        onDelete = ForeignKey.CASCADE))
public class VariationsItem {

    @SerializedName("product_id")
    private int productId;
    private int productIdForeignKey;
    @SerializedName("name")
    @Ignore
    private String name;

    @SerializedName("options")
    @Ignore
    private List<OptionsItem> options;
    private String variations;
    @SerializedName("id")
    @Ignore
    private int variation_id;
    @PrimaryKey(autoGenerate = true)
    private int variation_room_id;
    @SerializedName("type")
    @Ignore
    private Object type;

    @SerializedName("required")
    @Ignore
    private Object required;

    public VariationsItem(int productId, int productIdForeignKey, String variations) {
        this.productId = productId;
        this.productIdForeignKey = productIdForeignKey;
        this.variations = variations;
    }

    public String getVariations() {
        return variations;
    }

    public void setVariations(String variations) {
        this.variations = variations;
    }

    public int getProductIdForeignKey() {
        return productIdForeignKey;
    }

    public void setProductIdForeignKey(int productIdForeignKey) {
        this.productIdForeignKey = productIdForeignKey;
    }

    public int getProductId() {
        return productId;
    }

    public String getName() {
        return name;
    }

    public List<OptionsItem> getOptions() {
        return options;
    }

    public int getVariation_room_id() {
        return variation_room_id;
    }

    public void setVariation_room_id(int variation_room_id) {
        this.variation_room_id = variation_room_id;
    }

    public int getVariation_id() {
        return variation_id;
    }

    public Object getType() {
        return type;
    }

    public Object getRequired() {
        return required;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setOptions(List<OptionsItem> options) {
        this.options = options;
    }

    public void setVariation_id(int variation_id) {
        this.variation_id = variation_id;
    }

    public void setType(Object type) {
        this.type = type;
    }

    public void setRequired(Object required) {
        this.required = required;
    }
}