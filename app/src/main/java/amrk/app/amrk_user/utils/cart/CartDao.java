package amrk.app.amrk_user.utils.cart;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;

import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;

@Dao
public interface CartDao {
    //Product Operations
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long addProduct(ProductDetails productDetails);

    @Query("select * from product")
    LiveData<List<ProductDetails>> getProducts();

    @Query("select * from variations")
    LiveData<List<VariationsItem>> getVariations();

    @Query("select * from options")
    LiveData<List<OptionsItem>> getOptions();

    @Query("select sum(priceAfter) from product")
    LiveData<String> getCartTotal();

    @Query("DELETE FROM PRODUCT WHERE product_room_id=:productId")
    void deleteItem(int productId);

    @Query("DELETE FROM PRODUCT")
    void emptyProductCart();

    @Query("DELETE FROM PRODUCT WHERE product_id=:productId")
    void deleteOneProductFromCart(int productId);

    @Update
    void updateProduct(ProductDetails productDetails);

    @Query("UPDATE PRODUCT SET quantity=quantity+:quantity where product_id=:productId")
    void updateProductQuantity(int quantity, int productId);

    @Query("UPDATE PRODUCT SET quantity=:quantity where product_id=:productId")
    void updateProductFromCartQuantity(int quantity, int productId);

    // Variations Operations
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long addVariations(VariationsItem variationsItem);

    // Options Operations
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void addOption(OptionsItem optionsItem);

    //    @Query("SELECT EXISTS (SELECT * FROM OPTIONS WHERE option_id=:optionId)")
    @Query("SELECT COUNT(product_id) FROM PRODUCT WHERE" +
            " EXISTS (SELECT * FROM VARIATIONS WHERE variations=:variations)" +
            "AND  EXISTS (SELECT * FROM OPTIONS WHERE options=:options)")
    int getIfOptionExists(String options, String variations);

    @Query("SELECT COUNT(product_id) FROM PRODUCT WHERE product_id=:productId")
    int getIfProductExist(int productId);

}
