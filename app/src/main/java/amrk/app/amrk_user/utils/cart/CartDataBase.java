package amrk.app.amrk_user.utils.cart;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverters;

import amrk.app.amrk_user.pages.markets.models.productDialogModel.OptionsItem;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.ProductDetails;
import amrk.app.amrk_user.pages.markets.models.productDialogModel.VariationsItem;
import amrk.app.amrk_user.utils.cart.cartModels.Converters;

@Database(entities = {ProductDetails.class, VariationsItem.class, OptionsItem.class}, version = 21)
@TypeConverters({Converters.class})
public abstract class CartDataBase extends RoomDatabase {
    private static CartDataBase instance;

    public abstract CartDao cartDao();

    public static synchronized CartDataBase getInstance(Context context) {
        if (instance == null)
            instance = Room.databaseBuilder(context.getApplicationContext(), CartDataBase.class, "cart_db")
                    .fallbackToDestructiveMigration() // we do it for increase db version
                    .build();
        return instance;
    }
}
