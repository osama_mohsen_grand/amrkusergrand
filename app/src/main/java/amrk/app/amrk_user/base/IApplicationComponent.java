package amrk.app.amrk_user.base;

import javax.inject.Singleton;

import dagger.Component;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.activity.MainActivity;
import amrk.app.amrk_user.activity.SupportActivity;
import amrk.app.amrk_user.connection.ConnectionModule;
import amrk.app.amrk_user.pages.Categories.CategoriesFragment;
import amrk.app.amrk_user.pages.appWallet.AppWalletFragment;
import amrk.app.amrk_user.pages.auth.changePassword.ChangePasswordFragment;
import amrk.app.amrk_user.pages.auth.confirmCode.ConfirmCodeFragment;
import amrk.app.amrk_user.pages.auth.forgetPassword.ForgetPasswordFragment;
import amrk.app.amrk_user.pages.auth.login.LoginFragment;
import amrk.app.amrk_user.pages.auth.register.ProviderRegisterFragment;
import amrk.app.amrk_user.pages.auth.register.RegisterFragment;
import amrk.app.amrk_user.pages.cart.CartFragment;
import amrk.app.amrk_user.pages.chat.view.ChatFragment;
import amrk.app.amrk_user.pages.chatAdmin.view.ChatAdminFragment;
import amrk.app.amrk_user.pages.countries.UserDetectLocation;
import amrk.app.amrk_user.pages.gifts.GiftsFragment;
import amrk.app.amrk_user.pages.heavyDepartment.HeavyFormFragment;
import amrk.app.amrk_user.pages.heavyOrders.CurrentHeavyOrdersFragment;
import amrk.app.amrk_user.pages.heavyOrders.MyHeavyOrdersMainFragment;
import amrk.app.amrk_user.pages.heavyOrders.PreviousHeavyOrdersFragment;
import amrk.app.amrk_user.pages.home.HomeFragment;
import amrk.app.amrk_user.pages.home.HomeMainFragment;
import amrk.app.amrk_user.pages.home.SearchFragment;
import amrk.app.amrk_user.pages.markets.FragmentMenuItemSheetDialog;
import amrk.app.amrk_user.pages.markets.MarketDetailsFragment;
import amrk.app.amrk_user.pages.markets.MarketsFragment;
import amrk.app.amrk_user.pages.myAccount.MyAccountFragment;
import amrk.app.amrk_user.pages.myFatora.MyFatooraMethodFragment;
import amrk.app.amrk_user.pages.myFatora.SendPaymentFragment;
import amrk.app.amrk_user.pages.myOrders.FollowUpOrderFragment;
import amrk.app.amrk_user.pages.myOrders.MyOrdersMainFragment;
import amrk.app.amrk_user.pages.myOrders.NormalOrdersFragment;
import amrk.app.amrk_user.pages.myOrders.ReorderFragment;
import amrk.app.amrk_user.pages.myOrders.StoreOrdersFragment;
import amrk.app.amrk_user.pages.newOrder.NewOrderFragment;
import amrk.app.amrk_user.pages.newOrder.SavedLocationsFragment;
import amrk.app.amrk_user.pages.notifications.NotificationsFragment;
import amrk.app.amrk_user.pages.offers.OffersDetailsFragment;
import amrk.app.amrk_user.pages.offers.OffersFragment;
import amrk.app.amrk_user.pages.onBoard.OnBoardFragment;
import amrk.app.amrk_user.pages.profile.ProfileFragment;
import amrk.app.amrk_user.pages.publicOrder.FragmentPublicOrderConfirm;
import amrk.app.amrk_user.pages.publicOrder.PublicNewOrderFragment;
import amrk.app.amrk_user.pages.publicOrder.PublicOrdersFragment;
import amrk.app.amrk_user.pages.reviews.ReviewsFragment;
import amrk.app.amrk_user.pages.settings.AboutAppFragment;
import amrk.app.amrk_user.pages.settings.ContactUsFragment;
import amrk.app.amrk_user.pages.settings.LangFragment;
import amrk.app.amrk_user.pages.settings.TermsFragment;
import amrk.app.amrk_user.pages.splash.SplashFragment;
import amrk.app.amrk_user.uber.chat.view.ChatUberFragment;
import amrk.app.amrk_user.uber.favorites.FavoritesLocationsFragment;
import amrk.app.amrk_user.uber.favorites.SearchForLocationsFragment;
import amrk.app.amrk_user.uber.favorites.SendFavoriteLocationFragment;
import amrk.app.amrk_user.uber.history.HistoryDetailsFragment;
import amrk.app.amrk_user.uber.history.HistoryFragment;
import amrk.app.amrk_user.uber.history.LostItemFragment;
import amrk.app.amrk_user.uber.history.ReportIssueFragment;
import amrk.app.amrk_user.uber.history.SendIssueFragment;
import amrk.app.amrk_user.uber.home.AddDropFragment;
import amrk.app.amrk_user.uber.home.HomeUberFragment;
import amrk.app.amrk_user.uber.paymentMethods.PaymentMethodsFragment;
import amrk.app.amrk_user.utils.locations.MapAddressActivity;

//Component refer to an interface or waiter for make an coffee cup to me
@Singleton
@Component(modules = {ConnectionModule.class, LiveData.class})
public interface IApplicationComponent {
    void inject(MainActivity mainActivity);

    void inject(BaseActivity tmpActivity);

    void inject(SupportActivity supportActivity);

    void inject(MapAddressActivity mapAddressActivity);

    void inject(SplashFragment splashFragment);

    void inject(OnBoardFragment onBoardFragment);

    void inject(UserDetectLocation countriesFragment);

    void inject(LoginFragment loginFragment);

    void inject(ForgetPasswordFragment forgetPasswordFragment);

    void inject(ConfirmCodeFragment confirmCodeFragment);

    void inject(ChangePasswordFragment changePasswordFragment);

    void inject(RegisterFragment registerFragment);

    void inject(ProviderRegisterFragment providerRegisterFragment);

    void inject(HomeFragment homeFragment);

    void inject(SearchFragment searchFragment);

    void inject(HomeMainFragment mainFragment);

    void inject(CategoriesFragment categoriesFragment);

    void inject(OffersFragment offersFragment);

    void inject(OffersDetailsFragment offersDetailsFragment);

    void inject(AppWalletFragment appWalletFragment);

    void inject(MyAccountFragment myAccountFragment);

    void inject(MyOrdersMainFragment ordersMainFragment);

    void inject(StoreOrdersFragment storeOrdersFragment);

    void inject(NormalOrdersFragment normalOrdersFragment);

    void inject(FollowUpOrderFragment followUpOrderFragment);

    void inject(GiftsFragment giftsFragment);

    void inject(ProfileFragment profileFragment);

    void inject(ContactUsFragment contactUsFragment);

    void inject(AboutAppFragment aboutAppFragment);

    void inject(LangFragment langFragment);

    void inject(TermsFragment termsFragment);

    void inject(MarketsFragment marketsFragment);

    void inject(MarketDetailsFragment marketDetailsFragment);

    void inject(ReviewsFragment reviewsFragment);

    void inject(CartFragment cartFragment);

    void inject(NewOrderFragment newOrderFragment);

    void inject(SavedLocationsFragment savedLocationsFragment);

    void inject(PublicOrdersFragment publicOrdersFragment);

    void inject(PublicNewOrderFragment publicNewOrderFragment);

    void inject(FragmentPublicOrderConfirm fragmentPublicOrderConfirm);

    void inject(NotificationsFragment notificationsFragment);

    void inject(PaymentMethodsFragment paymentMethodsFragment);

    void inject(FragmentMenuItemSheetDialog fragmentMenuItemSheetDialog);

    void inject(ChatFragment chatFragment);

    void inject(ChatAdminFragment chatAdminFragment);

    void inject(ReorderFragment reorderFragment);

    void inject(HistoryFragment historyFragment);

    void inject(HistoryDetailsFragment historyDetailsFragment);

    void inject(LostItemFragment lostItemFragment);

    void inject(ReportIssueFragment reportIssueFragment);

    void inject(SendIssueFragment sendIssueFragment);

    void inject(HomeUberFragment homeUberFragment);

    void inject(AddDropFragment addDropFragment);

    void inject(ChatUberFragment chatUberFragment);

    void inject(CurrentHeavyOrdersFragment currentOrdersFragment);

    void inject(PreviousHeavyOrdersFragment previousOrdersFragment);

    void inject(MyHeavyOrdersMainFragment myHeavyOrdersMainFragment);

    void inject(HeavyFormFragment heavyFormFragment);

    void inject(FavoritesLocationsFragment favoritesLocationsFragment);

    void inject(SearchForLocationsFragment searchForLocationsFragment);

    void inject(SendFavoriteLocationFragment sendFavoriteLocationFragment);

    void inject(MyFatooraMethodFragment paymentMethodFragment);
    void inject(SendPaymentFragment sendPaymentFragment);


    @Component.Builder
    interface Builder {
        IApplicationComponent build();
    }
}
