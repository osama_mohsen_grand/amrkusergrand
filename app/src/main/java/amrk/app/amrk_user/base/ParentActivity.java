package amrk.app.amrk_user.base;

import android.Manifest;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.MutableLiveData;

import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.location.LocationSettingsStatusCodes;
import com.google.android.gms.location.SettingsClient;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.Task;
import com.google.android.material.snackbar.Snackbar;
import com.google.firebase.messaging.FirebaseMessaging;
import es.dmoral.toasty.Toasty;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.model.base.Mutable;
import amrk.app.amrk_user.pages.auth.login.LoginFragment;
import amrk.app.amrk_user.pages.chat.model.Chat;
import amrk.app.amrk_user.pages.chatAdmin.model.ChatAdmin;
import amrk.app.amrk_user.pages.offers.models.offerDetails.OfferDetails;
import amrk.app.amrk_user.uber.chat.model.ChatData;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.services.RealTimeReceiver;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import amrk.app.amrk_user.utils.session.MyContextWrapper;
import amrk.app.amrk_user.utils.session.UserHelper;

public class ParentActivity extends AppCompatActivity implements
        ConnectivityReceiver.ConnectivityReceiverListener, RealTimeReceiver.MessageReceiverListener
        , RealTimeReceiver.MessageAdminReceiverListener, RealTimeReceiver.OfferDetailsReceiverListener, RealTimeReceiver.NewTripReceiverListener, RealTimeReceiver.MessageUberReceiverListener, RealTimeReceiver.NotificationCounterListener {
    ConnectivityReceiver connectivityReceiver = new ConnectivityReceiver();
    public MutableLiveData<Boolean> connectionMutableLiveData = new MutableLiveData<>();
    public MutableLiveData<Integer> notificationsCount = new MutableLiveData<>();
    RealTimeReceiver realTimeReceiver = new RealTimeReceiver();
    public LocationManager locationManager;
    public MutableLiveData<Boolean> locationLiveData;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        initializeLanguage();
        super.onCreate(savedInstanceState);
        locationLiveData = new MutableLiveData<>();
        initializeToken();
        initializeProgress();
//        initializeDarkMode();
    }

    private void initializeDarkMode() {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
    }

    public void enableLocationDialog() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1001);
        }
        try {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER,
                    2000,
                    10, locationListenerGPS);
            LocationRequest locationRequest = getLocationRequest();
            LocationSettingsRequest settingsRequest = new LocationSettingsRequest.Builder()
                    .addLocationRequest(locationRequest).build();
            SettingsClient client = LocationServices.getSettingsClient(this);
            Task<LocationSettingsResponse> task = client
                    .checkLocationSettings(settingsRequest);

            task.addOnFailureListener(this, e -> {
                int statusCode = ((ApiException) e).getStatusCode();
                if (statusCode == LocationSettingsStatusCodes.RESOLUTION_REQUIRED) {
                    try {
                        ResolvableApiException resolvable =
                                (ResolvableApiException) e;
                        resolvable.startResolutionForResult
                                (this,
                                        1019);
                    } catch (IntentSender.SendIntentException sendEx) {
                        sendEx.printStackTrace();
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private LocationRequest getLocationRequest() {
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(10000);
        locationRequest.setFastestInterval(5000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return locationRequest;
    }

    public LocationListener locationListenerGPS = new LocationListener() {
        @Override
        public void onLocationChanged(android.location.Location location) {
            double latitude = location.getLatitude();
            double longitude = location.getLongitude();
            UserHelper.getInstance(MyApplication.getInstance()).addSaveLastKnownLocation(new LatLng(latitude, longitude));
            locationLiveData.setValue(true);
        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onProviderDisabled(String provider) {

        }
    };

    protected boolean notification_checked = false;

    protected void initializeLanguage() {
        LanguagesHelper.changeLanguage(this, LanguagesHelper.getCurrentLanguage());
        LanguagesHelper.changeLanguage(MyApplication.getInstance(), LanguagesHelper.getCurrentLanguage());
    }

    protected void initializeToken() {
        FirebaseMessaging.getInstance().getToken()
                .addOnCompleteListener(task -> {
                    if (!task.isSuccessful()) {
                        return;
                    }
                    UserHelper.getInstance(MyApplication.getInstance()).addToken(task.getResult());
                });
    }

    @Override
    public void onResume() {
        super.onResume();
        MyApplication.getInstance().setOfferReceiverListener(this);
        MyApplication.getInstance().setConnectivityListener(this);
        MyApplication.getInstance().setMessageReceiverListener(this);
        MyApplication.getInstance().setMessageAdminReceiverListener(this);
        MyApplication.getInstance().setMessageUberReceiverListener(this);
        MyApplication.getInstance().setTripReceiverListener(this);
        MyApplication.getInstance().setNotificationsCount(this);

    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE");
        intentFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(connectivityReceiver, intentFilter);

        IntentFilter chatFilter = new IntentFilter("app.te.receiver");
        chatFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(realTimeReceiver, chatFilter);

        IntentFilter notifiFilter = new IntentFilter("app.te.receiver");
        notifiFilter.addCategory(Intent.CATEGORY_DEFAULT);
        registerReceiver(realTimeReceiver, notifiFilter);
    }


    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(connectivityReceiver);
        unregisterReceiver(realTimeReceiver);
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.N_MR1) {
            super.attachBaseContext(MyContextWrapper.wrap(newBase, LanguagesHelper.getCurrentLanguage()));
        } else {
            super.attachBaseContext(newBase);
        }
    }

    protected Dialog dialogLoader;

    public void initializeProgress() {
        View view = LayoutInflater.from(this).inflate(R.layout.loader_animation, null);
        final AlertDialog.Builder builder = new AlertDialog.Builder(this, R.style.customDialog);
        builder.setView(view);
        dialogLoader = builder.create();
        dialogLoader.setOnCancelListener(dialogInterface -> {
            dialogLoader.dismiss();
        });
    }

    public void showProgress(boolean isAllowCancel) {
        //show dialog
        if (dialogLoader != null && !this.isFinishing()) {
            dialogLoader.show();
        }
        if(dialogLoader != null) dialogLoader.setCancelable(isAllowCancel);
    }

    public void hideProgress() {
        if (dialogLoader != null && dialogLoader.isShowing() && !this.isFinishing())
            dialogLoader.dismiss();
    }


    public void handleActions(Mutable mutable) {
        Log.e("handleActions", "handleActions: " + mutable.message);
        if (mutable.message.equals(Constants.SHOW_PROGRESS)) showProgress(true);
        else if (mutable.message.equals(Constants.HIDE_PROGRESS)) hideProgress();
        else if (mutable.message.equals(Constants.SHOW_PROGRESS_FORCE)) showProgress(false);
        else if (mutable.message.equals(Constants.HIDE_PROGRESS_FORCE)) hideProgress();
        else if (mutable.message.equals(Constants.SERVER_ERROR) && mutable.object == null) {
            hideProgress();
            showError(ResourceManager.getString(R.string.msg_server_error));
        } else if (mutable.message.equals(Constants.ERROR) && mutable.object instanceof String) {
            hideProgress();
            showError((String) mutable.object);
        } else if (mutable.message.equals(Constants.NOT_VERIFIED) && mutable.object instanceof String) {
            hideProgress();
            showError((String) mutable.object);
        } else if (mutable.message.equals(Constants.ERROR_TOAST) && mutable.object instanceof String) {
            toastError((String) mutable.object);
        } else if (mutable.message.equals(Constants.FAILURE_CONNECTION)) {
            hideProgress();
        } else if (mutable.message.equals(Constants.LOGOUT)) {
            if (UserHelper.getInstance(this).getUserData() != null) {
                try {
                    // clearing app data
                    Runtime runtime = Runtime.getRuntime();
                    runtime.exec("pm clear" + MyApplication.getInstance().getPackageName());
                    UserHelper.getInstance(this).logout();
                    MovementHelper.startActivityBase(this, LoginFragment.class.getName(), null, null);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        Log.e("handleActions", "handleActions: End" + mutable.message);
    }

    public void noConnection() {
        Resources resources = getResources();
        toastError(resources.getString(R.string.connection_invaild_body)); // Optional
    }

    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {
        if (isConnected) {

        } else {
            noConnection();
        }
    }

    public void showError(String msg) {
        Snackbar snackBar = Snackbar.make(findViewById(R.id.ll_base_container),
                msg, Snackbar.LENGTH_LONG);
        View view = snackBar.getView();
        view.setBackgroundColor(ContextCompat.getColor(this, R.color.colorAccent));
        TextView textView = view.findViewById(R.id.snackbar_text);
        textView.setGravity(Gravity.CENTER_VERTICAL);
        textView.setTextColor(ContextCompat.getColor(this, R.color.white));
        snackBar.show();
    }

    public void toastMessage(String message, int icon, int color) {
        Toasty.custom(this, message, icon, color, Toasty.LENGTH_SHORT, true, true).show();
    }

    public void toastError(String message) {
        Toasty.error(this, message, Toasty.LENGTH_SHORT, true).show();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (resultCode == RESULT_OK) {
                Fragment fragment = getSupportFragmentManager().findFragmentById(R.id.fl_home_container);
                if (fragment != null)
                    fragment.onActivityResult(requestCode, resultCode, data);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    @Override
    public void onNotificationsCounter(int count) {
        notificationsCount.setValue(count);
    }

    @Override
    public void onMessageChanged(Chat messagesItem) {
    }

    @Override
    public void onMessageChanged(ChatAdmin messagesItem) {

    }

    @Override
    public void onNewOrderChanged(OfferDetails offerDetails) {

    }

    @Override
    public void onTripChanged(String dataFromNotification) {

    }

    @Override
    public void onMessageUberChanged(ChatData messagesItem) {

    }
}
