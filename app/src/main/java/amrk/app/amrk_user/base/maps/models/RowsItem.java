package amrk.app.amrk_user.base.maps.models;

import java.util.List;
import com.google.gson.annotations.SerializedName;

public class RowsItem{

	@SerializedName("elements")
	private List<ElementsItem> elements;

	public List<ElementsItem> getElements(){
		return elements;
	}
}