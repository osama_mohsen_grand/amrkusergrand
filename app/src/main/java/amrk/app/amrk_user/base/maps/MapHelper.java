package amrk.app.amrk_user.base.maps;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.location.LocationManager;
import android.os.Build;
import android.os.Looper;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowMetrics;

import androidx.core.app.ActivityCompat;

import com.firebase.geofire.GeoFire;
import com.firebase.geofire.GeoLocation;
import com.firebase.geofire.GeoQuery;
import com.firebase.geofire.GeoQueryDataEventListener;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationResult;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MapStyleOptions;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.jetbrains.annotations.NotNull;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.MainActivity;
import amrk.app.amrk_user.base.MyApplication;
import amrk.app.amrk_user.base.ParentActivity;
import amrk.app.amrk_user.base.maps.models.ElementsItem;
import amrk.app.amrk_user.uber.home.models.NearestDrivers;
import amrk.app.amrk_user.uber.home.models.NearestDriversMarkers;
import amrk.app.amrk_user.uber.home.viewModels.HomeViewModels;
import amrk.app.amrk_user.utils.Constants;
import amrk.app.amrk_user.utils.helper.AppHelper;
import amrk.app.amrk_user.utils.locations.MapAddress;
import amrk.app.amrk_user.utils.locations.MapAddressInterface;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.UserHelper;

public class MapHelper {
    public double driverLat, driverLng;
    public MarkerOptions markerOptionsDriver;
    public Marker driverMarker = null, CurrentLocationSelectedMarker = null;
    public LocationManager locationManager;
    public GoogleMap mMap;
    public Context context;
    private LocationCallback locationCallback;
    public View markerCustom = null;
    public HomeViewModels homeViewModels;
    public DatabaseReference databaseReference;
    public ValueEventListener valueEventListener;
    public List<NearestDrivers> nearestDrivers = new ArrayList<>();
    public List<NearestDriversMarkers> nearestDriversMarker = new ArrayList<>();
    public GeoQuery geoQuery;
    LastLocationModel lastDriverLocation;
    CarAnimation carAnimation;
    public ArrayList<LatLng> latLngs = new ArrayList<>();

    public MapHelper(GoogleMap mMap, Context context, HomeViewModels homeViewModels) {
        this.mMap = mMap;
        this.context = context;
        this.homeViewModels = homeViewModels;
        carAnimation = new CarAnimation(mMap);
//        setMapStyle();
    }

    public void requestLocationUpdates() {
        locationManager = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        // for listen changes in locations
        getLocationCallBack();
        // for first time open app
        getLastKnownLocation();
    }

    public void addMarker(LatLng position) {
        latLngs.clear();
        Log.e("addMarker", "addMarker: " + driverMarker);
        if (driverMarker == null) {
            markerOptionsDriver = new MarkerOptions();
            markerOptionsDriver.draggable(true);
            markerOptionsDriver.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.car_type), ResourceManager.getDrawable(R.drawable.car_type).getMinimumWidth(), ResourceManager.getDrawable(R.drawable.car_type).getMinimumHeight())));
            markerOptionsDriver.anchor(0.5f, 0.5f);
            markerOptionsDriver.position(position);
            driverMarker = mMap.addMarker(markerOptionsDriver);
        } else {
            latLngs.add(position);
            latLngs.add(homeViewModels.end);
            if (homeViewModels.mid != null)
                latLngs.add(homeViewModels.mid);
        }
        carAnimation.animateMarker(position, driverMarker, latLngs);
    }

    @SuppressLint("InflateParams")
    public void addUserMarker(LatLng position) {
        if (markerCustom == null)
            markerCustom = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(R.layout.user_custom_marker, null);
        MarkerOptions markerOptionsFirst = new MarkerOptions();
        markerOptionsFirst.draggable(true);
        markerOptionsFirst.position(position);
        markerOptionsFirst.anchor(0.5f, 0.5f);
        if (homeViewModels.getTripDataFromNotifications() != null && (homeViewModels.getTripDataFromNotifications().getStatus() == 2 || homeViewModels.getTripDataFromNotifications().getStatus() == 5))
            markerOptionsFirst.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.ic_location_destination), 100, 100)));
        else
            markerOptionsFirst.icon(BitmapDescriptorFactory.fromBitmap(createDrawableFromView(context, markerCustom)));

        if (CurrentLocationSelectedMarker != null)
            CurrentLocationSelectedMarker.remove();
        CurrentLocationSelectedMarker = mMap.addMarker(markerOptionsFirst);
    }

    public void getLessDriverTime(List<ElementsItem> elementsItemList) {
        String minTime = "";
        long duration = 10423; // 33 min
        for (int i = 0; i < elementsItemList.size(); i++) {
            if (elementsItemList.get(i).getDuration() != null)
                if (elementsItemList.get(i).getDuration().getValue() <= duration) {
                    duration = elementsItemList.get(i).getDuration().getValue();
                    minTime = elementsItemList.get(i).getDuration().getText();
                }
        }
        ((MainActivity) context).liveData.setValue(minTime);
    }

    public void animateCamera(LatLng position) {
        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(position, 13));
        CameraPosition cameraPosition = new CameraPosition.Builder().target(position).zoom(17).bearing(90).tilt(40).build();
        mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        driverLat = position.latitude;
        driverLng = position.longitude;
        addMarker(position);

    }

    private void getLastKnownLocation() {
        if (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        LocationRequest locationRequest = new LocationRequest();
        locationRequest.setInterval(1000);
        locationRequest.setFastestInterval(1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        LocationServices.getFusedLocationProviderClient(context)
                .requestLocationUpdates(locationRequest, locationCallback, Looper.getMainLooper());
        FusedLocationProviderClient fusedLocationClient = LocationServices.getFusedLocationProviderClient(MyApplication.getInstance());
        fusedLocationClient.getLastLocation()
                .addOnSuccessListener((Activity) context, location -> {
                    if (location != null) {
                        addMarker(new LatLng(location.getLatitude(), location.getLongitude()));
                    }
                });
    }

    private void getLocationCallBack() {
        locationCallback = new LocationCallback() {
            @Override
            public void onLocationResult(@NotNull LocationResult locationResult) {
                locationResult.getLastLocation();
                addMarker(new LatLng(locationResult.getLastLocation().getLatitude(), locationResult.getLastLocation().getLongitude()));
            }
        };
    }

    public void stopLocationCallBack() {
        if (locationCallback != null)
            LocationServices.getFusedLocationProviderClient(context)
                    .removeLocationUpdates(locationCallback);
    }

    public void getDriverLastLocation() {
        databaseReference = FirebaseDatabase.getInstance().getReference("Trips")
                .child(String.valueOf(UserHelper.getInstance(MyApplication.getInstance()).getTripId()))
                .child(String.valueOf(homeViewModels.getTripDataFromNotifications().getDriverId()));
        valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@org.jetbrains.annotations.NotNull DataSnapshot dataSnapshot) {
                lastDriverLocation = dataSnapshot.getValue(LastLocationModel.class);
                if (lastDriverLocation != null) {
                    addMarker(new LatLng(lastDriverLocation.getLat(), lastDriverLocation.getLng()));
                } else {
                    if (homeViewModels.getTripDataFromNotifications() != null && homeViewModels.getTripDataFromNotifications().getUser() != null)
                        addMarker(new LatLng(Double.parseDouble(homeViewModels.getTripDataFromNotifications().getUser().getLat()), Double.parseDouble(homeViewModels.getTripDataFromNotifications().getUser().getLng())));
                }
            }

            @Override
            public void onCancelled(@org.jetbrains.annotations.NotNull DatabaseError databaseError) {
                Log.e("TAG", "loadPost:onCancelled", databaseError.toException());
            }
        };
        databaseReference.addValueEventListener(valueEventListener);
    }

    public void getAddress(final double lat, final double lng, MapAddressInterface mapAddressInterface) {
        MapAddress mapAddress = new MapAddress((Activity) context, lat, lng);
        mapAddress.getAddressFromUrl((address, city) -> {
            if (mapAddressInterface != null)
                mapAddressInterface.fetchFullAddress(address, city);
        });

    }

    public static void getAddress(Context context, final double lat, final double lng, MapAddressInterface mapAddressInterface) {
        MapAddress mapAddress = new MapAddress((Activity) context, lat, lng);
        mapAddress.getAddressFromUrl((address, city) -> {
            if (mapAddressInterface != null)
                mapAddressInterface.fetchFullAddress(address, city);
        });

    }

    // Convert a view to bitmap
    public static Bitmap createDrawableFromView(Context context, View view) {
        view.setLayoutParams(new ViewGroup.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT));
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.R) {
            WindowMetrics windowMetrics = ((Activity) context).getWindowManager().getCurrentWindowMetrics();
            view.measure(windowMetrics.getBounds().width(), windowMetrics.getBounds().height());
            view.layout(0, 0, windowMetrics.getBounds().width(), windowMetrics.getBounds().height());
        } else {
            DisplayMetrics displayMetrics = new DisplayMetrics();
            ((Activity) context).getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);
            view.measure(displayMetrics.widthPixels, displayMetrics.heightPixels);
            view.layout(0, 0, displayMetrics.widthPixels, displayMetrics.heightPixels);
        }
        view.buildDrawingCache();
        Bitmap bitmap = Bitmap.createBitmap(view.getMeasuredWidth(), view.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas canvas = new Canvas(bitmap);
        view.draw(canvas);

        return bitmap;
    }

    public void loginToFirebase(LatLng latLng) {
        String email = UserHelper.getInstance(MyApplication.getInstance()).getUserData().getEmail();
        String password = "12345678";

        FirebaseAuth.getInstance().signInWithEmailAndPassword(
                email, password).addOnCompleteListener(task -> {
            if (task.isSuccessful()) {
                getAllNearestDrivers(latLng);
            } else {
                FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password).addOnCompleteListener(task1 -> {
                    if (task1.isSuccessful()) {
                        getAllNearestDrivers(latLng);
                    }
                });
            }
        });
    }

    public void getAllNearestDrivers(LatLng location) {
        mMap.clear();
        DatabaseReference ref = FirebaseDatabase.getInstance().getReference("Drivers");
        GeoFire geoFire = new GeoFire(ref);
        geoQuery = geoFire.queryAtLocation(new GeoLocation(location.latitude, location.longitude), 10.6);
        geoQuery.addGeoQueryDataEventListener(new GeoQueryDataEventListener() {
            @Override
            public void onDataEntered(DataSnapshot dataSnapshot, GeoLocation location) {
                if (isMarkerFound(dataSnapshot.getKey()) == -1) {
                    nearestDrivers.add(new NearestDrivers(dataSnapshot.getKey(), new LatLng(location.latitude, location.longitude)));
                } else {
                    nearestDrivers.set(isMarkerFound(dataSnapshot.getKey()), new NearestDrivers(dataSnapshot.getKey(), new LatLng(location.latitude, location.longitude)));
                }
                addNearestDriversMarker(new LatLng(location.latitude, location.longitude), dataSnapshot.getKey());
            }

            @Override
            public void onDataExited(DataSnapshot dataSnapshot) {

            }

            @Override
            public void onDataMoved(DataSnapshot dataSnapshot, GeoLocation location) {

            }

            @Override
            public void onDataChanged(DataSnapshot dataSnapshot, GeoLocation location) {
                if (isMarkerFound(dataSnapshot.getKey()) == -1) {
                    nearestDrivers.add(new NearestDrivers(dataSnapshot.getKey(), new LatLng(location.latitude, location.longitude)));
                } else {
                    nearestDrivers.set(isMarkerFound(dataSnapshot.getKey()), new NearestDrivers(dataSnapshot.getKey(), new LatLng(location.latitude, location.longitude)));
                }
                addNearestDriversMarker(new LatLng(location.latitude, location.longitude), dataSnapshot.getKey());
            }

            @Override
            public void onGeoQueryReady() {
                homeViewModels.getLessDriverTime(nearestDrivers, location.latitude + "," + location.longitude);
            }

            @Override
            public void onGeoQueryError(DatabaseError error) {
            }

        });

    }

    protected void addNearestDriversMarker(LatLng position, String driverId) {
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.draggable(false);
        markerOptions.position(position);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeVectorIcon(ResourceManager.getDrawable(R.drawable.car_type), ResourceManager.getDrawable(R.drawable.car_type).getMinimumWidth(), ResourceManager.getDrawable(R.drawable.car_type).getMinimumHeight())));
        int markerIndex = isMarkerFound(driverId);
        if (markerIndex != -1) { //marker is exists
            nearestDriversMarker.get(markerIndex).getNearestDriverMarker().remove();
            nearestDriversMarker.remove(markerIndex);
        }
        nearestDriversMarker.add(new NearestDriversMarkers(driverId, mMap.addMarker(markerOptions)));
    }

    private int isMarkerFound(String driverId) {
        for (int i = 0; i < nearestDriversMarker.size(); i++) {
            if (nearestDriversMarker.get(i).getDriverId().equals(driverId)) {
                return i;
            }
        }
        return -1;
    }

    public void searchGoogelPlaces() {
        if (!com.google.android.libraries.places.api.Places.isInitialized()) {
            Places.initialize(context, ResourceManager.getString(R.string.google_map));
        }
        List<Place.Field> fields = Arrays.asList(com.google.android.libraries.places.api.model.Place.Field.NAME, Place.Field.ADDRESS,
                com.google.android.libraries.places.api.model.Place.Field.LAT_LNG);
        Intent intent = new Autocomplete.IntentBuilder(
                AutocompleteActivityMode.FULLSCREEN, fields)
                .build(context);
        ((ParentActivity) context).startActivityForResult(intent, Constants.AUTOCOMPLETE_REQUEST_CODE);
    }

    public void setMapStyle() {
        try {
            boolean isSuccess = mMap.setMapStyle(MapStyleOptions.loadRawResourceStyle(context, R.raw.map_style));
            if (!isSuccess)
                Log.e("errorMapStyle", "errorMap");
        } catch (Resources.NotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
