package amrk.app.amrk_user.base.maps;

public class LastLocationModel {
    private double lat;
    private double lng;

    public LastLocationModel(double lat, double lng) {
        this.lat = lat;
        this.lng = lng;
    }

    public LastLocationModel() {
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }
}
