package amrk.app.amrk_user.base;

import android.graphics.Color;
import android.net.Uri;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RatingBar;

import androidx.databinding.BindingAdapter;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.utils.helper.AppHelper;

public class ApplicationBinding {
    @BindingAdapter("imgUrl")
    public static void loadImage(ImageView imageView, Object image) {
        Log.e("loadImage", "loadImage: " + image);
        if (image instanceof String) {
            Glide.with(imageView.getContext())
                    .load((String) image)
                    .placeholder(R.drawable.logo)
                    .into(imageView);
        } else if (image instanceof Integer) {
            imageView.setImageResource((Integer) image);
        } else if (image instanceof Uri) {
            imageView.setImageURI((Uri) image);
        }
    }

    @BindingAdapter("imgOrderUrl")
    public static void loadOrderImage(ImageView imageView, Object image) {
        if (image instanceof String) {
            Glide.with(imageView.getContext())
                    .load((String) image)
                    .centerCrop()
                    .placeholder(R.drawable.border_gray)
                    .into(imageView);
        } else if (image instanceof Integer) {
            imageView.setImageResource((Integer) image);
        } else if (image instanceof Uri) {
            imageView.setImageURI((Uri) image);
        }
    }

    @BindingAdapter("img")
    public static void loadImg(ImageView imageView, Object image) {
        if (image instanceof String) {
            Picasso.get().load(String.valueOf(image)).networkPolicy(NetworkPolicy.NO_CACHE).placeholder(R.drawable.logo).into(imageView);
        }
    }


    @BindingAdapter("color")
    public static void color(ImageView imageView, String color) {
        if (color != null && !color.equals("") && color.charAt(0) == '#') {
            imageView.setBackgroundColor(Color.parseColor(color));
        }
    }


    @BindingAdapter({"app:adapter", "app:span", "app:orientation"})
    public static void getItemsV2Binding(RecyclerView recyclerView, RecyclerView.Adapter<?> itemsAdapter, String spanCount, String orientation) {
        if (orientation.equals("1"))
            AppHelper.initVerticalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        else
            AppHelper.initHorizontalRV(recyclerView, recyclerView.getContext(), Integer.parseInt(spanCount));
        recyclerView.setAdapter(itemsAdapter);
    }


    @BindingAdapter("rate")
    public static void setRate(final RatingBar ratingBar, String rate) {
        if (!TextUtils.isEmpty(rate))
            ratingBar.setRating(Float.parseFloat(rate));
    }

}
