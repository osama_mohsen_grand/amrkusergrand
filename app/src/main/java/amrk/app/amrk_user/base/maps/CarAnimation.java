package amrk.app.amrk_user.base.maps;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.ValueAnimator;
import android.view.animation.LinearInterpolator;

import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;

import java.util.List;

import amrk.app.amrk_user.base.MyApplication;

public class CarAnimation {
    //start new bearing
    GoogleMap googleMap;

    public CarAnimation(GoogleMap googleMap) {
        this.googleMap = googleMap;
    }

    //start new bearing
    private interface LatLngInterpolatorNew {
        LatLng interpolate(float fraction, LatLng a, LatLng b);

        class LinearFixed implements LatLngInterpolatorNew {
            @Override
            public LatLng interpolate(float fraction, LatLng a, LatLng b) {
                double lat = (b.latitude - a.latitude) * fraction + a.latitude;
                double lngDelta = b.longitude - a.longitude;
                // Take the shortest path across the 180th meridian.
                if (Math.abs(lngDelta) > 180) {
                    lngDelta -= Math.signum(lngDelta) * 360;
                }
                double lng = lngDelta * fraction + a.longitude;
                return new LatLng(lat, lng);
            }
        }
    }

    public void animateMarker(final LatLng endPosition, final Marker marker, List<LatLng> latLngs) {
        if (marker != null) {
            final LatLng startPosition = marker.getPosition();
            final LatLngInterpolatorNew latLngInterpolator = new LatLngInterpolatorNew.LinearFixed();

            ValueAnimator valueAnimator = ValueAnimator.ofFloat(0, 1);
            valueAnimator.setDuration(1000); // duration 3 second
            valueAnimator.setInterpolator(new LinearInterpolator());
            valueAnimator.addUpdateListener(animation -> {
                try {
                    float v = animation.getAnimatedFraction();
                    float bearing = getBearing(startPosition, endPosition);
                    LatLng newPosition = latLngInterpolator.interpolate(v, startPosition, endPosition);
                    marker.setPosition(newPosition);
                    if (latLngs.size() > 0) {
                        LatLngBounds.Builder builder = new LatLngBounds.Builder();
                        for (LatLng latLng : latLngs) {
                            builder.include(latLng);
                        }
                        int w = MyApplication.getInstance().getResources().getDisplayMetrics().heightPixels;
                        int padding = (int) (w * .10); // offset from edges of the map in pixels
                        LatLngBounds bounds = builder.build();
                        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                        googleMap.setOnMapLoadedCallback(() -> googleMap.animateCamera(cu));
                    } else
                        googleMap.animateCamera(CameraUpdateFactory.newCameraPosition(new CameraPosition.Builder()
                                .target(newPosition)
                                .zoom(20.0f)
                                .bearing(googleMap.getCameraPosition().bearing)
                                .build()));

                    marker.setRotation(getBearing(startPosition, endPosition));
                } catch (Exception ex) {
                    //I don't care atm..
                    ex.printStackTrace();
                }
            });
            valueAnimator.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
//
//                    if (marker != null) {
//                        marker.remove();
//                    }
//                    MarkerOptions markerOptions = new MarkerOptions();
//                    markerOptions.position(endPosition);
//                    markerOptions.icon(BitmapDescriptorFactory.fromBitmap(AppHelper.resizeIcon(R.drawable.car_type, 80, 80)));
//                    marker = googleMap.addMarker(markerOptions);

                }
            });
            valueAnimator.start();
        }
    }

    private float getBearing(LatLng begin, LatLng end) {
        double lat = Math.abs(begin.latitude - end.latitude);
        double lng = Math.abs(begin.longitude - end.longitude);

        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)));
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 90);
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (float) (Math.toDegrees(Math.atan(lng / lat)) + 180);
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (float) ((90 - Math.toDegrees(Math.atan(lng / lat))) + 270);
        return -1;
    }
    //end new bearing

}
