package amrk.app.amrk_user.base;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import androidx.databinding.Bindable;
import androidx.databinding.Observable;
import androidx.databinding.PropertyChangeRegistry;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import amrk.app.amrk_user.BR;
import amrk.app.amrk_user.PassingObject;
import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.pages.auth.models.UserData;
import amrk.app.amrk_user.pages.home.HomeMainFragment;
import amrk.app.amrk_user.utils.helper.MovementHelper;
import amrk.app.amrk_user.utils.images.PhotoFullPopupWindow;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.LanguagesHelper;
import amrk.app.amrk_user.utils.session.UserHelper;


public class BaseViewModel extends ViewModel implements Observable {
    private MutableLiveData<Object> mutableLiveData = new MutableLiveData<>();
    String message;
    PropertyChangeRegistry mCallBacks;
    private PassingObject passingObject = new PassingObject();
    String countryCurrency;
    public UserData userData = UserHelper.getInstance(MyApplication.getInstance()).getUserData();
    public String lang = LanguagesHelper.getCurrentLanguage();
    int searchProgressVisible = View.GONE;

    public BaseViewModel() {
        mCallBacks = new PropertyChangeRegistry();
    }

    public MutableLiveData<Object> getLiveData() {
        return mutableLiveData == null ? mutableLiveData = new MutableLiveData<>() : mutableLiveData;
    }

    @Bindable
    public String getMessage() {
        return message;
    }

    @Bindable
    public void setMessage(String message) {
        notifyChange(BR.message);
        this.message = message;
    }

    public String getString(int stringRes) {
        return ResourceManager.getString(stringRes);
    }

    @Override
    public void addOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        mCallBacks.add(callback);
    }

    @Override
    public void removeOnPropertyChangedCallback(OnPropertyChangedCallback callback) {
        mCallBacks.remove(callback);
    }


    public void notifyChange() {
        mCallBacks.notifyChange(this, 0);
    }

    public void notifyChange(int propertyId) {
        mCallBacks.notifyChange(this, propertyId);
    }

    public void goBack(Context context) {
        if (((BaseActivity) context).backActionBarView.flag == 1) {
            if (((ParentActivity) context).isTaskRoot()) {
                // This activity is at root of task, so launch main activity
                MovementHelper.startActivityBase(context, HomeMainFragment.class.getName(), null, null);
            } else {
                // This activity isn't at root of task, so just finish()
                ((Activity) context).finish();
            }
        } else
            ((Activity) context).finish();
    }
    @Bindable
    public int getSearchProgressVisible() {
        return searchProgressVisible;
    }

    @Bindable
    public void setSearchProgressVisible(int searchProgressVisible) {
        notifyChange(androidx.databinding.library.baseAdapters.BR.searchProgressVisible);
        this.searchProgressVisible = searchProgressVisible;
    }
    @Bindable
    public PassingObject getPassingObject() {
        return passingObject;
    }

    @Bindable
    public void setPassingObject(PassingObject passingObject) {
        notifyChange(BR.passingObject);
        this.passingObject = passingObject;
    }

    public String getCountryCurrency() {
        return UserHelper.getInstance(MyApplication.getInstance()).getCountryCurrency();
    }

    public void showImage(String imgUrl, View imageView) {
        new PhotoFullPopupWindow(MyApplication.getInstance(), R.layout.popup_photo_full, imageView, imgUrl, null);
    }
}
