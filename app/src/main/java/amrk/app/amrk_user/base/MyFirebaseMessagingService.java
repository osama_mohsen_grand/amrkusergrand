package amrk.app.amrk_user.base;

import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioAttributes;
import android.media.Ringtone;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.text.TextUtils;
import android.util.Log;

import androidx.annotation.RequiresApi;
import androidx.core.app.NotificationCompat;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.pusher.pushnotifications.fcm.MessagingService;

import org.jetbrains.annotations.NotNull;

import java.util.Map;

import amrk.app.amrk_user.R;
import amrk.app.amrk_user.activity.BaseActivity;
import amrk.app.amrk_user.activity.MainActivity;
import amrk.app.amrk_user.pages.auth.models.UserData;
import amrk.app.amrk_user.utils.resources.ResourceManager;
import amrk.app.amrk_user.utils.session.UserHelper;

import static androidx.core.app.NotificationCompat.DEFAULT_SOUND;
import static androidx.core.app.NotificationCompat.DEFAULT_VIBRATE;


public class MyFirebaseMessagingService extends MessagingService {

    @Override
    public void onNewToken(@NotNull String s) {
        super.onNewToken(s);
    }

    @RequiresApi(api = Build.VERSION_CODES.Q)
    @Override
    public void onMessageReceived(@NotNull RemoteMessage remoteMessage) {
//        super.onMessageReceived(remoteMessage);
        Log.e("onMessageReceived", "onMessageReceived: " + remoteMessage.getData());
//        if (remoteMessage.getData().get("chat") != null) {
//            showNotification(remoteMessage.getData());
//            Intent intent = new Intent();
//            intent.putExtra("chat", remoteMessage.getData().get("chat"));
//            intent.setAction("app.te.receiver");
//            sendBroadcast(intent);
//        }
//        if (remoteMessage.getData().get("new_offer") != null) {
//            showNotification(remoteMessage.getData());
//            Intent intent = new Intent();
//            intent.putExtra("new_offer", remoteMessage.getData().get("new_offer"));
//            intent.setAction("app.te.receiver");
//            sendBroadcast(intent);
//        }
//        if (remoteMessage.getData().get("trip_details") != null) {
//            showNotification(remoteMessage.getData());
//            Intent intent = new Intent();
//            intent.putExtra("trip_details", remoteMessage.getData().get("trip_details"));
//            intent.setAction("app.te.receiver");
//            sendBroadcast(intent);
//        }
//        if (remoteMessage.getData().get("uber_chat") != null) {
//            showNotification(remoteMessage.getData());
//            Intent intent = new Intent();
//            intent.putExtra("uber_chat", remoteMessage.getData().get("uber_chat"));
//            intent.setAction("app.te.receiver");
//            sendBroadcast(intent);
//        } else {
        UserHelper.getInstance(MyApplication.getInstance()).addCountNotification(UserHelper.getInstance(MyApplication.getInstance()).getCountNotification() + 1);
        Intent intent = new Intent();
        intent.putExtra("counter", UserHelper.getInstance(MyApplication.getInstance()).getCountNotification());
        intent.setAction("app.te.receiver");
        sendBroadcast(intent);
        showNotification(remoteMessage.getData());
//        }
    }


    private void showNotification(Map<String, String> notification) {
        String type = notification.get("type");
        Intent intent;
        if (!TextUtils.isEmpty(notification.get("type")) && type.equals("1")) {
            intent = new Intent(this, MainActivity.class);
        } else {
            intent = new Intent(this, BaseActivity.class);
        }
        intent.putExtra("is_notification", true);
        intent.putExtra("type", type);
        intent.putExtra("attribute_id", notification.get("attribute_id"));
        intent.putExtra("delegate_id", notification.get("delegate_id"));
        if (!TextUtils.isEmpty(notification.get("trip_id")))
            UserHelper.getInstance(MyApplication.getInstance()).addTripId(Integer.parseInt(notification.get("trip_id")));
        if (notification.get("wallet") != null) {
            UserData userData = UserHelper.getInstance(MyApplication.getInstance()).getUserData();
            userData.setWallet(notification.get("wallet"));
            UserHelper.getInstance(MyApplication.getInstance()).userLogin(userData);
        }

        // Set the Activity to start in a new, empty task
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        String channelId = "channelId";
        try {
            Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE + "://" + MyApplication.getInstance().getPackageName() + "/" + R.raw.notify_default);
            Ringtone r = RingtoneManager.getRingtone(getApplicationContext(), defaultSoundUri);
            r.play();
            NotificationCompat.Builder notificationBuilder =
                    new NotificationCompat.Builder(this, channelId)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle(ResourceManager.getString(R.string.app_name))
                            .setContentText(notification.get("body"))
                            .setPriority(Notification.PRIORITY_HIGH)
                            .setAutoCancel(true)
                            .setDefaults(DEFAULT_SOUND | DEFAULT_VIBRATE) //Important for heads-up notification
                            .setSound(defaultSoundUri)
                            .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                            .setContentIntent(pendingIntent);
            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
            // Since android Oreo notification channel is needed.
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                NotificationChannel channel = new NotificationChannel(channelId,
                        "Channel human readable title",
                        NotificationManager.IMPORTANCE_HIGH);
                AudioAttributes attributes = new AudioAttributes.Builder()
                        .setContentType(AudioAttributes.CONTENT_TYPE_SONIFICATION)
                        .setUsage(AudioAttributes.USAGE_NOTIFICATION)
                        .build();
                channel.enableVibration(true);
                channel.setSound(defaultSoundUri, attributes);
                channel.setVibrationPattern(new long[]{100, 200, 300, 400, 500, 400, 300, 200, 400});
                notificationManager.createNotificationChannel(channel);
            }
            notificationManager.notify(0, notificationBuilder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}