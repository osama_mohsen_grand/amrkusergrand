package amrk.app.amrk_user.base;

import androidx.databinding.DataBindingComponent;

public class ApplicationComponent implements DataBindingComponent {

    private ApplicationBinding applicationBinding = new ApplicationBinding();

    public ApplicationBinding getApplicationBinding() {
        return applicationBinding;
    }



}
