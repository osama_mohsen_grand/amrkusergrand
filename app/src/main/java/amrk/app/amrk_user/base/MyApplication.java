package amrk.app.amrk_user.base;

import android.app.Application;

import androidx.databinding.DataBindingUtil;

import amrk.app.amrk_user.utils.services.RealTimeReceiver;

public class MyApplication extends Application {
    IApplicationComponent applicationComponent;
    private static MyApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        mInstance = this;
        applicationComponent = DaggerIApplicationComponent.builder().build();
        DataBindingUtil.setDefaultComponent(new ApplicationComponent());
    }

    public void setConnectivityListener(ConnectivityReceiver.ConnectivityReceiverListener listener) {
        ConnectivityReceiver.connectivityReceiverListener = listener;
    }

    public void setMessageReceiverListener(RealTimeReceiver.MessageReceiverListener listener) {
        RealTimeReceiver.messageReceiverListene = listener;
    }

    public void setMessageUberReceiverListener(RealTimeReceiver.MessageUberReceiverListener listener) {
        RealTimeReceiver.messageUberReceiverListener = listener;
    }

    public void setMessageAdminReceiverListener(RealTimeReceiver.MessageAdminReceiverListener listener) {
        RealTimeReceiver.messageAdminReceiverListener = listener;
    }

    public void setOfferReceiverListener(RealTimeReceiver.OfferDetailsReceiverListener listener) {
        RealTimeReceiver.offerDetailsReceiverListener = listener;
    }

    public void setTripReceiverListener(RealTimeReceiver.NewTripReceiverListener listener) {
        RealTimeReceiver.tripReceiverListener = listener;
    }

    public void setNotificationsCount(RealTimeReceiver.NotificationCounterListener counterListener) {
        RealTimeReceiver.notificationCounterListener = counterListener;
    }

    public IApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }

    public static synchronized MyApplication getInstance() {
        return mInstance;
    }

}
